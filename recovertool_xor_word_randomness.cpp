/*
 *
 * This file is part of recovertool -- A toolbox for recovering files
 *  from damaged file systems
 *
 * recovertool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3.0 of the
 * License, or (at your option) any later version.
 *
 * recovertool is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with recovertool.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2023 Isidor Zeuner <devel@quidecco.de>
 *
 */

#include "recovertool_throw_exception.h"
#include "recovertool_malloc_c_allocator.h"
#include <bitset>
#include "recovertool_component_error.h"
#include <cstdlib>
#include "recovertool_resizable_buffer.h"
#include <cstdio>

int xor_word_randomness(
    int argc,
    char* argv[
    ]
) {
    size_t index = 0;
    int const full_record = atoi(
        argv[
            ++index
        ]
    );
    int const min_bits = atoi(
        argv[
            ++index
        ]
    );
    int const max_bits = atoi(
        argv[
            ++index
        ]
    );
    recovertool::resizable_buffer<
        recovertool::malloc_c_allocator,
        std::uint16_t
    > retrieved(
        full_record
    );
    while (
        true
    ) {
        size_t read_records = fread(
            retrieved.get_data(
            ),
            full_record * sizeof(
                std::uint16_t
            ),
            1,
            stdin
        );
        if (
            0 == read_records
        ) {
            if (
                feof(
                    stdin
                )
            ) {
                break;
            }
            recovertool::throw_exception(
                recovertool::component_error(
                    "xor_word_randomness"
                )
            );
        }
        std::uint16_t aggregated = 0;
        for (
            std::uint16_t const* aggregating = retrieved.get_data(
            );
            retrieved.get_data(
            ) + retrieved.get_size(
            ) > aggregating;
            aggregating++
        ) {
            aggregated ^= *aggregating;
        }
        int const bits = std::bitset<
            16
        >(
            aggregated
        ).count(
        );
        if (
            min_bits > bits
        ) {
            continue;
        }
        if (
            max_bits < bits
        ) {
            continue;
        }
        if (
            1 != fwrite(
                retrieved.get_data(
                ),
                full_record * sizeof(
                    std::uint16_t
                ),
                1,
                stdout
            )
        ) {
            recovertool::throw_exception(
                recovertool::component_error(
                    "xor_word_randomness"
                )
            );
        }
    };
    return 0;
}
