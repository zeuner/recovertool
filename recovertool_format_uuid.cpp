/*
 *
 * This file is part of recovertool -- A toolbox for recovering files
 *  from damaged file systems
 *
 * recovertool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3.0 of the
 * License, or (at your option) any later version.
 *
 * recovertool is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with recovertool.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2023 Isidor Zeuner <devel@quidecco.de>
 *
 */


#include "recovertool_format_uuid.h"
#include "recovertool_hex_string.h"
namespace recovertool {
    
    std::string format_uuid(
        std::string binary
    ) {
        std::string hex = hex_string(
            binary
        );
        std::string::size_type const delimiter0 = hex.size(
        ) * 2 / 8;
        std::string::size_type const delimiter1 = hex.size(
        ) * 3 / 8;
        std::string::size_type const delimiter2 = hex.size(
        ) * 4 / 8;
        std::string::size_type const delimiter3 = hex.size(
        ) * 5 / 8;
        return "\"" + std::string(
            hex.begin(
            ),
            hex.begin(
            ) + delimiter0
        ) + "-" + std::string(
            hex.begin(
            ) + delimiter0,
            hex.begin(
            ) + delimiter1
        ) + "-" + std::string(
            hex.begin(
            ) + delimiter1,
            hex.begin(
            ) + delimiter2
        ) + "-" + std::string(
            hex.begin(
            ) + delimiter2,
            hex.begin(
            ) + delimiter3
        ) + "-" + std::string(
            hex.begin(
            ) + delimiter3,
            hex.end(
            )
        ) + "\"";
    }
}
