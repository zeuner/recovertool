/*
 *
 * This file is part of recovertool -- A toolbox for recovering files
 *  from damaged file systems
 *
 * recovertool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3.0 of the
 * License, or (at your option) any later version.
 *
 * recovertool is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with recovertool.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2023 Isidor Zeuner <devel@quidecco.de>
 *
 */

#include "recovertool_throw_exception.h"
#include "recovertool_c_buffer.h"
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <string>
#include "recovertool_component_error.h"
#include <cstdlib>
#include <cstdio>
#include "recovertool_posix_file.h"
#include <sys/stat.h>

int get_surrounding(
    int argc,
    char* argv[
    ]
) {
    size_t index = 0;
    std::string const input_filename = argv[
        ++index
    ];
    int const offset_start = atoi(
        argv[
            ++index
        ]
    );
    int const offset_end = atoi(
        argv[
            ++index
        ]
    );
    recovertool::posix_file input_file(
        input_filename,
        O_RDONLY
    );
    size_t const chunk_size = offset_end - offset_start;
    recovertool::c_buffer retrieved(
        chunk_size
    );
    while (
        true
    ) {
        unsigned long offset;
        ssize_t read_bytes = read(
            0,
            &offset,
            sizeof(
                offset
            )
        );
        if (
            0 > read_bytes
        ) {
            recovertool::throw_exception(
                recovertool::component_error(
                    "get_surrounding"
                )
            );
        }
        if (
            0 == read_bytes
        ) {
            break;
        }
        if (
            sizeof(
                offset
            ) > read_bytes
        ) {
            recovertool::throw_exception(
                recovertool::component_error(
                    "get_surrounding"
                )
            );
        }
        off_t const required_start = offset + offset_start;
        off_t const achieved_start = lseek(
            input_file.get_data(
            ),
            required_start,
            SEEK_SET
        );
        if (
            achieved_start != required_start
        ) {
            recovertool::throw_exception(
                recovertool::component_error(
                    "get_surrounding"
                )
            );
        }
        read_bytes = read(
            input_file.get_data(
            ),
            retrieved.get_data(
            ),
            chunk_size
        );
        if (
            chunk_size != read_bytes
        ) {
            recovertool::throw_exception(
                recovertool::component_error(
                    "get_surrounding"
                )
            );
        }
        if (
            1 != fwrite(
                retrieved.get_data(
                ),
                chunk_size,
                1,
                stdout
            )
        ) {
            recovertool::throw_exception(
                recovertool::component_error(
                    "get_surrounding"
                )
            );
        }
    };
    return 0;
}
