%option noyywrap
	/*
	 *
	 * This file is part of recovertool -- A toolbox for recovering files
	 *  from damaged file systems
	 *
	 * recovertool is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published
	 * by the Free Software Foundation, either version 3.0 of the
	 * License, or (at your option) any later version.
	 *
	 * recovertool is distributed in the hope that it will be useful, but
	 * WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with recovertool.  If not, see
	 * <http://www.gnu.org/licenses/>.
	 *
	 * Copyright 2023 Isidor Zeuner <devel@quidecco.de>
	 *
	 */

	#include <cstdio>
	long num_chars = 0;
%%
PK	{
	    fwrite(
	        &num_chars,
	        sizeof(
	            num_chars
	        ),
	        1,
	        stdout
	    );
	    num_chars += 2;
	}
\n	++num_chars;
.	++num_chars;
%%
int main(
) {
    yylex(
    );
    return 0;
}
