/*
 *
 * This file is part of recovertool -- A toolbox for recovering files
 *  from damaged file systems
 *
 * recovertool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3.0 of the
 * License, or (at your option) any later version.
 *
 * recovertool is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with recovertool.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2023 Isidor Zeuner <devel@quidecco.de>
 *
 */

#include "recovertool_component_error.h"
#include "recovertool_repetition_scanner.h"
#include "recovertool_throw_exception.h"
#include <cstdlib>
#include "recovertool_c_buffer.h"
#include <iostream>
#include <unistd.h>

int scan_repeated(
    int argc,
    char* argv[
    ]
) {
    size_t index = 0;
    size_t const read_buffer_size = atoi(
        argv[
            ++index
        ]
    );
    size_t const scan_buffer_size = atoi(
        argv[
            ++index
        ]
    );
    size_t const pattern_size = atoi(
        argv[
            ++index
        ]
    );
    size_t const pattern_distance = atoi(
        argv[
            ++index
        ]
    );
    recovertool::c_buffer retrieved(
        read_buffer_size
    );
    recovertool::repetition_scanner scanning(
        scan_buffer_size,
        pattern_size,
        pattern_distance
    );
    while (
        true
    ) {
        ssize_t read_bytes = read(
            0,
	    retrieved.get_data(
            ),
            retrieved.get_size(
            )
        );
        if (
            0 > read_bytes
        ) {
            recovertool::throw_exception(
                recovertool::component_error(
                    "scan_repeated"
                )
            );
        }
        if (
            0 == read_bytes
        ) {
            break;
        }
        char const* from = retrieved.get_data(
        );
        char const* const to = from + read_bytes;
        do {
            std::string pattern;
            bool const found = scanning.scan_data(
                from,
                to,
                pattern
            );
            if (
                found
            ) {
                std::cout << pattern;
            }
        } while (
            to > from
        );
    }
    return 0;
}
