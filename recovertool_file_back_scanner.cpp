/*
 *
 * This file is part of recovertool -- A toolbox for recovering files
 *  from damaged file systems
 *
 * recovertool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3.0 of the
 * License, or (at your option) any later version.
 *
 * recovertool is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with recovertool.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2023 Isidor Zeuner <devel@quidecco.de>
 *
 */


#include "recovertool_file_back_scanner.h"
#include <unistd.h>
#include <sys/types.h>
#include "recovertool_component_error.h"
#include <algorithm>
#include "recovertool_throw_exception.h"
namespace recovertool {
    
    file_back_scanner::file_back_scanner(
        posix_file& input,
        off_t start
    ) :
    input(
        input
    ),
    position(
        start
    ),
    buffer_end(
        start
    ) {
        off_t const retrievable = std::min(
            static_cast<
                off_t
            >(
                start
            ),
            static_cast<
                off_t
            >(
                buffer_size
            )
        );
        data.resize(
            retrievable
        );
        off_t const required_start = start - retrievable;
        off_t const achieved_start = lseek(
            input.get_data(
            ),
            required_start,
            SEEK_SET
        );
        if (
            achieved_start != required_start
        ) {
            throw_exception(
                component_error(
                    "file_back_scanner"
                )
            );
        }
        buffer_start = achieved_start;
        ssize_t const read_bytes = read(
            input.get_data(
            ),
            data.get_data(
            ),
            retrievable
        );
        if (
            retrievable != read_bytes
        ) {
            throw_exception(
                component_error(
                    "file_back_scanner"
                )
            );
        }
    }
    
    bool file_back_scanner::get_char(
        char& retrieved
    ) {
        if (
            0 == position
        ) {
            return false;
        }
        position--;
        if (
            buffer_start <= position
        ) {
            retrieved = data.get_data(
            )[
                position - buffer_start
            ];
            return true;
        }
        off_t const retrievable = std::min(
            static_cast<
                off_t
            >(
                buffer_start
            ),
            static_cast<
                off_t
            >(
                data.get_size(
                )
            )
        );
        buffer_end = buffer_start;
        buffer_start -= retrievable;
        off_t const achieved_start = lseek(
            input.get_data(
            ),
            buffer_start,
            SEEK_SET
        );
        if (
            achieved_start != buffer_start
        ) {
            throw_exception(
                component_error(
                    "file_back_scanner"
                )
            );
        }
        ssize_t const read_bytes = read(
            input.get_data(
            ),
            data.get_data(
            ),
            retrievable
        );
        if (
            retrievable != read_bytes
        ) {
            throw_exception(
                component_error(
                    "file_back_scanner"
                )
            );
        }
        retrieved = data.get_data(
        )[
            position - buffer_start
        ];
        return true;
    }
}
