/*
 *
 * This file is part of recovertool -- A toolbox for recovering files
 *  from damaged file systems
 *
 * recovertool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3.0 of the
 * License, or (at your option) any later version.
 *
 * recovertool is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with recovertool.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2023 Isidor Zeuner <devel@quidecco.de>
 *
 */

#include <fcntl.h>
#include "recovertool_component_error.h"
#include <string>
#include <sys/types.h>
#include <unistd.h>
#include "recovertool_serialized.h"
#include "recovertool_c_buffer.h"
#include "recovertool_posix_file.h"
#include "recovertool_throw_exception.h"
#include <sys/stat.h>

int get_length_string(
    int argc,
    char* argv[
    ]
) {
    size_t index = 0;
    std::string const input_filename = argv[
        ++index
    ];
    recovertool::posix_file input_file(
        input_filename,
        O_RDONLY
    );
    recovertool::c_buffer string_data;
    while (
        true
    ) {
        unsigned long offset;
        ssize_t read_bytes = read(
            0,
            &offset,
            sizeof(
                offset
            )
        );
        if (
            0 > read_bytes
        ) {
            recovertool::throw_exception(
                recovertool::component_error(
                    "get_length_string"
                )
            );
        }
        if (
            0 == read_bytes
        ) {
            break;
        }
        if (
            sizeof(
                offset
            ) > read_bytes
        ) {
            recovertool::throw_exception(
                recovertool::component_error(
                    "get_length_string"
                )
            );
        }
        off_t const required_start = offset;
        off_t const achieved_start = lseek(
            input_file.get_data(
            ),
            required_start,
            SEEK_SET
        );
        if (
            achieved_start != required_start
        ) {
            recovertool::throw_exception(
                recovertool::component_error(
                    "get_length_string"
                )
            );
        }
        unsigned char reading_byte;
        if (
            1 != read(
                input_file.get_data(
                ),
                &reading_byte,
                1
            )
        ) {
            recovertool::throw_exception(
                recovertool::component_error(
                    "get_length_string"
                )
            );
        }
        size_t const lower = reading_byte;
        if (
            1 != read(
                input_file.get_data(
                ),
                &reading_byte,
                1
            )
        ) {
            recovertool::throw_exception(
                recovertool::component_error(
                    "get_length_string"
                )
            );
        }
        size_t upper = reading_byte;
        upper <<= 8;
        size_t const string_length = upper + lower;
        if (
            string_data.get_size(
            ) < string_length
        ) {
            string_data.resize(
                string_length
            );
        }
        if (
            string_length != read(
                input_file.get_data(
                ),
                string_data.get_data(
                ),
                string_length
            )
        ) {
            recovertool::throw_exception(
                recovertool::component_error(
                    "get_length_string"
                )
            );
        }
        std::string const retrieved(
            string_data.get_data(
            ),
            string_length
        );
        std::cout << recovertool::serialized(
            retrieved
        );
    };
    return 0;
}
