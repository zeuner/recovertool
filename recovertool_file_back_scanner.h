/*
 *
 * This file is part of recovertool -- A toolbox for recovering files
 *  from damaged file systems
 *
 * recovertool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3.0 of the
 * License, or (at your option) any later version.
 *
 * recovertool is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with recovertool.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2023 Isidor Zeuner <devel@quidecco.de>
 *
 */

#ifndef FILE_BACK_SCANNER_INCLUDED
#define FILE_BACK_SCANNER_INCLUDED

#include "recovertool_c_buffer.h"
#include "recovertool_posix_file.h"
namespace recovertool {
    
    class file_back_scanner {
    public:
        file_back_scanner(
            posix_file& input,
            off_t start
        );
        bool get_char(
            char& retrieved
        );
    private:
        posix_file& input;
        off_t position;
        off_t buffer_start;
        off_t buffer_end;
        c_buffer data;
        enum {
            buffer_size = 0x400,
        };
    };
}

#endif
