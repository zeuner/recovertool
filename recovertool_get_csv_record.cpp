/*
 *
 * This file is part of recovertool -- A toolbox for recovering files
 *  from damaged file systems
 *
 * recovertool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3.0 of the
 * License, or (at your option) any later version.
 *
 * recovertool is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with recovertool.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2023 Isidor Zeuner <devel@quidecco.de>
 *
 */

#include <vector>
#include <iostream>
#include "recovertool_throw_exception.h"
#include <cstdlib>
#include "recovertool_component_error.h"
#include "recovertool_c_buffer.h"
#include "recovertool_format_uuid.h"
#include <string>
#include <map>
#include <functional>
#include <utility>
#include "recovertool_format_uint.h"
#include <cstdio>

int get_csv_record(
    int argc,
    char* argv[
    ]
) {
    size_t index = 0;
    std::string delimiter;
    std::vector<
        std::pair<
            std::string::size_type,
            std::function<
                std::string(
                    std::string
                )
            >
        >
    > column_format;
    std::string::size_type full_record = 0;
    std::map<
        std::string,
        std::function<
            std::string(
                std::string
            )
        >
    > formatters;
    formatters[
        "uuid"
    ] = &recovertool::format_uuid;
    formatters[
        "uint"
    ] = &recovertool::format_uint;
    do {
        index++;
        if (
            argc <= index
        ) {
            break;
        }
        std::cout << delimiter << argv[
            index
        ];
        delimiter = ",";
        std::string::size_type const column_size = atoi(
            argv[
                ++index
            ]
        );
        std::string const data_type = argv[
            ++index
        ];
        auto const formatter = formatters.at(
            data_type
        );
        column_format.push_back(
            std::make_pair(
                column_size,
                formatter
            )
        );
        full_record += column_size;
    } while (
        true
    );
    std::cout << "\n";
    recovertool::c_buffer retrieved(
        full_record
    );
    while (
        true
    ) {
        size_t const read_records = fread(
            retrieved.get_data(
            ),
            full_record,
            1,
            stdin
        );
        if (
            0 == read_records
        ) {
            if (
                feof(
                    stdin
                )
            ) {
                break;
            }
            recovertool::throw_exception(
                recovertool::component_error(
                    "get_csv_record"
                )
            );
        }
        char const* column_start = retrieved.get_data(
        );
        delimiter = "";
        for (
            auto writing = column_format.begin(
            );
            column_format.end(
            ) != writing;
            writing++
        ) {
            std::string const raw(
                column_start,
                writing->first
            );
            std::cout << delimiter << writing->second(
                raw
            );
            delimiter = ",";
            column_start += writing->first;
        }
        std::cout << "\n";
    };
    return 0;
}
