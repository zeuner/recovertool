/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef ICMP_ROUTE_TRACE_INCLUDED
#define ICMP_ROUTE_TRACE_INCLUDED

#include <boost/function.hpp>
#include <string>
#include <boost/shared_ptr.hpp>
namespace @qlib@ {
    
    
    class icmp_route_trace {
        typedef boost::function<
            void(
                boost::system::error_code const& error,
                boost::asio::ip::address const& remote_address
            )
        > handler_function;
    public:
        static inline boost::arg<
            2
        > remote_address_argument(
        );
        icmp_route_trace(
            boost::asio::io_service& communicating,
            boost::asio::ip::address const& destination,
            boost::posix_time::time_duration const& timeout = boost::posix_time::seconds(
                2
            )
        );
        void async_trace(
            int ttl,
            handler_function const& response_handler
        );
    protected:
        void handle_send(
            boost::shared_ptr<
                boost::shared_ptr<
                    handler_function
                >
            > const& response_handler,
            boost::system::error_code const& error
        );
        void handle_timeout(
            boost::shared_ptr<
                boost::shared_ptr<
                    handler_function 
                > 
            > const& response_handler,
            boost::system::error_code const& error
        );
        void handle_receive(
            boost::shared_ptr<
                boost::shared_ptr<
                    handler_function 
                > 
            > const& response_handler,
            boost::system::error_code const& error,
            size_t bytes
        );
    private:
        boost::asio::ip::icmp::socket tracing;
        boost::asio::ip::icmp::endpoint destination;
        boost::asio::deadline_timer expiring;
        std::string payload;
        std::string sending;
        enum {
            receiving_size = 0x400
        };
        char receiving[
            receiving_size
        ];
        uint16_t identifier;
        uint16_t sequence_number;
        boost::posix_time::time_duration timeout;
    };
    
    boost::arg<
        2
    > icmp_route_trace::remote_address_argument(
    ) {
        return boost::arg<
            2
        >(
        );
    }
}

#endif
