/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_file_contents.h"
#include <sys/stat.h>
#include <stdexcept>
#include "@qlib@_throw_exception.h"
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
namespace @qlib@ {
    
    #ifndef O_BINARY
    #define O_BINARY 0
    #endif
    
    std::string file_contents(
        std::string const& filename,
        std::string::size_type offset
    ) {
        int opened = open(
            filename.c_str(
            ),
            O_RDONLY | O_BINARY
        );
        if (
            -1 == opened
        ) {
            throw_exception(
                std::runtime_error(
                    "Could not open " + filename + " for reading."
                )
            );
        }
        if (
            0 != offset
        ) {
            if (
                static_cast<
                    off_t
                >(
                    offset
                ) != lseek(
                    opened,
                    offset,
                    SEEK_SET
                )
            ) {
                if (
                    -1 == close(
                        opened
                    )
                ) {
                    throw_exception(
                        std::runtime_error(
                            "Could not close " + filename + "."
                        )
                    );
                }
                throw_exception(
                    std::runtime_error(
                        "Could not seek."
                    )
                );
            }
        }
        std::string result = "";
        while (
            true
        ) {
            int count;
            char data[
                0x400
            ];
            count = read(
                opened,
                data,
                sizeof(
                    data
                )
            );
            if (
                0 > count
            ) {
                if (
                    -1 == close(
                        opened
                    )
                ) {
                    throw_exception(
                        std::runtime_error(
                            "Could not close " + filename + "."
                        )
                    );
                }
                throw_exception(
                    std::runtime_error(
                        "Could read from " + filename + "."
                    )
                );
            }
            if (
                0 == count
            ) {
                break;
            }
            result += std::string(
                data,
                count
            );
        }
        if (
            -1 == close(
                opened
            )
        ) {
            throw_exception(
                std::runtime_error(
                    "Could not close " + filename + " after reading."
                )
            );
        }
        return result;
    }
}
