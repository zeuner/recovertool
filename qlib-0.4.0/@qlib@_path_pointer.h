/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef PATH_POINTER_INCLUDED
#define PATH_POINTER_INCLUDED

#include "@qlib@_deserialize.h"
#include <boost/shared_ptr.hpp>
#include <string>
#include "@qlib@_serialized.h"
namespace @qlib@ {
    
    
    typedef boost::shared_ptr<
        boost::filesystem::path
    > path_pointer;
        
    template<
    >
    class serialized_implementation<
        path_pointer
    > {
    public:
        static std::string apply(
            path_pointer const& raw
        );
    };
        
    template<
    >
    class deserialize_implementation<
        path_pointer
    > {
    public:
        static void apply(
            std::string::const_iterator& from,
            std::string::const_iterator const& to,
            path_pointer& raw
        );
    };
}

#endif
