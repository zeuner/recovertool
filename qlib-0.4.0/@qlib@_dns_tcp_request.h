/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef DNS_TCP_REQUEST_INCLUDED
#define DNS_TCP_REQUEST_INCLUDED

#include <string>
#include <boost/asio/ip/tcp.hpp>
#include <boost/function.hpp>
namespace @qlib@ {
    
    
    class dns_tcp_request :
    public boost::asio::ip::tcp::socket {
    public:
        dns_tcp_request(
            boost::asio::io_service& io_service
        );
        dns_tcp_request(
            boost::asio::io_service& io_service,
            boost::asio::ip::tcp::endpoint const& local_endpoint
        );
        void async_request(
            boost::asio::ip::address const& destination,
            std::string const& request,
            boost::function<
                void(
                    boost::system::error_code const& error,
                    std::string const& response
                )
            > const& result_handler
        );
    protected:
        void handle_connect(
            boost::system::error_code const& error
        );
        void handle_write(
            boost::system::error_code const& error
        );
        void handle_size_read(
            boost::system::error_code const& error,
            size_t transferred
        );
        void handle_data_read(
            boost::system::error_code const& error,
            size_t transferred
        );
    private:
        enum {
            buffer_size = 0x400
        };
        char reading_data[
            buffer_size
        ];
        std::string::size_type input_size;
        std::string input;
        std::string writing_data;
        boost::function<
            void(
                boost::system::error_code const& error,
                std::string const& response
            )
        > result_handler;
    };
}

#endif
