/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_filter_sum.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    std::list<
        amplitude_value 
    > filter_sum(
        std::list<
            amplitude_value
        > const& one,
        std::list<
            amplitude_value
        > const& other
    ) {
        if (
            one.size(
            ) != other.size(
            )
        ) {
            throw component_error(
                "filter_sum"
            );
        }
        std::list<
            amplitude_value
        > result;
        for (
            std::list<
                amplitude_value
            >::const_iterator one_adding = one.begin(
            ),
            other_adding = other.begin(
            );
            one.end(
            ) != one_adding;
            one_adding++,
            other_adding++
        ) {
            result.push_back(
                (
                    *one_adding
                ) + (
                    *other_adding
                )
            );
        }
        return result;
    }
}
