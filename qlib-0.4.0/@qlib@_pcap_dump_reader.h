/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef PCAP_DUMP_READER_INCLUDED
#define PCAP_DUMP_READER_INCLUDED

#include <string>
namespace @qlib@ {
    
    
    class pcap_dump_reader {
    public:
        pcap_dump_reader(
            std::string const& filename
        );
        ~pcap_dump_reader(
        );
        bool get_packet(
            u_char const*& data,
            bpf_u_int32& size
        );
    private:
        char error_buffer[
            PCAP_ERRBUF_SIZE
        ];
        pcap_t* handle;
    };
}

#endif
