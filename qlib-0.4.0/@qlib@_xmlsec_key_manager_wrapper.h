/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef XMLSEC_KEY_MANAGER_WRAPPER_INCLUDED
#define XMLSEC_KEY_MANAGER_WRAPPER_INCLUDED

namespace @qlib@ {
    
    
    template<
        typename storing
    >
    class xmlsec_key_manager_wrapper :
    public xmlsec_key_manager_base {
    public:
        xmlsec_key_manager_wrapper(
        );
        storing* get_store(
        );
        template<
            typename name_string,
            typename key_string
        >
        void add_key(
            name_string const& name,
            key_string const& key
        );
    private:
        typedef xmlsec_key_store_wrapper<
            storing
        > store_type;
    };
    
    template<
        typename storing
    >
    template<
        typename name_string,
        typename key_string
    >
    void xmlsec_key_manager_wrapper<
        storing
    >::add_key(
        name_string const& name,
        key_string const& key
    ) {
        this->get_store(
        )->add_key(
            xmlsec_memory_imported_key(
                name,
                key
            )
        );
    }
    
    template<
        typename storing
    >
    xmlsec_key_manager_wrapper<
        storing
    >::xmlsec_key_manager_wrapper(
    ) :
    xmlsec_key_manager_base(
        xmlSecKeyStoreCreate(
            store_type::get_store_id(
            )
        )
    ) {
    }
    
    template<
        typename storing
    >
    storing* xmlsec_key_manager_wrapper<
        storing
    >::get_store(
    ) {
        return store_type::get_object(
            this->get_native_store(
            )
        );
    }
}

#endif
