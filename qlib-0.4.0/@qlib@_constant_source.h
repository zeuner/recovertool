/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef CONSTANT_SOURCE_INCLUDED
#define CONSTANT_SOURCE_INCLUDED

namespace @qlib@ {
    
    
    template<
        typename value_type
    >
    class constant_source {
    public:
        constant_source(
            value_type value
        );
        bool get_sample(
            value_type& retrieved
        );
        size_t get_samples(
            value_type* first,
            value_type* last
        );
        bool is_master(
        ) const;
    private:
        value_type const value;
    };
    
    template<
        typename value_type
    >
    class bulk_audio_retrieval<
        constant_source<
            value_type
        >,
        value_type
    > {
    public:
        static size_t get_samples(
            constant_source<
                value_type
            >& source,
            value_type* first,
            value_type* last
        );
    };
    
    template<
        typename value_type
    >
    constant_source<
        value_type
    >::constant_source(
        value_type value
    ) :
    value(
        value
    ) {
    }
    
    template<
        typename value_type
    >
    bool constant_source<
        value_type
    >::get_sample(
        value_type& retrieved
    ) {
        retrieved = value;
        return true;
    }
    
    template<
        typename value_type
    >
    size_t constant_source<
        value_type
    >::get_samples(
        value_type* first,
        value_type* last
    ) {
        std::fill(
            first,
            last,
            value
        );
        return last - first;
    }
    
    template<
        typename value_type
    >
    bool constant_source<
        value_type
    >::is_master(
    ) const {
        return true;
    }
    
    template<
        typename value_type
    >
    size_t bulk_audio_retrieval<
        constant_source<
            value_type
        >,
        value_type
    >::get_samples(
        constant_source<
            value_type
        >& source,
        value_type* first,
        value_type* last
    ) {
        return source.get_samples(
            first,
            last
        );
    }
}

#endif
