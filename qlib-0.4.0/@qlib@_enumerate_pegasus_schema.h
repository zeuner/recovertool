/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef ENUMERATE_PEGASUS_SCHEMA_INCLUDED
#define ENUMERATE_PEGASUS_SCHEMA_INCLUDED

#include <set>
#include <list>
#include <boost/function.hpp>
namespace @qlib@ {
    
    
    void enumerate_pegasus_schema(
        PEGASUS_NAMESPACE(
            CIMClient
        )& cim_client,
        PEGASUS_NAMESPACE(
            CIMNamespaceName
        ) const& cim_namespace,
        std::list<
            PEGASUS_NAMESPACE(
                CIMObjectPath
            )
        > const& names,
        boost::function<
            void(
                PEGASUS_NAMESPACE(
                    CIMObjectPath
                ) const&
            )
        > const& object_processing,
        boost::function<
            void(
                PEGASUS_NAMESPACE(
                    CIMObjectPath
                ) const&
            )
        > const& association_processing,
        std::set<
            PEGASUS_NAMESPACE(
                String
            )
        > const& blacklist = std::set<
            PEGASUS_NAMESPACE(
                String
            )
        >(
        )
    );
}

#endif
