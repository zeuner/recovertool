/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef DISSECT_MULTIPART_INCLUDED
#define DISSECT_MULTIPART_INCLUDED

#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    template<
        typename string,
        typename iterator
    >
    bool dissect_multipart(
        string const& delimiter,
        bool& start,
        bool& next,
        bool& done,
        iterator const& from,
        iterator const& to,
        iterator& this_to,
        iterator& next_from
    );
    
    template<
        typename string,
        typename iterator
    >
    bool dissect_multipart(
        string const& delimiter,
        bool& start,
        bool& next,
        bool& done,
        iterator const& from,
        iterator const& to,
        iterator& this_to,
        iterator& next_from
    ) {
        if (
            start
        ) {
            string const start_marker = "--" + delimiter + "\r\n";
            if (
                (
                    from + start_marker.size(
                    )
                ) > to
            ) {
                return false;
            }
            if (
                string(
                    from,
                    from + start_marker.size(
                    )
                ) != start_marker
            ) {
                throw component_error(
                    "dissect_multipart"
                );
            }
            this_to = from;
            next_from = from + start_marker.size(
            );
            start = false;
            next = true;
            return true;
        }
        string const materialized(
            from,
            to
        );
        string const next_marker = "\r\n"
        "--" + delimiter + "\r\n";
        typename string::size_type const next_marker_found = materialized.find(
            next_marker
        );
        if (
            string::npos != next_marker_found
        ) {
            this_to = from + next_marker_found;
            next_from = from + next_marker_found + next_marker.size(
            );
            next = true;
            return true;
        }
        string const end_marker = "\r\n"
        "--" + delimiter + "--\r\n";
        typename string::size_type const end_marker_found = materialized.find(
            end_marker
        );
        if (
            string::npos != end_marker_found
        ) {
            this_to = from + end_marker_found;
            next_from = from + end_marker_found + end_marker.size(
            );
            done = true;
            next = true;
            return true;
        }
        if (
            (
                from + end_marker.size(
                )
            ) >= to
        ) {
            return false;
        }
        this_to = to - end_marker.size(
        );
        return true;
    }
}

#endif
