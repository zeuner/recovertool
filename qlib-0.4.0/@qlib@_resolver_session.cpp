/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_resolver_session.h"
#include <stdexcept>
#include "@qlib@_throw_exception.h"
#include "@qlib@_system_log.h"
#include <boost/bind.hpp>
#include <boost/asio/ip/tcp.hpp>
namespace @qlib@ {
    
    
    using namespace boost;
    
    resolver_session::resolver_session(
        asio::io_service& communicating,
        std::list<
            asio::ip::address
        > const& dns_servers,
        boost::asio::ip::udp::endpoint const& local_endpoint_v4,
        boost::asio::ip::udp::endpoint const& local_endpoint_v6
    ) :
    communicating(
        communicating
    ),
    local_endpoint_v4(
        local_endpoint_v4
    ),
    local_endpoint_v6(
        local_endpoint_v6
    ),
    id_generating(
        1 << 16
    ),
    dns_servers(
        dns_servers
    ),
    current_dns_server(
        this->dns_servers.begin(
        )
    ),
    no_ttl(
        false
    ),
    waiting_v4(
        false
    ),
    waiting_v6(
        false
    ),
    resending_v4(
        false
    ),
    checked_v4_request(
        result_handlers_v4.begin(
        )
    ),
    checked_v6_request(
        result_handlers_v6.begin(
        )
    ),
    resending(
        false
    ),
    closed(
        false
    ) {
        if (
            sizeof(
                int
            ) <= 2
        ) {
            throw_exception(
                std::runtime_error(
                    "resolver_session: incompatible platform"
                )
            );
        }
    }
    
    void resolver_session::async_resolve(
        resolver_session::record_type requested,
        char const* domain_name,
        resolver_session::handling_result const& result_handler,
        resolver_session::reply_section requested_section
    ) {
        if (
            closed
        ) {
            communicating.post(
                boost::bind(
                    result_handler,
                    boost::system::error_code(
                        dns_error_codes::resolver_shutdown,
                        dns_category(
                        )
                    ),
                    std::string(
                    ),
                    std::list<
                        resource_record
                    >(
                    )
                )
            );
            return;
        }
        do {
            std::map<
                cache_selector,
                cached_record
            >::iterator cached = cached_results.find(
                cache_selector(
                    domain_name,
                    requested,
                    requested_section
                )
            );
            if (
                cached_results.end(
                ) == cached
            ) {
                break;
            }
            time_t now = time(
                NULL
            );
            if (
                (
                    !no_ttl
                ) && (
                    now >= fusion::at_c<
                        expiry
                    >(
                        cached->second
                    )
                )
            ) {
                cached_results.erase(
                    cached
                );
                break;
            }
            std::list<
                resource_record
            > shifted = fusion::at_c<
                result_records
            >(
                cached->second
            );
            int displacement = now - fusion::at_c<
                fetch_time
            >(
                cached->second
            );
            for (
                std::list<
                    resource_record
                >::iterator shifting = shifted.begin(
                );
                shifted.end(
                ) != shifting;
                shifting++
            ) {
                fusion::at_c<
                    resource_record_ttl
                >(
                    *shifting
                ) -= displacement;
            }
            communicating.post(
                result_binder(
                    result_handler,
                    fusion::at_c<
                        raw_result_string
                    >(
                        cached->second
                    ),
                    shifted
                )
            );
            return;
        } while (
            false
        );
        std::string string_id;
        int attempts = 0;
        asio::ip::address const& used = get_dns_server(
        );
        if (
            used.is_v6(
            )
        ) {
            do {
                uint16_t const numeric_id = id_generating(
                );
                string_id = std::string(
                    reinterpret_cast<
                        char const*
                    >(
                        &numeric_id
                    ),
                    2
                );
                attempts++;
                if (
                    0x100 < attempts
                ) {
                    throw_exception(
                        std::runtime_error(
                            "resolver_session: could not generate request id"
                        )
                    );
                }
            } while (
                result_handlers_v6.end(
                ) != result_handlers_v6.find(
                    string_id
                )
            );
            char record_type_character = requested;
            result_handlers_v6[
                string_id
            ] = processing_record(
                string_id + std::string(
                    "\x01\x00"
                    "\x00\x01"
                    "\x00\x00"
                    "\x00\x00"
                    "\x00\x00",
                    10
                ) + name_labels(
                    domain_name
                ) + '\x00' + record_type_character + std::string(
                    "\x00\x01",
                    2
                ),
                domain_name,
                requested,
                result_handler,
                false,
                requested_section
            );
            std::string const& sent_data = fusion::at_c<
                query_string
            >(
                result_handlers_v6[
                    string_id
                ]
            );
            if (
                NULL == socket_v6.get(
                )
            ) {
                socket_v6.reset(
                    new asio::ip::udp::socket(
                        communicating,
                        local_endpoint_v6
                    )
                );
            }
            socket_v6->async_send_to(
                asio::buffer(
                    sent_data.data(
                    ),
                    sent_data.size(
                    )
                ),
                asio::ip::udp::endpoint(
                    used,
                    53
                ),
                boost::bind(
                    &resolver_session::handle_send_v6,
                    this,
                    string_id,
                    asio::placeholders::error
                )
            );
            if (
                !waiting_v6
            ) {
                waiting_v6 = true;
                socket_v6->async_receive_from(
                    asio::buffer(
                        buffer_data_v6,
                        buffer_size
                    ),
                    sender_endpoint_v6,
                    boost::bind(
                        &resolver_session::handle_receive_v6,
                        this,
                        asio::placeholders::bytes_transferred,
                        asio::placeholders::error
                    )
                );
            }
        } else {
            do {
                uint16_t const numeric_id = id_generating(
                );
                string_id = std::string(
                    reinterpret_cast<
                        char const*
                    >(
                        &numeric_id
                    ),
                    2
                );
                attempts++;
                if (
                    0x100 < attempts
                ) {
                    throw_exception(
                        std::runtime_error(
                            "resolver_session: could not generate request id"
                        )
                    );
                }
            } while (
                result_handlers_v4.end(
                ) != result_handlers_v4.find(
                    string_id
                )
            );
            char record_type_character = requested;
            result_handlers_v4[
                string_id
            ] = processing_record(
                string_id + std::string(
                    "\x01\x00"
                    "\x00\x01"
                    "\x00\x00"
                    "\x00\x00"
                    "\x00\x00",
                    10
                ) + name_labels(
                    domain_name
                ) + '\x00' + record_type_character + std::string(
                    "\x00\x01",
                    2
                ),
                domain_name,
                requested,
                result_handler,
                false,
                requested_section
            );
            std::string const& sent_data = fusion::at_c<
                query_string
            >(
                result_handlers_v4[
                    string_id
                ]
            );
            if (
                NULL == socket_v4.get(
                )
            ) {
                socket_v4.reset(
                    new asio::ip::udp::socket(
                        communicating,
                        local_endpoint_v4
                    )
                );
            }
            socket_v4->async_send_to(
                asio::buffer(
                    sent_data.data(
                    ),
                    sent_data.size(
                    )
                ),
                asio::ip::udp::endpoint(
                    used,
                    53
                ),
                boost::bind(
                    &resolver_session::handle_send_v4,
                    this,
                    string_id,
                    asio::placeholders::error
                )
            );
            if (
                !waiting_v4
            ) {
                waiting_v4 = true;
                socket_v4->async_receive_from(
                    asio::buffer(
                        buffer_data_v4,
                        buffer_size
                    ),
                    sender_endpoint_v4,
                    boost::bind(
                        &resolver_session::handle_receive_v4,
                        this,
                        asio::placeholders::bytes_transferred,
                        asio::placeholders::error
                    )
                );
            }
        }
        start_resending(
        );
    }
    
    void resolver_session::async_resolve(
        resolver_session::record_type requested,
        std::string const& domain_name,
        resolver_session::handling_result const& result_handler,
        resolver_session::reply_section requested_section
    ) {
        async_resolve(
            requested,
            interned_string(
                domain_name
            ),
            result_handler,
            requested_section
        );
    }
    
    void resolver_session::close(
    ) {
        closed = true;
        socket_v4.reset(
        );
        socket_v6.reset(
        );
        resend_pulse.reset(
        );
    }
    
    bool resolver_session::get_closed(
    ) const {
        return closed;
    }
    
    void resolver_session::set_no_ttl(
    ) {
        no_ttl = true;
    }
    
    std::string resolver_session::name_labels(
        std::string const& name
    ) {
        std::string processed = name;
        if (
            processed.empty(
            )
        ) {
            throw_exception(
                std::runtime_error(
                    "resolver_session: request too short"
                )
            );
        }
        if (
            '.' != processed[
                processed.size(
                ) - 1
            ]
        ) {
            processed += '.';
        }
        std::string::size_type found;
        std::string::size_type last = 0;
        std::string result;
        while (
            std::string::npos != (
                found = processed.find(
                    '.',
                    last
                )
            )
        ) {
            std::string const label(
                processed.begin(
                ) + last,
                processed.begin(
                ) + found
            );
            if (
                64 <= label.size(
                )
            ) {
                throw_exception(
                    std::runtime_error(
                        "resolver_session: request too long"
                    )
                );
            }
            char transmitted = label.size(
            );
            result += transmitted;
            result += label;
            last = found + 1;
        }
        return result + '\x00';
    }
    
    std::string resolver_session::decode_labels(
        std::string::const_iterator& label_start,
        std::string::const_iterator const& container_end,
        std::string::const_iterator const& message_start,
        std::string::const_iterator const& message_end
    ) {
        if (
            container_end == label_start
        ) {
            throw_exception(
                std::runtime_error(
                    "resolver_session: unexpected end in request"
                )
            );
        }
        unsigned char length = static_cast<
            unsigned char
        >(
            *label_start++
        );
        if (
            0xc0 == (
                0xc0 & length
            )
        ) {
            int offset = 0x3f & length;
            offset <<= 8;
            if (
                container_end == label_start
        ) {
                throw_exception(
                    std::runtime_error(
                        "resolver_session: missing data"
                    )
                );
            }
            length = static_cast<
                unsigned char
            >(
                *label_start++
            );
            offset += length;
            std::string::const_iterator seeked = message_start + offset;
            return decode_labels(
                seeked,
                message_end,
                message_start,
                message_end
            );
        } else {
            std::string::const_iterator begin = label_start;
            label_start += length;
            if (
                container_end < label_start
            ) {
                throw_exception(
                    std::runtime_error(
                        "resolver_session: missing data"
                    )
                );
            }
            std::string::const_iterator end = label_start;
            std::string tail;
            if (
                0 != length
            ) {
                tail = "." + decode_labels(
                    label_start,
                    container_end,
                    message_start,
                    message_end
                );
            }
            return std::string(
                begin,
                end
            ) + tail;
        }
    }
    
    void resolver_session::handle_receive_v4(
        size_t received,
        boost::system::error_code const& error
    ) {
        if (
            error
        ) {
            return;
        }
        std::string const message(
            buffer_data_v4,
            received
        );
        if (
            3 > message.size(
            )
        ) {
            throw_exception(
                std::runtime_error(
                    "resolver_session: message too short"
                )
            );
        }
        std::string string_id(
            message.begin(
            ),
            message.begin(
            ) + 2
        );
        result_handler_map::iterator handling = result_handlers_v4.find(
            string_id
        );
        if (
            result_handlers_v4.end(
            ) == handling
        ) {
            throw_exception(
                std::runtime_error(
                    "resolver_session: unexpected header"
                )
            );
        }
        bool message_truncated = false;
        handle_message_v4(
            handling,
            message,
            &message_truncated
        );
        if (
            message_truncated
        ) {
            dns_tcp_request* resolving_by_tcp = new dns_tcp_request(
                communicating,
                boost::asio::ip::tcp::endpoint(
                    local_endpoint_v4.address(
                    ),
                    local_endpoint_v4.port(
                    )
                )
            );
            resolving_by_tcp->async_request(
                sender_endpoint_v4.address(
                ),
                fusion::at_c<
                    query_string
                >(
                    handling->second
                ),
                boost::bind(
                    &resolver_session::handle_message_v4,
                    this,
                    handling,
                    raw_result_argument,
                    static_cast<
                        bool*
                    >(
                        NULL
                    ),
                    asio::placeholders::error
                )
            );
        }
    }
    
    void resolver_session::handle_receive_v6(
        size_t received,
        boost::system::error_code const& error
    ) {
        if (
            error
        ) {
            return;
        }
        std::string const message(
            buffer_data_v6,
            received
        );
        if (
            3 > message.size(
            )
        ) {
            throw_exception(
                std::runtime_error(
                    "resolver_session: message too short"
                )
            );
        }
        std::string string_id(
            message.begin(
            ),
            message.begin(
            ) + 2
        );
        result_handler_map::iterator handling = result_handlers_v6.find(
            string_id
        );
        if (
            result_handlers_v6.end(
            ) == handling
        ) {
            throw_exception(
                std::runtime_error(
                    "resolver_session: unexpected header"
                )
            );
        }
        bool message_truncated = false;
        handle_message_v6(
            handling,
            message,
            &message_truncated
        );
        if (
            message_truncated
        ) {
            dns_tcp_request* resolving_by_tcp = new dns_tcp_request(
                communicating,
                boost::asio::ip::tcp::endpoint(
                    local_endpoint_v6.address(
                    ),
                    local_endpoint_v6.port(
                    )
                )
            );
            resolving_by_tcp->async_request(
                sender_endpoint_v6.address(
                ),
                fusion::at_c<
                    query_string
                >(
                    handling->second
                ),
                boost::bind(
                    &resolver_session::handle_message_v6,
                    this,
                    handling,
                    raw_result_argument,
                    static_cast<
                        bool*
                    >(
                        NULL
                    ),
                    asio::placeholders::error
                )
            );
        }
    }
    
    boost::asio::io_service& resolver_session::get_io_service(
    ) {
        return communicating;
    }
    
    void resolver_session::skip_question_record(
        std::string::const_iterator& decoding,
        std::string::const_iterator begin,
        std::string::const_iterator end
    ) {
        std::string const domain = decode_labels(
            decoding,
            end,
            begin,
            end
        );
        if (
            end == decoding
        ) {
            throw_exception(
                std::runtime_error(
                    "resolver_session: decode buffer exceeded"
                )
            );
        }
        decoding += 4;
        if (
            end <= decoding
        ) {
            throw_exception(
                std::runtime_error(
                    "resolver_session: decode buffer exceeded"
                )
            );
        }
    }
    
    void resolver_session::decode_record(
        resource_record& decoded,
        std::string::const_iterator& decoding,
        std::string::const_iterator begin,
        std::string::const_iterator end
    ) {
        std::string const domain = decode_labels(
            decoding,
            end,
            begin,
            end
        );
        if (
            end == decoding
        ) {
            throw_exception(
                std::runtime_error(
                    "resolver_session: decode buffer exceeded"
                )
            );
        }
        unsigned fetched = *decoding++;
        int record_type = fetched;
        record_type <<= 8;
        if (
            end == decoding
        ) {
            throw_exception(
                std::runtime_error(
                    "resolver_session: decode buffer exceeded"
                )
            );
        }
        fetched = *decoding++;
        record_type += fetched;
        if (
            end == decoding
        ) {
            throw_exception(
                std::runtime_error(
                    "resolver_session: decode buffer exceeded"
                )
            );
        }
        fetched = *decoding++;
        int record_class = fetched;
        record_class <<= 8;
        if (
            end == decoding
        ) {
            throw_exception(
                std::runtime_error(
                    "resolver_session: decode buffer exceeded"
                )
            );
        }
        fetched = *decoding++;
        record_class += fetched;
        if (
            end == decoding
        ) {
            throw_exception(
                std::runtime_error(
                    "resolver_session: decode buffer exceeded"
                )
            );
        }
        fetched = *decoding++;
        unsigned record_ttl = fetched;
        record_ttl <<= 8;
        if (
            end == decoding
        ) {
            throw_exception(
                std::runtime_error(
                    "resolver_session: decode buffer exceeded"
                )
            );
        }
        fetched = *decoding++;
        record_ttl += fetched;
        record_ttl <<= 8;
        if (
            end == decoding
        ) {
            throw_exception(
                std::runtime_error(
                    "resolver_session: decode buffer exceeded"
                )
            );
        }
        fetched = *decoding++;
        record_ttl += fetched;
        record_ttl <<= 8;
        if (
            end == decoding
        ) {
            throw_exception(
                std::runtime_error(
                    "resolver_session: decode buffer exceeded"
                )
            );
        }
        fetched = *decoding++;
        record_ttl += fetched;
        if (
            end == decoding
        ) {
            throw_exception(
                std::runtime_error(
                    "resolver_session: decode buffer exceeded"
                )
            );
        }
        fetched = *decoding++;
        int record_length = fetched;
        record_length <<= 8;
        if (
            end == decoding
        ) {
            throw_exception(
                std::runtime_error(
                    "resolver_session: decode buffer exceeded"
                )
            );
        }
        fetched = *decoding++;
        record_length += fetched;
        std::string::const_iterator record_begin = decoding;
        decoding += record_length;
        if (
            end < decoding
        ) {
            throw_exception(
                std::runtime_error(
                    "resolver_session: decode buffer exceeded"
                )
            );
        }
        std::string record_data(
            record_begin,
            decoding
        );
        decoded = resource_record(
            domain,
            record_type,
            record_class,
            record_ttl,
            record_data
        );
    }
    
    void resolver_session::handle_message_v4(
        result_handler_map::iterator handling,
        std::string const& message,
        bool* message_truncated,
        boost::system::error_code const& error
    ) {
        std::list<
            resource_record
        > records;
        if (
            !error
        ) {
            std::string::const_iterator decoding = message.begin(
            ) + 12;
            if (
                decoding > message.end(
                )
            ) {
                throw_exception(
                    std::runtime_error(
                        "resolver_session: decode buffer exceeded"
                    )
                );
            }
            if (
                0 != (
                    2 & message[
                        2
                    ]
                )
            ) {
                if (
                    NULL == message_truncated
                ) { 
                    throw_exception(
                        std::runtime_error(
                            "resolver_session: message truncated"
                        )
                    );
                }
                *message_truncated = true;
                return;
            }
            int current_section = reply_sections_begin;
            int skipped_records = 0;
            int requested_records;
            std::string::const_iterator parsing = message.begin(
            ) + 4;
            do {
                unsigned char const high = static_cast<
                    unsigned char
                >(
                    *parsing
                );
                parsing++;
                unsigned char const low = static_cast<
                    unsigned char
                >(
                    *parsing
                );
                parsing++;
                int current_records = high;
                current_records <<= 8;
                current_records += low;
                if (
                    fusion::at_c<
                        queried_reply_section
                    >(
                        handling->second
                    ) == current_section
                ) {
                    requested_records = current_records;
                    break;
                }
                skipped_records += current_records;
                current_section++;
            } while (
                true
            );
            for (
                int skipped = 0;
                skipped_records > skipped;
                skipped++
            ) {
                skip_question_record(
                    decoding,
                    message.begin(
                    ),
                    message.end(
                    )
                );
            }
            unsigned lowest_ttl = 0xffffffff;
            for (
                int stored = 0;
                requested_records > stored;
                stored++
            ) {
                resource_record decoded;
                try {
                    decode_record(
                        decoded,
                        decoding,
                        message.begin(
                        ),
                        message.end(
                        )
                    );
                } catch (
                    std::exception& caught
                ) {
                    system_log.log_error(
                        "resolver_session: could not parse DNS record: " + std::string(
                            caught.what(
                            )
                        )
                    );
                    break;
                }
                if (
                    fusion::at_c<
                        resource_record_ttl
                    >(
                        decoded
                    ) < lowest_ttl
                ) {
                    lowest_ttl = fusion::at_c<
                        resource_record_ttl
                    >(
                        decoded
                    );
                }
                records.push_back(
                    decoded
                );
            }
            if (
                !records.empty(
                )
            ) {
                time_t now = time(
                    NULL
                );
                cached_results[
                    cache_selector(
                        fusion::at_c<
                            queried_domain
                        >(
                            handling->second
                        ),
                        fusion::at_c<
                            queried_record_type
                        >(
                            handling->second
                        ),
                        fusion::at_c<
                            queried_reply_section
                        >(
                            handling->second
                        )
                    )
                ] = cached_record(
                    now,
                    now + lowest_ttl,
                    message,
                    records
                );
            }
        }
        if (
            !fusion::at_c<
                resent
            >(
                handling->second
            )
        ) {
            communicating.post(
                boost::bind(
                    fusion::at_c<
                        result_handler_function
                    >(
                        handling->second
                    ),
                    error,
                    message,
                    records
                )
            );
        }
        if (
            handling == checked_v4_request
        ) {
            checked_v4_request++;
        }
        result_handlers_v4.erase(
            handling
        );
        if (
            result_handlers_v4.empty(
            )
        ) {
            waiting_v4 = false;
        } else {
            socket_v4->async_receive_from(
                asio::buffer(
                    buffer_data_v4,
                    buffer_size
                ),
                sender_endpoint_v4,
                boost::bind(
                    &resolver_session::handle_receive_v4,
                    this,
                    asio::placeholders::bytes_transferred,
                    asio::placeholders::error
                )
            );
        }
    }
    
    void resolver_session::handle_message_v6(
        result_handler_map::iterator handling,
        std::string const& message,
        bool* message_truncated,
        boost::system::error_code const& error
    ) {
        std::list<
            resource_record
        > records;
        if (
            !error
        ) {
            std::string::const_iterator decoding = message.begin(
            ) + 12;
            if (
                message.end(
                ) == decoding
            ) {
                throw_exception(
                    std::runtime_error(
                        "resolver_session: decode buffer exceeded"
                    )
                );
            }
            if (
                0 != (
                    2 & message[
                        2
                    ]
                )
            ) {
                if (
                    NULL == message_truncated
                ) { 
                    throw_exception(
                        std::runtime_error(
                            "resolver_session: message truncated"
                        )
                    );
                }
                *message_truncated = true;
                return;
            }
            int current_section = reply_sections_begin;
            int skipped_records = 0;
            int requested_records;
            std::string::const_iterator parsing = message.begin(
            ) + 4;
            do {
                unsigned char const high = static_cast<
                    unsigned char
                >(
                    *parsing
                );
                parsing++;
                unsigned char const low = static_cast<
                    unsigned char
                >(
                    *parsing
                );
                parsing++;
                int current_records = high;
                current_records <<= 8;
                current_records += low;
                if (
                    fusion::at_c<
                        queried_reply_section
                    >(
                        handling->second
                    ) == current_section
                ) {
                    requested_records = current_records;
                    break;
                }
                skipped_records += current_records;
                current_section++;
            } while (
                true
            );
            for (
                int skipped = 0;
                skipped_records > skipped;
                skipped++
            ) {
                skip_question_record(
                    decoding,
                    message.begin(
                    ),
                    message.end(
                    )
                );
            }
            unsigned lowest_ttl = 0xffffffff;
            for (
                int stored = 0;
                requested_records > stored;
                stored++
            ) {
                resource_record decoded;
                decode_record(
                    decoded,
                    decoding,
                    message.begin(
                    ),
                    message.end(
                    )
                );
                if (
                    fusion::at_c<
                        resource_record_ttl
                    >(
                        decoded
                    ) < lowest_ttl
                ) {
                    lowest_ttl = fusion::at_c<
                        resource_record_ttl
                    >(
                        decoded
                    );
                }
                records.push_back(
                    decoded
                );
            }
            if (
                !records.empty(
                )
            ) {
                time_t now = time(
                    NULL
                );
                cached_results[
                    cache_selector(
                        fusion::at_c<
                            queried_domain
                        >(
                            handling->second
                        ),
                        fusion::at_c<
                            queried_record_type
                        >(
                            handling->second
                        ),
                        fusion::at_c<
                            queried_reply_section
                        >(
                            handling->second
                        )
                    )
                ] = cached_record(
                    now,
                    now + lowest_ttl,
                    message,
                    records
                );
            }
        }
        if (
            !fusion::at_c<
                resent
            >(
                handling->second
            )
        ) {
            communicating.post(
                boost::bind(
                    fusion::at_c<
                        result_handler_function
                    >(
                        handling->second
                    ),
                    error,
                    message,
                    records
                )
            );
        }
        if (
            handling == checked_v6_request
        ) {
            checked_v6_request++;
        }
        result_handlers_v6.erase(
            handling
        );
        if (
            result_handlers_v6.empty(
            )
        ) {
            waiting_v6 = false;
        } else {
            socket_v6->async_receive_from(
                asio::buffer(
                    buffer_data_v6,
                    buffer_size
                ),
                sender_endpoint_v6,
                boost::bind(
                    &resolver_session::handle_receive_v6,
                    this,
                    asio::placeholders::bytes_transferred,
                    asio::placeholders::error
                )
            );
        }
    }
    
    void resolver_session::handle_send_v4(
        std::string const& string_id,
        boost::system::error_code const& error
    ) {
        if (
            asio::error::operation_aborted == error
        ) {
            return;
        }
        if (
            error
        ) {
            result_handler_map::iterator handling = result_handlers_v4.find(
                string_id
            );
            if (
                result_handlers_v4.end(
                ) == handling
            ) {
                throw_exception(
                    std::runtime_error(
                        "resolver_session: unexpected header"
                    )
                );
            }
            if (
                !fusion::at_c<
                    resent
                >(
                    handling->second
                )
            ) {
                communicating.post(
                    boost::bind(
                        fusion::at_c<
                            result_handler_function
                        >(
                            handling->second
                        ),
                        error,
                        std::string(
                        ),
                        std::list<
                            resource_record
                        >(
                        )
                    )
                );
            }
            if (
                handling == checked_v4_request
            ) {
                checked_v4_request++;
            }
            result_handlers_v4.erase(
                handling
            );
        }
    }
    
    void resolver_session::handle_send_v6(
        std::string const& string_id,
        boost::system::error_code const& error
    ) {
        if (
            asio::error::operation_aborted == error
        ) {
            return;
        }
        if (
            error
        ) {
            result_handler_map::iterator handling = result_handlers_v6.find(
                string_id
            );
            if (
                result_handlers_v6.end(
                ) == handling
            ) {
                throw_exception(
                    std::runtime_error(
                        "resolver_session: unexpected header"
                    )
                );
            }
            if (
                !fusion::at_c<
                    resent
                >(
                    handling->second
                )
            ) {
                communicating.post(
                    boost::bind(
                        fusion::at_c<
                            result_handler_function
                        >(
                            handling->second
                        ),
                        error,
                        std::string(
                        ),
                        std::list<
                            resource_record
                        >(
                        )
                    )
                );
            }
            if (
                handling == checked_v6_request
            ) {
                checked_v6_request++;
            }
            result_handlers_v6.erase(
                handling
            );
        }
    }
    
    asio::ip::address const& resolver_session::get_dns_server(
    ) {
        if (
            dns_servers.end(
            ) == current_dns_server
        ) {
            throw_exception(
                std::runtime_error(
                    "resolver_session: no dns servers found"
                )
            );
        }
        current_dns_server++;
        if (
            dns_servers.end(
            ) == current_dns_server
        ) {
            current_dns_server = dns_servers.begin(
            );
        }
        return *current_dns_server;
    }
    
    void resolver_session::start_resending(
    ) {
        if (
            resending
        ) {
            return;
        }
        resending = true;
        if (
            NULL == resend_pulse.get(
            )
        ) {
            resend_pulse.reset(
                new boost::asio::deadline_timer(
                    communicating
                )
            );
        }
        resend_pulse->expires_from_now(
            boost::posix_time::seconds(
                1
            )
        );
        resend_pulse->async_wait(
            boost::bind(
                &resolver_session::check_resend,
                this,
                asio::placeholders::error
            )
        );
    }
    
    void resolver_session::check_resend(
        boost::system::error_code const& error
    ) {
        if (
            error
        ) {
            return;
        }
        resending_v4 = !resending_v4;
        do {
            if (
                resending_v4
            ) {
                if (
                    result_handlers_v4.end(
                    ) == checked_v4_request
                ) {
                    checked_v4_request = result_handlers_v4.begin(
                    );
                    break;
                }
                if (
                    !fusion::at_c<
                        resent
                    >(
                        checked_v4_request->second
                    )
                ) {
                    async_resolve(
                        fusion::at_c<
                            queried_record_type
                        >(
                            checked_v4_request->second
                        ),
                        fusion::at_c<
                            queried_domain
                        >(
                            checked_v4_request->second
                        ),
                        fusion::at_c<
                            result_handler_function
                        >(
                            checked_v4_request->second
                        )
                    );
                    fusion::at_c<
                        resent
                    >(
                        checked_v4_request->second
                    ) = true;
                }
                checked_v4_request++;
            } else {
                if (
                    result_handlers_v6.end(
                    ) == checked_v6_request
                ) {
                    checked_v6_request = result_handlers_v6.begin(
                    );
                    break;
                }
                if (
                    !fusion::at_c<
                        resent
                    >(
                        checked_v6_request->second
                    )
                ) {
                    async_resolve(
                        fusion::at_c<
                            queried_record_type
                        >(
                            checked_v6_request->second
                        ),
                        fusion::at_c<
                            queried_domain
                        >(
                            checked_v6_request->second
                        ),
                        fusion::at_c<
                            result_handler_function
                        >(
                            checked_v6_request->second
                        )
                    );
                    fusion::at_c<
                        resent
                    >(
                        checked_v6_request->second
                    ) = true;
                }
                checked_v6_request++;
            }
        } while (
            false
        );
        if (
            result_handlers_v4.empty(
            ) && result_handlers_v6.empty(
            )
        ) {
            resending = false;
            return;
        }
        resend_pulse->expires_from_now(
            boost::posix_time::seconds(
                1
            )
        );
        resend_pulse->async_wait(
            boost::bind(
                &resolver_session::check_resend,
                this,
                asio::placeholders::error
            )
        );
    }
    
    resolver_session::result_binder::result_binder(
        resolver_session::handling_result const& result_handler,
        std::string const& raw_result,
        std::list<
            resource_record
        > const& records
    ) :
    result_handler(
        result_handler
    ),
    raw_result(
        raw_result
    ),
    records(
        records
    ) {
    }
    
    void resolver_session::result_binder::operator(
    )(
    ) {
        result_handler(
            boost::system::error_code(
            ),
            raw_result,
            records
        );
    }
}
