/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_tcp_header_checksum.h"
#include <string>
#include <cstring>
namespace @qlib@ {
    
    
    unsigned short tcp_header_checksum(
        uint32_t source,
        uint32_t destination,
        unsigned short const* data,
        size_t size
    ) {
        struct {
            struct in_addr source_address;
            struct in_addr destination_address;
            unsigned char padding;
            unsigned char protocol;
            unsigned short tcp_length;
            struct tcphdr tcp_header;
        } arranged;
        memset(
            &arranged,
            0,
            sizeof(
                arranged
            )
        );
        arranged.source_address.s_addr = source;
        arranged.destination_address.s_addr = destination;
        arranged.padding = 0;
        arranged.protocol = IPPROTO_TCP;
        arranged.tcp_length = htons(
            size
        );
        arranged.tcp_header = *reinterpret_cast<
            struct tcphdr const*
        >(
            data
        );
        std::string const complete = std::string(
            reinterpret_cast<
                char const*
            >(
                &arranged
            ),
            sizeof(
                arranged
            )
        ) + std::string(
            reinterpret_cast<
                char const*
            >(
                data
            ) + sizeof(
                arranged.tcp_header
            ),
            size - sizeof(
                arranged.tcp_header
            )
        );
        return ip_header_checksum(
            reinterpret_cast<
                unsigned short const*
            >(
                complete.data(
                )
            ),
            complete.size(
            )
        );
    }
}
