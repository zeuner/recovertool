/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_extracted_netlink_attributes.h"
#include "@qlib@_component_error.h"
#include "@qlib@_throw_exception.h"
namespace @qlib@ {
    
    
    std::map<
        int,
        std::string
    > extracted_netlink_attributes(
        char const* attribute,
        char const* data,
        size_t payload_length
    ) {
        std::map<
            int,
            std::string
        > attributes;
        do {
            if (
                (
                    data + payload_length
                ) <= attribute
            ) {
                break;
            }
            uint16_t const length = *reinterpret_cast<
                uint16_t const*
            >(
                attribute
            );
            uint16_t const type = *reinterpret_cast<
                uint16_t const*
            >(
                attribute + sizeof(
                    length
                )
            );
            static size_t const header_length = sizeof(
                length
            ) + sizeof(
                type
            );
            attributes[
                type
            ] = std::string(
                attribute + header_length,
                length - header_length
            );
            if (
                0 == length
            ) {
                throw_exception(
                    component_error(
                        "extracted_netlink_attributes"
                    )
                );
            }
            attribute += NLA_ALIGN(
                length
            );
        } while (
            true
        );
        return attributes;
    }
}
