/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef RANDOM_ITERATOR_INCLUDED
#define RANDOM_ITERATOR_INCLUDED

#include "@qlib@_component_error.h"
#include <map>
#include <boost/shared_ptr.hpp>
#include <vector>
namespace @qlib@ {
    
    
    template<
        typename containing
    >
    typename containing::iterator random_iterator(
        containing& container
    );
    
    template<
        typename containing
    >
    typename containing::iterator random_iterator(
        containing& container
    ) {
        std::vector<
            typename containing::iterator
        > choices;
        for (
            typename containing::iterator populated = container.begin(
            );
            container.end(
            ) != populated;
            populated++
        ) {
            choices.push_back(
                populated
            );
        }
        if (
            choices.empty(
            )
        ) {
            throw component_error(
                "random_iterator"
            );
        }
        static std::map<
            int,
            boost::shared_ptr<
                integer_random_source
            >
        > choosers;
        boost::shared_ptr<
            integer_random_source
        >& chooser = choosers[
            choices.size(
            )
        ];
        if (
            !chooser
        ) {
            chooser.reset(
                new integer_random_source(
                    choices.size(
                    )
                )
            );
        }
        return choices[
            (
                *chooser
            )(
            )
        ];
    }
}

#endif
