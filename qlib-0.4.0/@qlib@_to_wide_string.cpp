/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_to_wide_string.h"
#include "@qlib@_c_string.h"
#include "@qlib@_component_error.h"
#include <iconv.h>
#include "@qlib@_charset_converter.h"
namespace @qlib@ {
    
    
    std::wstring to_wide_string(
        std::string const& narrow,
        std::string const& charset
    ) {
        charset_converter converting(
            charset,
            "WCHAR_T"
        );
        size_t const writing_total = narrow.size(
        ) * sizeof(
            std::wstring::value_type
        ) / sizeof(
            std::string::value_type
        );
        size_t writing_size = writing_total;
        char writing[
            writing_size
        ];
        char* writing_position = writing;
        c_string reading(
            narrow
        );
        char* reading_position = reading.get_data(
        );
        size_t reading_size = narrow.size(
        );
        if (
            static_cast<
                size_t
            >(
                -1
            ) == iconv(
                converting.get_data(
                ),
                &reading_position,
                &reading_size,
                &writing_position,
                &writing_size
            )
        ) {
            throw component_error(
                "to_wide_string"
            );
        }
        return std::wstring(
            reinterpret_cast<
                std::wstring::value_type*
            >(
                writing
            ),
            (
                writing_total - writing_size
            ) / sizeof(
                std::wstring::value_type
            )
        );
    }
}
