/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef TCP_SOCKET_INCLUDED
#define TCP_SOCKET_INCLUDED

#include <boost/asio/ip/tcp.hpp>
#include <string>
#include <boost/function.hpp>
#include <unistd.h>
#include <list>
namespace @qlib@ {
    
    
    class tcp_socket :
    public generic_socket {
    public:
        tcp_socket(
            boost::asio::io_service& communicating,
            resolver_decoding_session* resolving
        );
        tcp_socket(
            std::pair<
                boost::asio::io_service*,
                resolver_decoding_session*
            > const& initializer
        );
        tcp_socket(
            boost::asio::io_service& communicating,
            resolver_decoding_session* resolving,
            boost::asio::ip::tcp::endpoint const& local
        );
        void async_connect(
            boost::asio::ip::tcp::endpoint const& endpoint,
            boost::function<
                void(
                    boost::system::error_code const& error
                )
            > connect_handler
        );
        void async_connect(
            std::string const& hostname,
            unsigned short port,
            boost::function<
                void(
                    boost::system::error_code const& error
                )
            > const& connect_handler
        );
        void close(
        );
        void cancel(
        );
        boost::asio::io_service& get_io_service(
        );
        void async_read_some(
            boost::asio::mutable_buffer const& destination,
            boost::function<
                void(
                    boost::system::error_code const& error,
                    size_t received
                )
            > const& read_handler
        );
        void async_write_some(
            boost::asio::mutable_buffer const& source,
            boost::function<
                void(
                    boost::system::error_code const& error,
                    size_t sent
                )
            > const& write_handler
        );
        void async_write_some(
            boost::asio::const_buffer const& source,
            boost::function<
                void(
                    boost::system::error_code const& error,
                    size_t sent
                )
            > const& write_handler
        );
        template<
            typename buffer,
            typename buffers
        >
        void async_write_some(
            boost::asio::detail::consuming_buffers<
                buffer,
                buffers
            > const& source,
            boost::function<
                void(
                    boost::system::error_code const& error,
                    size_t sent
                )
            > const& write_handler
        );
        boost::asio::ip::tcp::endpoint local_endpoint(
        ) const;
    protected:
        void handle_resolve(
            boost::function<
                void(
                    boost::system::error_code const& error
                )
            > connect_handler,
            boost::system::error_code const& error,
            std::list<
                resolver_decoding_session::a_resource_record
            > const& records,
            std::string hostname,
            unsigned short port
        );
    private:
        boost::asio::ip::tcp::socket implementation;
        resolver_decoding_session* resolving;
    };
    
    template<
        typename buffer,
        typename buffers
    >
    void tcp_socket::async_write_some(
        boost::asio::detail::consuming_buffers<
            buffer,
            buffers
        > const& source,
        boost::function<
            void(
                boost::system::error_code const& error,
                size_t sent
            )
        > const& write_handler
    ) {
        typename boost::asio::detail::consuming_buffers<
            buffer,
            buffers
        >::const_iterator current = source.begin(
        );
        typename boost::asio::detail::consuming_buffers<
            buffer,
            buffers
        >::const_iterator const end = source.end(
        );
        do {
            if (
                end == current
            ) {
                write_handler(
                    boost::system::error_code(
                    ),
                    0
                );
                return;
            }
            if (
                0 == boost::asio::buffer_size(
                    *current
                )
            ) {
                current++;
                continue;
            }
            async_write_some(
                *current,
                write_handler
            );
            return;
        } while (
            true
        );
    }
}

#endif
