/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef WORKER_POOL_INCLUDED
#define WORKER_POOL_INCLUDED

#include <boost/fusion/sequence/intrinsic/at_c.hpp>
#include <boost/fusion/include/vector_fwd.hpp>
#include <boost/fusion/container/vector.hpp>
#include <boost/bind.hpp>
#include <boost/fusion/container/vector/vector_fwd.hpp>
#include <list>
#include <boost/noncopyable.hpp>
#include <boost/fusion/include/at_c.hpp>
#include <map>
#include <vector>
#include <boost/function.hpp>
#include <boost/fusion/include/vector.hpp>
#include <string>
#include <boost/shared_ptr.hpp>
namespace @qlib@ {
    
    
    template<
        typename working
    >
    class worker_pool :
    private boost::noncopyable {
    public:
        worker_pool(
            int count
        );
        worker_pool(
            boost::function<
                working*(
                )
            > const& factory,
            int count
        );
        ~worker_pool(
        );
        void initialize(
        );
        void shutdown(
        );
        void post_work(
            boost::function<
                void(
                    working& processor
                )
            > const& processed,
            boost::function<
                void(
                    std::string error
                )
            > const& error_reporting
        );
    protected:
        static working* default_construct(
        );
        enum {
            executing_thread,
            processing_instance,
            scheduling,
            keeping
        };
        typedef boost::fusion::vector<
            boost::shared_ptr<
                boost::thread
            >,
            boost::shared_ptr<
                working
            >,
            boost::shared_ptr<
                boost::asio::io_service
            >,
            boost::shared_ptr<
                boost::asio::io_service::work
            >
        > worker_thread;
        void process_job(
            worker_thread& processing,
            boost::function<
                void(
                    working& processor
                )
            > processed,
            boost::function<
                void(
                    std::string error
                )
            > error_reporting
        );
    private:
        boost::shared_ptr<
            bool
        > stopping;
        std::vector<
            worker_thread
        > threads;
        std::map<
            boost::thread::id,
            worker_thread*
        > available;
        boost::function<
            working*(
            )
        > factory;
        typedef std::list<
            std::pair<
                boost::function<
                    void(
                        working& processor
                    )
                >,
                boost::function<
                    void(
                        std::string error
                    )
                >
            >
        > job_queue;
        job_queue pending_work;
        boost::mutex manipulating;
    };
    
    template<
        typename working
    >
    worker_pool<
        working
    >::worker_pool(
        int count
    ) :
    threads(
        count
    ),
    factory(
        &worker_pool::default_construct
    ) {
        initialize(
        );
    }
    
    template<
        typename working
    >
    worker_pool<
        working
    >::worker_pool(
        boost::function<
            working*(
            )
        > const& factory,
        int count
    ) :
    threads(
        count
    ),
    factory(
        factory
    ) {
        initialize(
        );
    }
    
    template<
        typename working
    >
    void worker_pool<
        working
    >::initialize(
    ) {
        stopping.reset(
            new bool(
                false
            )
        );
        boost::unique_lock<
            boost::mutex
        > lock(
            manipulating
        );
        for (
            typename std::vector<
                worker_thread
            >::iterator initializing = threads.begin(
            );
            threads.end(
            ) != initializing;
            initializing++
        ) {
            boost::fusion::at_c<
                scheduling
            >(
                *initializing
            ).reset(
                new boost::asio::io_service
            );
            boost::fusion::at_c<
                keeping
            >(
                *initializing
            ).reset(
                new boost::asio::io_service::work(
                    *boost::fusion::at_c<
                        scheduling
                    >(
                        *initializing
                    )
                )
            );
            size_t (
                boost::asio::io_service::*running
            )(
            ) = &boost::asio::io_service::run;
            boost::fusion::at_c<
                executing_thread
            >(
                *initializing
            ).reset(
                new boost::thread(
                    running,
                    boost::ref(
                        *boost::fusion::at_c<
                            scheduling
                        >(
                            *initializing
                        )
                    )
                )
            );
            available[
                boost::fusion::at_c<
                    executing_thread
                >(
                    *initializing
                )->get_id(
                )
            ] = &(
                *initializing
            );
        }
    }
    
    template<
        typename working
    >
    worker_pool<
        working
    >::~worker_pool(
    ) {
        this->shutdown(
        );
        for (
            typename std::vector<
                worker_thread
            >::iterator finalizing = threads.begin(
            );
            threads.end(
            ) != finalizing;
            finalizing++
        ) {
            boost::fusion::at_c<
                executing_thread
            >(
                *finalizing
            )->join(
            );
        }
    }
    
    template<
        typename working
    >
    void worker_pool<
        working
    >::shutdown(
    ) {
        stopping.reset(
            new bool(
                true
            )
        );
        for (
            typename std::vector<
                worker_thread
            >::iterator finalizing = threads.begin(
            );
            threads.end(
            ) != finalizing;
            finalizing++
        ) {
            void (
                boost::shared_ptr<
                    working
                >::*resetting
            )(
            ) = &boost::shared_ptr<
                working
            >::reset;
            boost::fusion::at_c<
                scheduling
            >(
                *finalizing
            )->post(
                boost::bind(
                    resetting,
                    boost::ref(
                        boost::fusion::at_c<
                            processing_instance
                        >(
                            *finalizing
                        )
                    )
                )
            );
            boost::fusion::at_c<
                keeping
            >(
                *finalizing
            ).reset(
            );
        }
    }
    
    template<
        typename working
    >
    void worker_pool<
        working
    >::post_work(
        boost::function<
            void(
                working&
            )
        > const& processed,
        boost::function<
            void(
                std::string error
            )
        > const& error_reporting
    ) {
        if (
            *stopping
        ) {
            error_reporting(
                "shutting down"
            );
            return;
        }
        boost::unique_lock<
            boost::mutex
        > lock(
            manipulating
        );
        if (
            available.empty(
            )
        ) {
            pending_work.push_back(
                std::make_pair(
                    processed,
                    error_reporting
                )
            );
        } else {
            typename std::map<
                boost::thread::id,
                worker_thread*
            >::iterator assigned = available.begin(
            );
            worker_thread& processing = *assigned->second;
            available.erase(
                assigned
            );
            boost::fusion::at_c<
                scheduling
            >(
                processing
            )->post(
                boost::bind(
                    &worker_pool<
                        working
                    >::process_job,
                    this,
                    boost::ref(
                        processing
                    ),
                    processed,
                    error_reporting
                )
            );
        }
    }
    
    template<
        typename working
    >
    working* worker_pool<
        working
    >::default_construct(
    ) {
        return new working;
    }
    
    template<
        typename working
    >
    void worker_pool<
        working
    >::process_job(
        typename worker_pool<
            working
        >::worker_thread& processing,
        boost::function<
            void(
                working&
            )
        > processed,
        boost::function<
            void(
                std::string error
            )
        > error_reporting
    ) {
        try {
            if (
                !boost::fusion::at_c<
                    processing_instance
                >(
                    processing
                )
            ) {
                boost::fusion::at_c<
                    processing_instance
                >(
                    processing
                ).reset(
                    factory(
                    )
                );
            }
            processed(
                *boost::fusion::at_c<
                    processing_instance
                >(
                    processing
                )
            );
        } catch (
            std::exception& caught
        ) {
            boost::fusion::at_c<
                scheduling
            >(
                processing
            )->post(
                boost::bind(
                    error_reporting,
                    caught.what(
                    )
                )
            );
        }
        boost::unique_lock<
            boost::mutex
        > lock(
            manipulating
        );
        if (
            *stopping
        ) {
            while (
                !pending_work.empty(
                )
            ) {
                pending_work.front(
                ).second(
                    "shutting down"
                );
                pending_work.pop_front(
                );
            }
            return;
        }
        if (
            pending_work.empty(
            )
        ) {
            available[
                boost::this_thread::get_id(
                )
            ] = &processing;
        } else {
            std::pair<
                boost::function<
                    void(
                        working& processor
                    )
                >,
                boost::function<
                    void(
                        std::string error
                    )
                >
            > next = pending_work.front(
            );
            pending_work.pop_front(
            );
            boost::fusion::at_c<
                scheduling
            >(
                processing
            )->post(
                boost::bind(
                    &worker_pool<
                        working
                    >::process_job,
                    this,
                    boost::ref(
                        processing
                    ),
                    next.first,
                    next.second
                )
            );
        }
    }
}

#endif
