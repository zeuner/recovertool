/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef DISSECT_HEADER_ATTRIBUTE_INCLUDED
#define DISSECT_HEADER_ATTRIBUTE_INCLUDED

#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    template<
        typename iterator,
        typename string
    >
    void dissect_header_attribute(
        iterator const& attribute_start,
        iterator& attribute_end,
        iterator const& definition_end,
        bool& last,
        string& key,
        string& value
    );
    
    template<
        typename iterator,
        typename string
    >
    void dissect_header_attribute(
        iterator const& attribute_start,
        iterator& attribute_end,
        iterator const& definition_end,
        bool& last,
        string& key,
        string& value
    ) {
        iterator key_end = attribute_start;
        while (
            true
        ) {
            if (
                definition_end == key_end
            ) {
                throw component_error(
                    "dissect_header_attribute"
                );
            }
            if (
                '=' == *key_end
            ) {
                break;
            }
            key_end++;
        }
        key = string(
            attribute_start,
            key_end
        );
        iterator value_start = key_end + 1;
        switch (
            *value_start
        ) {
        case '"':
        case '\'':
            do {
                typename string::value_type quoting = *value_start;
                value_start++;
                iterator value_end = value_start;
                while (
                    true
                ) {
                    if (
                        definition_end == value_end
                    ) {
                        throw component_error(
                            "dissect_header_attribute"
                        );
                    }
                    if (
                        quoting == *value_end
                    ) {
                        break;
                    }
                    value_end++;
                }
                value = string(
                    value_start,
                    value_end
                );
                attribute_end = value_end + 1;
                if (
                    definition_end == attribute_end
                ) {
                    last = true;
                }
            } while (
                false
            );
            break;
        default:
            do {
                iterator value_end = value_start;
                while (
                    true
                ) {
                    if (
                        definition_end == value_end
                    ) {
                        last = true;
                        break;
                    }
                    if (
                        ';' == *value_end
                    ) {
                        break;
                    }
                    value_end++;
                }
                value = string(
                    value_start,
                    value_end
                );
                attribute_end = value_end;
            } while (
                false
            );
        }
    }
}

#endif
