/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_daemonized.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
namespace @qlib@ {
    
    
    int daemonized(
        pid_t* daemon_pid
    ) {
        if (
            fork(
            )
        ) {
            return 0;
        }
        for (
            int closed = 3;
            256 > closed;
            closed++
        ) {
            close(
                closed
            );
        }
        if (
            fork(
            )
        ) {
            return 0;
        }
        if (
            0 > setsid(
            )
        ) {
            return 0;
        }
        pid_t daemon = fork(
        );
        if (
            daemon
        ) {
            if (
                NULL != daemon_pid
            ) {
                *daemon_pid = daemon;
            }
            return 0;
        }
        int discarded = open(
            "/dev/null",
            O_RDONLY
        );
        dup2(
            discarded,
            0
        );
        close(
            discarded
        );
        discarded = open(
            "/dev/null",
            O_WRONLY
        );
        dup2(
            discarded,
            1
        );
        close(
            discarded
        );
        discarded = open(
            "/dev/null",
            O_WRONLY
        );
        dup2(
            discarded,
            2
        );
        close(
            discarded
        );
        return 1;
    }
}
