/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef BATCH_PROCESSING_THREAD_INCLUDED
#define BATCH_PROCESSING_THREAD_INCLUDED

#include <boost/function.hpp>
#include <list>
namespace @qlib@ {
    
    
    class batch_processing_thread {
    public:
        batch_processing_thread(
        );
        ~batch_processing_thread(
        );
        void enqueue_action(
            boost::function<
                void(
                )
            > const& enqueued
        );
    protected:
        void async_run_action(
        );
        void run_action(
            boost::system::error_code const& error
        );
        void run_processing(
        );
    private:
        boost::asio::io_service timing;
        boost::thread* running;
        std::list<
            boost::function<
                void(
                )
            >
        > actions;
        boost::mutex modifying_actions;
        boost::asio::deadline_timer pulse;
    };
}

#endif
