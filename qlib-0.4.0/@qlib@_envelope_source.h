/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef ENVELOPE_SOURCE_INCLUDED
#define ENVELOPE_SOURCE_INCLUDED

#include <boost/noncopyable.hpp>
#include <map>
namespace @qlib@ {
    
    
    template<
        typename value_type
    >
    class envelope_source :
    private boost::noncopyable {
    public:
        envelope_source(
            std::map<
                time_value,
                value_type
            > const& envelope_points,
            time_value rate,
            time_value duration
        );
        bool is_master(
        ) const;
        bool get_sample(
            value_type& retrieved
        );
    private:
        time_value const duration;
        time_value const real_rate;
        time_value const hz;
        linear_unidirectional_envelope<
            value_type
        > envelope;
        time_value position;
    };
    
    template<
        typename value_type
    >
    envelope_source<
        value_type
    >::envelope_source(
        std::map<
            time_value,
            value_type
        > const& envelope_points,
        time_value rate,
        time_value duration
    ) :
    duration(
        duration
    ),
    real_rate(
        rate
    ),
    hz(
        real_number(
            1 
        ) / real_rate
    ),
    envelope(
        envelope_points
    ),
    position(
        0
    ) {
    }
    
    template<
        typename value_type
    >
    bool envelope_source<
        value_type
    >::is_master(
    ) const {
        return true;
    }
    
    template<
        typename value_type
    >
    bool envelope_source<
        value_type
    >::get_sample(
        value_type& retrieved
    ) {
        if (
            duration <= position
        ) {
            return false;
        }
        retrieved = envelope.get_value(
            position
        );
        position += hz;
        return true;
    }
}

#endif
