/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_xmlsec_transform_context.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    xmlsec_transform_context::xmlsec_transform_context(
    ) {
        if (
            0 != xmlSecTransformCtxInitialize(
                &data
            )
        ) {
            throw component_error(
                "xmlsec_transform_context"
            );
        }
    }
    
    xmlsec_transform_context::~xmlsec_transform_context(
    ) {
        xmlSecTransformCtxFinalize(
            &data
        );
    }
    
    xmlSecTransformPtr xmlsec_transform_context::read_node(
        xmlNodePtr describing,
        xmlSecTransformUsage usage
    ) {
        xmlSecTransformPtr result = xmlSecTransformCtxNodeRead(
            &data,
            describing,
            usage
        );
        if (
            NULL == result
        ) {
            throw component_error(
                "xmlsec_transform_context"
            );
        }
        return result;
    }
    
    xmlSecTransformPtr xmlsec_transform_context::append_created(
        xmlSecTransformId created
    ) {
        xmlSecTransformPtr result = xmlSecTransformCtxCreateAndAppend(
            &data,
            created
        );
        if (
            NULL == result
        ) {
            throw component_error(
                "xmlsec_transform_context"
            );
        }
        return result;
    }
    
    xmlSecTransformCtxPtr xmlsec_transform_context::get_data(
    ) {
        return &data;
    }
}
