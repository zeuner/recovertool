/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_ip_address_decoding.h"
#include "@qlib@_stringified.h"
namespace @qlib@ {
    
    
    ip_address_decoding::ip_address_decoding(
    ) :
    raw_to_ipv4_chosen(
        (
            raw_to_ipv4_fast(
                "\x12\x34\x56\x78"
            ) == raw_to_ipv4_basic(
                "\x12\x34\x56\x78"
            )
        ) ? &ip_address_decoding::raw_to_ipv4_fast : &ip_address_decoding::raw_to_ipv4_basic
    ),
    raw_to_ipv6_chosen(
        (
            raw_to_ipv6_fast(
                "\x12\x34\x56\x78\x23\x45\x67\x89\x11\x22\x33\x44\x55\x66\x77\x88"
            ) == raw_to_ipv6_basic(
                "\x12\x34\x56\x78\x23\x45\x67\x89\x11\x22\x33\x44\x55\x66\x77\x88"
            )
        ) ? &ip_address_decoding::raw_to_ipv6_fast : &ip_address_decoding::raw_to_ipv6_basic
    ) {
    }
    
    boost::asio::ip::address_v4 ip_address_decoding::raw_to_ipv4(
        std::string const& raw
    ) {
        return (
            *raw_to_ipv4_chosen
        )(
            raw
        );
    }
    
    boost::asio::ip::address_v6 ip_address_decoding::raw_to_ipv6(
        std::string const& raw
    ) {
        return (
            *raw_to_ipv6_chosen
        )(
            raw
        );
    }
    
    int ip_address_decoding::char_to_int(
        char raw
    ) {
        return static_cast<
            int
        >(
            static_cast<
                unsigned char
            >(
                raw
            ) 
        );
    }
    
    boost::asio::ip::address_v4 ip_address_decoding::raw_to_ipv4_basic(
        std::string const& raw
    ) {
        return boost::asio::ip::address_v4::from_string(
            stringified(
                char_to_int(
                    raw[
                        0
                    ]
                )
            ) + "." + stringified(
                char_to_int(
                    raw[
                        1
                    ]
                )
            ) + "." + stringified(
                char_to_int(
                    raw[
                        2
                    ]
                )
            ) + "." + stringified(
                char_to_int(
                    raw[
                        3
                    ]
                )
            )
        );
    }
    
    boost::asio::ip::address_v4 ip_address_decoding::raw_to_ipv4_fast(
        std::string const& raw
    ) {
        boost::asio::ip::address_v4::bytes_type initializer;
        boost::asio::ip::address_v4::bytes_type::iterator writing;
        std::string::const_iterator reading;
        for (
            writing = initializer.begin(
            ),
            reading = raw.begin(
            );
            initializer.end(
            ) != writing;
            writing++,
            reading++
        ) {
            *writing = *reading;
        }
        return boost::asio::ip::address_v4(
            initializer
        );
    }
    
    boost::asio::ip::address_v6 ip_address_decoding::raw_to_ipv6_basic(
        std::string const& raw
    ) {
        std::string delimiter = "";
        std::string ip_string;
        for (
            std::string::const_iterator traversing = raw.begin(
            );
            raw.end(
            ) != traversing;
        ) {
            std::string const digits(
                "0123456789abcdef"
            );
            ip_string += delimiter;
            ip_string += digits[
                char_to_int(
                    *traversing
                ) >> 4
            ];
            ip_string += digits[
                char_to_int(
                    *traversing
                ) & 0xf
            ];
            traversing++;
            ip_string += digits[
                char_to_int(
                    *traversing
                ) >> 4
            ];
            ip_string += digits[
                char_to_int(
                    *traversing
                ) & 0xf
            ];
            traversing++;
            delimiter = ":";
        }
        return boost::asio::ip::address_v6::from_string(
            ip_string
        );
    }
    
    boost::asio::ip::address_v6 ip_address_decoding::raw_to_ipv6_fast(
        std::string const& raw
    ) {
        boost::asio::ip::address_v6::bytes_type initializer;
        boost::asio::ip::address_v6::bytes_type::iterator writing;
        std::string::const_iterator reading;
        for (
            writing = initializer.begin(
            ),
            reading = raw.begin(
            );
            initializer.end(
            ) != writing;
            writing++,
            reading++
        ) {
            *writing = *reading;
        }
        return boost::asio::ip::address_v6(
            initializer
        );
    }
}
