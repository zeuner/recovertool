/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_callback_synchronizer.h"
namespace @qlib@ {
    
    
    callback_synchronizer::callback_synchronizer(
        long callbacks
    ) :
    callback_pending(
        new boost::unique_lock<
            boost::mutex
        >(
            synchronizing
        )
    ),
    callbacks(
        callbacks
    ) {
    }
    
    void callback_synchronizer::await_callback(
    ) {
        boost::unique_lock<
            boost::mutex
        > waiting(
            synchronizing
        );
    }
    
    void callback_synchronizer::call_back(
    ) {
        if (
            0 < (
                --callbacks
            )
        ) {
            return;
        }
        call_back_final(
        );
    }
    
    void callback_synchronizer::call_back_final(
    ) {
        callback_pending.reset(
        );
    }
}
