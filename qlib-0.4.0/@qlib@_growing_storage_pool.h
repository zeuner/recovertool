/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef GROWING_STORAGE_POOL_INCLUDED
#define GROWING_STORAGE_POOL_INCLUDED

#include <list>
namespace @qlib@ {
    
    
    class growing_storage_pool {
    public:
        growing_storage_pool(
            size_t element_size,
            size_t smallest_pool = 0x400,
            size_t alignment = sizeof(
                void*
            )
        );
        void* get(
        );
        void put(
            void* freed
        );
    protected:
        void add_pool(
        );
    private:
        size_t current_pool_size;
        size_t aligned_element_size;
        flushed_pointer_container<
            std::list<
                fixed_storage_pool*
            >
        > pools;
        fixed_storage_pool* active_pool;
    };
}

#endif
