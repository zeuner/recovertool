/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_interface_check_promiscuous.h"
#include <cstring>
#include <unistd.h>
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    bool interface_check_promiscuous(
        std::string const& interface
    ) {
        int const checking = socket(
            AF_INET,
            SOCK_STREAM,
            0
        );
        if (
            0 > checking
        ) {
            throw component_error(
                "interface_check_promiscuous"
            );
        }
        struct ifreq interface_request;
        memset(
            &interface_request,
            0,
            sizeof(
                interface_request
            )
        );
        if (
            interface.size(
            ) >= IFNAMSIZ
        ) {
            close(
                checking
            );
            throw component_error(
                "interface_check_promiscuous"
            );
        }
        strcpy(
            interface_request.ifr_name,
            interface.data(
            )
        );
        if (
            0 > ioctl(
                checking,
                SIOCGIFFLAGS,
                &interface_request
            )
        ) {
            close(
                checking
            );
            throw component_error(
                "interface_check_promiscuous"
            );
        }
        bool const result = 0 != (
            IFF_PROMISC & interface_request.ifr_flags
        );
        close(
            checking
        );
        return result;
    }
}
