/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_triangle_wave.h"
#include <map>
#include <boost/shared_ptr.hpp>
namespace @qlib@ {
    
    
    time_value triangle_wave(
        time_value phase
    ) {
        static time_value const pi = 4.0 * std::atan(
            1.0
        );
        static boost::shared_ptr<
            linear_envelope<
                time_value
            >
        > values;
        if (
            !values
        ) {
            std::map<
                time_value,
                time_value
            > points;
            points[
                real_number(
                    0
                )
            ] = real_number(
                0
            );
            points[
                pi / real_number(
                    2
                )
            ] = 1;
            points[
                pi
            ] = 0;
            points[
                pi * real_number(
                    3
                ) / real_number(
                    2
                )
            ] = -1;
            points[
                pi * real_number(
                    2
                )
            ] = 0;
            values.reset(
                new linear_envelope<
                    time_value
                >(
                    points
                )
            );
        }
        return values->get_value(
            phase - (
                pi * real_number(
                    2
                ) * std::floor(
                    phase / real_number(
                    2
                ) / pi
                )
            )
        );
    }
}
