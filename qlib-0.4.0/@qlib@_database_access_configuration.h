/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef DATABASE_ACCESS_CONFIGURATION_INCLUDED
#define DATABASE_ACCESS_CONFIGURATION_INCLUDED

#include <string>
namespace @qlib@ {
    
    
    class database_access_configuration {
    public:
        void set_database(
            std::string const& hostname,
            std::string const& database,
            std::string const& username,
            std::string const& password
        );
        std::string const& get_database_hostname(
        ) const;
        std::string const& get_database_database(
        ) const;
        std::string const& get_database_username(
        ) const;
        std::string const& get_database_password(
        ) const;
    private:
        std::string hostname;
        std::string database;
        std::string username;
        std::string password;
    };
}

#endif
