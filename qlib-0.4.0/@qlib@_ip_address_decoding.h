/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef IP_ADDRESS_DECODING_INCLUDED
#define IP_ADDRESS_DECODING_INCLUDED

#include <boost/asio/ip/address_v4.hpp>
#include <string>
namespace @qlib@ {
    
    
    class ip_address_decoding {
    public:
        ip_address_decoding(
        );
        boost::asio::ip::address_v4 raw_to_ipv4(
            std::string const& raw
        );
        boost::asio::ip::address_v6 raw_to_ipv6(
            std::string const& raw
        );
    protected:
        static boost::asio::ip::address_v4 raw_to_ipv4_basic(
            std::string const& raw
        );
        static boost::asio::ip::address_v4 raw_to_ipv4_fast(
            std::string const& raw
        );
        static boost::asio::ip::address_v6 raw_to_ipv6_basic(
            std::string const& raw
        );
        static boost::asio::ip::address_v6 raw_to_ipv6_fast(
            std::string const& raw
        );
        static int char_to_int(
            char raw
        );
    private:
        boost::asio::ip::address_v4 (
            *raw_to_ipv4_chosen
        )(
            std::string const& raw
        );
        boost::asio::ip::address_v6 (
            *raw_to_ipv6_chosen
        )(
            std::string const& raw
        );
    };
}

#endif
