/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef DISSECT_HEADER_BLOCK_INCLUDED
#define DISSECT_HEADER_BLOCK_INCLUDED

#include <map>
#include "@qlib@_dissect_single_header.h"
#include "@qlib@_downcased.h"
namespace @qlib@ {
    
    
    template<
        typename string
    >
    void dissect_header_block(
        string const& whole,
        std::map<
            string,
            string
        >& headers
    );
    
    template<
        typename string
    >
    void dissect_header_block(
        string const& whole,
        std::map<
            string,
            string
        >& headers
    ) {
        headers.clear(
        );
        string const delimiter(
            "\r\n"
        );
        typename string::size_type before = 0;
        bool done;
        do {
            typename string::size_type after = whole.find(
                delimiter,
                before
            );
            done = false;
            if (
                string::npos == after
            ) {
                after = whole.size(
                );
                done = true;
            }
            string key;
            string value;
            dissect_single_header(
                string(
                    whole.begin(
                    ) + before,
                    whole.begin(
                    ) + after
                ),
                key,
                value
            );
            headers[
                downcased(
                    key
                )
            ] = value;
            before = after + delimiter.size(
            );
        } while (
            !done
        );
    }
}

#endif
