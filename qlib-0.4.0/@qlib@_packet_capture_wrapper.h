/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef PACKET_CAPTURE_WRAPPER_INCLUDED
#define PACKET_CAPTURE_WRAPPER_INCLUDED

#include <string>
namespace @qlib@ {
    
    
    class packet_capture_wrapper {
    public:
        packet_capture_wrapper(
            std::string const& interface,
            bool promiscuous = true,
            size_t snapshot_length = 0x1000,
            size_t buffer_size = 0x200000,
            int timeout_milliseconds = -1
        );
        ~packet_capture_wrapper(
        );
        pcap_t* get_pcap_handle(
        );
        int get_file_descriptor(
        ) const;
        size_t get_snapshot_length(
        ) const;
        std::string get_last_error(
        );
    private:
        char error[
            PCAP_ERRBUF_SIZE
        ];
        pcap_t* pcap_handle;
        int file_descriptor;
        size_t const snapshot_length;
    };
}

#endif
