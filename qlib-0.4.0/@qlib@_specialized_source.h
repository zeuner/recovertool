/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef SPECIALIZED_SOURCE_INCLUDED
#define SPECIALIZED_SOURCE_INCLUDED

#include <boost/shared_ptr.hpp>
#include <boost/noncopyable.hpp>
namespace @qlib@ {
    
    
    template<
        typename playable
    >
    class specialized_source :
    public generic_source,
    private boost::noncopyable {
    public:
        specialized_source(
            boost::shared_ptr<
                playable
            > implementation
        );
        specialized_source(
            playable* implementation
        );
        bool get_sample(
            amplitude_value& retrieved
        );
        size_t get_samples(
            amplitude_value* first,
            amplitude_value* last
        );
        bool is_master(
        ) const;
    protected:
        specialized_source(
        );
        void reset(
            boost::shared_ptr<
                playable
            > implementation
        );
    private:
        boost::shared_ptr<
            playable
        > implementation;
    };
    
    template<
        typename playable
    >
    class bulk_audio_retrieval<
        specialized_source<
            playable
        >
    > {
    public:
        static size_t get_samples(
            specialized_source<
                playable
            >& source,
            amplitude_value* first,
            amplitude_value* last
        );
    };
    
    template<
        typename playable
    >
    specialized_source<
        playable
    >::specialized_source(
    ) {
    }
    
    template<
        typename playable
    >
    specialized_source<
        playable
    >::specialized_source(
        boost::shared_ptr<
            playable
        > implementation
    ) :
    implementation(
        implementation
    ) {
    }
    
    template<
        typename playable
    >
    specialized_source<
        playable
    >::specialized_source(
        playable* implementation
    ) :
    implementation(
        implementation
    ) {
    }
    
    template<
        typename playable
    >
    bool specialized_source<
        playable
    >::get_sample(
        amplitude_value& retrieved
    ) {
        return implementation->get_sample(
            retrieved
        );
    }
    
    template<
        typename playable
    >
    size_t specialized_source<
        playable
    >::get_samples(
        amplitude_value* first,
        amplitude_value* last
    ) {
        return bulk_audio_retrieval<
            playable
        >::get_samples(
            *implementation,
            first,
            last
        );
    }
    
    template<
        typename playable
    >
    bool specialized_source<
        playable
    >::is_master(
    ) const { 
        return implementation->is_master(
        );
    }
    
    template<
        typename playable
    >
    void specialized_source<
        playable
    >::reset(
        boost::shared_ptr<
            playable
        > implementation
    ) {
        this->implementation = implementation;
    }
    
    template<
        typename playable
    >
    size_t bulk_audio_retrieval<
        specialized_source<
            playable
        >
    >::get_samples(
        specialized_source<
            playable
        >& source,
        amplitude_value* first,
        amplitude_value* last
    ) {
        return source.get_samples(
            first,
            last
        );
    }
}

#endif
