/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef DESERIALIZE_INCLUDED
#define DESERIALIZE_INCLUDED

#include <vector>
#include <string>
#include <boost/date_time/posix_time/ptime.hpp>
#include "@qlib@_component_error.h"
#include "@qlib@_supported_type.h"
#include <boost/unordered_map.hpp>
#include <boost/date_time/posix_time/time_parsers.hpp>
#include "@qlib@_throw_exception.h"
#include <set>
#include <boost/asio/ip/address_v4.hpp>
#include <list>
#include "@qlib@_deserializing_short_data.h"
#include <boost/asio/ip/tcp.hpp>
#include <map>
#include <boost/optional.hpp>
namespace @qlib@ {
    
    
    template<
        typename deserializable
    >
    class deserialize_implementation {
    public:
        template<
            typename iterator
        >
        static void apply(
            iterator& from,
            iterator const& to,
            deserializable& raw,
            bool& short_data
        );
    };
    
    template<
        typename deserializable,
        typename iterator
    >
    void deserialize(
        iterator& from,
        iterator const& to,
        deserializable& raw
    ) {
        bool short_data = false;
        deserialize_implementation<
            deserializable
        >::apply(
            from,
            to,
            raw,
            short_data
        );
        if (
            short_data
        ) {
            throw_exception(
                deserializing_short_data(
                )
            );
        }
    }
    
    template<
        typename deserializable,
        typename iterator
    >
    void deserialize(
        iterator& from,
        iterator const& to,
        deserializable& raw,
        bool& short_data
    ) {
        deserialize_implementation<
            deserializable
        >::apply(
            from,
            to,
            raw,
            short_data
        );
    }
    
    template<
        typename deserializable
    >
    template<
        typename iterator
    >
    void deserialize_implementation<
        deserializable
    >::apply(
        iterator& from,
        iterator const& to,
        deserializable& raw,
        bool& short_data
    ) {
        supported_type<
            deserializable
        > supported(
            raw
        );
        throw_exception(
            component_error(
                "deserialize"
            )
        );
    }
    
    template<
    >
    class deserialize_implementation<
        std::string
    > {
    public:
        template<
            typename iterator
        >
        static void apply(
            iterator& from,
            iterator const& to,
            std::string& raw,
            bool& short_data
        );
    };
    
    template<
        typename first_type,
        typename second_type
    >
    class deserialize_implementation<
        std::pair<
            first_type,
            second_type
        >
    > {
    public:
        template<
            typename iterator 
        >
        static void apply(
            iterator& from,
            iterator const& to,
            std::pair<
                first_type,
                second_type
            >& raw,
            bool& short_data
        );
    };
    
    template<
        typename first_type,
        typename second_type
    >
    template<
        typename iterator
    >
    void deserialize_implementation<
        std::pair<
            first_type,
            second_type
        >
    >::apply(
        iterator& from,
        iterator const& to,
        std::pair<
            first_type,
            second_type
        >& raw,
        bool& short_data
    ) {
        deserialize(
            from,
            to,
            raw.first,
            short_data
        );
        if (
            short_data
        ) {
            return;
        }
        deserialize(
            from,
            to,
            raw.second,
            short_data
        );
    }
    
    template<
        typename value_type
    >
    class deserialize_implementation<
        std::set<
            value_type
        >
    > {
    public:
        template<
            typename iterator 
        >
        static void apply(
            iterator& from,
            iterator const& to,
            std::set<
                value_type
            >& raw,
            bool& short_data
        );
    };
    
    template<
        typename value_type
    >
    class deserialize_implementation<
        boost::optional<
            value_type
        >
    > {
    public:
        template<
            typename iterator 
        >
        static void apply(
            iterator& from,
            iterator const& to,
            boost::optional<
                value_type
            >& raw,
            bool& short_data
        );
    };
    
    template<
        typename value_type
    >
    class deserialize_implementation<
        std::vector<
            value_type
        >
    > {
    public:
        template<
            typename iterator 
        >
        static void apply(
            iterator& from,
            iterator const& to,
            std::vector<
                value_type
            >& raw,
            bool& short_data
        );
    };
    
    template<
        typename value_type
    >
    class deserialize_implementation<
        std::list<
            value_type
        >
    > {
    public:
        template<
            typename iterator 
        >
        static void apply(
            iterator& from,
            iterator const& to,
            std::list<
                value_type
            >& raw,
            bool& short_data
        );
    };
    
    template<
        typename key_type,
        typename value_type
    >
    class deserialize_implementation<
        std::map<
            key_type,
            value_type
        >
    > {
    public:
        template<
            typename iterator 
        >
        static void apply(
            iterator& from,
            iterator const& to,
            std::map<
                key_type,
                value_type
            >& raw,
            bool& short_data
        );
    };
    
    template<
        typename key_type,
        typename value_type
    >
    class deserialize_implementation<
        boost::unordered_map<
            key_type,
            value_type
        >
    > {
    public:
        template<
            typename iterator 
        >
        static void apply(
            iterator& from,
            iterator const& to,
            boost::unordered_map<
                key_type,
                value_type
            >& raw,
            bool& short_data
        );
    };
    
    template<
        typename value_type
    >
    template<
        typename iterator
    >
    void deserialize_implementation<
        std::set<
            value_type
        >
    >::apply(
        iterator& from,
        iterator const& to,
        std::set<
            value_type
        >& raw,
        bool& short_data
    ) {
        do {
            if (
                to == from
            ) {
                short_data = true;
                return;
            }
            if (
                '1' == *from++
            ) {
                value_type entry;
                deserialize(
                    from,
                    to,
                    entry,
                    short_data
                );
                if (
                    short_data
                ) {
                    return;
                }
                raw.insert(
                    entry
                );
            } else {
                break;
            }
        } while (
            true
        );
    }
    
    template<
        typename value_type
    >
    template<
        typename iterator
    >
    void deserialize_implementation<
        boost::optional<
            value_type
        >
    >::apply(
        iterator& from,
        iterator const& to,
        boost::optional<
            value_type
        >& raw,
        bool& short_data
    ) {
        if (
            to == from
        ) {
            short_data = true;
            return;
        }
        if (
            '1' == *from++
        ) {
            value_type initialized;
            deserialize(
                from,
                to,
                initialized,
                short_data
            );
            if (
                short_data
            ) {
                return;
            }
            raw = initialized;
        } else {
            raw.reset(
            );
        }
    }
    
    template<
        typename value_type
    >
    template<
        typename iterator
    >
    void deserialize_implementation<
        std::vector<
            value_type
        >
    >::apply(
        iterator& from,
        iterator const& to,
        std::vector<
            value_type
        >& raw,
        bool& short_data
    ) {
        typename std::vector<
            value_type
        >::size_type size;
        deserialize(
            from,
            to,
            size,
            short_data
        );
        if (
            short_data
        ) {
            return;
        }
        raw.resize(
            size
        );
        for (
            typename std::vector<
                value_type
            >::size_type traversed = 0;
            size > traversed;
            traversed++
        ) {
            deserialize(
                from,
                to,
                raw[
                    traversed
                ],
                short_data
            );
            if (
                short_data
            ) {
                return;
            }
        }
    }
    
    template<
        typename value_type
    >
    template<
        typename iterator
    >
    void deserialize_implementation<
        std::list<
            value_type
        >
    >::apply(
        iterator& from,
        iterator const& to,
        std::list<
            value_type
        >& raw,
        bool& short_data
    ) {
        do {
            if (
                to == from
            ) {
                short_data = true;
                return;
            }
            if (
                '1' == *from++
            ) {
                value_type entry;
                deserialize(
                    from,
                    to,
                    entry,
                    short_data
                );
                if (
                    short_data
                ) {
                    return;
                }
                raw.push_back(
                    entry
                );
            } else {
                break;
            }
        } while (
            true
        );
    }
    
    template<
        typename key_type,
        typename value_type
    >
    template<
        typename iterator
    >
    void deserialize_implementation<
        std::map<
            key_type,
            value_type
        >
    >::apply(
        iterator& from,
        iterator const& to,
        std::map<
            key_type,
            value_type
        >& raw,
        bool& short_data
    ) {
        do {
            if (
                to == from
            ) {
                short_data = true;
                return;
            }
            if (
                '1' == *from++
            ) {
                std::pair<
                    key_type,
                    value_type
                > entry;
                deserialize(
                    from,
                    to,
                    entry,
                    short_data
                );
                if (
                    short_data
                ) {
                    return;
                }
                raw[
                    entry.first
                ] = entry.second;
            } else {
                break;
            }
        } while (
            true
        );
    }
    
    template<
        typename key_type,
        typename value_type
    >
    template<
        typename iterator
    >
    void deserialize_implementation<
        boost::unordered_map<
            key_type,
            value_type
        >
    >::apply(
        iterator& from,
        iterator const& to,
        boost::unordered_map<
            key_type,
            value_type
        >& raw,
        bool& short_data
    ) {
        do {
            if (
                to == from
            ) {
                short_data = true;
                return;
            }
            if (
                '1' == *from++
            ) {
                std::pair<
                    key_type,
                    value_type
                > entry;
                deserialize(
                    from,
                    to,
                    entry,
                    short_data
                );
                if (
                    short_data
                ) {
                    return;
                }
                raw[
                    entry.first
                ] = entry.second;
            } else {
                break;
            }
        } while (
            true
        );
    }
    
    template<
    >
    class deserialize_implementation<
        boost::asio::ip::address_v4
    > {
    public:
        template<
            typename iterator 
        >
        static void apply(
            iterator& from,
            iterator const& to,
            boost::asio::ip::address_v4& raw,
            bool& short_data
        );
    };
    
    template<
    >
    class deserialize_implementation<
        boost::posix_time::ptime
    > {
    public:
        template<
            typename iterator 
        >
        static void apply(
            iterator& from,
            iterator const& to,
            boost::posix_time::ptime& raw,
            bool& short_data
        );
    };
    
    template<
    >
    class deserialize_implementation<
        boost::asio::ip::tcp::endpoint
    > {
    public:
        template<
            typename iterator 
        >
        static void apply(
            iterator& from,
            iterator const& to,
            boost::asio::ip::tcp::endpoint& raw,
            bool& short_data
        );
    };
    
    template<
    >
    class deserialize_implementation<
        long
    > {
    public:
        template<
            typename iterator 
        >
        static void apply(
            iterator& from,
            iterator const& to,
            long& raw,
            bool& short_data
        );
    };
    
    template<
    >
    class deserialize_implementation<
        unsigned long
    > {
    public:
        template<
            typename iterator 
        >
        static void apply(
            iterator& from,
            iterator const& to,
            unsigned long& raw,
            bool& short_data
        );
    };
    
    template<
    >
    class deserialize_implementation<
        long long
    > {
    public:
        template<
            typename iterator 
        >
        static void apply(
            iterator& from,
            iterator const& to,
            long long& raw,
            bool& short_data
        );
    };
    
    template<
    >
    class deserialize_implementation<
        unsigned long long
    > {
    public:
        template<
            typename iterator 
        >
        static void apply(
            iterator& from,
            iterator const& to,
            unsigned long long& raw,
            bool& short_data
        );
    };
    
    template<
    >
    class deserialize_implementation<
        unsigned char
    > {
    public:
        template<
            typename iterator 
        >
        static void apply(
            iterator& from,
            iterator const& to,
            unsigned char& raw,
            bool& short_data
        );
    };
    
    template<
    >
    class deserialize_implementation<
        bool
    > {
    public:
        template<
            typename iterator 
        >
        static void apply(
            iterator& from,
            iterator const& to,
            bool& raw,
            bool& short_data
        );
    };
    
    template<
    >
    class deserialize_implementation<
        unsigned
    > {
    public:
        template<
            typename iterator 
        >
        static void apply(
            iterator& from,
            iterator const& to,
            unsigned& raw,
            bool& short_data
        );
    };
    
    template<
    >
    class deserialize_implementation<
        unsigned short
    > {
    public:
        template<
            typename iterator 
        >
        static void apply(
            iterator& from,
            iterator const& to,
            unsigned short& raw,
            bool& short_data
        );
    };
    
    template<
    >
    class deserialize_implementation<
        int
    > {
    public:
        template<
            typename iterator 
        >
        static void apply(
            iterator& from,
            iterator const& to,
            int& raw,
            bool& short_data
        );
    };
    
    template<
    >
    class deserialize_implementation<
        float
    > {
    public:
        template<
            typename iterator 
        >
        static void apply(
            iterator& from,
            iterator const& to,
            float& raw,
            bool& short_data
        );
    };
    
    template<
    >
    class deserialize_implementation<
        double
    > {
    public:
        template<
            typename iterator 
        >
        static void apply(
            iterator& from,
            iterator const& to,
            double& raw,
            bool& short_data
        );
    };
    
    template<
        typename iterator
    >
    void deserialize_implementation<
        unsigned long
    >::apply(
        iterator& from,
        iterator const& to,
        unsigned long& raw,
        bool& short_data
    ) {
        BOOST_STATIC_ASSERT(
            sizeof(
                unsigned long
            ) <= sizeof(
                uint64_t
            )
        );
        uint64_t const* value;
        iterator const after_value = from + sizeof(
            *value
        );
        if (
            to < after_value
        ) {
            short_data = true;
            return;
        }
        std::string const value_bytes(
            from,
            after_value
        );
        value = reinterpret_cast<
            uint64_t const*
        >(
            value_bytes.data(
            )
        );
        raw = *value;
        from = after_value;
    }
    
    template<
        typename iterator
    >
    void deserialize_implementation<
        long long
    >::apply(
        iterator& from,
        iterator const& to,
        long long& raw,
        bool& short_data
    ) {
        BOOST_STATIC_ASSERT(
            sizeof(
                long long
            ) <= sizeof(
                uint64_t
            )
        );
        uint64_t const* value;
        iterator const after_value = from + sizeof(
            *value
        );
        if (
            to < after_value
        ) {
            short_data = true;
            return;
        }
        std::string const value_bytes(
            from,
            after_value
        );
        value = reinterpret_cast<
            uint64_t const*
        >(
            value_bytes.data(
            )
        );
        raw = *value;
        from = after_value;
    }
    
    template<
        typename iterator
    >
    void deserialize_implementation<
        unsigned long long
    >::apply(
        iterator& from,
        iterator const& to,
        unsigned long long& raw,
        bool& short_data
    ) {
        BOOST_STATIC_ASSERT(
            sizeof(
                unsigned long long
            ) <= sizeof(
                uint64_t
            )
        );
        uint64_t const* value;
        iterator const after_value = from + sizeof(
            *value
        );
        if (
            to < after_value
        ) {
            short_data = true;
            return;
        }
        std::string const value_bytes(
            from,
            after_value
        );
        value = reinterpret_cast<
            uint64_t const*
        >(
            value_bytes.data(
            )
        );
        raw = *value;
        from = after_value;
    }
    
    template<
        typename iterator
    >
    void deserialize_implementation<
        unsigned char
    >::apply(
        iterator& from,
        iterator const& to,
        unsigned char& raw,
        bool& short_data
    ) {
        if (
            to <= from
        ) {
            short_data = true;
            return;
        }
        unsigned char const* value = reinterpret_cast<
            unsigned char const*
        >(
            &(
                *from
            )
        );
        raw = *value;
        from++;
    }
    
    template<
        typename iterator
    >
    void deserialize_implementation<
        bool
    >::apply(
        iterator& from,
        iterator const& to,
        bool& raw,
        bool& short_data
    ) {
        if (
            to <= from
        ) {
            short_data = true;
            return;
        }
        raw = '1' == *from;
        from++;
    }
    
    template<
        typename iterator
    >
    void deserialize_implementation<
        long
    >::apply(
        iterator& from,
        iterator const& to,
        long& raw,
        bool& short_data
    ) {
        BOOST_STATIC_ASSERT(
            sizeof(
                long
            ) <= sizeof(
                uint64_t
            )
        );
        uint64_t const* value;
        iterator const after_value = from + sizeof(
            *value
        );
        if (
            to < after_value
        ) {
            short_data = true;
            return;
        }
        std::string const value_bytes(
            from,
            after_value
        );
        value = reinterpret_cast<
            uint64_t const*
        >(
            value_bytes.data(
            )
        );
        raw = *value;
        from = after_value;
    }
    
    template<
        typename iterator
    >
    void deserialize_implementation<
        unsigned
    >::apply(
        iterator& from,
        iterator const& to,
        unsigned& raw,
        bool& short_data
    ) {
        BOOST_STATIC_ASSERT(
            sizeof(
                unsigned
            ) <= sizeof(
                uint64_t
            )
        );
        uint64_t const* value;
        iterator const after_value = from + sizeof(
            *value
        );
        if (
            to < after_value
        ) {
            short_data = true;
            return;
        }
        std::string const value_bytes(
            from,
            after_value
        );
        value = reinterpret_cast<
            uint64_t const*
        >(
            value_bytes.data(
            )
        );
        raw = *value;
        from = after_value;
    }
    
    template<
        typename iterator
    >
    void deserialize_implementation<
        unsigned short
    >::apply(
        iterator& from,
        iterator const& to,
        unsigned short& raw,
        bool& short_data
    ) {
        BOOST_STATIC_ASSERT(
            sizeof(
                unsigned short
            ) <= sizeof(
                uint64_t
            )
        );
        uint64_t const* value;
        iterator const after_value = from + sizeof(
            *value
        );
        if (
            to < after_value
        ) {
            short_data = true;
            return;
        }
        std::string const value_bytes(
            from,
            after_value
        );
        value = reinterpret_cast<
            uint64_t const*
        >(
            value_bytes.data(
            )
        );
        raw = *value;
        from = after_value;
    }
    
    template<
        typename iterator
    >
    void deserialize_implementation<
        int
    >::apply(
        iterator& from,
        iterator const& to,
        int& raw,
        bool& short_data
    ) {
        BOOST_STATIC_ASSERT(
            sizeof(
                int
            ) <= sizeof(
                uint64_t
            )
        );
        uint64_t const* value;
        iterator const after_value = from + sizeof(
            *value
        );
        if (
            to < after_value
        ) {
            short_data = true;
            return;
        }
        std::string const value_bytes(
            from,
            after_value
        );
        value = reinterpret_cast<
            uint64_t const*
        >(
            value_bytes.data(
            )
        );
        raw = *value;
        from = after_value;
    }
    
    template<
        typename iterator
    >
    void deserialize_implementation<
        float
    >::apply(
        iterator& from,
        iterator const& to,
        float& raw,
        bool& short_data
    ) {
        std::string textual;
        deserialize(
            from,
            to,
            textual,
            short_data
        );
        if (
            short_data
        ) {
            return;
        }
        raw = atof(
            textual.c_str(
            )
        );
    }
    
    template<
        typename iterator
    >
    void deserialize_implementation<
        double
    >::apply(
        iterator& from,
        iterator const& to,
        double& raw,
        bool& short_data
    ) {
        std::string textual;
        deserialize(
            from,
            to,
            textual,
            short_data
        );
        if (
            short_data
        ) {
            return;
        }
        raw = atof(
            textual.c_str(
            )
        );
    }
    
    template<
        typename iterator
    >
    void deserialize_implementation<
        std::string
    >::apply(
        iterator& from,
        iterator const& to,
        std::string& raw,
        bool& short_data
    ) {
        BOOST_STATIC_ASSERT(
            sizeof(
                std::string::size_type
            ) <= sizeof(
                uint64_t
            )
        );
        uint64_t size;
        iterator const at_data = from + sizeof(
            size
        );
        if (
            to < at_data
        ) {
            short_data = true;
            return;
        }
        std::copy(
            from,
            at_data,
            reinterpret_cast<
                char*
            >(
                &size
            )
        );
        iterator const after_data = at_data + size;
        if (
            to < after_data
        ) {
            short_data = true;
            return;
        }
        raw.assign(
            at_data,
            after_data
        );
        from = after_data;
    }
    
    template<
        typename iterator
    >
    void deserialize_implementation<
        boost::asio::ip::tcp::endpoint
    >::apply(
        iterator& from,
        iterator const& to,
        boost::asio::ip::tcp::endpoint& raw,
        bool& short_data
    ) {
        boost::asio::ip::address_v4 host;
        deserialize(
            from,
            to,
            host,
            short_data
        );
        if (
            short_data
        ) {
            return;
        }
        unsigned short port = 0;
        deserialize(
            from,
            to,
            port,
            short_data
        );
        if (
            short_data
        ) {
            return;
        }
        raw = boost::asio::ip::tcp::endpoint(
            host,
            port
        );
    }
    
    template<
        typename iterator
    >
    void deserialize_implementation<
        boost::asio::ip::address_v4
    >::apply(
        iterator& from,
        iterator const& to,
        boost::asio::ip::address_v4& raw,
        bool& short_data
    ) {
        std::string as_string;
        deserialize(
            from,
            to,
            as_string,
            short_data
        );
        if (
            short_data
        ) {
            return;
        }
        raw = boost::asio::ip::address_v4::from_string(
            as_string
        );
    }
    
    template<
        typename iterator
    >
    void deserialize_implementation<
        boost::posix_time::ptime
    >::apply(
        iterator& from,
        iterator const& to,
        boost::posix_time::ptime& raw,
        bool& short_data
    ) {
        std::string as_string;
        deserialize(
            from,
            to,
            as_string,
            short_data
        );
        if (
            short_data
        ) {
            return;
        }
        raw = boost::posix_time::from_iso_string(
            as_string
        );
    }
}

#endif
