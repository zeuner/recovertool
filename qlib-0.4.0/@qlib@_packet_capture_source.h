/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef PACKET_CAPTURE_SOURCE_INCLUDED
#define PACKET_CAPTURE_SOURCE_INCLUDED

#include <string>
#include <boost/function.hpp>
namespace @qlib@ {
    
    
    class packet_capture_source :
    public packet_capture_socket {
    public:
        static inline boost::arg<
            2
        > data_argument(
        );
        static inline boost::arg<
            3
        > size_argument(
        );
        static inline boost::arg<
            4
        > original_size_argument(
        );
        static inline boost::arg<
            5
        > time_argument(
        );
        packet_capture_source(
            boost::asio::io_service& communicating,
            std::string const& interface,
            bool promiscuous = true,
            size_t snapshot_length = 0x1000,
            size_t buffer_size = 0x200000,
            int timeout_milliseconds = -1
        );
        typedef boost::function<
            void(
                boost::system::error_code const& error,
                void const* data,
                size_t size,
                size_t original_size,
                struct timeval const& capture_time
            )
        > packet_handler;
        void async_get_packet(
            packet_handler const& handling
        );
    protected:
        void handle_packet(
            boost::system::error_code const& error
        );
    private:
        std::auto_ptr<
            packet_handler
        > handling_packet;
    };
    
    boost::arg<
        2
    > packet_capture_source::data_argument(
    ) {
        return boost::arg<
            2
        >(
        );
    }
    
    boost::arg<
        3
    > packet_capture_source::size_argument(
    ) {
        return boost::arg<
            3
        >(
        );
    }
    
    boost::arg<
        4
    > packet_capture_source::original_size_argument(
    ) {
        return boost::arg<
            4
        >(
        );
    }
    
    boost::arg<
        5
    > packet_capture_source::time_argument(
    ) {
        return boost::arg<
            5
        >(
        );
    }
}

#endif
