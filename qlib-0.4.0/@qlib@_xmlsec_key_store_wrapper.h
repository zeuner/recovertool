/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef XMLSEC_KEY_STORE_WRAPPER_INCLUDED
#define XMLSEC_KEY_STORE_WRAPPER_INCLUDED

namespace @qlib@ {
    
    
    template<
        typename storing
    >
    class xmlsec_key_store_wrapper :
    public storing {
    public:
        static xmlSecKeyStoreId get_store_id(
        );
        static xmlsec_key_store_wrapper* get_object(
            xmlSecKeyStorePtr accessed
        );
    protected:
        static int initialize(
            xmlSecKeyStorePtr initialized
        );
        static void finalize(
            xmlSecKeyStorePtr finalized
        );
        static xmlSecKeyPtr find_key(
            xmlSecKeyStorePtr searching,
            xmlChar const* name,
            xmlSecKeyInfoCtxPtr key_info_context
        );
    };
    
    template<
        typename storing
    >
    xmlSecKeyStoreId xmlsec_key_store_wrapper<
        storing
    >::get_store_id(
    ) {
        static xmlSecKeyStoreKlass self = {
            sizeof(
                xmlSecKeyStoreKlass
            ),
            sizeof(
                xmlSecKeyStore
            ) + sizeof(
                xmlsec_key_store_wrapper<
                    storing
                >
            ),
            BAD_CAST "xmlsec_key_store_wrapper",
            &xmlsec_key_store_wrapper<
                storing
            >::initialize,
            &xmlsec_key_store_wrapper<
                storing
            >::finalize,
            &xmlsec_key_store_wrapper<
                storing
            >::find_key,
            NULL,
            NULL,
        };
        return static_cast<
            xmlSecKeyStoreId
        >(
            &self
        );
    }
    
    template<
        typename storing
    >
    int xmlsec_key_store_wrapper<
        storing
    >::initialize(
        xmlSecKeyStorePtr initialized
    ) {
        try {
            new(
                get_object(
                    initialized
                )
            ) xmlsec_key_store_wrapper<
                storing
            >;
            return 0;
        } catch (
            std::exception& caught
        ) {
            return -1;
        }
    }
    
    template<
        typename storing
    >
    void xmlsec_key_store_wrapper<
        storing
    >::finalize(
        xmlSecKeyStorePtr finalized
    ) {
        get_object(
            finalized
        )->~xmlsec_key_store_wrapper(
        );
    }
    
    template<
        typename storing
    >
    xmlsec_key_store_wrapper<
        storing
    >* xmlsec_key_store_wrapper<
        storing
    >::get_object(
        xmlSecKeyStorePtr accessed
    ) {
        return reinterpret_cast<
            xmlsec_key_store_wrapper<
                storing
            >*
        >(
            reinterpret_cast<
                char*
            >(
                accessed
            ) + sizeof(
                xmlSecKeyStore
            )
        );
    }
    
    template<
        typename storing
    >
    xmlSecKeyPtr xmlsec_key_store_wrapper<
        storing
    >::find_key(
        xmlSecKeyStorePtr searching,
        xmlChar const* name,
        xmlSecKeyInfoCtxPtr key_info_context
    ) {
        if (
            NULL == name
        ) {
            return NULL;
        }
        if (
            xmlSecKeyDataIdUnknown == key_info_context->keyReq.keyId
        ) {
            return NULL;
        }
        try {
            return get_object(
                searching
            )->get_key(
                name,
                key_info_context
            );
        } catch (
            std::exception& caught
        ) {
            return NULL;
        }
    }
}

#endif
