/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef DATABASE_QUERY_ERROR_INCLUDED
#define DATABASE_QUERY_ERROR_INCLUDED

#include <string>
namespace @qlib@ {
    
    
    class database_query_error :
    public std::exception {
    public:
        database_query_error(
            std::string const& message
        );
        ~database_query_error(
        ) throw(
        );
        const char* what(
        ) const throw(
        );
        std::string const& get_message(
        ) const;
    private:
        std::string const message;
    };
}

#endif
