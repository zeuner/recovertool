/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_netlink_received_message.h"
#include "@qlib@_component_error.h"
#include <cstring>
#include "@qlib@_throw_exception.h"
namespace @qlib@ {
    
    
    netlink_received_message::netlink_received_message(
        int descriptor,
        size_t buffer_size
    ) :
    buffer(
        new char[
            buffer_size
        ]
    ) {
        memset(
            &data,
            0,
            sizeof(
                data
            )
        );
        io_vector.iov_base = buffer;
        io_vector.iov_len = buffer_size;
        data.msg_iov = &io_vector;
        data.msg_iovlen = 1;
        ssize_t const received_length_raw = recvmsg(
            descriptor,
            &data,
            0
        );
        if (
            0 > received_length_raw
        ) {
            this->~netlink_received_message(
            );
            throw_exception(
                component_error(
                    "netlink_received_message"
                )
            );
        }
        if (
            0 != (
                MSG_TRUNC & data.msg_flags
            )
        ) {
            this->~netlink_received_message(
            );
            throw_exception(
                component_error(
                    "netlink_received_message"
                )
            );
        }
        size = received_length_raw;
    }
    
    netlink_received_message::~netlink_received_message(
    ) {
        delete[
        ] buffer;
    }
    
    size_t netlink_received_message::get_size(
    ) const {
        return size;
    }
    
    netlink_received_message::iterator netlink_received_message::begin(
    ) const {
        return iterator(
            *this,
            reinterpret_cast<
                struct nlmsghdr const*
            >(
                buffer
            )
        );
    }
    
    netlink_received_message::iterator netlink_received_message::end(
    ) const {
        return iterator(
            *this,
            NULL
        );
    }
    
    netlink_received_message::iterator::iterator(
        netlink_received_message const& message,
        struct nlmsghdr const* position
    ) :
    message(
        message
    ),
    position(
        position
    ),
    available(
        message.get_size(
        )
    ) {
        if (
            !NLMSG_OK(
                position,
                available 
            )
        ) {
            position = NULL;
            return;
        }
        if (
            NLMSG_DONE == position->nlmsg_type
        ) {
            position = NULL;
        }
    }
    
    size_t netlink_received_message::iterator::payload_length(
    ) const {
        return NLMSG_PAYLOAD(
            position,
            0
        );
    }
    
    void const* netlink_received_message::iterator::data(
    ) const {
        return NLMSG_DATA(
            position
        );
    }
    
    struct nlmsghdr const* netlink_received_message::iterator::operator*(
    ) const {
        return position;
    }
    
    struct nlmsghdr const* netlink_received_message::iterator::operator->(
    ) const {
        return position;
    }
    
    bool netlink_received_message::iterator::operator==(
        iterator const& other
    ) const {
        return (
            position == other.position
        ) && (
            &message == &other.message
        );
    }
    
    bool netlink_received_message::iterator::operator!=(
        iterator const& other
    ) const {
        return (
            position != other.position
        ) || (
            &message != &other.message
        );
    }
    
    netlink_received_message::iterator netlink_received_message::iterator::operator++(
    ) {
        do {
            if (
                0 == (
                    NLM_F_MULTI & position->nlmsg_flags
                )
            ) {
                position = NULL;
                break;
            }
            position = NLMSG_NEXT(
                position,
                available
            );
            if (
                !NLMSG_OK(
                    position,
                    available
                )
            ) {
                position = NULL;
                break;
            }
            if (
                NLMSG_DONE == position->nlmsg_type
            ) {
                position = NULL;
            }
        } while (
            false
        );
        return *this;
    }
    
    netlink_received_message::iterator netlink_received_message::iterator::operator++(
        int
    ) {
        iterator pre = *this;
        ++(
            *this
        );
        return pre;
    }
}
