/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_http_message.h"
#include "@qlib@_throw_exception.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    http_message::http_message(
        std::string const& raw
    ) {
        std::string::size_type at_delimiter = raw.find(
            body_delimiter
        );
        if (
            std::string::npos == at_delimiter
        ) {
            throw_exception(
                component_error(
                    "http_message"
                )
            );
        }
        headers = std::string(
            raw.begin(
            ),
            raw.begin(
            ) + at_delimiter
        );
        body = std::string(
            raw.begin(
            ) + at_delimiter + body_delimiter.size(
            ),
            raw.end(
            )
        );
    }
    
    std::string const& http_message::get_headers(
    ) const {
        return headers;
    }
    
    std::string const& http_message::get_body(
    ) const {
        return body;
    }
    
    bool http_message::valid_message(
        std::string const& raw
    ) {
        return std::string::npos != raw.find(
            body_delimiter
        );
    }
    
    std::string const http_message::body_delimiter(
        "\r\n"
        "\r\n"
    );
}
