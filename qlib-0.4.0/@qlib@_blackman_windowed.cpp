/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_blackman_windowed.h"
namespace @qlib@ {
    
    
    std::list< 
        amplitude_value 
    > blackman_windowed(
        std::list<
            amplitude_value
        > windowing
    ) {
        static time_value const pi = 4.0 * std::atan(
            1.0
        );
        time_value const size = windowing.size(
        ) - 1;
        int i = 0;
        for (
            std::list<
                amplitude_value
            >::iterator adapting = windowing.begin(
            );
            windowing.end(
            ) != adapting;
            adapting++,
            i++
        ) {
            *adapting *= real_number(
                0.42
            ) - (
                real_number(
                    0.5
                ) * cos(
                    pi * real_number(
                        2
                    ) * real_number(
                        i
                    ) / size
                )
            ) + (
                real_number(
                    0.08
                ) * cos(
                    pi * real_number(
                        4
                    ) * real_number(
                        i
                    ) / size
                )
            );
        }
        return windowing;
    }
}
