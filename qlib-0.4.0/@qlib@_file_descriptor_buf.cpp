/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_file_descriptor_buf.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    file_descriptor_buf::file_descriptor_buf(
        int descriptor
    ) :
    descriptor(
        descriptor
    ) {
    }
    
    std::streamsize file_descriptor_buf::xsputn(
        char const* data,
        std::streamsize size
    ) {
        int const written = write(
            descriptor,
            data,
            size
        );
        if (
            0 > written
        ) {
            throw component_error(
                "file_descriptor_buf"
            );
        }
        return written;
    }
    
    int file_descriptor_buf::overflow(
        int character
    ) {
        if (
            traits_type::eof(
            ) != character
        ) {
            char byte = character;
            if (
                1 != write(
                    descriptor,
                    &byte,
                    1
                )
            ) {
                return traits_type::eof(
                );
            }
        }
        return character;
    }
}
