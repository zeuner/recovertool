/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef PDF_PAGE_INCLUDED
#define PDF_PAGE_INCLUDED

#include <string>
namespace @qlib@ {
    
    
    class pdf_page {
    public:
        pdf_page(
            pdf_document& document
        );
        float width(
        );
        float height(
        );
        void write_text(
            pdf_font const& font,
            std::string const& text,
            int size,
            float width,
            float height
        );
        void write_line(
            float line_width,
            float width_begin,
            float height_begin,
            float width_end,
            float height_end
        );
    private:
        HPDF_Page native;
    };
}

#endif
