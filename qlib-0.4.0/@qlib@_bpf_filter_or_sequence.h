/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef BPF_FILTER_OR_SEQUENCE_INCLUDED
#define BPF_FILTER_OR_SEQUENCE_INCLUDED

#include <list>
#include <set>
namespace @qlib@ {
    
    
    template<
        typename value_type
    >
    class bpf_filter_or_sequence :
    public bpf_filter_sequence_base {
    public:
        bpf_filter_or_sequence(
            std::auto_ptr<
                bpf_filter_sequence_base
            >& match_branch,
            std::auto_ptr<
                bpf_filter_sequence_base
            >& else_branch,
            unsigned offset
        );
        typedef value_type matchable;
        void add_matched(
            matchable matched
        );
        void add_all_match(
        );
        filter_vector filter_sequence(
        );
    private:
        std::auto_ptr<
            bpf_filter_sequence_base
        > match_branch;
        std::auto_ptr<
            bpf_filter_sequence_base
        > else_branch;
        unsigned offset;
        std::set<
            matchable
        > options;
        bool match_all;
    };
    
    template<
        typename value_type
    >
    bpf_filter_or_sequence<
        value_type
    >::bpf_filter_or_sequence(
        std::auto_ptr<
            bpf_filter_sequence_base
        >& match_branch,
        std::auto_ptr<
            bpf_filter_sequence_base
        >& else_branch,
        unsigned offset
    ) :
    match_branch(
        match_branch
    ),
    else_branch(
        else_branch
    ),
    offset(
        offset
    ),
    match_all(
        false
    ) {
    }
    
    template<
        typename value_type
    >
    void bpf_filter_or_sequence<
        value_type
    >::add_matched(
        matchable matched
    ) {
        options.insert(
            matched
        );
    }
    
    template<
        typename value_type
    >
    void bpf_filter_or_sequence<
        value_type
    >::add_all_match(
    ) {
        match_all = true;
    }
    
    template<
        typename value_type
    >
    typename bpf_filter_or_sequence<
        value_type
    >::filter_vector bpf_filter_or_sequence<
        value_type
    >::filter_sequence(
    ) {
        if (
            match_all
        ) {
            return match_branch->filter_sequence(
            );
        }
        filter_vector const match_sequence = match_branch->filter_sequence(
        );
        filter_vector const else_sequence = else_branch->filter_sequence(
        );
        struct sock_filter const loading = BPF_STMT(
            BPF_LD | bpf_traits<
                value_type
            >::size_flag | BPF_ABS,
            offset
        );
        std::list<
            struct sock_filter
        > constructing;
        uint8_t false_branch = match_sequence.size(
        );
        uint8_t true_branch = 0;
        for (
            typename std::set<
                matchable
            >::const_iterator adding = options.begin(
            );
            options.end(
            ) != adding;
            adding++
        ) {
            struct sock_filter const comparing = BPF_JUMP(
                BPF_JMP | BPF_JEQ,
                *adding,
                true_branch,
                false_branch
            );
            true_branch++;
            false_branch = 0;
            constructing.push_front(
                comparing
            );
        }
        constructing.push_front(
            loading
        );
        constructing.insert(
            constructing.end(
            ),
            match_sequence.begin(
            ),
            match_sequence.end(
            )
        );
        constructing.insert(
            constructing.end(
            ),
            else_sequence.begin(
            ),
            else_sequence.end(
            )
        );
        return filter_vector(
            constructing.begin(
            ),
            constructing.end(
            )
        );
    }
}

#endif
