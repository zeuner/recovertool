/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_soap_service.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    soap_service::soap_service(
        xml_document const& definition
    ) {
        xmlNodePtr const root = definition.get_root_node(
        );
        check_xml_node(
            root,
            BAD_CAST "definitions",
            BAD_CAST "http://schemas.xmlsoap.org/wsdl/"
        );
        populate_types(
            root
        );
        populate_messages(
            root
        );
        populate_bindings(
            root
        );
        populate_service(
            root
        );
    }
    
    void soap_service::populate_service(
        xmlNodePtr const root
    ) {
        xml_matching_child const service(
            root,
            BAD_CAST "service",
            BAD_CAST "http://schemas.xmlsoap.org/wsdl/"
        );
        xml_matching_child const port(
            service.get_data(
            ),
            BAD_CAST "port",
            BAD_CAST "http://schemas.xmlsoap.org/wsdl/"
        );
        xml_matching_child const address(
            port.get_data(
            ),
            BAD_CAST "address",
            BAD_CAST "http://schemas.xmlsoap.org/wsdl/soap/"
        );
        this->address = reinterpret_cast<
            char const*
        >(
            xml_get_attribute(
                address.get_data(
                ),
                BAD_CAST "location"
            )
        );
    }
    
    void soap_service::populate_bindings(
        xmlNodePtr const root
    ) {
        for (
            xmlNodePtr binding = root->children;
            binding;
            binding = binding->next
        ) {
            if (
                XML_ELEMENT_NODE != binding->type
            ) {
                continue;
            }
            if (
                !correct_xml_node(
                    binding,
                    BAD_CAST "binding",
                    BAD_CAST "http://schemas.xmlsoap.org/wsdl/"
                )
            ) {
                continue;
            }
            for (
                xmlNodePtr operation = binding->children;
                operation;
                operation = operation->next
            ) {
                if (
                    XML_ELEMENT_NODE != operation->type
                ) {
                    continue;
                }
                if (
                    !correct_xml_node(
                        operation,
                        BAD_CAST "operation",
                        BAD_CAST "http://schemas.xmlsoap.org/wsdl/"
                    )
                ) {
                    continue;
                }
                std::string const operation_name(
                    reinterpret_cast<
                        char const*
                    >(
                        xml_get_attribute(
                            operation,
                            BAD_CAST "name"
                        )
                    )
                );
                do {
                    xml_matching_child const input(
                        operation,
                        BAD_CAST "input",
                        BAD_CAST "http://schemas.xmlsoap.org/wsdl/"
                    );
                    std::string const name(
                        reinterpret_cast<
                            char const*
                        >(
                            xml_get_attribute(
                                input.get_data(
                                ),
                                BAD_CAST "name"
                            )
                        )
                    );
                    xml_matching_child const header(
                        input.get_data(
                        ),
                        BAD_CAST "header",
                        BAD_CAST "http://schemas.xmlsoap.org/wsdl/soap/"
                    );
                    std::string const part(
                        reinterpret_cast<
                            char const*
                        >(
                            xml_get_attribute(
                                header.get_data(
                                ),
                                BAD_CAST "part"
                            )
                        )
                    );
                    known_operations_input[
                        operation_name
                    ] = header_body(
                        part,
                        name
                    );
                } while (
                    false
                );
                do {
                    xml_matching_child const output(
                        operation,
                        BAD_CAST "output",
                        BAD_CAST "http://schemas.xmlsoap.org/wsdl/"
                    );
                    std::string const name(
                        reinterpret_cast<
                            char const*
                        >(
                            xml_get_attribute(
                                output.get_data(
                                ),
                                BAD_CAST "name"
                            )
                        )
                    );
                    xml_matching_child const header(
                        output.get_data(
                        ),
                        BAD_CAST "header",
                        BAD_CAST "http://schemas.xmlsoap.org/wsdl/soap/"
                    );
                    std::string const part(
                        reinterpret_cast<
                            char const*
                        >(
                            xml_get_attribute(
                                header.get_data(
                                ),
                                BAD_CAST "part"
                            )
                        )
                    );
                    known_operations_output[
                        operation_name
                    ] = header_body(
                        part,
                        name
                    );
                } while (
                    false
                );
            }
        }
    }
    
    void soap_service::populate_messages(
        xmlNodePtr const root
    ) {
        for (
            xmlNodePtr message = root->children;
            message;
            message = message->next
        ) {
            if (
                XML_ELEMENT_NODE != message->type
            ) {
                continue;
            }
            if (
                !correct_xml_node(
                    message,
                    BAD_CAST "message",
                    BAD_CAST "http://schemas.xmlsoap.org/wsdl/"
                )
            ) {
                continue;
            }
            std::string const name(
                reinterpret_cast<
                    char const*
                >(   
                    xml_get_attribute(
                        message,
                        BAD_CAST "name"
                    )
                )
            );
            xml_matching_child const part(
                message,
                BAD_CAST "part",
                BAD_CAST "http://schemas.xmlsoap.org/wsdl/"
            );
            std::string const part_name(
                reinterpret_cast<
                    char const*
                >(   
                    xml_get_attribute(
                        part.get_data(
                        ),
                        BAD_CAST "name"
                    )
                )
            );
            std::string element_name;
            std::string element_namespace;
            lookup_namespaced(
                part.get_data(
                ),
                BAD_CAST "element",
                element_name,
                element_namespace
            );
            known_messages[
                name
            ][
                element_identification(
                    element_name,
                    element_namespace
                )
            ] = part_name;
        }
    }
    
    void soap_service::populate_types(
        xmlNodePtr const root
    ) {
        xml_matching_child const types(
            root,
            BAD_CAST "types",
            BAD_CAST "http://schemas.xmlsoap.org/wsdl/"
        );
        std::list<
            std::pair<
                std::string,
                std::string
            >
        > imports;
        std::map<
            type_identification,
            xmlNodePtr
        > type_definitions;
        std::map<
            element_identification,
            xmlNodePtr
        > element_definitions;
        for (
            xmlNodePtr schema = types.get_data(
            )->children;
            schema;
            schema = schema->next
        ) {
            if (
                XML_ELEMENT_NODE != schema->type
            ) {
                continue;
            }
            check_xml_node(
                schema,
                BAD_CAST "schema",
                BAD_CAST "http://www.w3.org/2001/XMLSchema"
            );
            std::string const target_namespace(
                reinterpret_cast<
                    char const*
                >(
                    xml_get_attribute(
                        schema,
                        BAD_CAST "targetNamespace"
                    )
                )
            );
            for (
                xmlNodePtr type = schema->children;
                type;
                type = type->next
            ) {
                if (
                    XML_ELEMENT_NODE != type->type
                ) {
                    continue;
                }
                if (
                    correct_xml_node(
                        type,
                        BAD_CAST "import",
                        BAD_CAST "http://www.w3.org/2001/XMLSchema"
                    )
                ) {
                    imports.push_back(
                        std::make_pair(
                            target_namespace,
                            reinterpret_cast<
                                char const*
                            >(
                                xml_get_attribute(
                                    type,
                                    BAD_CAST "namespace"
                                )
                            )
                        )
                    );
                    continue;
                }
                if (
                    correct_xml_node(
                        type,
                        BAD_CAST "element",
                        BAD_CAST "http://www.w3.org/2001/XMLSchema"
                    )
                ) {
                    element_definitions[
                        element_identification(
                            reinterpret_cast<
                                char const*
                            >(
                                xml_get_attribute(
                                    type,
                                    BAD_CAST "name"
                                )
                            ),
                            target_namespace
                        )
                    ] = type;
                    continue;
                }
                if (
                    correct_xml_node(
                        type,
                        BAD_CAST "annotation",
                        BAD_CAST "http://www.w3.org/2001/XMLSchema"
                    )
                ) {
                    continue;
                }
                if (
                    !(
                        correct_xml_node(
                            type,
                            BAD_CAST "complexType",
                            BAD_CAST "http://www.w3.org/2001/XMLSchema"
                        ) || correct_xml_node(
                            type,
                            BAD_CAST "simpleType",
                            BAD_CAST "http://www.w3.org/2001/XMLSchema"
                        )
                    )
                ) { 
                    throw component_error(
                        "soap_service"
                    );
                }
                std::string const type_name(
                    reinterpret_cast<
                        char const*
                    >(
                        xml_get_attribute(
                            type,
                            BAD_CAST "name"
                        )
                    )
                );
                known_types[
                    type_identification(
                        type_name,
                        target_namespace
                    )
                ] = type_descriptor(
                );
                type_definitions[
                    type_identification(
                        type_name,
                        target_namespace
                    )
                ] = type;
            }
        }
        std::list<
            std::pair<
                type_identification,
                xmlNodePtr
            >
        > ordered;
        for (
            std::map<
                type_identification,
                xmlNodePtr
            >::iterator ordering = type_definitions.begin(
            );
            type_definitions.end(
            ) != ordering;
            type_definitions.erase(
                ordering++
            )
        ) {
            std::map<
                type_identification,
                xmlNodePtr
            >::iterator lowest = ordering;
            type_identification dependency;
            std::list<
                std::map<
                    type_identification,
                    xmlNodePtr
                >::iterator
            > dependencies;
            while (
                type_dependency(
                    dependency,
                    *lowest
                )
            ) {
                std::map<
                    type_identification,
                    xmlNodePtr
                >::iterator const found = type_definitions.find(
                    dependency
                );
                if (
                    type_definitions.end(
                    ) == found
                ) {
                    break;
                }
                if (
                    found == ordering
                ) {   
                    throw component_error(
                        "soap_service"
                    );
                }
                lowest = found;
                dependencies.push_front(
                    found
                );
            }
            for (
                std::list<
                    std::map<
                        type_identification,
                        xmlNodePtr
                    >::iterator
                >::const_iterator applying = dependencies.begin(
                );
                dependencies.end(
                ) != applying;
                applying++
            ) {
                ordered.push_back(
                    **applying
                );
                type_definitions.erase(
                    *applying
                );
            }
            ordered.push_back(
                *ordering
            );
        }
        for (
            std::list<
                std::pair<
                    type_identification,
                    xmlNodePtr
                >
            >::const_iterator defining = ordered.begin(
            );
            ordered.end(
            ) != defining;
            defining++
        ) {
            if (
                correct_xml_node(
                    defining->second,
                    BAD_CAST "simpleType",
                    BAD_CAST "http://www.w3.org/2001/XMLSchema"
                )
            ) {
                xml_matching_child const restriction(
                    defining->second,
                    BAD_CAST "restriction",
                    BAD_CAST "http://www.w3.org/2001/XMLSchema"
                );
                known_types[
                    defining->first
                ] = lookup_type(
                    restriction.get_data(
                    ),
                    BAD_CAST "base"
                );
                continue;
            }
            if (
                correct_xml_node(
                    defining->second,
                    BAD_CAST "complexType",
                    BAD_CAST "http://www.w3.org/2001/XMLSchema"
                )
            ) {
                for (
                    xmlNodePtr sequence = defining->second->children;
                    sequence;
                    sequence = sequence->next
                ) {
                    if (
                        XML_ELEMENT_NODE != sequence->type
                    ) {
                        continue;
                    }
                    if (
                        correct_xml_node(
                            sequence,
                            BAD_CAST "annotation",
                            BAD_CAST "http://www.w3.org/2001/XMLSchema"
                        )
                    ) {
                        continue;
                    }
                    if (
                        correct_xml_node(
                            sequence,
                            BAD_CAST "complexContent",
                            BAD_CAST "http://www.w3.org/2001/XMLSchema"
                        )
                    ) {
                        known_types[
                            defining->first
                        ] = complex_content_value(
                            defining->first.second,
                            sequence
                        );
                        continue;
                    }
                    check_xml_node(
                        sequence,
                        BAD_CAST "sequence",
                        BAD_CAST "http://www.w3.org/2001/XMLSchema"
                    );
                    known_types[
                        defining->first
                    ] = sequence_value(
                        defining->first.second,
                        sequence
                    );
                }
                continue;
            }
        }
        for (
            std::map<
                element_identification,
                xmlNodePtr
            >::const_iterator defining = element_definitions.begin(
            );
            element_definitions.end(
            ) != defining;
            defining++
        ) {
            type_descriptor* current_type = NULL;
            for (
                xmlNodePtr complex_type = defining->second->children;
                complex_type;
                complex_type = complex_type->next
            ) {
                if (
                    XML_ELEMENT_NODE != complex_type->type
                ) {
                    continue;
                }
                if (   
                    correct_xml_node( 
                        complex_type,
                        BAD_CAST "annotation",
                        BAD_CAST "http://www.w3.org/2001/XMLSchema"
                    )
                ) {
                    continue;
                }
                if (   
                    correct_xml_node( 
                        complex_type,
                        BAD_CAST "sequence",
                        BAD_CAST "http://www.w3.org/2001/XMLSchema"
                    )
                ) {
                    boost::shared_ptr<
                        type_descriptor
                    > const anonymous(
                        new type_descriptor(
                            sequence_value(
                                defining->first.second,
                                complex_type
                            )
                        )
                    );
                    current_type = anonymous.get(
                    );
                    anonymous_types.push_back(
                        anonymous
                    );
                    continue;
                }
                check_xml_node(
                    complex_type,
                    BAD_CAST "complexType",
                    BAD_CAST "http://www.w3.org/2001/XMLSchema"
                );
                for (
                    xmlNodePtr sequence = complex_type->children;
                    sequence;
                    sequence = sequence->next
                ) {
                    if (
                        XML_ELEMENT_NODE != sequence->type
                    ) {
                        continue;
                    }
                    check_xml_node(
                        sequence,
                        BAD_CAST "sequence",
                        BAD_CAST "http://www.w3.org/2001/XMLSchema"
                    );
                    boost::shared_ptr<
                        type_descriptor
                    > const anonymous(
                        new type_descriptor(
                            sequence_value(
                                defining->first.second,
                                sequence
                            )
                        )
                    );
                    current_type = anonymous.get(
                    );
                    anonymous_types.push_back(
                        anonymous
                    );
                }
            }
            if (
                NULL == current_type
            ) {
                current_type = &(
                    lookup_type(
                        defining->second
                    )
                );
            }
            element_types[
                defining->first
            ] = current_type;
        }
        for (
            std::list<
                std::pair<
                    std::string,
                    std::string
                >
            >::const_iterator applied = imports.begin(
            );
            imports.end(
            ) != applied;
            applied++
        ) {
            for (
                std::map<
                    type_identification,
                    type_descriptor
                >::const_iterator pulled = known_types.begin(
                );
                known_types.end(
                ) != pulled;
                pulled++
            ) {
                if (
                    pulled->first.second == applied->second
                ) {
                    known_types[
                        type_identification(
                            pulled->first.first,
                            applied->first
                        )
                    ] = pulled->second;
                }
            }
        }
    }
    
    soap_service::type_descriptor const& soap_service::get_type(
        std::string const& name,
        std::string const& name_space
    ) const {
        return known_types.at(
            type_identification(
                name,
                name_space
            )
        );
    }
    
    soap_service::type_descriptor const& soap_service::get_element_type(
        std::string const& name,
        std::string const& name_space
    ) const {
        return *element_types.at(
            element_identification(
                name,
                name_space
            )
        );
    }
    
    bool soap_service::type_dependency(
        type_identification& dependency,
        std::pair<
            type_identification,
            xmlNodePtr
        > const& dependent
    ) {
        xmlNodePtr complex_content = dependent.second->children;
        while (
            true
        ) {
            if (
                !complex_content
            ) {
                return false;
            }
            if (
                XML_ELEMENT_NODE != complex_content->type
            ) {
                complex_content = complex_content->next;
                continue;
            }
            if (
                correct_xml_node(
                    complex_content,
                    BAD_CAST "complexContent",
                    BAD_CAST "http://www.w3.org/2001/XMLSchema"
                )
            ) {
                break;
            }
            complex_content = complex_content->next;
        }
        xml_matching_child const extension(
            complex_content,
            BAD_CAST "extension",
            BAD_CAST "http://www.w3.org/2001/XMLSchema"
        );
        std::string local;
        std::string ns;
        lookup_namespaced(
            extension.get_data(
            ),
            BAD_CAST "base",
            local,
            ns
        );
        dependency = type_identification(
            local,
            ns
        );
        return true;
    }
    
    boost::shared_ptr<
        soap_element
    > soap_service::get_header_element(
        std::string const& request,
        std::map<        
            std::string,
            header_body
        > const& direction
    ) const {
        return boost::shared_ptr<
            soap_element
        >(
            new soap_element(
                *this,
                known_messages.at(
                    direction.at(
                        request
                    ).first
                ).begin(
                )->first.first,
                known_messages.at(
                    direction.at(
                        request
                    ).first
                ).begin(
                )->first.second
            )
        );
    }
    
    boost::shared_ptr<
        soap_element
    > soap_service::get_body_element(
        std::string const& request,
        std::map<
            std::string,
            header_body
        > const& direction
    ) const {
        return boost::shared_ptr<
            soap_element
        >(
            new soap_element(
                *this,
                known_messages.at(
                    direction.at(
                        request
                    ).second
                ).begin(
                )->first.first,
                known_messages.at(
                    direction.at(
                        request
                    ).second
                ).begin(
                )->first.second
            )
        );
    }
    
    boost::shared_ptr<
        soap_element
    > soap_service::get_request_header(
        std::string const& request
    ) const {
        return get_header_element(
            request,
            known_operations_input
        );
    }
    
    boost::shared_ptr<
        soap_element
    > soap_service::get_request_body(
        std::string const& request
    ) const {
        return get_body_element(
            request,
            known_operations_input
        );
    }
    
    boost::shared_ptr<
        soap_element
    > soap_service::get_response_header(
        std::string const& request
    ) const {
        return get_header_element(
            request,
            known_operations_output
        );
    }
    
    boost::shared_ptr<
        soap_element
    > soap_service::get_response_body(
        std::string const& request
    ) const {
        return get_body_element(
            request,
            known_operations_output
        );
    }
    
    std::string const& soap_service::get_address(
    ) const {
        return address;
    }
    
    soap_service::type_descriptor::type_descriptor(
    ) {
    }
    
    std::list<
        soap_service::element_type
    > soap_service::sequence_value(
        std::string const& target_namespace,
        xmlNodePtr sequence,
        std::list<
            soap_service::element_type
        > result
    ) {
        for (
            xmlNodePtr element = sequence->children;
            element;
            element = element->next
        ) {
            if (
                XML_ELEMENT_NODE != element->type
            ) {
                continue;
            }
            check_xml_node(
                element,
                BAD_CAST "element",
                BAD_CAST "http://www.w3.org/2001/XMLSchema"
            );
            type_descriptor* const current_type = &(
                lookup_type(
                    element
                )
            );
            bounds const occurrences(
                atoi(
                    reinterpret_cast<
                        char const*
                    >(       
                        xml_get_attribute(
                            element, 
                            BAD_CAST "minOccurs"
                        )
                    )
                ),
                atoi(
                    reinterpret_cast<
                        char const*
                    >(
                        xml_get_attribute(
                            element,
                            BAD_CAST "maxOccurs"
                        )
                    )   
                )
            );
            if (
                occurrences.second < occurrences.first
            ) {
                throw component_error(
                    "soap_service"
                );
            }
            element_type const added(
                element_identification(
                    reinterpret_cast<
                        char const*
                    >(
                        xml_get_attribute(
                            element,
                            BAD_CAST "name"
                        )
                    ),
                    target_namespace
                ),
                value_list(
                    occurrences,
                    current_type
                )
            );
            result.push_back(
                added
            );
        }
        return result;
    }
    
    std::list<
        soap_service::element_type
    > soap_service::complex_content_value(
        std::string const& target_namespace,
        xmlNodePtr complex_content
    ) {
        xml_matching_child const extension(
            complex_content,
            BAD_CAST "extension",
            BAD_CAST "http://www.w3.org/2001/XMLSchema"
        );
        xml_matching_child const sequence(
            extension.get_data(
            ),
            BAD_CAST "sequence",
            BAD_CAST "http://www.w3.org/2001/XMLSchema"
        );
        std::string local;
        std::string ns;
        lookup_namespaced(
            extension.get_data(
            ),  
            BAD_CAST "base",
            local, 
            ns
        );
        std::list<
            element_type
        > base = boost::get<
            std::list<
                element_type
            >
        >(
            known_types.at(
                type_identification(
                    local,
                    ns
                )
            )
        );
        return sequence_value(
            target_namespace,
            sequence.get_data(
            ),
            base
        );
    }
    
    soap_service::type_descriptor& soap_service::lookup_type(
        xmlNodePtr element,
        xml_string const& attribute
    ) {
        std::string name;
        std::string name_space;
        lookup_namespaced(
            element,
            attribute,
            name,
            name_space
        );
        if (
            "http://www.w3.org/2001/XMLSchema" == name_space
        ) {
            return basic_type(
                name
            );
        } else {
            return known_types.at(
                type_identification(
                    name,
                    name_space
                )
            );
        }
    }
    
    soap_service::type_descriptor& soap_service::basic_type(
        std::string const& name
    ) {
        std::map<
            std::string,
            type_descriptor
        >::iterator found = basic_types.find(
            name
        );
        if (
            basic_types.end(
            ) == found
        ) {
            basic_types[
                name
            ] = type_descriptor(
                name
            );
            return basic_types[
                name
            ];
        } else {
            return found->second;
        }
    }
    
    void soap_service::lookup_namespaced(
        xmlNodePtr element,
        xml_string const& attribute,
        std::string& name,
        std::string& name_space
    ) {
        std::string const type(
            reinterpret_cast<
                char const*
            >(
                xml_get_attribute(
                    element,
                    attribute
                )
            )
        );
        std::string::size_type const at_delimiter = type.find(
            ':'
        );
        if (
            std::string::npos == at_delimiter
        ) {
            throw component_error(
                "soap_service"
            );
        }
        xmlNsPtr const scope = xmlSearchNs(
            element->doc,
            element,
            BAD_CAST std::string(
                type.begin(
                ),
                type.begin(
                ) + at_delimiter
            ).c_str(
            )
        );
        if (
            NULL == scope
        ) {
            throw component_error(
                "soap_service"
            );
        }
        std::string const local_type(
            type.begin(
            ) + at_delimiter + 1,
            type.end(
            )
        );
        name_space = reinterpret_cast<
            char const*
        >(
            scope->href
        );
        name = local_type;
    }
    
    std::map<
        std::string,
        soap_service::type_descriptor
    > soap_service::basic_types;
}
