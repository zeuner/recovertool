/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_batch_processing_thread.h"
#include <boost/asio/placeholders.hpp>
#include <boost/bind.hpp>
namespace @qlib@ {
    
    
    batch_processing_thread::batch_processing_thread(
    ) :
    pulse(
        timing
    ) {
        async_run_action(
        );
        running = new boost::thread(
            boost::bind(
                &batch_processing_thread::run_processing,
                this
            )
        );
    }
    
    batch_processing_thread::~batch_processing_thread(
    ) {
        boost::unique_lock<
            boost::mutex
        > lock(
            modifying_actions
        );
        pulse.cancel(
        );
        running->join(
        );
        delete running;
    }
    
    void batch_processing_thread::enqueue_action(
        boost::function<
            void(
            )
        > const& enqueued
    ) {
        boost::unique_lock<
            boost::mutex
        > lock(
            modifying_actions
        );
        actions.push_back(
            enqueued
        );
    }
    
    void batch_processing_thread::async_run_action(
    ) {
        pulse.expires_from_now(
            boost::posix_time::seconds(
                1
            )
        );
        pulse.async_wait(
            boost::bind(
                &batch_processing_thread::run_action,
                this,
                boost::asio::placeholders::error
            )
        );
    }
    
    void batch_processing_thread::run_action(
        boost::system::error_code const& error
    ) {
        if (
            error
        ) {
            return;
        } 
        boost::function<
            void(
            )
        > processed;
        do {
            boost::unique_lock<
                boost::mutex
            > lock(
                modifying_actions
            );
            if (
                actions.empty(
                )
            ) {
                async_run_action(
                );
                return;
            }
            processed = actions.front(
            );
            actions.pop_front(
            );
        } while (
            false
        );
        processed(
        );
        timing.post(
            boost::bind(
                &batch_processing_thread::run_action,
                this,
                error
            )
        );
    }
    
    void batch_processing_thread::run_processing(
    ) {
        timing.run(
        );
    }
}
