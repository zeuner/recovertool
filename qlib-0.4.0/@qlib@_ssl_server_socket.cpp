/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_ssl_server_socket.h"
#include <boost/bind.hpp>
#include <boost/asio/placeholders.hpp>
namespace @qlib@ {
    
    
    ssl_server_socket::ssl_server_socket(
        boost::asio::io_service& communicating
    ) :
    boost::asio::ssl::stream<
        boost::asio::ip::tcp::socket
    >(
        communicating,
        *context
    ) {
    }
    
    void ssl_server_socket::async_start(
        boost::function<
            void(
                boost::system::error_code const& error
            )
        > start_handler
    ) {
        this->start_handler = start_handler;
        async_handshake(
            boost::asio::ssl::stream_base::server,
            boost::bind(
                &ssl_server_socket::handle_handshake,
                this,
                boost::asio::placeholders::error
            )
        );
    }
    
    void ssl_server_socket::set_context(
        boost::asio::ssl::context* context
    ) {
        ssl_server_socket::context = context;
    }
    
    void ssl_server_socket::handle_handshake(
        boost::system::error_code const& error
    ) {
        start_handler(
            error
        );
    }
    
    boost::asio::ssl::context* ssl_server_socket::context = NULL;
}
