/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_move_file.h"
#include "@qlib@_system_log.h"
#include <cstdio>
#include <cstring>
#include "@qlib@_component_error.h"
#include <cerrno>
#include "@qlib@_throw_exception.h"
namespace @qlib@ {
    
    
    void move_file(
        std::string const& old_path,
        std::string const& new_path
    ) {
        if (
            0 > rename(
                old_path.c_str(
                ),
                new_path.c_str(
                )
            )
        ) {
            system_log.log_error(
                "move_file failed: " + std::string(
                    strerror(
                        errno
                    )
                )
            );
            throw_exception(
                component_error(
                    "move_file"
                )
            );
        }
    }
}
