/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_http_stub_peer_handlers.h"
namespace @qlib@ {
    
    
    http_stub_peer_handlers::http_stub_peer_handlers(
    ) {
    }
    
    bool http_stub_peer_handlers::invoke_accept_handler(
        boost::system::error_code const& error
    ) {
        if (
            error
        ) {
            this->error = error;
        }
        return true;
    }
    
    void http_stub_peer_handlers::invoke_response_handler(
        boost::system::error_code const& error
    ) {
        if (
            error
        ) {
            this->error = error;
        }
    }
    
    void http_stub_peer_handlers::close_link(
    ) {
    }
    
    bool http_stub_peer_handlers::invoke_deferred_response_handler(
    ) {
        return false;
    }
    
    boost::system::error_code const& http_stub_peer_handlers::get_error(
    ) const {
        return error;
    }
}
