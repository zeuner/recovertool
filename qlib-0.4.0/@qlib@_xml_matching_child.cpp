/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_xml_matching_child.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    xml_matching_child::xml_matching_child(
        xmlNodePtr parent,
        xml_string const& local,
        xml_string const& ns
    ) :
    data(
        parent->children
    ) {
        while (
            true
        ) {
            if (
                !data
            ) {
                throw component_error(
                    "xml_matching_child"
                );
            }
            if (
                XML_ELEMENT_NODE != data->type
            ) {
                data = data->next;
                continue;
            }
            if (
                correct_xml_node(
                    data,
                    local,
                    ns
                )
            ) {
                break;
            }
            data = data->next;
        }
    }
    
    xmlNodePtr xml_matching_child::get_data(
    ) const {
        return data;
    }
}
