/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef RESOLVER_SESSION_INCLUDED
#define RESOLVER_SESSION_INCLUDED

#include <boost/fusion/container/vector/vector_fwd.hpp>
#include <unistd.h>
#include <map>
#include <boost/fusion/include/vector_fwd.hpp>
#include <boost/fusion/include/vector.hpp>
#include <list>
#include <string>
#include <boost/function.hpp>
#include <boost/fusion/container/vector.hpp>
namespace @qlib@ {
    
    
    class resolver_session {
    public:
        static inline boost::arg<
            2
        > raw_result_argument(
        );
        static inline boost::arg<
            3
        > records_result_argument(
        );
        enum {
            resource_record_name,
            resource_record_type,
            resource_record_class,
            resource_record_ttl,
            resource_record_data,
        };
        typedef boost::fusion::vector<
            std::string,
            unsigned,
            unsigned,
            unsigned,
            std::string
        > resource_record;
        resolver_session(
            boost::asio::io_service& communicating,
            std::list<
                boost::asio::ip::address
            > const& dns_servers,
            boost::asio::ip::udp::endpoint const& local_endpoint_v4 = boost::asio::ip::udp::endpoint(
                boost::asio::ip::udp::v4(
                ),
                0
            ),
            boost::asio::ip::udp::endpoint const& local_endpoint_v6 = boost::asio::ip::udp::endpoint(
                boost::asio::ip::udp::v6(
                ),      
                0
            )
        );
        typedef enum {
            a = 1,
            ns = 2,
            cname = 5,
            soa = 6,
            ptr = 12,
            mx = 15,
            txt = 16,
            aaaa = 28,
        } record_type;
        typedef enum {
            question,
            answer,
            authority,
            additional,
            reply_sections_end,
            reply_sections_begin = 0,
        } reply_section;
        static void skip_question_record(
            std::string::const_iterator& decoding,
            std::string::const_iterator begin,
            std::string::const_iterator end
        );
        static void decode_record(
            resource_record& decoded,
            std::string::const_iterator& decoding,
            std::string::const_iterator begin,
            std::string::const_iterator end
        );
    private:
        typedef boost::function<
            void(
                boost::system::error_code const& error,
                std::string const& raw_result,
                std::list<
                    resource_record
                > const& records
            )
        > handling_result;
    public:
        void async_resolve(
            record_type requested,
            char const* domain_name,
            handling_result const& result_handler,
            reply_section requested_section = answer
        );
        void async_resolve(
            record_type requested,
            std::string const& domain_name,
            handling_result const& result_handler,
            reply_section requested_section = answer
        );
        void close(
        );
        bool get_closed(
        ) const;
        void set_no_ttl(
        );
        boost::asio::io_service& get_io_service(
        );
    protected:
        static std::string name_labels(
            std::string const& name
        );
        static std::string decode_labels(
            std::string::const_iterator& label_start,
            std::string::const_iterator const& container_end,
            std::string::const_iterator const& message_start,
            std::string::const_iterator const& message_end
        );
        void handle_receive_v4(
            size_t received,
            boost::system::error_code const& error
        );
        void handle_receive_v6(
            size_t received,
            boost::system::error_code const& error
        );
    private:
        typedef boost::fusion::vector<
            std::string,
            char const*,
            record_type,
            handling_result,
            bool,
            reply_section
        > processing_record;
        typedef std::map<
            std::string,
            processing_record
        > result_handler_map;
    protected:
        void handle_message_v4(
            result_handler_map::iterator handling,
            std::string const& message,
            bool* message_truncated = NULL,
            boost::system::error_code const& error = boost::system::error_code(
            )
        );
        void handle_message_v6(
            result_handler_map::iterator handling,
            std::string const& message,
            bool* message_truncated = NULL,
            boost::system::error_code const& error = boost::system::error_code(
            )
        );
        void handle_send_v4(
            std::string const& string_id,
            boost::system::error_code const& error
        );
        void handle_send_v6(
            std::string const& string_id,
            boost::system::error_code const& error
        );
        boost::asio::ip::address const& get_dns_server(
        );
        void start_resending(
        );
        void check_resend(
            boost::system::error_code const& error
        );
    private:
        boost::asio::io_service& communicating;
        std::auto_ptr<
            boost::asio::ip::udp::socket
        > socket_v4;
        std::auto_ptr<
            boost::asio::ip::udp::socket
        > socket_v6;
        boost::asio::ip::udp::endpoint local_endpoint_v4;
        boost::asio::ip::udp::endpoint local_endpoint_v6;
        boost::asio::ip::udp::endpoint sender_endpoint_v4;
        boost::asio::ip::udp::endpoint sender_endpoint_v6;
        integer_random_source id_generating;
        enum {
            query_string,
            queried_domain,
            queried_record_type,
            result_handler_function,
            resent,
            queried_reply_section,
        };
        result_handler_map result_handlers_v4;
        result_handler_map result_handlers_v6;
        enum {
            fetch_time,
            expiry,
            raw_result_string,
            result_records,
        };
        typedef boost::fusion::vector<
            time_t,
            time_t,
            std::string,
            std::list<
                resource_record
            >
        > cached_record;
        typedef boost::fusion::vector<
            char const*,
            record_type,
            reply_section
        > cache_selector;
        std::map<
            cache_selector,
            cached_record
        > cached_results;
        enum {
            buffer_size = 0x200
        };
        char buffer_data_v4[
            buffer_size
        ];
        char buffer_data_v6[
            buffer_size
        ];
        std::list<
            boost::asio::ip::address
        > dns_servers;
        std::list<
            boost::asio::ip::address
        >::const_iterator current_dns_server;
        bool no_ttl;
        bool waiting_v4;
        bool waiting_v6;
        bool resending_v4;
        result_handler_map::iterator checked_v4_request;
        result_handler_map::iterator checked_v6_request;
        std::auto_ptr<
            boost::asio::deadline_timer
        > resend_pulse;
        bool resending;
        bool closed;
        class result_binder {
        public:
            result_binder(
                handling_result const& result_handler,
                std::string const& raw_result,
                std::list<
                    resource_record
                > const& records
            );
            void operator(
            )(
            );
        private:
            handling_result result_handler;
            std::string raw_result;
            std::list<
                resource_record
            > records;
        };
    };
    
    boost::arg<
        2
    > resolver_session::raw_result_argument(
    ) {
        return boost::arg<
            2
        >(
        );
    }
    
    boost::arg<
        3
    > resolver_session::records_result_argument(
    ) {
        return boost::arg<
            3
        >(
        );
    }
}

#endif
