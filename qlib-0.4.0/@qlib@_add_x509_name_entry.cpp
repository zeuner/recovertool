/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_add_x509_name_entry.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    void add_x509_name_entry(
        new_x509_name const& augmented,
        std::string const& key,
        std::string const& value
    ) {
        if (
            !X509_NAME_add_entry_by_txt(
                augmented.get_data(
                ),
                key.c_str(
                ),
                MBSTRING_ASC,
                reinterpret_cast<
                    unsigned char const*
                >(
                    value.data(
                    )
                ),
                value.size(
                ),
                -1,
                0
            )
        ) {
            ERR_print_errors_fp(
                stdout
            );
            throw component_error(
                "add_x509_name_entry"
            );
        }
    }
}
