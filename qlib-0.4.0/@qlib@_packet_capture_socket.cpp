/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_packet_capture_socket.h"
namespace @qlib@ {
    
    
    packet_capture_socket::packet_capture_socket(
        boost::asio::io_service& communicating,
        std::string const& interface,
        bool promiscuous,
        size_t snapshot_length,
        size_t buffer_size,
        int timeout_milliseconds
    ) :
    packet_capture_wrapper(
        interface,
        promiscuous,
        snapshot_length,
        buffer_size,
        timeout_milliseconds
    ),
    boost::asio::basic_raw_socket<
        packet_raw_protocol
    >(
        communicating,
        packet_raw_protocol(
        ),
        get_file_descriptor(
        )
    ) {
    }
}
