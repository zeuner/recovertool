/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef SOCKET_DATA_SINK_INCLUDED
#define SOCKET_DATA_SINK_INCLUDED

#include <string>
#include <boost/asio/placeholders.hpp>
#include <boost/asio/write.hpp>
#include <boost/function.hpp>
#include <boost/bind.hpp>
namespace @qlib@ {
    
    
    template<
        typename connected
    >
    class socket_data_sink {
    public:
        template<
            typename initializing
        >
        socket_data_sink(
            initializing& initializer,
            boost::function<
                void(
                )
            > const& shutdown_handler
        );
        void async_write(
            std::string const& written
        );
        bool get_writing(
        ) const;
        boost::asio::io_service& get_io_service(
        );
    protected:
        void handle_write(
            boost::system::error_code const& error
        );
    private:
        connected socket;
        std::string output;
        std::string next_output;
        bool writing;
        boost::function<
            void(
            )
        > shutdown_handler;
    };
    
    template<
        typename connected
    >
    template<
        typename initializing
    >
    socket_data_sink<
        connected
    >::socket_data_sink(
        initializing& initializer,
        boost::function<
            void(
            )
        > const& shutdown_handler
    ) :
    socket(
        initializer
    ),
    writing(
        false
    ),
    shutdown_handler(
        shutdown_handler
    ) {
    }
    
    template<
        typename connected
    >
    void socket_data_sink<
        connected
    >::async_write(
        std::string const& written
    ) {
        if (
            writing
        ) {
            next_output += written;
        } else {
            writing = true;
            output = written;
            boost::asio::async_write(
                socket,
                boost::asio::buffer(
                    output.data(
                    ),
                    output.size(
                    )
                ),
                boost::bind(
                    &socket_data_sink::handle_write,
                    this,
                    boost::asio::placeholders::error
                )
            );
        }
    }
    
    template<
        typename connected
    >
    bool  socket_data_sink<
        connected
    >::get_writing(
    ) const {
        return writing;
    }
    
    template<
        typename connected
    >
    boost::asio::io_service& socket_data_sink<
        connected
    >::get_io_service(
    ) {
        return socket.get_io_service(
        );
    }
    
    template<
        typename connected
    >
    void socket_data_sink<
        connected
    >::handle_write(
        boost::system::error_code const& error
    ) {
        if (
            error
        ) {
            shutdown_handler(
            );
            return;
        }
        writing = false;
        if (
            next_output.empty(
            )
        ) {
            return;
        }
        async_write(
            next_output
        );
        next_output.clear(
        );
    }
}

#endif
