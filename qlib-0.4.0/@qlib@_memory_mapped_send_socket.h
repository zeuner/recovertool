/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef MEMORY_MAPPED_SEND_SOCKET_INCLUDED
#define MEMORY_MAPPED_SEND_SOCKET_INCLUDED

#include <string>
namespace @qlib@ {
    
    
    class memory_mapped_send_socket {
    public:
        memory_mapped_send_socket(
            boost::asio::io_service& communicating,
            std::string const& interface,
            unsigned block_size,
            unsigned block_count,
            unsigned frame_size,
            unsigned frame_count
        );
        ~memory_mapped_send_socket(
        );
        bool get_packet_buffer(
            unsigned long*& status,
            unsigned*& size,
            unsigned char*& buffer
        );
        unsigned get_frame_size(
        ) const;
        void send_pending_packets(
        );
        boost::asio::io_service& get_io_service(
        );
    private:
        struct tpacket_req buffer_request;
        struct sockaddr_ll next_hop;
        unsigned char* mapped_bytes;
        unsigned char* last_frame;
        unsigned char* frames_end;
        boost::asio::basic_raw_socket<
            packet_raw_protocol
        >* handle;
    };
}

#endif
