/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef PERSISTENT_OBJECT_INCLUDED
#define PERSISTENT_OBJECT_INCLUDED

#include <string>
#include "@qlib@_move_file.h"
#include "@qlib@_component_error.h"
#include "@qlib@_serialized.h"
#include "@qlib@_null_preparation.h"
#include <boost/noncopyable.hpp>
#include <iostream>
#include "@qlib@_file_contents.h"
#include <fstream>
#include "@qlib@_throw_exception.h"
#include "@qlib@_file_exists.h"
#include "@qlib@_deserialize.h"
namespace @qlib@ {
    
    template<
        typename stored,
        typename file_preparing = null_preparation
    >
    class persistent_object :
    public file_preparing,
    public stored,
    private boost::noncopyable {
    public:
        persistent_object(
            std::string const& filename
        );
        template<
            typename initializing
        >
        persistent_object(
            std::string const& filename,
            initializing initializer
        );
        template<
            typename initializing1,
            typename initializing2
        >
        persistent_object(
            std::string const& filename,
            initializing1 initializer1,
            initializing2 initializer2
        );
        ~persistent_object(
        );
        void store(
        );
    protected:
        void restore(
        );
    private:
        std::string const filename;
    };
    
    template<
        typename stored,
        typename file_preparing
    >
    persistent_object<
        stored,
        file_preparing
    >::persistent_object(
        std::string const& filename
    ) :
    file_preparing(
        filename
    ),
    filename(
        filename
    ) {
        if (
            !file_exists(
                filename
            )
        ) {
            return;
        }
        restore(
        );
    }
    
    template<
        typename stored,
        typename file_preparing
    >
    template<
        typename initializing
    >
    persistent_object<
        stored,
        file_preparing
    >::persistent_object(
        std::string const& filename,
        initializing initializer
    ) :
    file_preparing(
        filename
    ),
    stored(
        initializer
    ),
    filename(
        filename
    ) {
        if (
            !file_exists(
                filename
            )
        ) {
            return;
        }
        restore(
        );
    }
    
    template<
        typename stored,
        typename file_preparing
    >
    template<
        typename initializing1,
        typename initializing2
    >
    persistent_object<
        stored,
        file_preparing
    >::persistent_object(
        std::string const& filename,
        initializing1 initializer1,
        initializing2 initializer2
    ) :
    file_preparing(
        filename
    ),
    stored(
        initializer1,
        initializer2
    ),
    filename(
        filename
    ) {
        if (
            !file_exists(
                filename
            )
        ) {
            return;
        }
        restore(
        );
    }
    
    template<
        typename stored,
        typename file_preparing
    >
    persistent_object<
        stored,
        file_preparing
    >::~persistent_object(
    ) {
    #ifdef QLIB_NO_EXCEPTIONS
        store(
        );
    #else
        try {
            store(
            );
        } catch (
            std::exception& caught
        ) {
        }
    #endif
    }
    
    template<
        typename stored,
        typename file_preparing
    >
    void persistent_object<
        stored,
        file_preparing
    >::store(
    ) {
        std::string const data = serialized(
            *static_cast<
                stored*
            >(
                this
            )
        );
        do {
            std::ofstream writing(
                (
                    filename + ".new"
                ).c_str(
                ),
                std::ios::binary
            );
            writing.exceptions(
                std::ios::badbit | std::ios::eofbit | std::ios::failbit
            );
            writing << data;
        } while (
            false
        );
        move_file(
            filename + ".new",
            filename
        );
    }
    
    template<
        typename stored,
        typename file_preparing
    >
    void persistent_object<
        stored,
        file_preparing
    >::restore(
    ) {
        std::string const data = file_contents(
            filename
        );
        std::string::const_iterator from = data.begin(
        );
        std::string::const_iterator const to = data.end(
        );
        deserialize(
            from,
            to,
            *static_cast<
                stored*
            >(
                this
            )
        );
        if (
            to != from
        ) {
            throw_exception(
                component_error(
                    "persistent_object"
                )
            );
        }
    }
}

#endif
