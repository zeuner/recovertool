/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef FLUSHED_POINTER_CONTAINER_INCLUDED
#define FLUSHED_POINTER_CONTAINER_INCLUDED

namespace @qlib@ {
    
    
    template<
        typename storing
    >
    class flushed_pointer_container :
    public storing {
    public:
        ~flushed_pointer_container(
        );
    };
    
    template<
        typename storing
    >
    flushed_pointer_container<
        storing
    >::~flushed_pointer_container(
    ) {
        for (
            typename storing::const_iterator flushed = this->begin(
            );
            this->end(
            ) != flushed;
            flushed++
        ) {
            delete *flushed;
        }
    }
}

#endif
