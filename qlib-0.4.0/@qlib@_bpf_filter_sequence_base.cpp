/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_bpf_filter_sequence_base.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    bpf_filter_sequence_base::~bpf_filter_sequence_base(
    ) {
    }
    
    void bpf_filter_sequence_base::install_filter(
        int socket_descriptor
    ) {
        filter_vector sequence = filter_sequence(
        );
        struct sock_fprog installed;
        installed.len = sequence.size(
        );
        installed.filter = sequence.data(
        );
        if (
            0 > setsockopt(
                socket_descriptor,
                SOL_SOCKET,
                SO_ATTACH_FILTER,
                &installed,
                sizeof(
                    installed
                )
            )
        ) {
            throw component_error(
                "bpf_filter_sequence_base"
            );
        }
    }
}
