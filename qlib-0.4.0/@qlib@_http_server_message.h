/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef HTTP_SERVER_MESSAGE_INCLUDED
#define HTTP_SERVER_MESSAGE_INCLUDED

#include <boost/noncopyable.hpp>
#include <map>
namespace @qlib@ {
    
    
    template<
        typename accepting
    >
    class http_server_message :
    private boost::noncopyable {
    public:
        http_server_message(
            accepting& acceptor
        );
        typedef typename accepting::string string;
        void set_resource(
            string const& resource
        );
        string const& get_resource(
        ) const;
        void set_data(
            string const& data
        );
        string const& get_data(
        ) const;
        void set_headers(
            std::map<
                string,
                string
            > const& headers
        );
        std::map<
            string,
            string
        > const& get_headers(
        ) const;
        void set_finished(
            bool finished
        );
        bool get_finished(
        ) const;
    protected:
        accepting& get_acceptor(
        );
    private:
        accepting& acceptor;
        string resource;
        string data;
        std::map<
            string, 
            string 
        > headers;
        bool finished;
    };
    
    template<
        typename accepting
    >
    http_server_message<
        accepting
    >::http_server_message(
        accepting& acceptor
    ) :
    acceptor(
        acceptor
    ),
    finished(
        true
    ) {
    }
    
    template<
        typename accepting
    >
    void http_server_message<
        accepting
    >::set_resource(
        typename http_server_message<
            accepting
        >::string const& resource
    ) {
        this->resource = resource;
    }
    
    template<
        typename accepting
    >
    typename http_server_message<
        accepting
    >::string const& http_server_message<
        accepting
    >::get_resource(
    ) const {
        return resource;
    }
    
    template<
        typename accepting
    >
    void http_server_message<
        accepting
    >::set_data(
        typename http_server_message<
            accepting
        >::string const& data
    ) {
        this->data = data;
    }
    
    template<
        typename accepting
    >
    typename http_server_message<
        accepting
    >::string const& http_server_message<
        accepting
    >::get_data(
    ) const {
        return data;
    }
    
    template<
        typename accepting
    >
    void http_server_message<
        accepting
    >::set_headers(
        std::map<
            typename http_server_message<
                accepting
            >::string,
            typename http_server_message<
                accepting
            >::string
        > const& headers
    ) {
        this->headers = headers;
    }
    
    template<
        typename accepting
    >
    std::map<
        typename http_server_message<
            accepting
        >::string,
        typename http_server_message<
            accepting
        >::string
    > const& http_server_message<
        accepting
    >::get_headers(
    ) const {
        return headers;
    }
    
    template<
        typename accepting
    >
    void http_server_message<
        accepting
    >::set_finished(
        bool finished
    ) {
        this->finished = finished;
    }
    
    template<
        typename accepting
    >
    bool http_server_message<
        accepting
    >::get_finished(
    ) const {
        return finished;
    }
    
    template<
        typename accepting
    >
    accepting& http_server_message<
        accepting
    >::get_acceptor(
    ) {
        return acceptor;
    }
}

#endif
