/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef SERIALIZED_INCLUDED
#define SERIALIZED_INCLUDED

#include "@qlib@_stringified.h"
#include <boost/unordered_map.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>
#include <string>
#include "@qlib@_component_error.h"
#include <boost/optional.hpp>
#include <map>
#include <boost/date_time/posix_time/ptime.hpp>
#include <set>
#include <boost/asio/ip/tcp.hpp>
#include <vector>
#include <boost/asio/ip/address_v4.hpp>
#include <list>
#include "@qlib@_supported_type.h"
namespace @qlib@ {
    
    
    template<
        typename serializable,
        typename storing
    >
    class serialized_implementation {
    public:
        static void apply(
            serializable const& raw,
            storing& result
        );
    };
    
    template<
        typename serializable,
        typename storing
    >
    void serialized_implementation<
        serializable,
        storing
    >::apply(
        serializable const& raw,
        storing& result
    ) {
        supported_type<
            serializable
        > supported(
            raw
        );
        throw component_error(
            "serialized"
        );
    }
    
    template<
        typename serializable,
        typename storing
    >
    void serialized(
        serializable const& raw,
        storing& result
    ) {
        return serialized_implementation<
            serializable,
            storing
        >::apply(
            raw,
            result
        );
    }
    
    template<
        typename serializable
    >
    std::string serialized(
        serializable const& raw
    ) {
        std::string result;
        serialized(
            raw,
            result
        );
        return result;
    }
    
    template<
        typename storing
    >
    class serialized_implementation<
        std::string,
        storing
    > {
    public:
        static void apply(
            std::string const& raw,
            storing& result
        );
    };
    
    template<
        typename first_type,
        typename second_type,
        typename storing
    >
    class serialized_implementation<
        std::pair<
            first_type,
            second_type
        >,
        storing
    > {
    public:
        static void apply(
            std::pair<
                first_type,
                second_type
            > const& raw,
            storing& result
        );
    };
    
    template<
        typename first_type,
        typename second_type,
        typename storing
    >
    void serialized_implementation<
        std::pair<
            first_type,
            second_type
        >,
        storing
    >::apply(
        std::pair<
            first_type,
            second_type
        > const& raw,
        storing& result
    ) {
        serialized(
            raw.first,
            result
        );
        serialized(
            raw.second,
            result
        );
    }
    
    template<
        typename value_type,
        typename storing
    >
    class serialized_implementation<
        std::set<
            value_type
        >,
        storing
    > {
    public:
        static void apply(
            std::set<
                value_type
            > const& raw,
            storing& result
        );
    };
    
    template<
        typename value_type,
        typename storing
    >
    class serialized_implementation<
        boost::optional<
            value_type
        >,
        storing
    > {
    public:
        static void apply(
            boost::optional<
                value_type
            > const& raw,
            storing& result
        );
    };
    
    template<
        typename value_type,
        typename storing
    >
    class serialized_implementation<
        std::vector<
            value_type
        >,
        storing
    > {
    public:
        static void apply(
            std::vector<
                value_type
            > const& raw,
            storing& result
        );
    };
    
    template<
        typename value_type,
        typename storing
    >
    class serialized_implementation<
        std::list<
            value_type
        >,
        storing
    > {
    public:
        static void apply(
            std::list<
                value_type
            > const& raw,
            storing& result
        );
    };
    
    template<
        typename key_type,
        typename value_type,
        typename storing
    >
    class serialized_implementation<
        std::map<
            key_type,
            value_type
        >,
        storing
    > {
    public:
        static void apply(
            std::map<
                key_type,
                value_type
            > const& raw,
            storing& result
        );
    };
    
    template<
        typename key_type,
        typename value_type,
        typename storing
    >
    class serialized_implementation<
        boost::unordered_map<
            key_type,
            value_type
        >,
        storing
    > {
    public:
        static void apply(
            boost::unordered_map<
                key_type,
                value_type
            > const& raw,
            storing& result
        );
    };
    
    template<
        typename value_type,
        typename storing
    >
    void serialized_implementation<
        std::set<
            value_type
        >,
        storing
    >::apply(
        std::set<
            value_type
        > const& raw,
        storing& result
    ) {
        for (
            typename std::set<
                value_type
            >::const_iterator traversed = raw.begin(
            );
            raw.end(
            ) != traversed;
            traversed++
        ) {
            result += "1";
            serialized(
                *traversed,
                result
            );
        }
        result += "0";
    }
    
    template<
        typename value_type,
        typename storing
    >
    void serialized_implementation<
        boost::optional<
            value_type
        >,
        storing
    >::apply(
        boost::optional<
            value_type
        > const& raw,
        storing& result
    ) {
        if (
            raw
        ) {
            result += "1";
            serialized(
                *raw,
                result
            );
        } else {
            result += "0";
        }
    }
    
    template<
        typename value_type,
        typename storing
    >
    void serialized_implementation<
        std::vector<
            value_type
        >,
        storing
    >::apply(
        std::vector<
            value_type
        > const& raw,
        storing& result
    ) {
        serialized(
            raw.size(
            ),
            result
        );
        for (
            typename std::vector<
                value_type
            >::size_type traversed = 0;
            raw.size(
            ) > traversed;
            traversed++
        ) {
            serialized(
                raw[
                    traversed
                ],
                result
            );
        }
    }
    
    template<
        typename value_type,
        typename storing
    >
    void serialized_implementation<
        std::list<
            value_type
        >,
        storing
    >::apply(
        std::list<
            value_type
        > const& raw,
        storing& result
    ) {
        for (
            typename std::list<
                value_type
            >::const_iterator traversed = raw.begin(
            );
            raw.end(
            ) != traversed;
            traversed++
        ) {
            result += "1";
            serialized(
                *traversed,
                result
            );
        }
        result += "0";
    }
    
    template<
        typename key_type,
        typename value_type,
        typename storing
    >
    void serialized_implementation<
        std::map<
            key_type,
            value_type
        >,
        storing
    >::apply(
        std::map<
            key_type,
            value_type
        > const& raw,
        storing& result
    ) {
        for (
            typename std::map<
                key_type,
                value_type
            >::const_iterator traversed = raw.begin(
            );
            raw.end(
            ) != traversed;
            traversed++
        ) {
            result += "1";
            serialized(
                *traversed,
                result
            );
        }
        result += "0";
    }
    
    template<
        typename key_type,
        typename value_type,
        typename storing
    >
    void serialized_implementation<
        boost::unordered_map<
            key_type,
            value_type
        >,
        storing
    >::apply(
        boost::unordered_map<
            key_type,
            value_type
        > const& raw,
        storing& result
    ) {
        for (
            typename boost::unordered_map<
                key_type,
                value_type
            >::const_iterator traversed = raw.begin(
            );
            raw.end(
            ) != traversed;
            traversed++
        ) {
            result += "1";
            serialized(
                *traversed,
                result
            );
        }
        result += "0";
    }
    
    template<
        typename storing
    >
    class serialized_implementation<
        unsigned long,
        storing
    > {
    public:
        static void apply(
            unsigned long const& raw,
            storing& result
        );
    };
    
    template<
        typename storing
    >
    class serialized_implementation<
        long long,
        storing
    > {
    public:
        static void apply(
            long long const& raw,
            storing& result
        );
    };
    
    template<
        typename storing
    >
    class serialized_implementation<
        unsigned long long,
        storing
    > {
    public:
        static void apply(
            unsigned long long const& raw,
            storing& result
        );
    };
    
    template<
        typename storing
    >
    class serialized_implementation<
        unsigned char,
        storing
    > {
    public:
        static void apply(
            unsigned char const& raw,
            storing& result
        );
    };
    
    template<
        typename storing
    >
    class serialized_implementation<
        bool,
        storing
    > {
    public:
        static void apply(
            bool const& raw,
            storing& result
        );
    };
    
    template<
        typename storing
    >
    class serialized_implementation<
        long,
        storing
    > {
    public:
        static void apply(
            long const& raw,
            storing& result
        );
    };
    
    template<
        typename storing
    >
    class serialized_implementation<
        unsigned,
        storing
    > {
    public:
        static void apply(
            unsigned const& raw,
            storing& result
        );
    };
    
    template<
        typename storing
    >
    class serialized_implementation<
        unsigned short,
        storing
    > {
    public:
        static void apply(
            unsigned short const& raw,
            storing& result
        );
    };
    
    template<
        typename storing
    >
    class serialized_implementation<
        int,
        storing
    > {
    public:
        static void apply(
            int const& raw,
            storing& result
        );
    };
    
    template<
        typename storing
    >
    class serialized_implementation<
        float,
        storing
    > {
    public:
        static void apply(
            float const& raw,
            storing& result
        );
    };
    
    template<
        typename storing
    >
    class serialized_implementation<
        double,
        storing
    > {
    public:
        static void apply(
            double const& raw,
            storing& result
        );
    };
    
    template<
        typename storing
    >
    class serialized_implementation<
        boost::asio::ip::tcp::endpoint,
        storing
    > {
    public:
        static void apply(
            boost::asio::ip::tcp::endpoint const& raw,
            storing& result
        );
    };
    
    template<
        typename storing
    >
    class serialized_implementation<
        boost::asio::ip::address_v4,
        storing
    > {
    public:
        static void apply(
            boost::asio::ip::address_v4 const& raw,
            storing& result
        );
    };
    
    template<
        typename storing
    >
    class serialized_implementation<
        boost::posix_time::ptime,
        storing
    > {
    public:
        static void apply(
            boost::posix_time::ptime const& raw,
            storing& result
        );
    };
    
    template<
        typename storing
    >
    void serialized_implementation<
        std::string,
        storing
    >::apply(
        std::string const& raw,
        storing& result
    ) {
        BOOST_STATIC_ASSERT(
            sizeof(
                std::string::size_type
            ) <= sizeof(
                uint64_t
            )
        );
        uint64_t size = raw.size(
        );
        result += storing(
            reinterpret_cast<
                char const*
            >(
                &size
            ),
            sizeof(
                size
            )
        ) + storing(
            raw.begin(
            ),
            raw.end(
            )
        );
    }
    
    template<
        typename storing
    >
    void serialized_implementation<
        unsigned long,
        storing
    >::apply(
        unsigned long const& raw,
        storing& result
    ) {
        BOOST_STATIC_ASSERT(
            sizeof(
                unsigned long
            ) <= sizeof(
                uint64_t
            )
        );
        uint64_t value = raw;
        result += storing(
            reinterpret_cast<
                char const*
            >(
                &value
            ),
            sizeof(
                value
            )
        );
    }
    
    template<
        typename storing
    >
    void serialized_implementation<
        long long,
        storing
    >::apply(
        long long const& raw,
        storing& result
    ) {
        BOOST_STATIC_ASSERT(
            sizeof(
                long long
            ) <= sizeof(
                uint64_t
            )
        );
        uint64_t value = raw;
        result += storing(
            reinterpret_cast<
                char const*
            >(
                &value
            ),
            sizeof(
                value
            )
        );
    }
    
    template<
        typename storing
    >
    void serialized_implementation<
        unsigned long long,
        storing
    >::apply(
        unsigned long long const& raw,
        storing& result
    ) {
        BOOST_STATIC_ASSERT(
            sizeof(
                unsigned long long
            ) <= sizeof(
                uint64_t
            )
        );
        uint64_t value = raw;
        result += storing(
            reinterpret_cast<
                char const*
            >(
                &value
            ),
            sizeof(
                value
            )
        );
    }
    
    template<
        typename storing
    >
    void serialized_implementation<
        unsigned char,
        storing
    >::apply(
        unsigned char const& raw,
        storing& result
    ) {
        char const* value = reinterpret_cast<
            char const*
        >(
            &raw
        );
        result += *value;
    }
    
    template<
        typename storing
    >
    void serialized_implementation<
        bool,
        storing
    >::apply(
        bool const& raw,
        storing& result
    ) {
        result += raw ? "1" : "0";
    }
    
    template<
        typename storing
    >
    void serialized_implementation<
        long,
        storing
    >::apply(
        long const& raw,
        storing& result
    ) {
        BOOST_STATIC_ASSERT(
            sizeof(
                long
            ) <= sizeof(
                uint64_t
            )
        );
        uint64_t value = raw;
        result += storing(
            reinterpret_cast<
                char const*
            >(
                &value
            ),
            sizeof(
                value
            )
        );
    }
    
    template<
        typename storing
    >
    void serialized_implementation<
        unsigned,
        storing
    >::apply(
        unsigned const& raw,
        storing& result
    ) {
        BOOST_STATIC_ASSERT(
            sizeof(
                unsigned
            ) <= sizeof(
                uint64_t
            )
        );
        uint64_t value = raw;
        result += storing(
            reinterpret_cast<
                char const*
            >(
                &value
            ),
            sizeof(
                value
            )
        );
    }
    
    template<
        typename storing
    >
    void serialized_implementation<
        unsigned short,
        storing
    >::apply(
        unsigned short const& raw,
        storing& result
    ) {
        BOOST_STATIC_ASSERT(
            sizeof(
                unsigned short
            ) <= sizeof(
                uint64_t
            )
        );
        uint64_t value = raw;
        result += storing(
            reinterpret_cast<
                char const*
            >(
                &value
            ),
            sizeof(
                value
            )
        );
    }
    
    template<
        typename storing
    >
    void serialized_implementation<
        int,
        storing
    >::apply(
        int const& raw,
        storing& result
    ) {
        BOOST_STATIC_ASSERT(
            sizeof(
                int
            ) <= sizeof(
                uint64_t
            )
        );
        uint64_t value = raw;
        result += storing(
            reinterpret_cast<
                char const*
            >(
                &value
            ),
            sizeof(
                value
            )
        );
    }
    
    template<
        typename storing
    >
    void serialized_implementation<
        float,
        storing
    >::apply(
        float const& raw,
        storing& result
    ) {
        serialized(
            stringified(
                raw
            ),
            result
        );
    }
    
    template<
        typename storing
    >
    void serialized_implementation<
        double,
        storing
    >::apply(
        double const& raw,
        storing& result
    ) {
        serialized(
            stringified(
                raw
            ),
            result
        );
    }
    
    template<
        typename storing
    >
    void serialized_implementation<
        boost::asio::ip::tcp::endpoint,
        storing
    >::apply(
        boost::asio::ip::tcp::endpoint const& raw,
        storing& result
    ) {
        serialized(
            raw.address(
            ).to_v4(
            ),
            result
        );
        serialized(
            raw.port(
            ),
            result
        );
    }
    
    template<
        typename storing
    >
    void serialized_implementation<
        boost::asio::ip::address_v4,
        storing
    >::apply(
        boost::asio::ip::address_v4 const& raw,
        storing& result
    ) {
        serialized(
            raw.to_string(
            ),
            result
        );
    }
    
    template<
        typename storing
    >
    void serialized_implementation<
        boost::posix_time::ptime,
        storing
    >::apply(
        boost::posix_time::ptime const& raw,
        storing& result
    ) {
        serialized(
            boost::posix_time::to_iso_string(
                raw
            ),
            result
        );
    }
}

#endif
