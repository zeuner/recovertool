/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef STREAM_ONE_SHOT_REQUEST_INCLUDED
#define STREAM_ONE_SHOT_REQUEST_INCLUDED

#include <boost/asio/write.hpp>
#include <boost/function.hpp>
#include "@qlib@_component_error.h"
#include <string>
#include "@qlib@_throw_exception.h"
#include <boost/asio/placeholders.hpp>
#include <boost/bind.hpp>
namespace @qlib@ {
    
    
    template<
        typename socket
    >
    class stream_one_shot_request :
    public socket {
    public:
        template<
            typename initializing
        >
        stream_one_shot_request(
            initializing& initializer
        );
        template<
            typename initializing1,
            typename initializing2
        >
        stream_one_shot_request(
            initializing1& initializer1,
            initializing2& initializer2
        );
        template<
            typename handling
        >
        void async_request(
            std::string const& host,
            unsigned short port,
            std::string const& request,
            handling handler
        );
    protected:
        void handle_connect(
            boost::system::error_code const& error
        );
        void handle_write(
            boost::system::error_code const& error,
            std::size_t transferred
        );
        void handle_read(
            boost::system::error_code const& error,
            std::size_t transferred
        );
        void async_read(
        );
    private:
        boost::function<
            void(
                boost::system::error_code const&,
                std::string const&
            )
        > result_handler;
        std::string request;
        std::string result;
        char input_data[
            0x400
        ];
    };
    
    template<
        typename socket
    >
    template<
        typename initializing
    >
    stream_one_shot_request<
        socket
    >::stream_one_shot_request(
        initializing& initializer
    ) :
    socket(
        initializer
    ) {
    }
    
    template<
        typename socket
    >
    template<
        typename initializing1,
        typename initializing2
    >
    stream_one_shot_request<
        socket
    >::stream_one_shot_request(
        initializing1& initializer1,
        initializing2& initializer2
    ) :
    socket(
        initializer1,
        initializer2
    ) {
    }
    
    template<
        typename socket
    >
    template<
        typename handling
    >
    void stream_one_shot_request<
        socket
    >::async_request(
        std::string const& host,
        unsigned short port,
        std::string const& request,
        handling handler
    ) {
        result_handler = handler;
        this->request = request;
        this->async_connect(
            host,
            port,
            boost::bind(
                &stream_one_shot_request::handle_connect,
                this,
                boost::asio::placeholders::error
            )
        );
    }
    
    template<
        typename socket
    >
    void stream_one_shot_request<
        socket
    >::async_read(
    ) {
        boost::asio::async_read(
            *this,
            boost::asio::buffer(
                input_data,
                sizeof(
                    input_data
                )
            ),
            boost::asio::transfer_at_least(
                1
            ),
            boost::bind(
                &stream_one_shot_request::handle_read,
                this,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )   
        );
    }
    
    template<
        typename socket
    >
    void stream_one_shot_request<
        socket
    >::handle_write(
        boost::system::error_code const& error,
        std::size_t transferred
    ) {
        if (
            !error
        ) {
            if (
                request.size(
                ) != transferred
            ) {
                throw_exception(
                    component_error(
                        "stream_one_shot_request"
                    )
                );
            }
            this->async_read(
            );
            return;
        }
        result_handler(
            error,
            std::string(
            )
        );
    }
    
    template<
        typename socket
    >
    void stream_one_shot_request<
        socket
    >::handle_read(
        boost::system::error_code const& error,
        std::size_t transferred
    ) {
        if (
            !error
        ) {
            result += std::string(
                input_data,
                transferred
            );
            this->async_read(
            );
            return;
        } else if (
            boost::asio::error::eof == error
        ) {
            result += std::string(
                input_data,
                transferred
            );
            result_handler(
                boost::system::error_code(
                ),
                result
            );
            return;
        }
        result_handler(
            error,
            std::string(
            )
        );
    }
    
    template<
        typename socket
    >
    void stream_one_shot_request<
        socket
    >::handle_connect(
        boost::system::error_code const& error
    ) {
        if (
            !error
        ) {
            boost::asio::async_write(
                *this,
                boost::asio::buffer(
                    request.data(
                    ),
                    request.size(
                    )
                ),
                boost::bind(
                    &stream_one_shot_request::handle_write,
                    this,
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred
                )
            );
            return;
        }
        result_handler(
            error,
            std::string(
            )
        );
    }
}

#endif
