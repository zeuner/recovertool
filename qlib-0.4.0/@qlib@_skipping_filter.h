/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef SKIPPING_FILTER_INCLUDED
#define SKIPPING_FILTER_INCLUDED

#include <boost/noncopyable.hpp>
namespace @qlib@ {
    
    
    template<
        typename playable
    >
    class skipping_filter :
    private boost::noncopyable {
    public:
        skipping_filter(
            playable& source,
            int skipped_samples
        );
        size_t get_samples(
            amplitude_value* first,
            amplitude_value* last
        );
        bool is_master(
        ) const;
        bool get_sample(
            amplitude_value& retrieved
        );
    private:
        playable& source;
        int skipped_samples;
    };
    
    template<
        typename playable
    >
    class bulk_audio_retrieval<
        skipping_filter<
            playable
        >
    > {
    public:
        static size_t get_samples(
            skipping_filter<
                playable
            >& source,
            amplitude_value* first,
            amplitude_value* last
        );
    };
    
    template<
        typename playable
    >
    skipping_filter<
        playable
    >::skipping_filter(
        playable& source,
        int skipped_samples
    ) :
    source(
        source
    ),
    skipped_samples(
        skipped_samples
    ) {
    }
    
    template<
        typename playable
    >
    size_t skipping_filter<
        playable
    >::get_samples(
        amplitude_value* first,
        amplitude_value* last
    ) {
        while (
            0 < skipped_samples
        ) {
            amplitude_value skipped[
                skipped_samples
            ];
            size_t const retrieved = bulk_audio_retrieval<
                playable
            >::get_samples(
                source,
                skipped,
                skipped + skipped_samples
            );
            if (
                0 == retrieved
            ) {
                return retrieved;
            }
            skipped_samples -= retrieved;
        }
        return bulk_audio_retrieval<
            playable
        >::get_samples(
            source,
            first,
            last
        );
    }
    
    template<
        typename playable
    >
    bool skipping_filter<
        playable
    >::is_master(
    ) const {
        return true;
    }
    
    template<
        typename playable
    >
    bool skipping_filter<
        playable
    >::get_sample(
        amplitude_value& retrieved
    ) {
        while (
            0 < skipped_samples
        ) {
            if (
                !source.get_sample(
                    retrieved
                )
            ) {
                return false;
            }
            skipped_samples--;
        }
        return source.get_sample(
            retrieved
        );
    }
    
    template<
        typename playable
    >
    size_t bulk_audio_retrieval<
        skipping_filter<
            playable
        >
    >::get_samples(
        skipping_filter<
            playable
        >& source,
        amplitude_value* first,
        amplitude_value* last
    ) {
        return source.get_samples(
            first,
            last
        );
    }
}

#endif
