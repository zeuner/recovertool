/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef PACKET_CAPTURE_STORAGE_INCLUDED
#define PACKET_CAPTURE_STORAGE_INCLUDED

#include <string>
namespace @qlib@ {
    
    
    template<
        typename consuming
    >
    class packet_capture_storage {
    public:
        packet_capture_storage(
            consuming& sink,
            size_t snapshot_length
        );
        void store_packet(
            void const* data,
            size_t size,
            size_t original_size,
            struct timeval const& capture_time
        );
    protected:
        template<
            typename stored
        >
        void store_value(
            stored value
        );
    private:
        consuming& sink;
    };
    
    template<
        typename consuming
    >
    packet_capture_storage<
        consuming
    >::packet_capture_storage(
        consuming& sink,
        size_t snapshot_length
    ) :
    sink(
        sink
    ) {
        store_value(
            uint32_t(
                0xa1b2c3d4
            )
        );
        store_value(
            uint16_t(
                2
            )
        );
        store_value(
            uint16_t(
                4
            )
        );
        store_value(
            uint32_t(
                0
            )
        );
        store_value(
            uint32_t(
                0
            )
        );
        store_value(
            uint32_t(
                snapshot_length
            )
        );
        store_value(
            uint32_t(
                1
            )
        );
    }
    
    template<
        typename consuming
    >
    void packet_capture_storage<
        consuming
    >::store_packet(
        void const* data,
        size_t size,
        size_t original_size,
        struct timeval const& capture_time
    ) {
        store_value(
            uint32_t(
                capture_time.tv_sec
            )
        );
        store_value(
            uint32_t(
                capture_time.tv_usec
            )
        );
        store_value(
            uint32_t(
                size
            )
        );
        store_value(
            uint32_t(
                original_size
            )
        );
        sink << std::string(
            static_cast<
                char const*
            >(
                data
            ),
            size
        );
    }
    
    template<
        typename consuming
    >
    template<
        typename stored
    >
    void packet_capture_storage<
        consuming
    >::store_value(
        stored value
    ) {
        sink << std::string(
            reinterpret_cast<
                char const*
            >(
                &value
            ),
            sizeof(
                value
            )
        );
    }
}

#endif
