/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef ALLOCATOR_C_WRAPPER_INCLUDED
#define ALLOCATOR_C_WRAPPER_INCLUDED

#include <cstring>
#include <new>
namespace @qlib@ {
    
    
    template<
        typename storing
    >
    class allocator_c_wrapper {
    public:
        static void* malloc(
            std::size_t count
        );
        static void* calloc(
            std::size_t count,
            std::size_t size
        );
        static void free(
            void* allocated
        );
        static void* realloc(
            void* previous,
            std::size_t count
        );
        static char* strdup(
            char const* copied
        );
    private:
        typedef typename storing::template rebind<
            char
        >::other char_allocator;
        static char_allocator allocating;
    };
    
    template<
        typename storing
    >
    void* allocator_c_wrapper<
        storing
    >::malloc(
        std::size_t count
    ) {
        std::size_t const required = sizeof(
            std::size_t
        ) + count;
    #ifndef QLIB_NO_EXCEPTIONS
        try {
    #else
        do {
    #endif
            std::size_t* const size_location = reinterpret_cast<
                std::size_t*
            >(
                allocating.allocate(
                    required
                )
            );
            *size_location = required;
            void* const result = reinterpret_cast<
                void*
            >(
                size_location + 1
            );
            return result;
    #ifndef QLIB_NO_EXCEPTIONS
        } catch (
            std::bad_alloc& caught
        ) {
            return NULL;
        }
    #else
        } while (
            false
        );
    #endif
    }
    
    template<
        typename storing
    >
    void* allocator_c_wrapper<
        storing
    >::calloc(
        std::size_t count,
        std::size_t size
    ) {
        std::size_t const needed = count * size;
        void* allocated = allocator_c_wrapper<
            storing
        >::malloc(
            needed
        );
        if (
            NULL != allocated
        ) {
            memset(
                allocated,
                0,
                needed
            );
        }
        return allocated;
    }
    
    template<
        typename storing
    >
    void allocator_c_wrapper<
        storing
    >::free(
        void* allocated
    ) {
        if (
            NULL == allocated
        ) {
            return;
        }
        std::size_t* const size_location = reinterpret_cast<
            std::size_t*
        >(
            allocated
        ) - 1;
        allocating.deallocate(
            reinterpret_cast<
                char*
            >(
                size_location
            ),
            *size_location
        );
    }
    
    template<
        typename storing
    >
    void* allocator_c_wrapper<
        storing
    >::realloc(
        void* previous,
        std::size_t count
    ) {
        std::size_t minimum;
        if (
            NULL == previous
        ) {
            minimum = 0;
        } else {
            std::size_t* const size_location = reinterpret_cast<
                std::size_t*
            >(
                previous
            ) - 1;
            minimum = *size_location - sizeof(
                std::size_t
            );
        }
        if (
            count < minimum
        ) {
            minimum = count;
        }
        void* result;
        if (
            0 == count
        ) {
            result = NULL;
        } else {
            result = allocator_c_wrapper<
                storing
            >::malloc(
                count
            );
            if (
                NULL == result
            ) {
                return result;
            }
        }
        memcpy(
            result,
            previous,
            minimum
        );
        allocator_c_wrapper<
            storing
        >::free(
            previous
        );
        return result;
    }
    
    template<
        typename storing
    >
    char* allocator_c_wrapper<
        storing
    >::strdup(
        char const* copied
    ) {
        std::size_t const required = strlen(
            copied
        ) + 1;
        char* const result = reinterpret_cast<
            char*
        >(
            allocator_c_wrapper<
                storing
            >::malloc(
                required
            )
        );
        memcpy(
            result,
            copied,
            required
        );
        return result;
    }
    
    template<
        typename storing
    >
    typename allocator_c_wrapper<
        storing
    >::char_allocator allocator_c_wrapper<
        storing
    >::allocating;
}

#endif
