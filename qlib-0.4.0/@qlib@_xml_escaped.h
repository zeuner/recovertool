/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef XML_ESCAPED_INCLUDED
#define XML_ESCAPED_INCLUDED

#include "@qlib@_throw_exception.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    template<
        typename string
    >
    string xml_escaped(
        string const& raw
    );
    
    template<
        typename string
    >
    string xml_escaped(
        string const& raw
    ) {
        string result;
        for (
            typename string::const_iterator current = raw.begin(
            );
            raw.end(
            ) != current;
            current++
        ) {
            if (
                9 > static_cast<
                    unsigned char
                >(
                    *current
                )
            ) {
                throw_exception(
                    component_error(
                        "xml_escaped"
                    )
                );
            } else if (
                static_cast<
                    typename string::value_type
                >(
                    '&'
                ) == *current
            ) {
                result += reinterpret_cast<
                    typename string::value_type const*
                >(
                    "&amp;"
                );
            } else if (
                static_cast<
                    typename string::value_type
                >(
                    '"'
                ) == *current
            ) {
                result += reinterpret_cast<
                    typename string::value_type const*
                >(
                    "&quot;"
                );
            } else if (
                static_cast<
                    typename string::value_type
                >(
                    '\''
                ) == *current
            ) {
                result += reinterpret_cast<
                    typename string::value_type const*
                >(
                    "&apos;"
                );
            } else if (
                static_cast<
                    typename string::value_type
                >(
                    '<'
                ) == *current
            ) {
                result += reinterpret_cast<
                    typename string::value_type const*
                >(
                    "&lt;"
                );
            } else if (
                static_cast<
                    typename string::value_type
                >(
                    '>'
                ) == *current
            ) {
                result += reinterpret_cast<
                    typename string::value_type const*
                >(
                    "&gt;"
                );
            } else {
                result += *current;
            }
        }
        return result;
    }
}

#endif
