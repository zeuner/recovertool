/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef CMPI_WRAPPER_INCLUDED
#define CMPI_WRAPPER_INCLUDED

#include <set>
#include <boost/noncopyable.hpp>
namespace @qlib@ {
    
    
    template<
        typename managed,
        typename key_policy = cmpi_oid_key
    >
    class cmpi_wrapper;
    
    template<
        typename managed,
        typename key_policy
    >
    class cmpi_wrapper :
    public managed,
    public key_policy,
    private boost::noncopyable {
    public:
        cmpi_wrapper(
        );
        template<
            typename initializing
        >
        cmpi_wrapper(
            initializing initializer
        );
        template<
            typename initializing1,
            typename initializing2
        >
        cmpi_wrapper(
            initializing1 initializer1,
            initializing2 initializer2
        );
        template<
            typename initializing1,
            typename initializing2,
            typename initializing3
        >
        cmpi_wrapper(
            initializing1 initializer1,
            initializing2 initializer2,
            initializing3 initializer3
        );
        ~cmpi_wrapper(
        );
        static boost::mutex& atomic_access(
        );
        static CMPIStatus cmpiEnumInstanceNames(
            CMPIInstanceMI* self,
            CMPIContext const* context,
            CMPIResult const* results,
            CMPIObjectPath const* reference
        );
        static CMPIStatus cmpiEnumInstances(
            CMPIInstanceMI* self,
            CMPIContext const* context,
            CMPIResult const* results,
            CMPIObjectPath const* reference,
            char const** properties
        );
        static CMPIStatus cmpiGetInstance(
            CMPIInstanceMI* self,
            CMPIContext const* context,
            CMPIResult const* results,
            CMPIObjectPath const* reference,
            char const** properties
        );
        static CMPIStatus cmpiModifyInstance(
            CMPIInstanceMI* self,
            CMPIContext const* context,
            CMPIResult const* results,
            CMPIObjectPath const* reference,
            CMPIInstance const* new_instance,
            char const** properties
        );
        static CMPIStatus cmpiCreateInstance(
            CMPIInstanceMI* self,
            CMPIContext const* context,
            CMPIResult const* results,
            CMPIObjectPath const* reference,
            CMPIInstance const* new_instance
        );
        static CMPIStatus cmpiDeleteInstance(
            CMPIInstanceMI* self,
            CMPIContext const* context,
            CMPIResult const* results,
            CMPIObjectPath const* reference
        );
        static CMPIStatus cmpiExecQuery(
            CMPIInstanceMI* self,
            CMPIContext const* context,
            CMPIResult const* results,
            CMPIObjectPath const* reference,
            char const* language,
            char const* query
        );
        static CMPIStatus cmpiCleanup(
            CMPIInstanceMI* self,
            CMPIContext const* context,
            CMPIBoolean terminating
        );
        static CMPIStatus cmpiMethodCleanup(
            CMPIMethodMI* instance,
            CMPIContext const* context,
            CMPIBoolean terminating
        );
        static CMPIStatus cmpiInvokeMethod(
            CMPIMethodMI* instance,
            CMPIContext const* context,
            CMPIResult const* results,
            CMPIObjectPath const* reference,
            char const* method_name,
            CMPIArgs const* in,
            CMPIArgs* out
        );
        static CMPIBroker const* broker;
        static std::set<
            cmpi_wrapper<
                managed,
                key_policy
            >*
        >& get_registered_instances(
        );
    protected:
        cmpi_wrapper<
            managed,
            key_policy
        >* get_wrapped(
        );
    private:
        static boost::mutex atomically_accessing_instances;
        static boost::mutex accessing_instances;
        static CMPIStatus const ok;
        static CMPIStatus const unsupported;
    };
    
    #define cmpi_wrapper_wrap(\
        wrapped,\
        key_policy\
    ) typedef cmpi_wrapper<\
        wrapped,\
        key_policy\
    > _internal_##cmpi_wrapper##_##wrapped##_##key_policy;\
    CMInstanceMIStub(\
        _internal_##cmpi_wrapper##_##wrapped##_##key_policy::cmpi,\
        wrapped,\
        _internal_##cmpi_wrapper##_##wrapped##_##key_policy::broker,\
        CMNoHook\
    )
    
    #define cmpi_wrapper_wrap_basic(\
        wrapped\
    ) CMInstanceMIStub(\
        cmpi_wrapper<\
            wrapped\
        >::cmpi,\
        wrapped,\
        cmpi_wrapper<\
            wrapped\
        >::broker,\
        CMNoHook\
    )
    
    #define cmpi_wrapper_wrap_methods(\
        wrapped,\
        key_policy\
    ) typedef cmpi_wrapper<\
        wrapped,\
        key_policy\
    > _internal_##cmpi_wrapper##_##wrapped##_##key_policy;\
    CMMethodMIStub(\
        _internal_##cmpi_wrapper##_##wrapped##_##key_policy::cmpi,\
        wrapped,\
        _internal_##cmpi_wrapper##_##wrapped##_##key_policy::broker,\
        CMNoHook\
    )
    
    #define cmpi_wrapper_wrap_methods_basic(\
        wrapped\
    ) CMMethodMIStub(\
        cmpi_wrapper<\
            wrapped\
        >::cmpi,\
        wrapped,\
        cmpi_wrapper<\
            wrapped\
        >::broker,\
        CMNoHook\
    )
    
    template<
        typename managed,
        typename key_policy
    >
    cmpi_wrapper<
        managed,
        key_policy
    >::cmpi_wrapper(
    ) {
        boost::unique_lock<
            boost::mutex
        > lock(
            accessing_instances
        );
        get_registered_instances(
        ).insert(
            this
        );
    }
    
    template<
        typename managed,
        typename key_policy
    >
    template<
        typename initializing
    >
    cmpi_wrapper<
        managed,
        key_policy
    >::cmpi_wrapper(
        initializing initializer
    ) :
    managed(
        initializer
    ) {
        boost::unique_lock<
            boost::mutex
        > lock(
            accessing_instances
        );
        get_registered_instances(
        ).insert(
            this
        );
    }
    
    template<
        typename managed,
        typename key_policy
    >
    template<
        typename initializing1,
        typename initializing2
    >
    cmpi_wrapper<
        managed,
        key_policy
    >::cmpi_wrapper(
        initializing1 initializer1,
        initializing2 initializer2
    ) :
    managed(
        initializer1,
        initializer2
    ) {
        boost::unique_lock<
            boost::mutex
        > lock(
            accessing_instances
        );
        get_registered_instances(
        ).insert(
            this
        );
    }
    
    template<
        typename managed,
        typename key_policy
    >
    template<
        typename initializing1,
        typename initializing2,
        typename initializing3
    >
    cmpi_wrapper<
        managed,
        key_policy
    >::cmpi_wrapper(
        initializing1 initializer1,
        initializing2 initializer2,
        initializing3 initializer3
    ) :
    managed(
        initializer1,
        initializer2,
        initializer3
    ) {
        boost::unique_lock<
            boost::mutex
        > lock(
            accessing_instances
        );
        get_registered_instances(
        ).insert(
            this
        );
    }
    
    template<
        typename managed,
        typename key_policy
    >
    cmpi_wrapper<
        managed,
        key_policy
    >::~cmpi_wrapper(
    ) {
        boost::unique_lock<
            boost::mutex
        > lock(
            accessing_instances
        );
        get_registered_instances(
        ).erase(
            this
        );
    }
    
    template<
        typename managed,
        typename key_policy
    >
    boost::mutex& cmpi_wrapper<
        managed,
        key_policy
    >::atomic_access(
    ) {
        return atomically_accessing_instances;
    }
    
    template<
        typename managed,
        typename key_policy
    >
    CMPIStatus cmpi_wrapper<
        managed,
        key_policy
    >::cmpiEnumInstanceNames(
        CMPIInstanceMI* self,
        CMPIContext const* context,
        CMPIResult const* results,
        CMPIObjectPath const* reference
    ) {
        CMPIStatus result = ok;
        char const* cim_namespace = CMGetCharPtr(
            CMGetNameSpace(
                reference,
                NULL
            )
        );
        boost::unique_lock<
            boost::mutex
        > atomic_lock(
            atomically_accessing_instances
        );
        boost::unique_lock<
            boost::mutex
        > lock(
            accessing_instances
        );
        for (
            typename std::set<
                cmpi_wrapper<
                    managed,
                    key_policy
                >*
            >::const_iterator enumerating = get_registered_instances(
            ).begin(
            );
            get_registered_instances(
            ).end(
            ) != enumerating;
            enumerating++
        ) {
            try {
                CMPIObjectPath* constructed = create_cmpi_object_path(
                    broker,
                    result,
                    cim_namespace,
                    *enumerating
                );
                CMReturnObjectPath(
                    results,
                    constructed
                );
            } catch (
                std::exception& caught
            ) {
                CMPIStatus* const writing = &result;
                CMSetStatusWithChars(
                    broker,
                    writing,
                    CMPI_RC_ERROR_SYSTEM,
                    caught.what(
                    )
                );
                return result;
            }
        }
        CMReturnDone(
            results
        );
        return result;
    }
    
    template<
        typename managed,
        typename key_policy
    >
    CMPIStatus cmpi_wrapper<
        managed,
        key_policy
    >::cmpiEnumInstances(
        CMPIInstanceMI* self,
        CMPIContext const* context,
        CMPIResult const* results,
        CMPIObjectPath const* reference,
        char const** properties
    ) {
        CMPIStatus result = ok;
        char const* cim_namespace = CMGetCharPtr(
            CMGetNameSpace(
                reference,
                NULL
            )
        );
        boost::unique_lock<
            boost::mutex
        > atomic_lock(
            atomically_accessing_instances
        );
        boost::unique_lock<
            boost::mutex
        > lock(
            accessing_instances
        );
        for (
            typename std::set<
                cmpi_wrapper<
                    managed,
                    key_policy
                >*
            >::const_iterator enumerating = get_registered_instances(
            ).begin(
            );
            get_registered_instances(
            ).end(
            ) != enumerating;
            enumerating++
        ) {
            try {
                CMPIInstance* constructed = create_cmpi_instance(
                    broker,
                    result,
                    cim_namespace,
                    *enumerating
                );
                CMReturnInstance(
                    results,
                    constructed
                );
            } catch (
                std::exception& caught
            ) {
                CMPIStatus* const writing = &result;
                CMSetStatusWithChars(
                    broker,
                    writing,
                    CMPI_RC_ERROR_SYSTEM,
                    caught.what(
                    )
                );
                return result;
            }
        }
        CMReturnDone(
            results
        );
        return result;
    }
    
    template<
        typename managed,
        typename key_policy
    >
    CMPIStatus cmpi_wrapper<
        managed,
        key_policy
    >::cmpiGetInstance(
        CMPIInstanceMI* self,
        CMPIContext const* context,
        CMPIResult const* results,
        CMPIObjectPath const* reference,
        char const** properties
    ) {
        CMPIStatus result = ok;
        cmpi_wrapper<
            managed,
            key_policy
        >* found = NULL; 
        get_by_cmpi_reference(
            broker,
            reference,
            found
        );
        if (
            NULL == found
        ) {
            return result;
        }
        char const* cim_namespace = CMGetCharPtr(
            CMGetNameSpace(
                reference,
                NULL
            )
        );
        try {
            CMPIInstance* constructed = create_cmpi_instance(
                broker,
                result,
                cim_namespace,
                found
            );
            CMReturnInstance(
                results,
                constructed
            );
        } catch (
            std::exception& caught
        ) {
            CMPIStatus* const writing = &result;
            CMSetStatusWithChars(
                broker,
                writing,
                CMPI_RC_ERROR_SYSTEM,
                caught.what(
                )
            );
            return result;
        }
        CMReturnDone(
            results
        );
        return result;
    }
    
    template<
        typename managed,
        typename key_policy
    >
    CMPIStatus cmpi_wrapper<
        managed,
        key_policy
    >::cmpiModifyInstance(
        CMPIInstanceMI* self,
        CMPIContext const* context,
        CMPIResult const* results,
        CMPIObjectPath const* reference,
        CMPIInstance const* new_instance,
        char const** properties
    ) {
        return unsupported;
    }
    
    template<
        typename managed,
        typename key_policy
    >
    CMPIStatus cmpi_wrapper<
        managed,
        key_policy
    >::cmpiCreateInstance(
        CMPIInstanceMI* self,
        CMPIContext const* context,
        CMPIResult const* results,
        CMPIObjectPath const* reference,
        CMPIInstance const* new_instance
    ) {
        return unsupported;
    }
    
    template<
        typename managed,
        typename key_policy
    >
    CMPIStatus cmpi_wrapper<
        managed,
        key_policy
    >::cmpiDeleteInstance(
        CMPIInstanceMI* self,
        CMPIContext const* context,
        CMPIResult const* results,
        CMPIObjectPath const* reference
    ) {
        return unsupported;
    }
    
    template<
        typename managed,
        typename key_policy
    >
    CMPIStatus cmpi_wrapper<
        managed,
        key_policy
    >::cmpiExecQuery(
        CMPIInstanceMI* self,
        CMPIContext const* context,
        CMPIResult const* results,
        CMPIObjectPath const* reference,
        char const* language,
        char const* query
    ) {
        return unsupported;
    }
    
    template<
        typename managed,
        typename key_policy
    >
    CMPIStatus cmpi_wrapper<
        managed,
        key_policy
    >::cmpiCleanup(
        CMPIInstanceMI* self,
        CMPIContext const* context,
        CMPIBoolean terminating
    ) {
        return unsupported;
    }
    
    template<
        typename managed,
        typename key_policy
    >
    CMPIStatus cmpi_wrapper<
        managed,
        key_policy
    >::cmpiMethodCleanup(
        CMPIMethodMI* instance,
        CMPIContext const* context,
        CMPIBoolean terminating
    ) {
        return unsupported;
    }
    
    template<
        typename managed,
        typename key_policy
    >
    CMPIStatus cmpi_wrapper<
        managed,
        key_policy
    >::cmpiInvokeMethod(
        CMPIMethodMI* instance,
        CMPIContext const* context,
        CMPIResult const* results,
        CMPIObjectPath const* reference,
        char const* method_name,
        CMPIArgs const* in,
        CMPIArgs* out
    ) {
        void (
            cmpi_wrapper::*native
        )(
            CMPIResult const* results,
            CMPIArgs const* in,
            CMPIArgs* out
        );
        try {
            native = managed::get_methods(
            ).at(
                method_name
            );
        } catch (
            std::exception& caught
        ) {
            CMPIStatus status = ok;
            CMPIStatus* const writing = &status;
            CMSetStatusWithChars(
                broker,
                writing,
                CMPI_RC_ERR_METHOD_NOT_FOUND,
                "Unrecognized method"
            );
            return status;
        }
        cmpi_wrapper<
            managed,
            key_policy
        >* found = NULL; 
        get_by_cmpi_reference(
            broker,
            reference,
            found
        );      
        if (    
            NULL == found
        ) { 
            CMPIStatus status = ok;
            CMPIStatus* const writing = &status;
            CMSetStatusWithChars(
                broker,
                writing,
                CMPI_RC_ERR_FAILED,
                "Could not retrieve object"
            );
            return status;
        }
        try {
            (
                found->*native
            )(
                results,
                in,
                out
            );
        } catch (
            std::exception& caught
        ) {
            CMPIStatus status = ok;
            CMPIStatus* const writing = &status;
            CMSetStatusWithChars(
                broker,
                writing,
                CMPI_RC_ERR_FAILED,
                caught.what(
                )
            );
            return status;
        }
        return ok;
    }
    
    template<
        typename managed,
        typename key_policy
    >
    std::set<
        cmpi_wrapper<
            managed,
            key_policy
        >*
    >& cmpi_wrapper<
        managed,
        key_policy
    >::get_registered_instances(
    ) {
        static std::set<
            cmpi_wrapper<
                managed,
                key_policy
            >*
        > value;
        return value;
    }
    
    template<
        typename managed,
        typename key_policy
    >
    cmpi_wrapper<
        managed,
        key_policy
    >* cmpi_wrapper<
        managed,
        key_policy
    >::get_wrapped(
    ) {
        return this;
    }
    
    template<
        typename managed,
        typename key_policy
    >
    CMPIBroker const* cmpi_wrapper<
        managed,
        key_policy
    >::broker = NULL;
    
    template<
        typename managed,
        typename key_policy
    >
    boost::mutex cmpi_wrapper<
        managed,
        key_policy
    >::atomically_accessing_instances;
    
    template<
        typename managed,
        typename key_policy
    >
    boost::mutex cmpi_wrapper<
        managed,
        key_policy
    >::accessing_instances;
    
    template<
        typename managed,
        typename key_policy
    >
    CMPIStatus const cmpi_wrapper<
        managed,
        key_policy
    >::ok = {
        CMPI_RC_OK,
        NULL
    };
    
    template<
        typename managed,
        typename key_policy
    >
    CMPIStatus const cmpi_wrapper<
        managed,
        key_policy
    >::unsupported = {
        CMPI_RC_ERR_NOT_SUPPORTED,
        NULL
    };
}

#endif
