/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef XML_PROCESSING_INCLUDED
#define XML_PROCESSING_INCLUDED

#include <boost/noncopyable.hpp>
#include "@qlib@_allocator_c_wrapper.h"
#include "@qlib@_component_error.h"
#include "@qlib@_throw_exception.h"
namespace @qlib@ {
    
    
    template<
        typename storing = std::allocator<
            char
        >
    >
    class xml_processing;
    
    template<
        typename storing
    >
    class xml_processing :
    private boost::noncopyable {
    public:
        xml_processing(
        );
        ~xml_processing(
        );
    };
    
    template<
        typename storing
    >
    xml_processing<
        storing
    >::xml_processing(
    ) {
        if (
            0 != xmlGcMemSetup(
                &allocator_c_wrapper<
                    storing
                >::free,
                &allocator_c_wrapper<
                    storing
                >::malloc,
                &allocator_c_wrapper<
                    storing
                >::malloc,
                &allocator_c_wrapper<
                    storing
                >::realloc,
                &allocator_c_wrapper<
                    storing
                >::strdup
            )
        ) {
            throw_exception(
                component_error(
                    "xml_processing"
                )
            );
        }
        xmlInitParser(
        );
    }
    
    template<
        typename storing
    >
    xml_processing<
        storing
    >::~xml_processing(
    ) {
        xmlCleanupParser(
        );
    }
}

#endif
