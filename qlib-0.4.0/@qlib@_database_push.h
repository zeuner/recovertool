/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef DATABASE_PUSH_INCLUDED
#define DATABASE_PUSH_INCLUDED

#include "@qlib@_throw_exception.h"
#include <boost/date_time/posix_time/ptime.hpp>
#include "@qlib@_system_log.h"
#include <string>
namespace @qlib@ {
    
    
    template<
        typename string
    >
    void database_push(
        database_connection& queried,
        string query
    );
    
    template<
    >
    void database_push(
        database_connection& queried,
        char const* query
    );
    
    template<
        typename string
    >
    void database_push(
        database_connection& queried,
        string query
    ) {
        boost::posix_time::ptime begin(
            boost::posix_time::microsec_clock::local_time(
            )
        );
        if (
            0 != mysql_real_query(
                queried.get_storage(
                ),
                query.data(
                ),
                query.size(
                )
            )
        ) {
            std::string const message(
                "could not query " + std::string(
                    query.data(
                    ),
                    query.size(
                    )
                ) + ":\n"
                "" + queried.last_error(
                )
            );   
            system_log.log_error(
                message
            );
            throw_exception(
                database_query_error(
                    message
                )
            );
        }
        boost::posix_time::ptime end(
            boost::posix_time::microsec_clock::local_time(
            )
        );
        queried.store_query_duration(
            std::string(
                query.data(
                ),
                query.size(
                )
            ),
            end - begin
        );
    }
}

#endif
