/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_netlink_socket.h"
#include <cstring>
#include "@qlib@_throw_exception.h"
#include "@qlib@_component_error.h"
#include <unistd.h>
namespace @qlib@ {
    
    
    netlink_socket::netlink_socket(
    ) :
    data(
        socket(
            PF_NETLINK,
            SOCK_RAW,
            NETLINK_GENERIC
        )
    ) {
        if (
            -1 == data
        ) {
            throw_exception(
                component_error(
                    "netlink_socket"
                )
            );
        }
        struct sockaddr_nl self;
        memset(
            &self,
            0,
            sizeof(
                self
            )
        );
        self.nl_family = AF_NETLINK;
        self.nl_pid = getpid(
        );
        if (
            0 > bind(
                data,
                reinterpret_cast<
                    struct sockaddr const*
                >(
                    &self
                ),
                sizeof(
                    self
                )
            )
        ) {
            this->~netlink_socket(
            );
            throw_exception(
                component_error(
                    "netlink_socket"
                )
            );
        }
    }
    
    netlink_socket::~netlink_socket(
    ) {
        close(
            data
        );
    }
    
    int netlink_socket::get_data(
    ) const {
        return data;
    }
}
