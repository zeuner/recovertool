/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_parse_double.h"
#include "@qlib@_component_error.h"
#include "@qlib@_throw_exception.h"
namespace @qlib@ {
    
    
    double parse_double(
        std::string const& raw
    ) {
        double value = 0;
        double factor = 1;
        bool point_passed = false;
        for (
            std::string::const_iterator added = raw.begin(
            );
            raw.end(
            ) != added;
            added++
        ) {
            switch (
                *added
            ) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                value *= 10;
                value += static_cast<
                    int
                >(
                    *added - '0'
                );
                if (
                    point_passed
                ) {
                    factor *= 0.1;
                }
                break;
            case '.':
                point_passed = true;
                break;
            default:
                throw_exception(
                    component_error(
                        "parse_double"
                    )
                );
            }
        }
        return value * factor;
    }
}
