/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef GENERIC_SOURCE_INCLUDED
#define GENERIC_SOURCE_INCLUDED

namespace @qlib@ {
    
    
    class generic_source {
    public:
        virtual bool get_sample(
            amplitude_value& retrieved
        ) = 0;
        virtual size_t get_samples(
            amplitude_value* first,
            amplitude_value* last
        ) = 0;
        virtual bool is_master(
        ) const = 0;
        virtual ~generic_source(
        );
    };
    
    template<
    >
    class bulk_audio_retrieval<
        generic_source
    > {
    public:
        static size_t get_samples(
            generic_source& source,
            amplitude_value* first,
            amplitude_value* last
        );
    };
}

#endif
