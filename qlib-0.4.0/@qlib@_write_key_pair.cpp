/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_write_key_pair.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    void write_key_pair(
        std::string const& public_key_file,
        std::string const& private_key_file
    ) {
        generated_rsa_key key;
        c_file public_key(
            public_key_file,
            "w"
        );
        if (
            1 != PEM_write_RSA_PUBKEY(
                public_key.get_data(
                ),
                key.get_data(
                )
            )
        ) {
            throw component_error(
                "write_key_pair"
            );
        }
        c_file private_key(
            private_key_file,
            "w"
        );
        if (
            1 != PEM_write_RSAPrivateKey(
                private_key.get_data(
                ),
                key.get_data(
                ),
                NULL,
                NULL,
                0,
                NULL,
                NULL
            )
        ) {
            throw component_error(
                "write_key_pair"
            );
        }
    }
}
