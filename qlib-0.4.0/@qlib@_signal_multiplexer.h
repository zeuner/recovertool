/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef SIGNAL_MULTIPLEXER_INCLUDED
#define SIGNAL_MULTIPLEXER_INCLUDED

#include <boost/shared_ptr.hpp>
#include <list>
#include <cstring>
namespace @qlib@ {
    
    
    template<
        typename playable
    >
    class signal_multiplexer {
    public:
        signal_multiplexer(
            boost::shared_ptr<
                playable
            > source
        );
        signal_multiplexer(
            signal_multiplexer<
                playable
            > const& copied
        );
        ~signal_multiplexer(
        );
        bool is_master(
        ) const;
        bool get_sample(
            amplitude_value& retrieved
        );
    private:
        boost::shared_ptr<
            boost::shared_ptr<
                playable
            >
        > source;
        boost::shared_ptr<
            resizable_buffer<
                malloc_c_allocator,
                amplitude_value
            >
        > buffered;
        boost::shared_ptr<
            size_t
        > available;
        boost::shared_ptr<
            std::list<
                std::pair<
                    size_t,
                    signal_multiplexer<
                        playable
                    >*
                >
            >
        > registered_positions;
        typename std::list<
            std::pair<
                size_t,
                signal_multiplexer<
                    playable
                >*
            >
        >::iterator own_registration;
        boost::shared_ptr<
            size_t
        > next_offset;
        size_t position;
    };
    
    template<
        typename playable
    >
    signal_multiplexer<
        playable
    >::signal_multiplexer(
        boost::shared_ptr<
            playable
        > source
    ) :
    source(
        new boost::shared_ptr<
            playable
        >(
            source
        )
    ),
    buffered(
        new resizable_buffer<
            malloc_c_allocator,
            amplitude_value
        >(
            1
        )
    ),
    available(
        new size_t(
            0
        )
    ),
    registered_positions(
        new std::list<
            std::pair<
                size_t,
                signal_multiplexer<
                    playable
                >*
            >
        >
    ),
    next_offset(
        new size_t(
            0
        )
    ),
    position(
        0
    ) {
        registered_positions->push_back(
            std::make_pair(
                position,
                this
            )
        );
        own_registration = registered_positions->begin(
        );
    }
    
    template<
        typename playable
    >
    signal_multiplexer<
        playable
    >::signal_multiplexer(
        signal_multiplexer<
            playable
        > const& copied
    ) :
    source(
        copied.source
    ),
    buffered(
        copied.buffered
    ),
    available(
        copied.available
    ),
    registered_positions(
        copied.registered_positions
    ),
    next_offset(
        copied.next_offset
    ),
    position(
        copied.position
    ) {
        own_registration = registered_positions->insert(
            copied.own_registration,
            std::make_pair(
                position,
                this
            )
        );
    }
    
    template<
        typename playable
    >
    signal_multiplexer<
        playable
    >::~signal_multiplexer(
    ) {
        registered_positions->erase(
            own_registration
        );
    }
    
    template<
        typename playable
    >
    bool signal_multiplexer<
        playable
    >::is_master(
    ) const {
        return true;
    }
    
    template<
        typename playable
    >
    bool signal_multiplexer<
        playable
    >::get_sample(
        amplitude_value& retrieved
    ) {
        position++;
        own_registration->first++;
        while (
            true
        ) {
            typename std::list<
                std::pair<
                    size_t,
                    signal_multiplexer<
                        playable
                    >*
                >
            >::iterator next = own_registration;
            next++;
            if (
                (
                    registered_positions->end(
                    ) != next
                ) && (
                    next->first < own_registration->first
                )
            ) {
                signal_multiplexer* surpassed = next->second;
                std::swap(
                    *next,
                    *own_registration
                );
                std::swap(
                    surpassed->own_registration,
                    own_registration
                );
            } else {
                break;
            }
        }
        if (
            *available >= position
        ) {
            retrieved = buffered->get_data(
            )[
                (
                    (
                        *next_offset
                    ) - (
                        *available
                    ) + (
                        position - 1
                    ) + buffered->get_size(
                    )
                ) % buffered->get_size(
                )
            ];
            return true;
        }
        if (
            !*source
        ) {
            return false;
        }
        if (
            !(
                *source
            )->get_sample(
                retrieved
            )
        ) {
            source->reset(
            );
            return false;
        }
        if (
            buffered->get_size(
            ) < (
                position - registered_positions->begin(
                )->first
            )
        ) {
            size_t const old_size = buffered->get_size(
            );
            buffered->resize(
                old_size * 2
            );
            memcpy(
                buffered->get_data(
                ) + old_size,
                buffered->get_data(
                ),
                sizeof(
                    *buffered->get_data(
                    )
                ) * old_size
            );
        }
        if (
            buffered->get_size(
            ) == (
                *next_offset
            )
        ) {
            (
                *next_offset
            ) = 0;
        }
        buffered->get_data(
        )[
            (
                *next_offset
            )
        ] = retrieved;
        (
            *available
        )++;
        (
            *next_offset
        )++;
        return true;
    }
}

#endif
