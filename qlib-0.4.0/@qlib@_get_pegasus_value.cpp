/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_get_pegasus_value.h"
#include "@qlib@_component_error.h"
#include <string>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/date_time/posix_time/time_parsers.hpp>
namespace @qlib@ {
    
    
    template<
    >
    void get_pegasus_value(
        PEGASUS_NAMESPACE(
            CIMValue
        ) const& native,
        bool& value
    ) {
        PEGASUS_NAMESPACE(
            Boolean
        ) retrieved;
        native.get(
            retrieved
        );
        value = retrieved;
    }
    
    template<
    >
    void get_pegasus_value(
        PEGASUS_NAMESPACE(
            CIMValue
        ) const& native,
        double& value
    ) {
        PEGASUS_NAMESPACE(
            Real64
        ) retrieved;
        native.get(
            retrieved
        );
        value = retrieved;
    }
    
    template<
    >
    void get_pegasus_value(
        PEGASUS_NAMESPACE(
            CIMValue
        ) const& native,
        uint32_t& value
    ) {
        PEGASUS_NAMESPACE(
            Uint32
        ) retrieved;
        native.get(
            retrieved
        );
        value = retrieved;
    }
    
    template<
    >
    void get_pegasus_value(
        PEGASUS_NAMESPACE(
            CIMValue 
        ) const& native,
        int32_t& value
    ) {
        PEGASUS_NAMESPACE(
            Sint32
        ) retrieved;
        native.get(
            retrieved
        );
        value = retrieved;
    }
    
    template<
    >
    void get_pegasus_value(
        PEGASUS_NAMESPACE(
            CIMValue
        ) const& native,
        uint64_t& value
    ) {
        PEGASUS_NAMESPACE(
            Uint64
        ) retrieved;
        native.get(
            retrieved
        );
        value = retrieved;
    }
    
    template<
    >
    void get_pegasus_value(
        PEGASUS_NAMESPACE(
            CIMValue
        ) const& native,
        std::string& value
    ) {
        PEGASUS_NAMESPACE(
            String
        ) retrieved;
        native.get(
            retrieved
        );
        value = pegasus_to_cpp(
            retrieved
        );
    }
    
    template<
    >
    void get_pegasus_value(
        PEGASUS_NAMESPACE(
            CIMValue
        ) const& native,
        boost::posix_time::ptime& value
    ) {
        PEGASUS_NAMESPACE(
            CIMDateTime
        ) retrieved;
        native.get(
            retrieved
        );
        std::string const as_string = pegasus_to_cpp(
            retrieved.toString(
            )
        );
        std::string::size_type at_time_zone = as_string.find(
            '+'
        );
        if (
            std::string::npos == at_time_zone
        ) {
            throw component_error(
                "get_pegasus_value"
            );
        }
        if (
            8 > at_time_zone
        ) {
            throw component_error(
                "get_pegasus_value"
            );
        }
        value = boost::posix_time::from_iso_string(
            std::string(
                as_string.begin(
                ),
                as_string.begin(
                ) + 8
            ) + "T" + std::string(
                as_string.begin(
                ) + 8,
                as_string.begin(
                ) + at_time_zone
            )
        );
    }
}
