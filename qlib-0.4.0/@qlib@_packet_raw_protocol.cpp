/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_packet_raw_protocol.h"
#include "@qlib@_throw_exception.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    packet_raw_protocol::packet_raw_protocol(
    ) :
    protocol_value(
        htons(
            ETH_P_ALL
        )
    ) {
    }
    
    packet_raw_protocol::packet_raw_protocol(
        int protocol_value
    ) :
    protocol_value(
        protocol_value
    ) {
    }
    
    int packet_raw_protocol::type(
    ) const {
        return SOCK_RAW;
    }
    
    int packet_raw_protocol::protocol(
    ) const {
        return protocol_value;
    }
    
    int packet_raw_protocol::family(
    ) const {
        return PF_PACKET;
    }
    
    std::size_t packet_raw_protocol::capacity(
    ) const {
        throw_exception(
            component_error(
                "packet_raw_protocol"
            )
        );
        return 0;
    }
    
    void packet_raw_protocol::resize(
        std::size_t new_size
    ) {
        throw_exception(
            component_error(
                "packet_raw_protocol"
            )
        );
    }
    
    packet_raw_protocol::endpoint::endpoint(
    ) {
    }
    
    packet_raw_protocol::endpoint::endpoint(
        struct sockaddr_ll link_layer
    ) {
        native_data.specific = link_layer;
    }
    
    int packet_raw_protocol::endpoint::type(
    ) const {
        return SOCK_RAW;
    }
    
    int packet_raw_protocol::endpoint::protocol(
    ) const {
        return htons(
            ETH_P_ALL
        );
    }
    
    std::size_t packet_raw_protocol::endpoint::size(
    ) const {
        return sizeof(
            native_data.specific
        );
    }
    
    std::size_t packet_raw_protocol::endpoint::capacity(
    ) const {
        return sizeof(
            native_data.specific
        );
    }
    
    void packet_raw_protocol::endpoint::resize(
        std::size_t new_size
    ) {
        throw_exception(
            component_error(
                "packet_raw_protocol"
            )
        );
    }
    
    boost::asio::detail::socket_addr_type* packet_raw_protocol::endpoint::data(
    ) {
        return &(
            native_data.base
        );
    }
    
    boost::asio::detail::socket_addr_type const* packet_raw_protocol::endpoint::data(
    ) const {
        return &(
            native_data.base
        );
    }
}
