/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef SERIALIZABLE_CIM_VALUE_INCLUDED
#define SERIALIZABLE_CIM_VALUE_INCLUDED

#include "@qlib@_component_error.h"
#include <string>
#include "@qlib@_deserialize.h"
#include "@qlib@_serialized.h"
namespace @qlib@ {
    
    
    class serializable_cim_value :
    public PEGASUS_NAMESPACE(
        CIMValue
    ) {
    public:
        serializable_cim_value(
        );
        template<
            typename initializing
        >
        serializable_cim_value(
            initializing const& initializer
        );
    };
    
    template<
        typename storing
    >
    class serialized_implementation<
        serializable_cim_value,
        storing
    > {
    public:
        static void apply(
            serializable_cim_value const& raw,
            storing& result
        );
    };
    
    template<
    >
    class deserialize_implementation<
        serializable_cim_value
    > {
    public:
        template<
            typename iterator
        >
        static void apply(
            iterator& from,
            iterator const& to,
            serializable_cim_value& raw,
            bool& short_data
        );
    };
    
    template<
        typename iterator
    >
    void deserialize_implementation<
        serializable_cim_value
    >::apply(
        iterator& from,
        iterator const& to,
        serializable_cim_value& raw,
        bool& short_data
    ) {
        int numeric_type;
        deserialize(
            from,
            to,
            numeric_type,
            short_data
        );
        if (
            short_data
        ) {
            return;
        }
        PEGASUS_NAMESPACE(
            CIMType
        ) const native_type = static_cast<
            PEGASUS_NAMESPACE(
                CIMType
            )
        >(
            numeric_type
        );
        switch (
            native_type
        ) {
        case PEGASUS_NAMESPACE(
            CIMTYPE_UINT32
        ):
            do {
                PEGASUS_NAMESPACE(
                    Uint32
                ) value;
                deserialize(
                    from,
                    to,
                    value,
                    short_data
                );
                if (
                    short_data
                ) {
                    return;
                }
                raw = serializable_cim_value(
                    value
                );
            } while (
                false
            );
            break;
        case PEGASUS_NAMESPACE(
            CIMTYPE_SINT32
        ):
            do {
                PEGASUS_NAMESPACE(
                    Sint32
                ) value;
                deserialize(
                    from,
                    to,
                    value,
                    short_data
                );
                if (
                    short_data
                ) {
                    return;
                }
                raw = serializable_cim_value(
                    value
                );
            } while (
                false
            );
            break;
        case PEGASUS_NAMESPACE(
            CIMTYPE_UINT64
        ):
            do {
                PEGASUS_NAMESPACE(
                    Uint64
                ) value;
                deserialize(
                    from,
                    to,
                    value,
                    short_data
                );
                if (
                    short_data
                ) {
                    return;
                }
                raw = serializable_cim_value(
                    value
                );
            } while (
                false
            );
            break;
        case PEGASUS_NAMESPACE(
            CIMTYPE_REAL64
        ):
            do {
                PEGASUS_NAMESPACE(
                    Real64
                ) value;
                deserialize(
                    from,
                    to,
                    value,
                    short_data
                );
                if (
                    short_data
                ) {
                    return;
                }
                raw = serializable_cim_value(
                    value
                );
            } while (
                false
            );
            break;
        case PEGASUS_NAMESPACE(
            CIMTYPE_STRING
        ):
            do {
                std::string value;
                deserialize(
                    from,
                    to,
                    value,
                    short_data
                );
                if (
                    short_data
                ) {
                    return;
                }
                PEGASUS_NAMESPACE(
                    String
                ) const native(
                    value.data(
                    ),
                    value.size(
                    )
                );
                raw = serializable_cim_value(
                    native
                );
            } while (
                false
            );
            break;
        case PEGASUS_NAMESPACE(
            CIMTYPE_DATETIME
        ):
            do {
                std::string string_value;
                deserialize(
                    from,
                    to,
                    string_value,
                    short_data
                );
                if (
                    short_data
                ) {
                    return;
                }
                PEGASUS_NAMESPACE(
                    String
                ) const string_native(
                    string_value.data(
                    ),
                    string_value.size(
                    )
                );
                raw = serializable_cim_value(
                    PEGASUS_NAMESPACE(
                        CIMDateTime
                    )(
                        string_native
                    )
                );
            } while (
                false
            );
            break;
        default:
            throw component_error(
                "serializable_cim_value"
            );
        }
    }
    
    template<
        typename storing
    >
    void serialized_implementation<
        serializable_cim_value,
        storing
    >::apply(
        serializable_cim_value const& raw,
        storing& result
    ) {
        PEGASUS_NAMESPACE(
            CIMType
        ) const native_type = raw.getType(
        );
        int const numeric_type = native_type;
        serialized(
            numeric_type,
            result
        );
        switch (
            native_type
        ) {
        case PEGASUS_NAMESPACE(
            CIMTYPE_UINT32
        ):
            do {
                PEGASUS_NAMESPACE(
                    Uint32
                ) typed;
                raw.get(
                    typed
                );
                serialized(
                    typed,
                    result
                );
            } while (
                false
            );
            break;
        case PEGASUS_NAMESPACE(
            CIMTYPE_SINT32
        ):
            do {
                PEGASUS_NAMESPACE(
                    Sint32
                ) typed;
                raw.get(
                    typed
                );
                serialized(
                    typed,
                    result
                );
            } while (
                false
            );
            break;
        case PEGASUS_NAMESPACE(
            CIMTYPE_UINT64
        ):
            do {
                PEGASUS_NAMESPACE(
                    Uint64
                ) typed;
                raw.get(
                    typed
                );
                serialized(
                    typed,
                    result
                );
            } while (
                false
            );
            break;
        case PEGASUS_NAMESPACE(
            CIMTYPE_REAL64
        ):
            do {
                PEGASUS_NAMESPACE(
                    Real64
                ) typed;
                raw.get(
                    typed
                );
                serialized(
                    typed,
                    result
                );
            } while (
                false
            );
            break;
        case PEGASUS_NAMESPACE(
            CIMTYPE_STRING
        ):
            do {
                PEGASUS_NAMESPACE(
                    String
                ) typed;
                raw.get(
                    typed
                );
                serialized(
                    pegasus_to_cpp(
                        typed
                    ),
                    result
                );
            } while (
                false
            );
            break;
        case PEGASUS_NAMESPACE(
            CIMTYPE_DATETIME
        ):
            do {
                PEGASUS_NAMESPACE(
                    CIMDateTime
                ) typed;
                raw.get(
                    typed
                );
                serialized(
                    pegasus_to_cpp(
                        typed.toString(
                        )
                    ),
                    result
                );
            } while (
                false
            );
            break;
        default:
            throw component_error(
                "serializable_cim_value"
            );
        }
    }
    
    template<
        typename initializing
    >
    serializable_cim_value::serializable_cim_value(
        initializing const& initializer
    ) :
    PEGASUS_NAMESPACE(
        CIMValue
    )(
        initializer
    ) {
    }
}

#endif
