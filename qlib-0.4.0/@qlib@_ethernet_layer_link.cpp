/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_ethernet_layer_link.h"
#include "@qlib@_component_error.h"
#include <cstring>
namespace @qlib@ {
    
    
    ethernet_layer_link::ethernet_layer_link(
        std::string const& interface,
        uint16_t network_protocol
    ) {
        memset(
            &native_header,
            0,
            sizeof(
                native_header
            )
        );
        std::string const source_mac = interface_mac_address(
            interface
        );
        if (
            6 < source_mac.size(
            )
        ) {
            throw component_error(
                "ethernet_layer_link"
            );
        }
        memcpy(
            native_header.ether_shost,
            source_mac.data(
            ),
            source_mac.size(
            )
        );
        std::string const destination_mac = interface_next_hop_mac(
            interface
        );
        if (
            6 < destination_mac.size(
            )
        ) {
            throw component_error(
                "ethernet_layer_link"
            );
        }
        memcpy(
            native_header.ether_dhost,
            destination_mac.data(
            ),
            destination_mac.size(
            )
        );
        native_header.ether_type = htons(
            network_protocol
        );
    }
    
    void ethernet_layer_link::write_header(
        struct ether_header& written
    ) const {
        memcpy(
            &written,
            &native_header,
            sizeof(
                native_header
            )
        );
    }
}
