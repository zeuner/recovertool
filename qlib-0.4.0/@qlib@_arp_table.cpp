/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_arp_table.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    std::map<
        std::string,
        std::map<
            std::string,
            std::string
        >
    > arp_table(
    ) {
        std::ifstream data(
            "/proc/net/arp"
        );
        std::string line;
        if (
            !std::getline(
                data,
                line
            )
        ) {
            throw component_error(
                "arp_table"
            );
        }
        std::map<
            std::string,
            std::map<
                std::string,
                std::string
            >
        > result;
        while (
            std::getline(
                data,
                line
            )
        ) {
            if (
                line.empty(
                )
            ) {
                throw component_error(
                    "arp_table"
                );
            }
            std::string::const_iterator const after_device = line.end(
            );
            std::string::const_iterator const before_ip = line.begin(
            );
            std::string::const_iterator after_ip = before_ip;
            while (
                ' ' != *after_ip
            ) {
                after_ip++;
                if (
                    after_device == after_ip
                ) {
                    throw component_error(
                    "arp_table"
                    );
                }
            }
            std::string::const_iterator before_mac = after_ip;
            while (
                ' ' == *before_mac
            ) {
                before_mac++;
                if (
                    after_device == before_mac
                ) {
                    throw component_error(
                    "arp_table"
                    );
                }
            }
            while (
                ' ' != *before_mac
            ) {
                before_mac++;
                if (
                    after_device == before_mac
                ) {
                    throw component_error(
                    "arp_table"
                    );
                }
            }
            while (
                ' ' == *before_mac
            ) {
                before_mac++;
                if (
                    after_device == before_mac
                ) {
                    throw component_error(
                    "arp_table"
                    );
                }
            }
            while (
                ' ' != *before_mac
            ) {
                before_mac++;
                if (
                    after_device == before_mac
                ) {
                    throw component_error(
                    "arp_table"
                    );
                }
            }
            while (
                ' ' == *before_mac
            ) {
                before_mac++;
                if (
                    after_device == before_mac
                ) {
                    throw component_error(
                    "arp_table"
                    );
                }
            }
            std::string::const_iterator after_mac = before_mac;
            while (
                ' ' != *after_mac
            ) {
                after_mac++;
                if (
                    after_device == after_mac
                ) {
                    throw component_error(
                    "arp_table"
                    );
                }
            }
            std::string::const_iterator before_device = after_mac;
            while (
                ' ' == *before_device
            ) {
                before_device++;
                if (
                    after_device == before_device
                ) {
                    throw component_error(
                    "arp_table"
                    );
                }
            }
            while (
                ' ' != *before_device
            ) {
                before_device++;
                if (
                    after_device == before_device
                ) {
                    throw component_error(
                    "arp_table"
                    );
                }
            }
            while (
                ' ' == *before_device
            ) {
                before_device++;
                if (
                    after_device == before_device
                ) {
                    throw component_error(
                    "arp_table"
                    );
                }
            }
            result[
                std::string(
                    before_device,
                    after_device
                )
            ][
                std::string(
                    before_ip,
                    after_ip
                )
            ] = std::string(
                before_mac,
                after_mac
            );
        }
        return result;
    }
}
