/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef SOCKS5_PASSWORD_AUTHENTICATOR_INCLUDED
#define SOCKS5_PASSWORD_AUTHENTICATOR_INCLUDED

#include <boost/bind.hpp>
#include <string>
#include <boost/asio/write.hpp>
#include "@qlib@_component_error.h"
#include <boost/asio/placeholders.hpp>
namespace @qlib@ {
    
    
    template<
        typename connected
    >
    class socks5_password_authenticator {
    public:
        socks5_password_authenticator(
            std::string const& username,
            std::string const& password
        );
        void async_authenticate(
            connected& socket,
            socks5_listener<
                connected
            >& protocol
        );
    protected:
        void handle_username_length_read(
            boost::system::error_code const& error,
            size_t bytes
        );
        void handle_username_read(
            size_t username_length,
            boost::system::error_code const& error,
            size_t bytes
        );
        void handle_password_read(
            size_t password_length,
            boost::system::error_code const& error,
            size_t bytes
        );
        void handle_response_write(
            boost::system::error_code const& error
        );
    private:
        connected* socket;
        socks5_listener<
            connected
        >* protocol_handler;
        std::string expected_username;
        std::string expected_password;
        enum {
            max_data = 0x100
        };
        char input[
            max_data
        ];
        std::string output;
    };
    
    template<
        typename connected
    >
    socks5_password_authenticator<
        connected
    >::socks5_password_authenticator(
        std::string const& username,
        std::string const& password
    ) :
    expected_username(
        username
    ),
    expected_password(
        password
    ) {
    }
    
    template<
        typename connected
    >
    void socks5_password_authenticator<
        connected
    >::async_authenticate(
        connected& socket,
        socks5_listener<
            connected
        >& protocol
    ) {
        this->socket = &socket;
        protocol_handler = &protocol;
        boost::asio::async_read(
            socket,
            boost::asio::buffer(
                input,
                2
            ),
            boost::asio::transfer_at_least(
                2
            ),
            boost::bind(
                &socks5_password_authenticator<
                    connected
                >::handle_username_length_read,
                this,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
    
    template<
        typename connected
    >
    void socks5_password_authenticator<
        connected
    >::handle_username_length_read(
        boost::system::error_code const& error,
        size_t bytes
    ) {
        if (
            error
        ) {
            protocol_handler->handle_error(
                error
            );
            return;
        }
        if (
            2 != bytes
        ) {
            throw component_error(
                "socks5_password_authenticator"
            );
        }
        if (
            '\x01' != input[
                0
            ]
        ) {
            protocol_handler->handle_error(
                boost::system::error_code(
                    socks5_error_codes::authentication_method_error,
                    socks5_category(
                    )
                )
            );
            return;
        }
        int const username_length = static_cast<
            int
        >(
            input[
                1
            ]
        );
        if (
            max_data < username_length
        ) {
            protocol_handler->handle_error(
                boost::system::error_code(
                    socks5_error_codes::authentication_method_error,
                    socks5_category(
                    )
                )
            );
            return;
        }
        boost::asio::async_read(
            *socket,
            boost::asio::buffer(
                input,
                username_length + 1
            ),
            boost::asio::transfer_at_least(
                username_length + 1
            ),
            boost::bind(
                &socks5_password_authenticator<
                    connected
                >::handle_username_read,
                this,
                username_length,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
    
    template<
        typename connected
    >
    void socks5_password_authenticator<
        connected
    >::handle_username_read(
        size_t username_length,
        boost::system::error_code const& error,
        size_t bytes
    ) {
        if (
            error
        ) {
            protocol_handler->handle_error(
                error
            );
            return;
        }
        if (
            (
                username_length + 1
            ) != bytes
        ) {
            throw component_error(
                "socks5_password_authenticator"
            );
        }
        std::string const username(
            input,
            username_length
        );
        if (
            expected_username != username
        ) {
            protocol_handler->handle_error(
                boost::system::error_code(
                    socks5_error_codes::authentication_failed,
                    socks5_category(
                    )
                )
            );
            return;
        }
        int const password_length = static_cast<
            int
        >(
            input[
                username_length
            ]
        );
        if (
            max_data < password_length
        ) {
            protocol_handler->handle_error(
                boost::system::error_code(
                    socks5_error_codes::authentication_method_error,
                    socks5_category(
                    )
                )
            );
            return;
        }
        boost::asio::async_read(
            *socket,
            boost::asio::buffer(
                input,
                password_length
            ),
            boost::asio::transfer_at_least(
                password_length
            ),
            boost::bind(
                &socks5_password_authenticator<
                    connected
                >::handle_password_read,
                this,
                password_length,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
    
    template<
        typename connected
    >
    void socks5_password_authenticator<
        connected
    >::handle_password_read(
        size_t password_length,
        boost::system::error_code const& error,
        size_t bytes
    ) {
        if (
            error
        ) {
            protocol_handler->handle_error(
                error
            );
            return;
        }
        if (
            password_length != bytes
        ) {
            throw component_error(
                "socks5_password_authenticator"
            );
        }
        std::string const password(
            input,
            password_length
        );
        if (
            expected_password != password
        ) {
            protocol_handler->handle_error(
                boost::system::error_code(
                    socks5_error_codes::authentication_failed,
                    socks5_category(
                    )
                )
            );
            return;
        }
        output = std::string(
            "\x01\x00",
            2
        );
        boost::asio::async_write(
            *socket,
            boost::asio::buffer(
                output.data(
                ),
                output.size(
                )
            ),
            boost::bind(
                &socks5_password_authenticator<
                    connected
                >::handle_response_write,
                this,
                boost::asio::placeholders::error
            )
        );
    }
    
    template<
        typename connected
    >
    void socks5_password_authenticator<
        connected
    >::handle_response_write(
        boost::system::error_code const& error
    ) {
        if (
            error
        ) {
            protocol_handler->handle_error(
                error
            );
            return;
        }
        protocol_handler->async_read_command(
        );
    }
}

#endif
