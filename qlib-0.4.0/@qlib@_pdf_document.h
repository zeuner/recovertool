/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef PDF_DOCUMENT_INCLUDED
#define PDF_DOCUMENT_INCLUDED

#include <string>
namespace @qlib@ {
    
    
    class pdf_document {
    public:
        pdf_document(
        );
        ~pdf_document(
        );
        std::string get_data(
        );
        HPDF_Page add_page(
        );
        HPDF_Font get_font(
            std::string const& name
        );
    private:
        static void error_handler(
            HPDF_STATUS error_number,
            HPDF_STATUS detail_number,
            void* user_data
        );
        HPDF_Doc native;
    };
}

#endif
