/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_csv_splitted.h"
namespace @qlib@ {
    
    
    std::list<
        std::string
    > csv_splitted(
        std::string const& splitted
    ) {
        std::list<
            std::string
        > result;
        std::string::const_iterator position = splitted.begin(
        );
        std::string current_value;
        enum {
            initial,
            quoted,
        } state = initial;
        while (
            splitted.end(
            ) != position
        ) {
            switch (
                *position
            ) {
            case '"':
                switch (
                    state
                ) {
                case initial:
                    state = quoted;
                    break;
                case quoted:
                    state = initial;
                }
                position++;
                break;
            case ',':
                switch (
                    state
                ) {
                case initial:
                    result.push_back(
                        current_value
                    );
                    current_value.clear(
                    );
                    break;
                case quoted:
                    current_value += *position;
                }
                position++;
                break;
            default:
                current_value += *position;
                position++;
            }
        }
        result.push_back(
            current_value
        );
        return result;
    }
}
