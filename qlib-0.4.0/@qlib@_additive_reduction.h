/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef ADDITIVE_REDUCTION_INCLUDED
#define ADDITIVE_REDUCTION_INCLUDED

namespace @qlib@ {
    
    
    template<
        typename valued
    >
    class additive_reduction {
    public:
        static valued initial_value(
        );
        static valued reduced(
            valued one,
            valued other
        );
        static void reduce(
            valued& one,
            valued other
        );
    };
    
    template<
        typename valued
    >
    valued additive_reduction<
        valued
    >::initial_value(
    ) {
        return real_number(
            0
        );
    }
    
    template<
        typename valued
    >
    valued additive_reduction<
        valued
    >::reduced(
        valued one,
        valued other
    ) {
        return one + other;
    }
    
    template<
        typename valued
    >
    void additive_reduction<
        valued
    >::reduce(
        valued& one,
        valued other
    ) {
        one += other;
    }
}

#endif
