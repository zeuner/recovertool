/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_process_branch.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    process_branch::process_branch(
    ) :
    child(
        fork(
        )
    ),
    parent_done(
        false
    ) {
        if (
            0 > child
        ) {
            throw component_error(
                "process_branch"
            );
        }
        if (
            0 == child
        ) {
            parent_connection = this;
            signal(
                SIGUSR1,
                &process_branch::handle_done_signal
            );
        }
    }
    
    process_branch::~process_branch(
    ) {
        if (
            0 != child
        ) {
            kill(
                child,
                SIGUSR1
            );
            waitpid(
                child,
                NULL,
                0
            );
        }
    }
    
    bool process_branch::is_child(
    ) const {
        return 0 == child;
    }
    
    bool process_branch::is_parent_done(
    ) const {
        return parent_done;
    }
    
    void process_branch::handle_done_signal(
        int signal_number
    ) {
        if (
            signal_number != SIGUSR1
        ) {
            throw component_error(
                "process_branch"
            );
        }
        parent_connection->parent_done = true;
    }
    
    process_branch* process_branch::parent_connection = NULL;
}
