/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef HTTP_CLIENT_REQUEST_INCLUDED
#define HTTP_CLIENT_REQUEST_INCLUDED

#include <boost/function.hpp>
#include "@qlib@_http_peer_request.h"
namespace @qlib@ {
    
    
    template<
        typename accepting
    >
    class http_client_request :
    public http_peer_request<
        accepting
    > {
    public:
        http_client_request(
            accepting& acceptor
        );
        void async_respond(
            typename http_client_request<
                accepting
            >::response_type const* response,
            boost::function<
                void(
                    boost::system::error_code const& error
                )
            > const& response_handler
        );
        bool get_headers_sent(
        ) const;
        void set_headers_sent(
        );
    private:
        bool headers_sent;
    };
    
    template<
        typename accepting
    >
    http_client_request<
        accepting
    >::http_client_request(
        accepting& acceptor
    ) :
    http_peer_request<
        accepting
    >(
        acceptor
    ),
    headers_sent(
        false
    ) {
    }
    
    template<
        typename accepting
    >
    void http_client_request<
        accepting
    >::async_respond(
        typename http_client_request<
            accepting
        >::response_type const* response,
        boost::function<
            void(
                boost::system::error_code const& error
            )
        > const& response_handler
    ) {
        this->get_transmitter(
        ).async_respond(
            *this,
            *response,
            response_handler
        );
    }
    
    template<
        typename accepting
    >
    bool http_client_request<
        accepting
    >::get_headers_sent(
    ) const {
        return headers_sent;
    }
    
    template<
        typename accepting
    >
    void http_client_request<
        accepting
    >::set_headers_sent(
    ) {
        headers_sent = true;
    }
}

#endif
