/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef FEEDBACKED_TERMINATED_SOURCE_INCLUDED
#define FEEDBACKED_TERMINATED_SOURCE_INCLUDED

#include <map>
#include "@qlib@_component_error.h"
#include <boost/noncopyable.hpp>
namespace @qlib@ {
    
    
    template<
        typename playable
    >
    class feedbacked_terminated_source :
    private boost::noncopyable {
    public:
        feedbacked_terminated_source(
            playable& source,
            std::map<
                int,
                amplitude_value
            > const& amplitudes,
            time_value epsilon = 0
        );
        bool is_master(
        ) const;
        bool get_sample(
            amplitude_value& retrieved
        );
    private:
        playable& source;
        std::map<
            int,
            amplitude_value
        > amplitudes;
        resizable_buffer<
            malloc_c_allocator,
            amplitude_value
        > history;
        amplitude_value* next;
        amplitude_value* end;
        time_value epsilon;
        size_t low_values;
    };
    
    template<
        typename playable
    >
    feedbacked_terminated_source<
        playable
    >::feedbacked_terminated_source(
        playable& source,
        std::map<
            int,
            amplitude_value
        > const& amplitudes,
        time_value epsilon
    ) :
    source(
        source
    ),
    amplitudes(
        amplitudes
    ),
    epsilon(
        epsilon
    ),
    low_values(
        0
    ) {
        std::map<
            int,
            amplitude_value
        >::const_iterator last = amplitudes.end(
        );
        if (
            amplitudes.begin(
            ) == last
        ) {
            throw component_error(
                "feedbacked_terminated_source"
            );
        }
        last--;
        history.resize(
            last->first + 1
        );
        for (
            size_t initializing = 0;
            history.get_size(
            ) > initializing;
            initializing++
        ) {
            history.get_data(
            )[
                initializing
            ] = 0;
        }
        next = history.get_data(
        );
        end = next + history.get_size(
        );
    }
    
    template<
        typename playable
    >
    bool feedbacked_terminated_source<
        playable
    >::is_master(
    ) const {
        return true;
    }
    
    template<
        typename playable
    >
    bool feedbacked_terminated_source<
        playable
    >::get_sample(
        amplitude_value& retrieved
    ) {
        bool source_done = false;
        if (
            !source.get_sample(
                *next
            )
        ) {
            if (
                history.get_size(
                ) <= low_values
            ) {
                return false;
            }
            *next = 0;
            source_done = true;
        }
        for (
            std::map<
                int,
                amplitude_value
            >::const_iterator accumulating = amplitudes.begin(
            );
            amplitudes.end(
            ) != accumulating;
            accumulating++
        ) {
            amplitude_value* value = next - accumulating->first;
            if (
                history.get_data(
                ) > value
            ) {
                value += history.get_size(
                );
            }
            *next += accumulating->second * (
                *value
            );
        }
        retrieved = *next;
        next++;
        if (
            end <= next
        ) {
            next = history.get_data(
            );
        }
        if (
            source_done
        ) {
            if (
                epsilon >= abs(
                    *next
                )
            ) {
                low_values++;
            } else {
                low_values = 0;
            }
        }
        return true;
    }
}

#endif
