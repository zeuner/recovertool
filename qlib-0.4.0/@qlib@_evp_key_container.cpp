/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_evp_key_container.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    evp_key_container::evp_key_container(
    ) :
    data(
        EVP_PKEY_new(
        )
    ) {
        if (
            NULL == data
        ) {
            throw component_error(
                "evp_key_container"
            );
        }
    }
    
    evp_key_container::~evp_key_container(
    ) {
        EVP_PKEY_free(
            data
        );
    }
    
    EVP_PKEY* evp_key_container::get_data(
    ) const {
        return data;
    }
    
    void evp_key_container::set_native_key(
        generated_rsa_key const& key
    ) {
        if (
            1 != EVP_PKEY_set1_RSA(
                data,
                key.get_data(
                )
            )
        ) {
            throw component_error(
                "evp_key_container"
            );
        }
    }
    
    void evp_key_container::write_key_data(
        memory_sink_bio& destination
    ) {
        if (
            1 != PEM_write_bio_PrivateKey(
                destination.get_data(
                ),
                data,
                NULL,
                NULL,
                0,
                NULL,
                NULL
            )
        ) {
            throw component_error(
                "evp_key_container"
            );
        }
    }
}
