/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_byte_random_source.h"
namespace @qlib@ {
    
    
    byte_random_source::byte_random_source(
    ) :
    distribution(
        0,
        0xff
    ),
    chooser(
        generator,
        distribution
    ) {
        randomizing::result_type seed;
        randomizing::result_type parallel = boost::process::self::get_instance(
        ).get_id(
        );
        parallel <<= 4;
        seed = boost::posix_time::microsec_clock::local_time(
        ).time_of_day(
        ).total_microseconds(
        );
        seed += parallel;
        seed += reinterpret_cast<
            intptr_t
        >(
            this
        );
        generator.seed(
            seed
        );
    }
    
    byte_random_source::byte_random_source(
        std::vector<
            seed_value_type
        > const& seed
    ) :
    distribution(
        0,
        0xff
    ),
    chooser(
        generator,
        distribution
    ) {
        std::vector<
            seed_value_type
        >::const_iterator from = seed.begin(
        );
        std::vector<
            seed_value_type
        >::const_iterator const to = seed.end(
        );
        generator.seed(
            from,
            to
        );
    }
    
    unsigned char byte_random_source::operator(
    )(
    ) {
        return chooser(
        );
    }
}
