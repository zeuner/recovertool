/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef CREATE_CMPI_INSTANCE_INCLUDED
#define CREATE_CMPI_INSTANCE_INCLUDED

#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    template<
        typename managed
    >
    CMPIInstance* create_cmpi_instance(
        CMPIBroker const* broker,
        CMPIStatus& result,
        char const* cim_namespace,
        managed* instantiated
    ) {
        CMPIInstance* constructed = CMNewInstance(
            broker,
            CMNewObjectPath(
                broker,
                cim_namespace,
                instantiated->get_class_name(
                ).c_str(
                ),
                &result
            ),
            &result
        );
        if (
            (
                CMPI_RC_OK != result.rc
            ) || CMIsNullObject(
                constructed
            )
        ) {
            throw component_error(
                "create_cmpi_instance"
            );
        }
        set_cmpi_property(
            broker,
            constructed,
            instantiated->get_key_name(
            ),
            instantiated->get_key(
            )
        );
        cmpi_property_setter setting(
            broker,
            constructed
        );
        instantiated->visit_properties(
            setting
        );
        return constructed;
    }
}

#endif
