/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef POSIX_FILE_INCLUDED
#define POSIX_FILE_INCLUDED

#include <boost/noncopyable.hpp>
#include <string>
namespace @qlib@ {
    
    
    class posix_file :
    private boost::noncopyable {
    public:
        posix_file(
            std::string const& filename,
            int flags
        );
        posix_file(
            std::string const& filename,
            int flags,
            mode_t mode
        );
        ~posix_file(
        );
        int get_data(
        ) const;
        off_t get_size(
        ) const;
    private:
        int const data;
    };
}

#endif
