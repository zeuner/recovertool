/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_database_result.h"
namespace @qlib@ {
    
    
    database_result::database_result(
        database_connection& queried,
        std::string const& query
    ) :
    result(
        queried.query_result(
            query
        )
    ) {
    }
    
    database_result::row_type database_result::fetch_row(
    ) {
        return mysql_fetch_row(
            result
        );
    }
    
    database_result::~database_result(
    ) {
        mysql_free_result(
            result      
        );
    }
    
    std::basic_string<
        unsigned char
    > database_result::get_binary_column(
        row_type& current,
        int column
    ) {
        std::basic_string<
            unsigned char
        > result;
        get_binary_column(
            current,
            column,
            result
        );
        return result;
    }
}
