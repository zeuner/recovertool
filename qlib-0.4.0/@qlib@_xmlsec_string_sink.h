/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef XMLSEC_STRING_SINK_INCLUDED
#define XMLSEC_STRING_SINK_INCLUDED

#include <cstring>
namespace @qlib@ {
    
    
    template<
        typename storing
    >
    class xmlsec_string_sink {
    public:
        xmlsec_string_sink(
        );
        storing const& get_data(
        ) const;
        static xmlSecTransformId get_transform_id(
        );
        static xmlsec_string_sink* get_object(
            xmlSecTransformPtr accessed
        );
    protected:
        static int initialize(
            xmlSecTransformPtr initialized
        );
        static void finalize(
            xmlSecTransformPtr finalized
        );
        static int push_binary(
            xmlSecTransformPtr receiving,
            xmlSecByte const* data,
            xmlSecSize size,
            int final,
            xmlSecTransformCtxPtr context
        );
        static int pop_binary(
            xmlSecTransformPtr sending,
            xmlSecByte* data,
            xmlSecSize max_size,
            xmlSecSize* size,
            xmlSecTransformCtxPtr context
        );
    private:
        storing gathered;
        typename storing::const_iterator popped;
    };
    
    template<
        typename storing
    >
    xmlsec_string_sink<
        storing
    >::xmlsec_string_sink(
    ) :
    popped(
        gathered.begin(
        )
    ) {
    }
    
    template<
        typename storing
    >
    storing const& xmlsec_string_sink<
        storing
    >::get_data(
    ) const {
        return gathered;
    }
    
    template<
        typename storing
    >
    xmlSecTransformId xmlsec_string_sink<
        storing
    >::get_transform_id(
    ) {
        static xmlSecTransformKlass self = {
            sizeof(
                xmlSecTransformKlass
            ),
            sizeof(
                xmlSecTransform
            ) + sizeof(
                xmlsec_string_sink<
                    storing
                >
            ),
            BAD_CAST "xmlsec_string_sink",
            NULL,
            xmlSecTransformUsageSignatureMethod,
            &xmlsec_string_sink<
                storing
            >::initialize,
            &xmlsec_string_sink<
                storing
            >::finalize,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            &xmlSecTransformDefaultGetDataType,
            &xmlsec_string_sink<
                storing
            >::push_binary,
            &xmlsec_string_sink<
                storing
            >::pop_binary,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
        };
        return static_cast<
            xmlSecTransformId
        >(
            &self
        );
    }
    
    template<
        typename storing
    >
    int xmlsec_string_sink<
        storing
    >::initialize(
        xmlSecTransformPtr initialized
    ) {
        try {
            new(
                get_object(
                    initialized
                )
            ) xmlsec_string_sink<
                storing
            >;
            return 0;
        } catch (
            std::exception& caught
        ) {
            return -1;
        }
    }
    
    template<
        typename storing
    >
    void xmlsec_string_sink<
        storing
    >::finalize(
        xmlSecTransformPtr finalized
    ) {
        get_object(
            finalized
        )->~xmlsec_string_sink(
        );
    }
    
    template<
        typename storing
    >
    xmlsec_string_sink<
        storing
    >* xmlsec_string_sink<
        storing
    >::get_object(
        xmlSecTransformPtr accessed
    ) {
        return reinterpret_cast<
            xmlsec_string_sink<
                storing
            >*
        >(
            reinterpret_cast<
                char*
            >(
                accessed
            ) + sizeof(
                xmlSecTransform
            )
        );
    }
    
    template<
        typename storing
    >
    int xmlsec_string_sink<
        storing
    >::push_binary(
        xmlSecTransformPtr receiving,
        xmlSecByte const* data,
        xmlSecSize size,
        int final,
        xmlSecTransformCtxPtr context
    ) {
        get_object(
            receiving
        )->gathered += storing(
            data,
            size
        );
        return 0;
    }
    
    template<
        typename storing
    >
    int xmlsec_string_sink<
        storing
    >::pop_binary(
        xmlSecTransformPtr sending,
        xmlSecByte* data,
        xmlSecSize max_size,
        xmlSecSize* size,
        xmlSecTransformCtxPtr context
    ) {
        xmlsec_string_sink* self = get_object(
            sending
        );
        xmlSecSize popping = self->gathered.end(
        ) - self->popped;
        if (
            max_size < popping
        ) {
            popping = max_size;
        }
        memcpy(
            data,
            self->gathered.data(
            ),
            popping
        );
        *size = popping;
        self->popped += popping;
        return 0;
    }
}

#endif
