/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef SOCKS5_SOCKET_INCLUDED
#define SOCKS5_SOCKET_INCLUDED

#include <boost/asio/ip/tcp.hpp>
#include <string>
#include <boost/function.hpp>
namespace @qlib@ {
    
    
    class socks5_socket :
    public tcp_socket {
    public:
        socks5_socket(
            boost::asio::io_service& communicating,
            boost::asio::ip::tcp::endpoint const& proxy
        );
        void async_connect(
            boost::asio::ip::tcp::endpoint const& endpoint,
            boost::function<
                void(
                    boost::system::error_code const& error
                )
            > connect_handler
        );
        void async_connect(
            std::string const& hostname,
            unsigned short port,
            boost::function<
                void(
                    boost::system::error_code const& error
                )
            > const& connect_handler
        );
        boost::asio::ip::tcp::endpoint local_endpoint(
        ) const;
    protected:
        void handle_socks_connect_resolved(
            boost::system::error_code const& error,
            boost::asio::ip::tcp::endpoint endpoint
        );
        void handle_socks_connect_resolving(
            boost::system::error_code const& error,
            std::string hostname,
            unsigned short port
        );
        void handle_socks_write(
            boost::system::error_code const& error,
            size_t transferred
        );
        void handle_socks_read(
            boost::system::error_code const& error,
            size_t transferred
        );
    private:
        boost::asio::ip::tcp::endpoint proxy;
        boost::function<
            void(
                boost::system::error_code const& error
            )
        > connect_handler;
        std::string connect_command;
        boost::asio::ip::tcp::endpoint local;
        enum {
            connect_response_size = 12
        };
        char connect_response[
            connect_response_size
        ];
    };
}

#endif
