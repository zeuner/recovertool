/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef CONNECTED_IPV4_SOCKET_INCLUDED
#define CONNECTED_IPV4_SOCKET_INCLUDED

#include <boost/function.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ip/address_v4.hpp>
namespace @qlib@ {
    
    
    class connected_ipv4_socket :
    public boost::asio::ip::tcp::socket {
    public:
        static inline boost::arg<
            2
        > connected_argument(
        );
        connected_ipv4_socket(
            boost::asio::io_service& io_service,
            boost::asio::ip::address_v4 const& destination,
            short port,
            boost::function<
                void(
                    boost::system::error_code const& error,
                    connected_ipv4_socket* connected
                )
            > const& connect_handler
        );
        connected_ipv4_socket(
            boost::asio::io_service& io_service,
            boost::asio::ip::address_v4 const& destination,
            short port,
            boost::function<
                void(
                    boost::system::error_code const& error,
                    connected_ipv4_socket* connected
                )
            > const& connect_handler,
            boost::asio::ip::tcp::endpoint const& local_endpoint
        );
    protected:
        void async_connect(
            boost::asio::ip::address_v4 const& destination,
            short port
        );
        void handle_connect(
            boost::system::error_code const& error
        );
    private:
        boost::function<
            void(
                boost::system::error_code const& error,
                connected_ipv4_socket* connected
            )
        > connect_handler;
    };
    
    boost::arg<
        2
    > connected_ipv4_socket::connected_argument(
    ) {
        return boost::arg<
            2
        >(
        );
    }
}

#endif
