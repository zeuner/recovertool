/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_interface_mac_address.h"
#include <unistd.h>
namespace @qlib@ {
    
    
    std::string interface_mac_address(
        std::string const& interface
    ) {
        struct ifreq interface_request;
        int descriptor = socket(
            AF_INET,
            SOCK_DGRAM,
            0
        );
        interface_request.ifr_addr.sa_family = AF_INET;
        strncpy(
            interface_request.ifr_name,
            interface.c_str(
            ),
            IFNAMSIZ - 1
        );
        ioctl(
            descriptor,
            SIOCGIFHWADDR,
            &interface_request
        );
        std::string const result(
            reinterpret_cast<
                const char*
            >(
                interface_request.ifr_hwaddr.sa_data
            ),
            6
        );
        close(
            descriptor
        );
        return result;
    }
}
