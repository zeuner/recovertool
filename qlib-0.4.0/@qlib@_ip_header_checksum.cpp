/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_ip_header_checksum.h"
namespace @qlib@ {
    
    
    unsigned short ip_header_checksum(
        unsigned short const* data,
        size_t size
    ) {
        int sum = 0;
        while (
            size > 0
        ) {
            if (
                1 == size
            ) {
                unsigned short padded = 0;
                *reinterpret_cast<
                    unsigned char*
                >(
                    &padded
                ) = *reinterpret_cast<
                    unsigned char const*
                >(
                    data
                );
                size--;
                data++;
            } else {
                sum += *data;
                data++;
                size -= 2;
            }
        }
        sum = (
            sum >> 16
        ) + (
            sum & (
                (
                    1 << 16
                ) - 1
            )
        );
        sum = (
            sum >> 16
        ) + (
            sum & (
                (
                    1 << 16
                ) - 1
            )
        );
        return ~sum;
    }
}
