/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_xmlsec_signing_context.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    xmlsec_signing_context::xmlsec_signing_context(
        xmlsec_key_manager_base const& key_manager
    ) :
    data(
        xmlSecDSigCtxCreate(
            key_manager.get_data(
            )
        )
    ) {
        if (
            NULL == data
        ) {
            throw component_error(
                "xmlsec_signing_context"
            );
        }
    }
    
    xmlsec_signing_context::~xmlsec_signing_context(
    ) {
        xmlSecDSigCtxDestroy(
            data
        );
    }
    
    xmlSecDSigCtxPtr xmlsec_signing_context::get_data(
    ) const {
        return data;
    }
}
