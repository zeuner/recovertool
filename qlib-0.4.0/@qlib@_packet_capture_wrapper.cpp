/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_packet_capture_wrapper.h"
#include "@qlib@_component_error.h"
#include "@qlib@_throw_exception.h"
#include <unistd.h>
#include <stdexcept>
namespace @qlib@ {
    
    
    packet_capture_wrapper::packet_capture_wrapper(
        std::string const& interface,
        bool promiscuous,
        size_t snapshot_length,
        size_t buffer_size,
        int timeout_milliseconds
    ) :
    pcap_handle(
        pcap_create(
            interface.c_str(
            ),
            error
        )
    ),
    snapshot_length(
        snapshot_length
    ) {
        if (
            NULL == pcap_handle
        ) {
            throw_exception(
                component_error(
                    "packet_capture_wrapper"
                )
            );
        }
        if (
            0 != pcap_set_snaplen(
                pcap_handle,
                snapshot_length
            )
        ) {
            this->~packet_capture_wrapper(
            );
            throw_exception(
                component_error(
                    "packet_capture_wrapper"
                )
            );
        }
        if (
            0 != pcap_set_buffer_size(
                pcap_handle,
                buffer_size
            )
        ) {
            this->~packet_capture_wrapper(
            );
            throw_exception(
                component_error(
                    "packet_capture_wrapper"
                )
            );
        }
        if (
            0 != pcap_set_promisc(
                pcap_handle,
                promiscuous ? 1 : 0
            )
        ) {
            this->~packet_capture_wrapper(
            );
            throw_exception(
                component_error(
                    "packet_capture_wrapper"
                )
            );
        }
        if (
            0 != pcap_set_timeout(
                pcap_handle,
                timeout_milliseconds
            )
        ) {
            this->~packet_capture_wrapper(
            );
            throw_exception(
                component_error(
                    "packet_capture_wrapper"
                )
            );
        }
        if (
            0 != pcap_activate(
                pcap_handle
            )
        ) {
            this->~packet_capture_wrapper(
            );
            throw_exception(
                std::runtime_error(
                    "packet_capture_wrapper:pcap_activate: " + get_last_error(
                    )
                )
            );
        }
        if (
            0 != pcap_setnonblock(
                pcap_handle,
                1,
                error
            )
        ) {
            this->~packet_capture_wrapper(
            );
            throw_exception(
                component_error(
                    "packet_capture_wrapper"
                )
            );
        }
        file_descriptor = pcap_get_selectable_fd(
            pcap_handle
        );
        if (
            0 > file_descriptor
        ) {
            this->~packet_capture_wrapper(
            );
            throw_exception(
                component_error(
                    "packet_capture_wrapper"
                )
            );
        }
    }
    
    packet_capture_wrapper::~packet_capture_wrapper(
    ) {
        pcap_close(
            pcap_handle
        );
    }
    
    pcap_t* packet_capture_wrapper::get_pcap_handle(
    ) {
        return pcap_handle;
    }
    
    int packet_capture_wrapper::get_file_descriptor(
    ) const {
        return file_descriptor;
    }
    
    size_t packet_capture_wrapper::get_snapshot_length(
    ) const {
        return snapshot_length;
    }
    
    std::string packet_capture_wrapper::get_last_error(
    ) {
        return pcap_geterr(
            pcap_handle
        );
    }
}
