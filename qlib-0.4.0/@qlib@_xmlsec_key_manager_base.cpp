/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_xmlsec_key_manager_base.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    xmlsec_key_manager_base::xmlsec_key_manager_base(
        xmlSecKeyStorePtr store
    ) :
    store(
        store
    ),
    data(
        xmlSecKeysMngrCreate(
        )
    ) {
        if (
            NULL == store
        ) {
            if (
                NULL != data
            ) {
                this->~xmlsec_key_manager_base(
                );
                throw component_error(
                    "xmlsec_key_manager_base"
                );
            }
        }
        if (
            NULL == data
        ) {
            xmlSecKeyStoreDestroy(
                store
            );
            throw component_error(
                "xmlsec_key_manager_base"
            );
        }
        if (
            0 > xmlSecKeysMngrAdoptKeysStore(
                data,
                store
            )
        ) {
            xmlSecKeyStoreDestroy(
                store
            );
            this->~xmlsec_key_manager_base(
            );
            throw component_error(
                "xmlsec_key_manager_base"
            );
        }
        if (
            0 > xmlSecCryptoKeysMngrInit(
                data
            )
        ) {
            this->~xmlsec_key_manager_base(
            );
            throw component_error(
                "xmlsec_key_manager_base"
            );
        }
        data->getKey = xmlSecKeysMngrGetKey;
    }
    
    xmlsec_key_manager_base::~xmlsec_key_manager_base(
    ) {
        xmlSecKeysMngrDestroy(
            data
        );
    }
    
    xmlSecKeyStorePtr xmlsec_key_manager_base::get_native_store(
    ) {
        return store;
    }
    
    xmlSecKeysMngrPtr xmlsec_key_manager_base::get_data(
    ) const {
        return data;
    }
}
