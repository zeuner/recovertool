/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_malloc_c_allocator.h"
#include <cstdlib>
#include <cstring>
namespace @qlib@ {
    
    void* malloc_c_allocator::malloc(
        std::size_t count
    ) {
        return std::malloc(
            count
        );
    }
    
    void malloc_c_allocator::free(
        void* allocated
    ) {
        return std::free(
            allocated
        );
    }
    
    void* malloc_c_allocator::realloc(
        void* previous,
        std::size_t count
    ) {
        return std::realloc(
            previous,
            count
        );
    }
    
    char* malloc_c_allocator::strdup(
        char const* copied
    ) {
        std::size_t length = std::strlen(
            copied
        ) + 1;
        char* result = reinterpret_cast<
            char*
        >(
            std::malloc(
                length
            )
        );
        if (
            NULL != result
        ) {
            std::memcpy(
                result,
                copied,
                length
            );
        }
        return result;
    }
}
