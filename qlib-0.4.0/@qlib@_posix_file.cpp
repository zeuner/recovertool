/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_posix_file.h"
#include <unistd.h>
#include <fcntl.h>
#include "@qlib@_component_error.h"
#include <sys/types.h>
#include "@qlib@_throw_exception.h"
#include <sys/stat.h>
namespace @qlib@ {
    
    
    posix_file::posix_file(
        std::string const& filename,
        int flags
    ) :
    data(
        open(
            filename.c_str(
            ),
            flags
        )
    ) {
        if (
            -1 == data
        ) {
            throw_exception(
                component_error(
                    "posix_file"
                )
            );
        }
    }
    
    posix_file::posix_file(
        std::string const& filename,
        int flags,
        mode_t mode
    ) :
    data(
        open(
            filename.c_str(
            ),
            flags,
            mode
        )
    ) {
        if (
            -1 == data
        ) {
            throw_exception(
                component_error(
                    "posix_file"
                )
            );
        }
    }
    
    posix_file::~posix_file(
    ) {
        close(
            data
        );
    }
    
    int posix_file::get_data(
    ) const {
        return data;
    }
    
    off_t posix_file::get_size(
    ) const {
        struct stat attributes;
        if (
            0 > fstat(
                data,
                &attributes
            )
        ) {
            throw_exception(
                component_error(
                    "posix_file"
                )
            );
        }
        return attributes.st_size;
    }
}
