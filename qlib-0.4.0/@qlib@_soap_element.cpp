/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_soap_element.h"
#include <list>
namespace @qlib@ {
    
    
    soap_element::soap_element(
        soap_service const& service,
        std::string const& element_name,
        std::string const& element_namespace
    ) :
    soap_typed(
        boost::get<
            std::list<
                soap_service::element_type 
            >
        >(
            service.get_element_type(
                element_name,
                element_namespace
            )
        )
    ),
    element_name(
        element_name
    ),
    element_namespace(
        element_namespace
    ) {
    }
    
    void soap_element::dump_to_xml(
        xmlNodePtr augmented
    ) {
        xmlNodePtr const added = xmlNewChild(
            augmented,
            namespace_for_child(
                element_namespace,
                augmented
            ),
            BAD_CAST element_name.c_str(
            ),
            NULL
        );
        soap_typed::dump_to_xml(
            added
        );
    }
    
    void soap_element::parse_xml(
        xmlNodePtr const parsed
    ) {
        xml_matching_child const data_node(
            parsed,
            BAD_CAST element_name.c_str(
            ),
            BAD_CAST element_namespace.c_str(
            )
        );
        soap_typed::parse_xml(
            data_node.get_data(
            )
        );
    }
}
