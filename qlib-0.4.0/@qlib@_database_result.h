/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef DATABASE_RESULT_INCLUDED
#define DATABASE_RESULT_INCLUDED

#include "@qlib@_c_string.h"
#include <string>
namespace @qlib@ {
    
    
    class database_result {
    public:
        database_result(
            database_connection& queried,
            std::string const& query
        );
        typedef MYSQL_ROW row_type;
        row_type fetch_row(
        );
        ~database_result(
        );
        std::basic_string<
            unsigned char
        > get_binary_column(
            row_type& current,
            int column
        );
        template<
            typename string
        >
        void get_text_column(
            row_type& current,
            int column,
            string& output
        );
        template<
            typename string
        >
        void get_binary_column(
            row_type& current,
            int column,
            string& output
        );
    private:
        MYSQL_RES* result;
    };
    
    template<
        typename string
    >
    void database_result::get_text_column(
        row_type& current,
        int column,
        string& output
    ) {
        get_binary_column(
            current,
            column,
            output
        );
    }
    
    template<
        typename string
    >
    void database_result::get_binary_column(
        row_type& current,
        int column,
        string& output
    ) {
        output.replace(
            0,
            output.size(
            ),
            reinterpret_cast<
                typename string::value_type const*
            >(
                current[
                    column
                ]
            ),
            mysql_fetch_lengths(
                result
            )[
                column
            ]
        );
    }
}

#endif
