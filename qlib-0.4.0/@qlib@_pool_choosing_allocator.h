/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef POOL_CHOOSING_ALLOCATOR_INCLUDED
#define POOL_CHOOSING_ALLOCATOR_INCLUDED

namespace @qlib@ {
    
    
    template<
        typename storable
    >
    class pool_choosing_allocator {
    public:
        explicit pool_choosing_allocator(
        );
        template<
            typename different
        >
        pool_choosing_allocator(
            pool_choosing_allocator<
                different
            > const& rebound
        );
        typedef storable value_type;
        typedef value_type* pointer;
        typedef const value_type* const_pointer;
        typedef value_type& reference;
        typedef const value_type& const_reference;
        typedef std::size_t size_type;
        typedef std::ptrdiff_t difference_type;
        template<
            typename different
        >
        class rebind {
        public:
            typedef pool_choosing_allocator<
                different
            > other;
        };
        pointer address(
            reference addressed
        );
        const_pointer address(
            const_reference addressed
        );
        pointer allocate(
            size_type count,
            typename std::allocator<
                void
            >::const_pointer hint = 0
        );
        void deallocate(
            pointer allocated,
            size_type count
        );
        size_type max_size(
        ) const;
        void construct(
            pointer location,
            storable const& prototype
        );
        void destroy(
            pointer location
        );
        bool operator==(
            pool_choosing_allocator const& compared
        );
        bool operator!=(
            pool_choosing_allocator const& compared
        );
    private:
        allocator_pool_chooser<
        > chooser;
    };
    
    template<
        typename storable
    >
    pool_choosing_allocator<
        storable
    >::pool_choosing_allocator(
    ) {
    }
    
    template<
        typename storable
    >
    template<
        typename different
    >
    pool_choosing_allocator<
        storable
    >::pool_choosing_allocator(
        pool_choosing_allocator<
            different
        > const& rebound
    ) {
    }
    
    template<
        typename storable
    >
    typename pool_choosing_allocator<
        storable
    >::pointer pool_choosing_allocator<
        storable
    >::address(
        typename pool_choosing_allocator< 
            storable
        >::reference addressed
    ) {
        return &addressed;
    }
    
    template<
        typename storable
    >
    typename pool_choosing_allocator<
        storable
    >::const_pointer pool_choosing_allocator<
        storable
    >::address(
        typename pool_choosing_allocator< 
            storable
        >::const_reference addressed
    ) {
        return &addressed;
    }
    
    template<
        typename storable
    >
    typename pool_choosing_allocator<
        storable
    >::pointer pool_choosing_allocator<
        storable
    >::allocate(
        typename pool_choosing_allocator< 
            storable
        >::size_type count,
        typename std::allocator<
            void
        >::const_pointer hint
    ) {
        char* allocated = chooser.allocate(
            count * sizeof(
                storable
            )
        );
        return reinterpret_cast<
            pointer
        >(
            allocated
        );
    }
    
    template<
        typename storable
    >
    void pool_choosing_allocator<
        storable
    >::deallocate(
        typename pool_choosing_allocator< 
            storable
        >::pointer allocated,
        typename pool_choosing_allocator< 
            storable
        >::size_type count
    ) {
        chooser.deallocate(
            reinterpret_cast<
                char*
            >(
                allocated
            ),
            sizeof(
                storable
            ) * count
        );
    }
    
    template<
        typename storable
    >
    typename pool_choosing_allocator<
        storable
    >::size_type pool_choosing_allocator<
        storable
    >::max_size(
    ) const {
        return std::numeric_limits<
            size_type
        >::max(
        ) / sizeof(
            storable
        );
    }
    
    template<
        typename storable
    >
    void pool_choosing_allocator<
        storable
    >::construct(
        typename pool_choosing_allocator< 
            storable
        >::pointer location,
        storable const& prototype
    ) {
        new(
            location
        ) storable(
            prototype
        );
    }
    
    template<
        typename storable
    >
    void pool_choosing_allocator<
        storable
    >::destroy(
        typename pool_choosing_allocator< 
            storable
        >::pointer location
    ) {
        location->~storable(
        );
    }
    
    template<
        typename storable
    >
    bool pool_choosing_allocator<
        storable
    >::operator==(
        pool_choosing_allocator< 
            storable 
        > const& compared
    ) {
        return true;
    }
    
    template<
        typename storable
    >
    bool pool_choosing_allocator<
        storable
    >::operator!=(
        pool_choosing_allocator<
            storable
        > const& compared
    ) {
        return !(
            compared == *this
        );
    }
}

#endif
