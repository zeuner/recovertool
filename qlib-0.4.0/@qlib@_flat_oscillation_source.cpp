/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_flat_oscillation_source.h"
#include <boost/shared_ptr.hpp>
namespace @qlib@ {
    
    
    flat_oscillation_source::flat_oscillation_source(
        std::map<
            time_value,
            time_value
        > frequency_envelope_points,
        time_value rate,
        time_value duration,
        time_value (
            *waveform
        )(
            time_value phase
        )
    ) :
    frequency_envelope_source(
        frequency_envelope_points,
        rate,
        duration
    ) {
        reset(
            boost::shared_ptr<
                oscillating_type
            >(
                new oscillating_type(
                    frequency_envelope_source,
                    rate,
                    waveform
                )
            )
        );
    }
}
