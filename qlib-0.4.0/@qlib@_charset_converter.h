/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef CHARSET_CONVERTER_INCLUDED
#define CHARSET_CONVERTER_INCLUDED

#include <iconv.h>
#include <boost/noncopyable.hpp>
#include <string>
namespace @qlib@ {
    
    
    class charset_converter :
    private boost::noncopyable {
    public:
        charset_converter(
            std::string const& from,
            std::string const& to
        );
        ~charset_converter(
        );
        iconv_t get_data(
        ) const;
    private:
        iconv_t data;
    };
}

#endif
