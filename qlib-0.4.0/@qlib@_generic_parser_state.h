/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef GENERIC_PARSER_STATE_INCLUDED
#define GENERIC_PARSER_STATE_INCLUDED

#include <map>
#include <string>
#include <list>
#include <boost/shared_ptr.hpp>
namespace @qlib@ {
    
    
    class generic_parser_state {
    public:
        virtual ~generic_parser_state(
        );
        virtual void on_start_element(
            const std::string& local_name,
            const std::string& namespace_prefix,
            const std::string& namespace_uri,
            const std::list<
                std::pair<
                    std::string,
                    std::string
                >        
            >& properties,
            boost::shared_ptr<
                std::map<
                    std::string,
                    std::string
                > const
            > namespaces
        ) = 0;
        virtual void on_end_element(
            const std::string& local_name,
            const std::string& namespace_prefix,
            const std::string& namespace_uri
        ) = 0;
        virtual void on_characters(
            const std::string& characters
        ) = 0;
        static std::string const& get_configuration_namespace(
        );
    protected:
        static const std::string configuration_namespace;
    };
}

#endif
