/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef LOCKING_ALLOCATOR_INCLUDED
#define LOCKING_ALLOCATOR_INCLUDED

#include <new>
#include <cstring>
#include "@qlib@_throw_exception.h"
namespace @qlib@ {
    
    
    template<
        typename storable
    >
    class locking_allocator {
    public:
        explicit locking_allocator(
        );
        template<
            typename different
        >
        locking_allocator(
            locking_allocator<
                different
            > const& rebound
        );
        typedef storable value_type;
        typedef value_type* pointer;
        typedef const value_type* const_pointer;
        typedef value_type& reference;
        typedef const value_type& const_reference;
        typedef std::size_t size_type;
        typedef std::ptrdiff_t difference_type;
        template<
            typename different
        >
        class rebind {
        public:
            typedef locking_allocator<
                different
            > other;
        };
        pointer address(
            reference addressed
        );
        const_pointer address(
            const_reference addressed
        );
        pointer allocate(
            size_type count,
            typename std::allocator<
                void
            >::const_pointer hint = 0
        );
        void deallocate(
            pointer allocated,
            size_type count
        );
        size_type max_size(
        ) const;
        void construct(
            pointer location,
            storable const& prototype
        );
        void destroy(
            pointer location
        );
        bool operator==(
            locking_allocator const& compared
        );
        bool operator!=(
            locking_allocator const& compared
        );
        static long page_size(
        );
    };
    
    template<
        typename storable
    >
    locking_allocator<
        storable
    >::locking_allocator(
    ) {
    }
    
    template<
        typename storable
    >
    template<
        typename different
    >
    locking_allocator<
        storable
    >::locking_allocator(
        locking_allocator<
            different
        > const& rebound
    ) {
    }
    
    template<
        typename storable
    >
    typename locking_allocator<
        storable
    >::pointer locking_allocator<
        storable
    >::address(
        typename locking_allocator< 
            storable
        >::reference addressed
    ) {
        return &addressed;
    }
    
    template<
        typename storable
    >
    typename locking_allocator<
        storable
    >::const_pointer locking_allocator<
        storable
    >::address(
        typename locking_allocator< 
            storable
        >::const_reference addressed
    ) {
        return &addressed;
    }
    
    template<
        typename storable
    >
    typename locking_allocator<
        storable
    >::pointer locking_allocator<
        storable
    >::allocate(
        typename locking_allocator< 
            storable
        >::size_type count,
        typename std::allocator<
            void
        >::const_pointer hint
    ) {
        std::size_t const data_required = count * sizeof(
            storable
        );
        std::size_t const pointer_required = sizeof(
            void*
        );
        std::size_t const size_required = sizeof(
            void*
        );
        std::size_t const data_page_required = (
            (
                data_required + page_size(
                ) - 1
            ) / page_size(
            )
        ) * page_size(
        );
        std::size_t const metadata_required = pointer_required + size_required;
        std::size_t const total_required = metadata_required + data_page_required;
        std::size_t const aligning_required = total_required + page_size(
        );
        char* const block_location = reinterpret_cast<
            char*
        >(
            ::operator new(
                aligning_required
            )
        );
        intptr_t const lowest_data = reinterpret_cast<
            intptr_t
        >(
            block_location + metadata_required
        );
        pointer const data_location = reinterpret_cast<
            pointer
        >(
            (
                (
                    lowest_data + page_size(
                    ) - 1
                ) / page_size(
                )
            ) * page_size(
            )
        );
        std::size_t* const size_location = reinterpret_cast<
            std::size_t*
        >(
            data_location
        ) - 1;
        void** const pointer_location = reinterpret_cast<
            void**
        >(
            size_location
        ) - 1;
        *pointer_location = block_location;
        *size_location = data_page_required;
        if (
            0 > mlock(
                data_location,
                data_page_required
            )
        ) {
            ::operator delete(
                block_location
            );
            throw_exception(
                std::bad_alloc(
                )
            );
        }
        return data_location;
    }
    
    template<
        typename storable
    >
    void locking_allocator<
        storable
    >::deallocate(
        typename locking_allocator< 
            storable
        >::pointer allocated,
        typename locking_allocator< 
            storable
        >::size_type count
    ) {
        std::size_t* const size_location = reinterpret_cast<
            std::size_t*
        >(
            allocated
        ) - 1;
        void** const pointer_location = reinterpret_cast<
            void**
        >(
            size_location
        ) - 1;
        memset(
            allocated,
            0,
            sizeof(
                storable
            ) * count
        );
        munlock(
            allocated,
            *size_location
        );
        ::operator delete(
            *pointer_location
        );
    }
    
    template<
        typename storable
    >
    typename locking_allocator<
        storable
    >::size_type locking_allocator<
        storable
    >::max_size(
    ) const {
        return std::numeric_limits<
            size_type
        >::max(
        ) / sizeof(
            storable
        );
    }
    
    template<
        typename storable
    >
    void locking_allocator<
        storable
    >::construct(
        typename locking_allocator< 
            storable
        >::pointer location,
        storable const& prototype
    ) {
        new(
            location
        ) storable(
            prototype
        );
    }
    
    template<
        typename storable
    >
    void locking_allocator<
        storable
    >::destroy(
        typename locking_allocator< 
            storable
        >::pointer location
    ) {
        location->~storable(
        );
    }
    
    template<
        typename storable
    >
    bool locking_allocator<
        storable
    >::operator==(
        locking_allocator< 
            storable 
        > const& compared
    ) {
        return true;
    }
    
    template<
        typename storable
    >
    bool locking_allocator<
        storable
    >::operator!=(
        locking_allocator<
            storable
        > const& compared
    ) {
        return !(
            compared == *this
        );
    }
    
    template<
        typename storable
    >
    long locking_allocator<
        storable
    >::page_size(
    ) {
        static long const value = sysconf(
            _SC_PAGESIZE
        );
        return value;
    }
}

#endif
