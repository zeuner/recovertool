/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef LINEAR_UNIDIRECTIONAL_ENVELOPE_INCLUDED
#define LINEAR_UNIDIRECTIONAL_ENVELOPE_INCLUDED

#include <map>
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    template<
        typename value_type
    >
    class linear_unidirectional_envelope {
    public:
        linear_unidirectional_envelope(
            std::map<
                time_value,
                value_type
            > const& points
        );
        value_type get_value(
            time_value at
        );
    private:
        std::map<
            time_value,
            value_type
        > points;
        typename std::map<
            time_value,
            value_type
        >::const_iterator fewer;
        typename std::map<
            time_value,
            value_type
        >::const_iterator greater;
    };
    
    template<
        typename value_type
    >
    linear_unidirectional_envelope<
        value_type
    >::linear_unidirectional_envelope(
        std::map<
            time_value,
            value_type
        > const& points
    ) :
    points(
        points
    ),
    fewer(
        this->points.begin(
        )
    ),
    greater(
        fewer
    ) {
        if (
            this->points.end(
            ) == fewer
        ) {
            throw component_error(
                "linear_unidirectional_envelope"
            );
        }
        greater++;
        if (
            this->points.end(
            ) == greater
        ) {
            throw component_error(
                "linear_unidirectional_envelope"
            );
        }
    }
    
    template<
        typename value_type
    >
    value_type linear_unidirectional_envelope<
        value_type
    >::get_value(
        time_value at
    ) {
        if (
            at < fewer->first
        ) {
            throw component_error(
                "linear_unidirectional_envelope"
            );
        }
        while (
            at > greater->first
        ) {
            fewer++;
            greater++;
            if (
                points.end(
                ) == greater
        ) {
                throw component_error(
                    "linear_unidirectional_envelope"
                );
            }
        }
        if (
            at == greater->first
        ) {
            return greater->second;
        }
        return fewer->second + (
            at - fewer->first
        ) * (
            greater->second - fewer->second
        ) / (
            greater->first - fewer->first
        );
    }
}

#endif
