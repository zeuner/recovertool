/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef GET_CMPI_PROPERTY_INCLUDED
#define GET_CMPI_PROPERTY_INCLUDED

#include "@qlib@_component_error.h"
#include <boost/date_time/posix_time/ptime.hpp>
#include <string>
namespace @qlib@ {
    
    
    template<
        typename object_type,
        typename value_type
    >
    void get_cmpi_property(
        CMPIBroker const* broker,
        object_type* object,
        std::string const& name,
        value_type& value
    );
    
    template<
        typename object_type
    >
    void get_cmpi_property(
        CMPIBroker const* broker,
        object_type* object,
        std::string const& name,
        void*& value
    ) {
        CMPIData native = get_native_cmpi_property(
            object,
            name.c_str(
            )
        );
        if (
            CMPI_sint64 == native.type
        ) {
            int64_t const signed_value = native.value.sint64;
            uint64_t const converted = signed_value;
            value = reinterpret_cast<
                void*
            >(
                static_cast<
                    uintptr_t
                >(
                    converted
                )
            );
            return;
        }
        if (
            CMPI_uint64 != native.type
        ) {
            throw component_error(
                "get_cmpi_property"
            );
        }
        value = reinterpret_cast<
            void*
        >(
            static_cast<
                uintptr_t
            >(
                native.value.uint64
            )
        );
    }
    
    template<
        typename object_type
    >
    void get_cmpi_property(
        CMPIBroker const* broker,
        object_type* object,
        std::string const& name,
        double& value
    ) {
        CMPIData native = get_native_cmpi_property(
            object,
            name.c_str(
            )
        );
        if (
            CMPI_real64 != native.type
        ) {
            throw component_error(
                "get_cmpi_property"
            );
        }
        value = native.value.real64;
    }
    
    template<
        typename object_type
    >
    void get_cmpi_property(
        CMPIBroker const* broker,
        object_type* object,
        std::string const& name,
        uint32_t& value
    ) {
        CMPIData native = get_native_cmpi_property(
            object,
            name.c_str(
            )
        );
        if (
            CMPI_uint32 != native.type
        ) {
            throw component_error(
                "get_cmpi_property"
            );
        }
        value = native.value.uint32;
    }
    
    template<
        typename object_type
    >
    void get_cmpi_property(
        CMPIBroker const* broker,
        object_type* object,
        std::string const& name,
        int32_t& value
    ) {
        CMPIData native = get_native_cmpi_property(
            object,
            name.c_str(
            )
        );
        if (
            CMPI_sint32 != native.type
        ) {
            throw component_error(
                "get_cmpi_property"
            );
        }
        value = native.value.sint32;
    }
    
    template<
        typename object_type
    >
    void get_cmpi_property(
        CMPIBroker const* broker,
        object_type* object,
        std::string const& name,
        uint64_t& value
    ) {
        CMPIData native = get_native_cmpi_property(
            object,
            name.c_str(
            )
        );
        if (
            CMPI_uint64 = native.type
        ) {
            throw component_error(
                "get_cmpi_property"
            );
        }
        value = native.value.uint64;
    }
    
    template<
        typename object_type
    >
    void get_cmpi_property(
        CMPIBroker const* broker,
        object_type* object,
        std::string const& name,
        std::string& value
    ) {
        CMPIData native = get_native_cmpi_property(
            object,
            name.c_str(
            )
        );
        if (
            CMPI_string != native.type
        ) {
            throw component_error(
                "get_cmpi_property"
            );
        }
        CMPIString* string = native.value.string;
        value = CMGetCharPtr(
            string
        );
    }
    
    template<
        typename object_type
    >
    void get_cmpi_property(
        CMPIBroker const* broker,
        object_type* object,
        std::string const& name,
        boost::posix_time::ptime& value
    ) {
        CMPIData native = get_native_cmpi_property(
            object,
            name.c_str(
            )
        );
        if (
            CMPI_dateTime != native.type
        ) {
            throw component_error(
                "get_cmpi_property"
            );
        }
        value = boost::posix_time::from_time_t(
            0
        ) + boost::posix_time::microseconds(
            CMGetBinaryFormat(
                native.value.dateTime,
                NULL
            )
        );
    }
}

#endif
