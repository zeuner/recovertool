/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef TRIM_PREFIX_INCLUDED
#define TRIM_PREFIX_INCLUDED

namespace @qlib@ {
    
    
    template<
        typename string1,
        typename string2
    >
    string2 trim_prefix(
        string1 const& prefix,
        string2 const& whole
    );
    
    template<
        typename string1,
        typename string2
    >
    string2 trim_prefix(
        string1 const& prefix,
        string2 const& whole
    ) {
        typename string1::const_iterator matching = prefix.begin(
        );
        typename string2::const_iterator matched = whole.begin(
        );
        for (
            ;
            (
                prefix.end(
                ) != matching
            ) && (
                whole.end(
                ) != matched
            );
            matching++,
            matched++
        ) {
            if (
                *matched != *matching
            ) {
                break;
            }
        }
        return string2(
            matched,
            whole.end(
            )
        );
    }
}

#endif
