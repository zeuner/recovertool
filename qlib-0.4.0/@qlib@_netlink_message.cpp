/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_netlink_message.h"
#include <cstring>
#include <string>
namespace @qlib@ {
    
    
    netlink_message::netlink_message(
        uint16_t command,
        uint16_t version,
        uint16_t flags,
        uint32_t sequence_number,
        std::list<
            netlink_attribute
        > const& attributes
    ) {
        struct nlmsghdr header;
        memset(
            &header,
            0,
            sizeof(
                header
            )
        );
        std::string const command_header = std::string(
            reinterpret_cast<
                char const*
            >(
                &command
            ),
            sizeof(
                command
            )
        ) + std::string(
            reinterpret_cast<
                char const*
            >(
                &version
            ),
            sizeof(
                version
            )
        );
        header.nlmsg_len = sizeof(
            header
        ) + command_header.size(
        );
        header.nlmsg_pid = getpid(
        );
        header.nlmsg_type = command;
        header.nlmsg_flags = flags;
        header.nlmsg_seq = sequence_number;
        std::string payload_data = command_header;
        for (
            std::list<
                netlink_attribute
            >::const_iterator adding = attributes.begin(
            );
            attributes.end(
            ) != adding;
            adding++
        ) {
            payload_data += adding->get_data(
            );
            header.nlmsg_len += adding->get_data(
            ).size(
            );
        }
        std::string const message_data = std::string(
            reinterpret_cast<
                char const*
            >(
                &header
            ),
            sizeof(
                header
            )
        ) + payload_data;
        data = new c_buffer(
            message_data
        );
    }
    
    netlink_message::~netlink_message(
    ) {
        delete data;
    }
    
    struct nlmsghdr* netlink_message::get_data(
    ) {
        return reinterpret_cast<
            struct nlmsghdr*
        >(
            data->get_data(
            )
        );
    }
    
    struct nlmsghdr const* netlink_message::get_data(
    ) const {
        return reinterpret_cast<
            struct nlmsghdr const*
        >(
            data->get_data(
            )
        );
    }
}
