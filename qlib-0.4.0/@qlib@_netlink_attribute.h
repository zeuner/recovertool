/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef NETLINK_ATTRIBUTE_INCLUDED
#define NETLINK_ATTRIBUTE_INCLUDED

#include <string>
namespace @qlib@ {
    
    
    class netlink_attribute {
    public:
        template<
            typename value_type
        >
        netlink_attribute(
            uint16_t attribute_type,
            value_type const& attribute_value
        );
        std::string const& get_data(
        ) const;
    private:
        std::string data;
    };
    
    template<
        typename value_type
    >
    netlink_attribute::netlink_attribute(
        uint16_t attribute_type,
        value_type const& attribute_value
    ) {
        uint16_t attribute_length;
        attribute_length = sizeof(
            attribute_length
        ) + sizeof(
            attribute_type
        ) + sizeof(
            attribute_value
        );
        data = std::string(
            reinterpret_cast<
                char const*
            >(
                &attribute_length
            ),
            sizeof(
                attribute_length
            )
        ) + std::string(
            reinterpret_cast<
                char const*
            >(
                &attribute_type
            ),
            sizeof(
                attribute_type
            )
        ) + std::string(
            reinterpret_cast<
                char const*
            >(
                &attribute_value
            ),
            sizeof(
                attribute_value
            )
        ) + std::string(
            NLA_ALIGN(
                attribute_length
            ) - attribute_length,
            '\x00'
        );
    }
}

#endif
