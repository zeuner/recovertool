/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef ALLOCATOR_WRAPPER_INCLUDED
#define ALLOCATOR_WRAPPER_INCLUDED

#include <new>
namespace @qlib@ {
    
    
    template<
        template<
            typename storable
        > class storing
    >
    class allocator_wrapper {
    private:
        typedef storing<
            char
        > char_allocator;
    public:
        typedef typename char_allocator::size_type size_type;
        typedef typename char_allocator::difference_type difference_type;
        static char* malloc(
            size_type const count
        );
        static void free(
            char* const allocated
        );
    private:
        static char_allocator allocating;
    };
    
    template<
        template<
            typename storable
        > class storing
    >
    char* allocator_wrapper<
        storing
    >::malloc(
        typename allocator_wrapper<
            storing
        >::size_type const count
    ) {
        std::size_t const required = sizeof(
            std::size_t
        ) + count;
        try {
            std::size_t* const size_location = reinterpret_cast<
                std::size_t*
            >(
                allocating.allocate(
                    required
                )
            );
            *size_location = required;
            return reinterpret_cast<
                char*
            >(
                size_location + 1
            );
        } catch (
            std::bad_alloc& caught
        ) {
            return NULL;
        }
    }
    
    template<
        template<
            typename storable
        > class storing
    >
    void allocator_wrapper<
        storing
    >::free(
        char* const allocated
    ) {
        std::size_t* const size_location = reinterpret_cast<
            std::size_t*
        >(
            allocated
        ) - 1;
        allocating.deallocate(
            reinterpret_cast<
                char*
            >(
                size_location
            ),
            *size_location
        );
    }
    
    template<
        template<
            typename storable
        > class storing
    >
    typename allocator_wrapper<
        storing
    >::char_allocator allocator_wrapper<
        storing
    >::allocating;
}

#endif
