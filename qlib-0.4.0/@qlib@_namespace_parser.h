/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef NAMESPACE_PARSER_INCLUDED
#define NAMESPACE_PARSER_INCLUDED

#include <boost/fusion/include/vector.hpp>
#include <boost/fusion/include/vector_fwd.hpp>
#include <map>
#include <boost/fusion/container/vector.hpp>
#include <libxml++/parsers/saxparser.h>
#include <boost/shared_ptr.hpp>
#include <stack>
#include "@qlib@_parser_state_interface.h"
#include <boost/fusion/container/vector/vector_fwd.hpp>
#include <string>
namespace @qlib@ {
    
    
    class namespace_parser :
    public xmlpp::SaxParser {
    public:
        namespace_parser(
            parser_state_interface* initial_state
        );
        boost::shared_ptr<
            parser_state_interface
        > get_state(
        ) const;
        void set_state(
            boost::shared_ptr<
                parser_state_interface
            > new_state
        );
        void set_state(
            parser_state_interface* new_state
        );
        void parse_chunk(
            Glib::ustring const& pending
        );
    protected:
        void on_start_document(
        );
        void on_end_document(
        );
        boost::fusion::vector<
            std::string,
            std::string,
            std::string
        > expand_namespace(
            std::string const& prefixed
        );
        void on_start_element(
            const Glib::ustring& unicode_name,
            const AttributeList& properties
        );
        void on_end_element(
            const Glib::ustring& name
        );
        void on_characters(
            const Glib::ustring& characters
        );
        void on_comment(
            const Glib::ustring& text
        );
        void on_warning(
            const Glib::ustring& text
        );
        void on_error(
            const Glib::ustring& text
        );
        void on_fatal_error(
            const Glib::ustring& text
        );
    private:
        std::stack<
            boost::shared_ptr<
                std::map<
                    std::string,
                    std::string
                >
            >
        > namespace_prefixes;
        boost::shared_ptr<
            parser_state_interface
        > current_state;
        bool failed;
    };
}

#endif
