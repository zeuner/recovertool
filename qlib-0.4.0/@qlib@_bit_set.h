/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef BIT_SET_INCLUDED
#define BIT_SET_INCLUDED

#include "@qlib@_throw_exception.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    template<
        typename storing
    >
    class bit_set {
    public:
        bit_set(
            storing value
        );
        class const_iterator {
        public:
            const_iterator(
            );
            const_iterator(
                storing const* bits
            );
            bool operator==(
                const_iterator const& other
            );
            bool operator!=(
                const_iterator const& other
            );
            int operator*(
            ) const;
            const_iterator& operator++(
            );
            const_iterator operator++(
                int
            );
            void find_next_bit(
            );
        private:
            storing bit;
            int value;
            storing const* bits;
            bool end;
        };
        const_iterator begin(
        ) const;
        const_iterator end(
        ) const;
    private:
        storing value;
    };
    
    template<
        typename storing
    >
    bit_set<
        storing
    >::bit_set(
        storing value
    ) :
    value(
        value
    ) {
    }
    
    template<
        typename storing
    >
    bit_set<
        storing
    >::const_iterator::const_iterator(
    ) :
    end(
        true
    ) {
    }
    
    template<
        typename storing
    >
    bit_set<
        storing
    >::const_iterator::const_iterator(
        storing const* bits
    ) :
    bit(
        1
    ),
    value(
        0
    ),
    bits(
        bits
    ),
    end(
        false
    ) {
        find_next_bit(
        );
    }
    
    template<
        typename storing
    >
    bool bit_set<
        storing
    >::const_iterator::operator==(
        const_iterator const& other
    ) {
        if (
            !(
                end || other.end
            )
        ) {
            return (
                value == value
            ) && (
                bits == bits
            );
        } else {
            return end == other.end;
        }
    }
    
    template<
        typename storing
    >
    bool bit_set<
        storing
    >::const_iterator::operator!=(
        const_iterator const& other
    ) {
        return !(
            (
                *this
            ) == other
        );
    }
    
    template<
        typename storing
    >
    int bit_set<
        storing
    >::const_iterator::operator*(
    ) const {
        if (
            end
        ) {
            throw_exception(
                component_error(
                    "bit_set"
                )
            );
        }
        return value;
    }
    
    template<
        typename storing
    >
    typename bit_set<
        storing
    >::const_iterator& bit_set<
        storing
    >::const_iterator::operator++(
    ) {
        value++;
        bit <<= 1;
        find_next_bit(
        );
        return *this;
    }
    
    template<
        typename storing
    >
    typename bit_set<
        storing
    >::const_iterator bit_set<
        storing
    >::const_iterator::operator++(
        int
    ) {
        const_iterator old = *this;
        ++(
            *this
        );
        return old;
    }
    
    template<
        typename storing
    >
    void bit_set<
        storing
    >::const_iterator::find_next_bit(
    ) {
        while (
            true
        ) {
            if (
                0 == bit
            ) {
                end = true;
                break;
            }
            if (
                bit > (
                    *bits
                )
            ) {
                end = true;
                break;
            }
            if (
                0 != (
                    bit & (
                        *bits
                    )
                )
            ) {
                break;
            }
            value++;
            bit <<= 1;
        }
    }
    
    template<
        typename storing
    >
    typename bit_set<
        storing
    >::const_iterator bit_set<
        storing
    >::begin(
    ) const {
        return const_iterator(
            &value
        );
    }
    
    template<
        typename storing
    >
    typename bit_set<
        storing
    >::const_iterator bit_set<
        storing
    >::end(
    ) const {
        return const_iterator(
        );
    }
}

#endif
