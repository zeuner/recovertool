/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef FEEDBACKED_SOURCE_INCLUDED
#define FEEDBACKED_SOURCE_INCLUDED

#include <boost/noncopyable.hpp>
#include <map>
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    template<
        typename playable
    >
    class feedbacked_source :
    private boost::noncopyable {
    public:
        feedbacked_source(
            playable& source,
            std::map<
                int,
                amplitude_value
            > const& amplitudes
        );
        bool is_master(
        ) const;
        bool get_sample(
            amplitude_value& retrieved
        );
    private:
        playable& source;
        std::map<
            int,
            amplitude_value
        > amplitudes;
        resizable_buffer<
            malloc_c_allocator,
            amplitude_value
        > history;
        amplitude_value* next;
        amplitude_value* end;
    };
    
    template<
        typename playable
    >
    feedbacked_source<
        playable
    >::feedbacked_source(
        playable& source,
        std::map<
            int,
            amplitude_value
        > const& amplitudes
    ) :
    source(
        source
    ),
    amplitudes(
        amplitudes
    ) {
        std::map<
            int,
            amplitude_value
        >::const_iterator last = amplitudes.end(
        );
        if (
            amplitudes.begin(
            ) == last
        ) {
            throw component_error(
                "feedbacked_source"
            );
        }
        last--;
        history.resize(
            last->first + 1
        );
        for (
            size_t initializing = 0;
            history.get_size(
            ) > initializing;
            initializing++
        ) {
            history.get_data(
            )[
                initializing
            ] = 0;
        }
        next = history.get_data(
        );
        end = next + history.get_size(
        );
    }
    
    template<
        typename playable
    >
    bool feedbacked_source<
        playable
    >::is_master(
    ) const {
        return true;
    }
    
    template<
        typename playable
    >
    bool feedbacked_source<
        playable
    >::get_sample(
        amplitude_value& retrieved
    ) {
        if (
            !source.get_sample(
                *next
            )
        ) {
            return false;
        }
        for (
            std::map<
                int,
                amplitude_value
            >::const_iterator accumulating = amplitudes.begin(
            );
            amplitudes.end(
            ) != accumulating;
            accumulating++
        ) {
            amplitude_value* value = next - accumulating->first;
            if (
                history.get_data(
                ) > value
            ) {
                value += history.get_size(
                );
            }
            *next += accumulating->second * (
                *value
            );
        }
        retrieved = *next;
        next++;
        if (
            end <= next
        ) {
            next = history.get_data(
            );
        }
        return true;
    }
}

#endif
