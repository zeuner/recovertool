/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef STREAM_ACCUMULATION_SOURCE_INCLUDED
#define STREAM_ACCUMULATION_SOURCE_INCLUDED

#include <boost/shared_ptr.hpp>
#include <list>
#include <cstring>
namespace @qlib@ {
    
    
    template<
        typename streaming,
        typename reducing = additive_reduction<
            amplitude_value
        >
    >
    class stream_accumulation_source;
    
    template<
        typename streaming,
        typename reducing
    >
    class stream_accumulation_source {
    public:
        stream_accumulation_source(
            streaming& stream,
            time_value rate
        );
        bool get_sample(
            amplitude_value& retrieved
        );
        size_t get_samples(
            amplitude_value* first,
            amplitude_value* last
        );
    private:
        streaming& stream;
        time_value next_offset;
        boost::shared_ptr<
            typename streaming::source_type
        > next_source;
        class source_entry {
        public:
            source_entry(
                boost::shared_ptr<
                    typename streaming::source_type
                > source
            );
            size_t fill_samples(
                size_t requested
            );
            void consume_samples(
                size_t consumed
            );
            class iterator :
            public std::iterator<
                std::random_access_iterator_tag,
                amplitude_value
            > {
            public:
                iterator(
                );
                iterator(
                    amplitude_value* current_sample,
                    amplitude_value* virtual_current_sample,
                    amplitude_value* sample_begin,
                    amplitude_value* sample_end
                );
                void operator++(
                );
                iterator operator+(
                    size_t added
                ) const;
                bool operator!=(
                    iterator const& other
                ) const;
                int operator-(
                    iterator const& other
                ) const;
                amplitude_value& operator*(
                );
            private:
                amplitude_value* current_sample;
                amplitude_value* virtual_current_sample;
                amplitude_value* sample_begin;
                amplitude_value* sample_end;
            };
            iterator begin(
            );
            iterator end(
            );
        protected:
            void resize(
                size_t requested
            );
        private:
            boost::shared_ptr<
                typename streaming::source_type
            > source;
            resizable_buffer<
                malloc_c_allocator,
                amplitude_value
            > samples;
            amplitude_value* first_sample;
            size_t available_samples;
            bool done;
        };
        std::list<
            boost::shared_ptr<
                source_entry
            >
        > active_source;
        time_value const real_rate;
        time_value const hz;
    };
    
    template<
        typename streaming,
        typename reducing
    >
    class bulk_audio_retrieval<
        stream_accumulation_source<
            streaming,
            reducing
        >
    > {
    public:
        static size_t get_samples(
            stream_accumulation_source<
                streaming,
                reducing
            >& source,
            amplitude_value* first,
            amplitude_value* last
        );
    };
    
    template<
        typename streaming,
        typename reducing
    >
    stream_accumulation_source<
        streaming,
        reducing
    >::stream_accumulation_source(
        streaming& stream,
        time_value rate
    ) :
    stream(
        stream
    ),
    next_offset(
        0
    ),
    real_rate(
        rate
    ),
    hz(
        real_number(
            1
        ) / real_rate
    ) {
    }
    
    template<
        typename streaming,
        typename reducing
    >
    bool stream_accumulation_source<
        streaming,
        reducing
    >::get_sample(
        amplitude_value& retrieved
    ) {
        return 0 < get_samples(
            &retrieved,
            (
                &retrieved
            ) + 1
        );
    }
    
    template<
        typename streaming,
        typename reducing
    >
    size_t stream_accumulation_source<
        streaming,
        reducing
    >::get_samples(
        amplitude_value* first,
        amplitude_value* last
    ) { 
        do {
            if (
                !next_source
            ) {
                time_value displacement;
                if (
                    !stream.get_source(
                        displacement,
                        next_source
                    )
                ) {
                    if (
                        active_source.empty(
                        )
                    ) {
                        return 0;
                    } else {
                        break;
                    }
                } else {
                    next_offset += displacement;
                }
            }
            if (
                next_offset <= real_number(
                    0
                )
            ) {
                active_source.push_back(
                    boost::shared_ptr<
                        source_entry
                    >(
                        new source_entry(
                            next_source
                        )
                    )
                );
                next_source.reset(
                );
            } else {
                break;
            }
        } while (
            true
        );
        size_t required_samples = last - first;
        if (
            next_source
        ) {
            size_t const before_onset = floor(
                next_offset / hz
            ) + 1;
            if (
                before_onset < required_samples
            ) {
                required_samples = before_onset;
            }
        }
        for (
            typename std::list<
                boost::shared_ptr<
                    source_entry
                >
            >::iterator pulling = active_source.begin(
            );
            active_source.end(
            ) != pulling;
        ) {
            size_t const available = (
                *pulling
            )->fill_samples(
                required_samples
            );
            if (
                0 != available
            ) {
                if (
                    required_samples > available
                ) {
                    required_samples = available;
                }
                pulling++;
            } else {
                pulling = active_source.erase(
                    pulling
                );
            }
        }
        typename std::list<
            boost::shared_ptr<
                source_entry
            >
        >::iterator accumulating = active_source.begin(
        );
        if (
            active_source.end(
            ) == accumulating
        ) {
            required_samples = 0;
        } else {
            typename std::list<
                boost::shared_ptr<
                    source_entry
                >
            >::iterator next = accumulating;
            next++;
            if (
                active_source.end(
                ) == next
            ) {
                std::copy(
                    (
                        *accumulating
                    )->begin(
                    ),
                    (
                        *accumulating
                    )->begin(
                    ) + required_samples,
                    first
                );
            } else {
                typename std::list<
                    boost::shared_ptr<
                        source_entry
                    >
                >::iterator over_next = next;
                over_next++;
                if (
                    active_source.end(
                    ) == over_next
                ) {
                    std::transform(
                        (
                            *accumulating
                        )->begin(
                        ),
                        (
                            *accumulating
                        )->begin(
                        ) + required_samples,
                        (
                            *next
                        )->begin(
                        ),
                        first,
                        &reducing::reduced
                    );
                } else {
                    amplitude_value reduced[
                        required_samples
                    ];
                    std::transform(
                        (
                            *accumulating
                        )->begin(
                        ),
                        (
                            *accumulating
                        )->begin(
                        ) + required_samples,
                        (
                            *next
                        )->begin(
                        ),
                        reduced,
                        &reducing::reduced
                    );
                    accumulating = over_next;
                    accumulating++;
                    if (
                        active_source.end(
                        ) == accumulating
                    ) {
                        std::transform(
                            reduced,
                            reduced + required_samples,
                            (
                                *over_next
                            )->begin(
                            ),
                            first,
                            &reducing::reduced
                        );
                    } else {
                        amplitude_value reduced_alternate[
                            required_samples
                        ];
                        std::transform(
                            reduced,
                            reduced + required_samples,
                            (
                                *over_next
                            )->begin(
                            ),
                            reduced_alternate,
                            &reducing::reduced
                        );
                        amplitude_value* previous_step = reduced_alternate;
                        amplitude_value* next_step = reduced;
                        while (
                            true
                        ) {
                            next = accumulating;
                            next++;
                            if (
                                active_source.end(
                                ) == next
                            ) {
                                std::transform(
                                    previous_step,
                                    previous_step + required_samples,
                                    (
                                        *accumulating
                                    )->begin(
                                    ),
                                    first,
                                    &reducing::reduced
                                );
                                break;
                            } else {
                                std::transform(
                                    previous_step,
                                    previous_step + required_samples,
                                    (
                                        *accumulating
                                    )->begin(
                                    ),
                                    next_step,
                                    &reducing::reduced
                                );
                                accumulating = next;
                                std::swap(
                                    previous_step,
                                    next_step
                                );
                            }
                        }
                    }
                }
            }
        }
        for (
            typename std::list<
                boost::shared_ptr<
                    source_entry
                >
            >::iterator consuming = active_source.begin(
            );
            active_source.end(
            ) != consuming;
            consuming++
        ) {
            (
                *consuming
            )->consume_samples(
                required_samples
            );
        }
        next_offset -= required_samples * hz;
        return required_samples;
    }
    
    template<
        typename streaming,
        typename reducing
    >
    stream_accumulation_source<
        streaming,
        reducing
    >::source_entry::source_entry(
        boost::shared_ptr<
            typename streaming::source_type
        > source
    ) :
    source(
        source
    ),
    first_sample(
        samples.get_data(
        )
    ),
    available_samples(
        0
    ),
    done(
        false
    ) {
    }
    
    template<
        typename streaming,
        typename reducing
    >
    size_t stream_accumulation_source<
        streaming,
        reducing
    >::source_entry::fill_samples(
        size_t requested
    ) {
        if (
            available_samples >= requested
        ) {
            return requested;
        }
        if (
            !done
        ) {
            if (
                samples.get_size(
                ) < requested
            ) {
                resize(
                    requested
                );
            }
            size_t needed = requested - available_samples;
            amplitude_value* const space_end = samples.get_data(
            ) + samples.get_size(
            );
            amplitude_value* free_space_start = first_sample + available_samples;
            if (
                space_end <= free_space_start
            ) {
                free_space_start -= samples.get_size(
                );
            }
            amplitude_value* request_end = free_space_start + needed;
            if (
                space_end < request_end
            ) {
                request_end = space_end;
            }
            size_t const retrieved = bulk_audio_retrieval<
                typename streaming::source_type
            >::get_samples(
                *source,
                free_space_start,
                request_end
            );
            if (
                0 == retrieved
            ) {
                done = true;
            }
            available_samples += retrieved;
            free_space_start += retrieved;
            needed -= retrieved;
            if (
                space_end == free_space_start
            ) {
                if (
                    0 < needed
                ) {
                    free_space_start = samples.get_data(
                    );
                    size_t const retrieved = bulk_audio_retrieval<
                        typename streaming::source_type
                    >::get_samples(
                        *source,
                        free_space_start,
                        free_space_start + needed
                    );
                    if (
                        0 == retrieved
                    ) {
                        done = true;
                    }
                    available_samples += retrieved;
                }
            }
        }
        return available_samples;
    }
    
    template<
        typename streaming,
        typename reducing
    >
    void stream_accumulation_source<
        streaming,
        reducing
    >::source_entry::consume_samples(
        size_t consumed
    ) {
        available_samples -= consumed;
        first_sample += consumed;
        amplitude_value* const space_end = samples.get_data(
        ) + samples.get_size(
        );
        if (
            space_end <= first_sample
        ) {
            first_sample -= samples.get_size(
            );
        }
    }
    
    template<
        typename streaming,
        typename reducing
    >
    stream_accumulation_source<
        streaming,
        reducing
    >::source_entry::iterator::iterator(
    ) {
    }
    
    template<
        typename streaming,
        typename reducing
    >
    stream_accumulation_source<
        streaming,
        reducing
    >::source_entry::iterator::iterator(
        amplitude_value* current_sample,
        amplitude_value* virtual_current_sample,
        amplitude_value* sample_begin,
        amplitude_value* sample_end
    ) :
    current_sample(
        current_sample
    ),
    virtual_current_sample(
        virtual_current_sample
    ),
    sample_begin(
        sample_begin
    ),
    sample_end(
        sample_end
    ) {
    }
    
    template<
        typename streaming,
        typename reducing
    >
    void stream_accumulation_source<
        streaming,
        reducing
    >::source_entry::iterator::operator++(
    ) {
        current_sample++;
        virtual_current_sample++;
        if (
            sample_end == current_sample
        ) {
            current_sample = sample_begin;
        }
    }
    
    template<
        typename streaming,
        typename reducing
    >
    bool stream_accumulation_source<
        streaming,
        reducing
    >::source_entry::iterator::operator!=(
        iterator const& other
    ) const {
        return virtual_current_sample != other.virtual_current_sample;
    }
    
    template<
        typename streaming,
        typename reducing
    >
    int stream_accumulation_source<
        streaming,
        reducing
    >::source_entry::iterator::operator-(
        iterator const& other
    ) const {
        return virtual_current_sample - other.virtual_current_sample;
    }
    
    template<
        typename streaming,
        typename reducing
    >
    typename stream_accumulation_source<
        streaming,
        reducing
    >::source_entry::iterator stream_accumulation_source<
        streaming,
        reducing
    >::source_entry::iterator::operator+(
        size_t added
    ) const {
        iterator result = *this;
        result.current_sample += added;
        result.virtual_current_sample += added;
        if (
            result.sample_end <= result.current_sample
        ) {
            result.current_sample -= (
                result.sample_end - result.sample_begin
            );
        }
        return result;
    }
    
    template<
        typename streaming,
        typename reducing
    >
    amplitude_value& stream_accumulation_source<
        streaming,
        reducing
    >::source_entry::iterator::operator*(
    ) {
        return *current_sample;
    }
    
    template<
        typename streaming,
        typename reducing
    >
    typename stream_accumulation_source<
        streaming,
        reducing
    >::source_entry::iterator stream_accumulation_source<
        streaming,
        reducing
    >::source_entry::begin(
    ) {
        return iterator(
            first_sample,
            first_sample,
            samples.get_data(
            ),
            samples.get_data(
            ) + samples.get_size(
            )
        );
    }
    
    template<
        typename streaming,
        typename reducing
    >
    typename stream_accumulation_source<
        streaming,
        reducing
    >::source_entry::iterator stream_accumulation_source<
        streaming,
        reducing
    >::source_entry::end(
    ) {
        amplitude_value* const after_end = samples.get_data(
        ) + samples.get_size(
        );
        amplitude_value* after_last = first_sample + available_samples;
        if (
            after_end <= after_last
        ) {
            after_last -= samples.get_size(
            );
        } 
        return iterator(
            after_last,
            first_sample + available_samples,
            samples.get_data(
            ),
            after_end
        );
    }
    
    template<
        typename streaming,
        typename reducing
    >
    void stream_accumulation_source<
        streaming,
        reducing
    >::source_entry::resize(
        size_t requested
    ) {
        size_t const old_offset = first_sample - samples.get_data(
        );
        size_t const old_size = samples.get_size(
        );
        samples.resize(
            requested
        );
        memcpy(
            samples.get_data(
            ) + old_size,
            samples.get_data(
            ),
            sizeof(
                *first_sample
            ) * (
                samples.get_size(
                ) - old_size
            )
        );
        first_sample = samples.get_data(
        ) + old_offset;
    }
    
    template<
        typename streaming,
        typename reducing
    >
    size_t bulk_audio_retrieval<
        stream_accumulation_source<
            streaming,
            reducing
        >
    >::get_samples(
        stream_accumulation_source<
            streaming,
            reducing
        >& source,
        amplitude_value* first,
        amplitude_value* last
    ) {
        return source.get_samples(
            first,
            last
        );
    }
}

#endif
