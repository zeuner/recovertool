/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef BPF_TRAITS_INCLUDED
#define BPF_TRAITS_INCLUDED

namespace @qlib@ {
    
    
    template<
        typename value_type
    >
    class bpf_traits {
    };
    
    template<
    >
    class bpf_traits<
        uint8_t
    > {
    public:
        enum {
            size_flag = BPF_B,
        };
    };
    
    template<
    >
    class bpf_traits<
        uint16_t
    > {
    public:
        enum {
            size_flag = BPF_H,
        };
    };
    
    template<
    >
    class bpf_traits<
        uint32_t
    > {
    public:
        enum {
            size_flag = BPF_W,
        };
    };
}

#endif
