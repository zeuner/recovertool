/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef SSL_PROCESSING_INCLUDED
#define SSL_PROCESSING_INCLUDED

#include "@qlib@_allocator_c_wrapper.h"
#include "@qlib@_throw_exception.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    template<
        typename storing = std::allocator<
            char
        >
    >
    class ssl_processing;
    
    template<
        typename storing
    >
    class ssl_processing {
    public:
        ssl_processing(
        );
        ~ssl_processing(
        );
    };
    
    template<
        typename storing
    >
    ssl_processing<
        storing
    >::ssl_processing(
    ) {
        if (
            0 == CRYPTO_set_mem_functions(
                &allocator_c_wrapper<
                    storing
                >::malloc,
                &allocator_c_wrapper<
                    storing
                >::realloc,
                &allocator_c_wrapper<
                    storing
                >::free
            )
        ) {
            throw_exception(
                component_error(
                    "ssl_processing"
                )
            );
        }
    }
    
    template<
        typename storing
    >
    ssl_processing<
        storing
    >::~ssl_processing(
    ) {
        CRYPTO_cleanup_all_ex_data(
        );
        ERR_free_strings(
        );
    }
}

#endif
