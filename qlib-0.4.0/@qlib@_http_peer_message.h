/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef HTTP_PEER_MESSAGE_INCLUDED
#define HTTP_PEER_MESSAGE_INCLUDED

#include <boost/noncopyable.hpp>
#include <map>
namespace @qlib@ {
    
    
    template<
        typename transmitting
    >
    class http_peer_message :
    private boost::noncopyable {
    public:
        http_peer_message(
            transmitting& transmitter
        );
        typedef typename transmitting::string string;
        void set_resource(
            string const& resource
        );
        string const& get_resource(
        ) const;
        void set_data(
            string const& data
        );
        string const& get_data(
        ) const;
        void set_headers(
            std::map<
                string,
                string
            > const& headers
        );
        std::map<
            string,
            string
        > const& get_headers(
        ) const;
        void set_finished(
            bool finished
        );
        bool get_finished(
        ) const;
    protected:
        transmitting& get_transmitter(
        );
    private:
        transmitting& transmitter;
        string resource;
        string data;
        std::map<
            string, 
            string 
        > headers;
        bool finished;
    };
    
    template<
        typename transmitting
    >
    http_peer_message<
        transmitting
    >::http_peer_message(
        transmitting& transmitter
    ) :
    transmitter(
        transmitter
    ),
    finished(
        true
    ) {
    }
    
    template<
        typename transmitting
    >
    void http_peer_message<
        transmitting
    >::set_resource(
        typename http_peer_message<
            transmitting
        >::string const& resource
    ) {
        this->resource = resource;
    }
    
    template<
        typename transmitting
    >
    typename http_peer_message<
        transmitting
    >::string const& http_peer_message<
        transmitting
    >::get_resource(
    ) const {
        return resource;
    }
    
    template<
        typename transmitting
    >
    void http_peer_message<
        transmitting
    >::set_data(
        typename http_peer_message<
            transmitting
        >::string const& data
    ) {
        this->data = data;
    }
    
    template<
        typename transmitting
    >
    typename http_peer_message<
        transmitting
    >::string const& http_peer_message<
        transmitting
    >::get_data(
    ) const {
        return data;
    }
    
    template<
        typename transmitting
    >
    void http_peer_message<
        transmitting
    >::set_headers(
        std::map<
            typename http_peer_message<
                transmitting
            >::string,
            typename http_peer_message<
                transmitting
            >::string
        > const& headers
    ) {
        this->headers = headers;
    }
    
    template<
        typename transmitting
    >
    std::map<
        typename http_peer_message<
            transmitting
        >::string,
        typename http_peer_message<
            transmitting
        >::string
    > const& http_peer_message<
        transmitting
    >::get_headers(
    ) const {
        return headers;
    }
    
    template<
        typename transmitting
    >
    void http_peer_message<
        transmitting
    >::set_finished(
        bool finished
    ) {
        this->finished = finished;
    }
    
    template<
        typename transmitting
    >
    bool http_peer_message<
        transmitting
    >::get_finished(
    ) const {
        return finished;
    }
    
    template<
        typename transmitting
    >
    transmitting& http_peer_message<
        transmitting
    >::get_transmitter(
    ) {
        return transmitter;
    }
}

#endif
