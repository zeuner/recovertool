/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_decoded_mac_address.h"
namespace @qlib@ {
    
    
    std::string decoded_mac_address(
        std::string const& raw
    ) {
        std::string value;
        static char const hex[
        ] = "0123456789ABCDEF";
        std::string delimiter;
        for (
            std::string::const_iterator extracting = raw.begin(
            );
            raw.end(
            ) != extracting;
            extracting++
        ) {
            value += delimiter;
            unsigned char const byte = *extracting;
            value += hex[
                byte >> 4
            ];
            value += hex[
                byte & 0xf
            ];
            delimiter = ":";
        }
        return value;
    }
}
