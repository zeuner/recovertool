/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_xml_document.h"
namespace @qlib@ {
    
    
    xml_document::xml_document(
    ) :
    data(
        xmlNewDoc(
            BAD_CAST "1.0"
        )
    ) {
        if (
            NULL == data
        ) {
            throw component_error(
                "xml_document"
            );
        }
    }
    
    xml_document::~xml_document(
    ) {
        xmlFreeDoc(
            data
        );
    }
    
    xmlNodePtr xml_document::get_root_node(
    ) const {
        return xmlDocGetRootElement(
            data
        );
    }
    
    xmlDocPtr xml_document::get_data(
    ) const {
        return data;
    }
    
    xml_document::xml_document(
        xmlDocPtr data
    ) :
    data(
        data
    ) {
        if (
            NULL == data
        ) {
            throw component_error(
                "xml_document"
            );
        }
    }
}
