/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef CMPI_ASSOCIATION_WRAPPER_INCLUDED
#define CMPI_ASSOCIATION_WRAPPER_INCLUDED

#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    template<
        typename managed
    >
    class cmpi_association_wrapper :
    public managed {
    public:
        static CMPIStatus cmpiAssociatorNames(
            CMPIAssociationMI* self,
            CMPIContext const* context,
            CMPIResult const* results,
            CMPIObjectPath const* reference,
            char const* association_class,
            char const* result_class,
            char const* queried_role,
            char const* result_role
        );
        static CMPIStatus cmpiAssociators(
            CMPIAssociationMI* self,
            CMPIContext const* context,
            CMPIResult const* results,
            CMPIObjectPath const* reference,
            char const* association_class,
            char const* result_class,
            char const* queried_role,
            char const* result_role,
            char const** properties
        );
        static CMPIStatus cmpiReferenceNames(
            CMPIAssociationMI* self,
            CMPIContext const* context,
            CMPIResult const* results,
            CMPIObjectPath const* reference,
            char const* association_class,
            char const* queried_role
        );
        static CMPIStatus cmpiReferences(
            CMPIAssociationMI* self,
            CMPIContext const* context,
            CMPIResult const* results,
            CMPIObjectPath const* reference,
            char const* association_class,
            char const* queried_role,
            char const** properties
        );
        static CMPIStatus cmpiAssociationCleanup(
            CMPIAssociationMI* self,
            CMPIContext const* context,
            CMPIBoolean terminating
        );
        static CMPIBroker const* broker;
    private:
        static CMPIStatus const ok;
        static CMPIStatus const unsupported;
        class dependent_adding_visitor {
        public:
            dependent_adding_visitor(
                CMPIResult const* augmented,
                CMPIBroker const* broker,
                char const* cim_namespace
            );
            void operator(
            )(
                typename cmpi_association_wrapper<
                    managed
                >::antecedent_type& antecedent,
                typename cmpi_association_wrapper<
                    managed
                >::dependent_type& dependent
            );
        private:
            CMPIResult const* augmented;
            CMPIBroker const* broker;
            char const* cim_namespace;
        };
        class antecedent_adding_visitor {
        public:
            antecedent_adding_visitor(
                CMPIResult const* augmented,
                CMPIBroker const* broker,
                char const* cim_namespace
            );
            void operator(
            )(
                typename cmpi_association_wrapper<
                    managed
                >::antecedent_type& antecedent,
                typename cmpi_association_wrapper<
                    managed
                >::dependent_type& dependent
            );
        private:
            CMPIResult const* augmented;
            CMPIBroker const* broker;
            char const* cim_namespace;
        };
        class association_adding_visitor {
        public:
            association_adding_visitor(
                CMPIResult const* augmented,
                CMPIBroker const* broker,
                char const* cim_namespace
            );
            void operator(
            )(
                typename cmpi_association_wrapper<
                    managed
                >::antecedent_type& antecedent,
                typename cmpi_association_wrapper<
                    managed
                >::dependent_type& dependent
            );
        private:
            CMPIResult const* augmented;
            CMPIBroker const* broker;
            char const* cim_namespace;
        };
    };
    
    #define cmpi_association_wrapper_wrap(\
        wrapped\
    ) CMAssociationMIStub(\
        cmpi_association_wrapper<\
            wrapped\
        >::cmpi,\
        wrapped,\
        cmpi_association_wrapper<\
            wrapped\
        >::broker,\
        CMNoHook\
    )
    
    template<
        typename managed
    >
    CMPIStatus cmpi_association_wrapper<
        managed
    >::cmpiAssociatorNames(
        CMPIAssociationMI* self,
        CMPIContext const* context,
        CMPIResult const* results,
        CMPIObjectPath const* reference,
        char const* association_class,
        char const* result_class,
        char const* queried_role,
        char const* result_role
    ) {
        CMPIString* reference_class = CMGetClassName(
            reference,
            NULL
        );
        if (
            NULL == reference_class
        ) {
            throw component_error(
                "cmpi_association_wrapper"
            );
        }
        if (
            CMClassPathIsA(
                broker,
                reference,
                cmpi_association_wrapper<
                    managed
                >::antecedent_type::get_class_name(
                ).c_str(
                ),
                NULL
            )
        ) {
            typename cmpi_association_wrapper<
                managed
            >::antecedent_type* found = NULL;
            get_by_cmpi_reference(
                broker,
                reference,
                found
            );
            if (
                NULL == found
            ) {
                return ok;
            }
            dependent_adding_visitor visiting(
                results,
                broker,
                CMGetCharPtr(
                    CMGetNameSpace(
                        reference,
                        NULL
                    )
                )
            );
            cmpi_association_wrapper<
                managed
            >::visit_antecedent_associations(
                *found,
                visiting
            );
            return ok;
        }
        if (
            CMClassPathIsA(
                broker,
                reference,
                cmpi_association_wrapper<
                    managed
                >::dependent_type::get_class_name(
                ).c_str(
                ),
                NULL
            )
        ) {
            typename cmpi_association_wrapper<
                managed
            >::dependent_type* found = NULL;
            get_by_cmpi_reference(
                broker,
                reference,
                found
            );
            if (
                NULL == found
            ) {
                return ok;
            }
            antecedent_adding_visitor visiting(
                results,
                broker,
                CMGetCharPtr(
                    CMGetNameSpace(
                        reference,
                        NULL
                    )
                )
            );
            cmpi_association_wrapper<
                managed
            >::visit_dependent_associations(
                *found,
                visiting
            );
            return ok;
        }
        throw component_error(
            "cmpi_association_wrapper"
        );
    }
    
    template<
        typename managed
    >
    CMPIStatus cmpi_association_wrapper<
        managed
    >::cmpiAssociators(
        CMPIAssociationMI* self,
        CMPIContext const* context,
        CMPIResult const* results,
        CMPIObjectPath const* reference,
        char const* association_class,
        char const* result_class,
        char const* queried_role,
        char const* result_role,
        char const** properties
    ) {
        return unsupported;
    }
    
    template<
        typename managed
    >
    CMPIStatus cmpi_association_wrapper<
        managed
    >::cmpiReferenceNames(
        CMPIAssociationMI* self,
        CMPIContext const* context,
        CMPIResult const* results,
        CMPIObjectPath const* reference,
        char const* association_class,
        char const* queried_role
    ) {
        CMPIString* reference_class = CMGetClassName(
            reference,
            NULL
        );
        if (
            NULL == reference_class
        ) {
            throw component_error(
                "cmpi_association_wrapper"
            );
        }
        association_adding_visitor visiting(
            results,
            broker,
            CMGetCharPtr(
                CMGetNameSpace(
                    reference,
                    NULL
                )
            )
        );
        if (
            CMClassPathIsA(
                broker,
                reference,
                cmpi_association_wrapper<
                    managed
                >::antecedent_type::get_class_name(
                ).c_str(
                ),
                NULL
            )
        ) {
            typename cmpi_association_wrapper<
                managed
            >::antecedent_type* found = NULL;
            get_by_cmpi_reference(
                broker,
                reference,
                found
            );
            if (
                NULL == found
            ) {
                return ok;
            }
            cmpi_association_wrapper<
                managed
            >::visit_antecedent_associations(
                *found,
                visiting
            );
            return ok;
        }
        if (
            CMClassPathIsA(
                broker,
                reference,
                cmpi_association_wrapper<
                    managed
                >::dependent_type::get_class_name(
                ).c_str(
                ),
                NULL
            )
        ) {
            typename cmpi_association_wrapper<
                managed
            >::dependent_type* found = NULL;
            get_by_cmpi_reference(
                broker,
                reference,
                found
            );
            if (
                NULL == found
            ) {
                return ok;
            }
            cmpi_association_wrapper<
                managed
            >::visit_dependent_associations(
                *found,
                visiting
            );
            return ok;
        }
        throw component_error(
            "cmpi_association_wrapper"
        );
    }
    
    template<
        typename managed
    >
    CMPIStatus cmpi_association_wrapper<
        managed
    >::cmpiReferences(
        CMPIAssociationMI* self,
        CMPIContext const* context,
        CMPIResult const* results,
        CMPIObjectPath const* reference,
        char const* association_class,
        char const* queried_role,
        char const** properties
    ) {
        return unsupported;
    }
    
    template<
        typename managed
    >
    CMPIStatus cmpi_association_wrapper<
        managed
    >::cmpiAssociationCleanup(
        CMPIAssociationMI* self,
        CMPIContext const* context,
        CMPIBoolean terminating
    ) {
        return ok;
    }
    
    template<
        typename managed
    >
    cmpi_association_wrapper<
        managed
    >::dependent_adding_visitor::dependent_adding_visitor(
        CMPIResult const* augmented,
        CMPIBroker const* broker,
        char const* cim_namespace
    ) :
    augmented(
        augmented
    ),
    broker(
        broker
    ),
    cim_namespace(
        cim_namespace
    ) {
    }
    
    template<
        typename managed
    >
    void cmpi_association_wrapper<
        managed
    >::dependent_adding_visitor::operator(
    )(
        typename cmpi_association_wrapper<
            managed
        >::antecedent_type& antecedent,
        typename cmpi_association_wrapper<
            managed
        >::dependent_type& dependent
    ) {
        CMPIStatus status;
        CMPIObjectPath* instantiated = create_cmpi_object_path(
            broker,
            status,
            cim_namespace,
            &dependent
        );
        CMReturnObjectPath(
            augmented,
            instantiated
        );
    }
    
    template<
        typename managed
    >
    cmpi_association_wrapper<
        managed
    >::antecedent_adding_visitor::antecedent_adding_visitor(
        CMPIResult const* augmented,
        CMPIBroker const* broker,
        char const* cim_namespace
    ) :
    augmented(
        augmented
    ),
    broker(
        broker
    ),
    cim_namespace(
        cim_namespace
    ) {
    }
    
    template<
        typename managed
    >
    void cmpi_association_wrapper<
        managed
    >::antecedent_adding_visitor::operator(
    )(
        typename cmpi_association_wrapper<
            managed
        >::antecedent_type& antecedent,
        typename cmpi_association_wrapper<
            managed
        >::dependent_type& dependent
    ) {
        CMPIStatus status;
        CMPIObjectPath* instantiated = create_cmpi_object_path(
            broker,
            status,
            cim_namespace,
            &antecedent
        );
        CMReturnObjectPath(
            augmented,
            instantiated
        );
    }
    
    template<
        typename managed
    >
    cmpi_association_wrapper<
        managed
    >::association_adding_visitor::association_adding_visitor(
        CMPIResult const* augmented,
        CMPIBroker const* broker,
        char const* cim_namespace
    ) :
    augmented(
        augmented
    ),
    broker(
        broker
    ),
    cim_namespace(
        cim_namespace
    ) {
    }
    
    template<
        typename managed
    >
    void cmpi_association_wrapper<
        managed
    >::association_adding_visitor::operator(
    )(
        typename cmpi_association_wrapper<
            managed
        >::antecedent_type& antecedent,
        typename cmpi_association_wrapper<
            managed
        >::dependent_type& dependent
    ) {
        CMPIStatus status;
        CMPIObjectPath* instantiated_antecedent = create_cmpi_object_path(
            broker,
            status,
            cim_namespace,
            &antecedent
        );
        CMPIObjectPath* instantiated_dependent = create_cmpi_object_path(
            broker,
            status,
            cim_namespace,
            &dependent
        );
        CMPIObjectPath* instantiated = CMNewObjectPath(
            broker,
            cim_namespace,
            cmpi_association_wrapper<
                managed
            >::get_class_name(
            ).c_str(
            ),
            &status
        );
        if (
            CMIsNullObject(
                instantiated
            )
        ) {
            throw component_error(
                "cmpi_association_wrapper"
            );
        }
        CMAddKey(
            instantiated,
            cmpi_association_wrapper<
                managed
            >::get_antecedent_name(
            ).c_str(
            ),
            &instantiated_antecedent,
            CMPI_ref
        );
        CMAddKey(
            instantiated,
            cmpi_association_wrapper<
                managed
            >::get_dependent_name(
            ).c_str(
            ),
            &instantiated_dependent,
            CMPI_ref
        );
        CMReturnObjectPath(
            augmented,
            instantiated
        );
    }
    
    template<
        typename managed
    >
    CMPIBroker const* cmpi_association_wrapper<
        managed
    >::broker = NULL;
    
    template<
        typename managed
    >
    CMPIStatus const cmpi_association_wrapper<
        managed
    >::ok = {
        CMPI_RC_OK,
        NULL
    };
    
    template<
        typename managed
    >
    CMPIStatus const cmpi_association_wrapper<
        managed
    >::unsupported = {
        CMPI_RC_ERR_NOT_SUPPORTED,
        NULL
    };
}

#endif
