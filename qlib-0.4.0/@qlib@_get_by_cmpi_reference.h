/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef GET_BY_CMPI_REFERENCE_INCLUDED
#define GET_BY_CMPI_REFERENCE_INCLUDED

namespace @qlib@ {
    
    
    template<
        typename managed
    >
    void get_by_cmpi_reference(
        CMPIBroker const* broker,
        CMPIObjectPath const* reference,
        managed*& retrieved
    ) {
        typename managed::key_type key_value;
        get_cmpi_property(
            broker,
            reference,
            managed::get_key_name(
            ),
            key_value
        );
        managed::retrieve_by_key(
            key_value,
            retrieved
        );
    }
}

#endif
