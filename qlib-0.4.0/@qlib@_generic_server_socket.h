/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef GENERIC_SERVER_SOCKET_INCLUDED
#define GENERIC_SERVER_SOCKET_INCLUDED

#include <boost/function.hpp>
#include <boost/asio/ip/tcp.hpp>
namespace @qlib@ {
    
    
    class generic_server_socket {
    public:
        typedef boost::asio::ip::tcp::socket::lowest_layer_type lowest_layer_type;
        virtual ~generic_server_socket(
        );
        virtual void async_start(
            boost::function<
                void(
                    boost::system::error_code const& error
                )
            > start_handler
        ) = 0;
        virtual lowest_layer_type& lowest_layer(
        ) = 0;
        virtual boost::asio::io_service& get_io_service(
        ) = 0;
        virtual void async_read_some(
            boost::asio::mutable_buffers_1 const& destination,
            boost::function<
                void(
                    boost::system::error_code const& error,
                    size_t received
                )
            > const& read_handler
        ) = 0;
        virtual void async_write_some(
            boost::asio::mutable_buffers_1 const& source,
            boost::function<
                void(
                    boost::system::error_code const& error,
                    size_t sent
                )
            > const& write_handler
        ) = 0;
        virtual void async_write_some(
            boost::asio::const_buffers_1 const& source,
            boost::function<
                void(
                    boost::system::error_code const& error,
                    size_t sent
                )
            > const& write_handler
        ) = 0;
    };
}

#endif
