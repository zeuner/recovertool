/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_icmp_route_trace.h"
#include <boost/asio/ip/address_v4.hpp>
#include <boost/asio/placeholders.hpp>
#include "@qlib@_component_error.h"
#include <boost/bind.hpp>
namespace @qlib@ {
    
    
    icmp_route_trace::icmp_route_trace(
        boost::asio::io_service& communicating,
        boost::asio::ip::address const& destination,
        boost::posix_time::time_duration const& timeout
    ) :
    tracing(
        communicating,
        boost::asio::ip::icmp::v4(
        )
    ),
    destination(
        destination,
        0
    ),
    expiring(
        communicating
    ),
    sequence_number(
        0
    ),
    timeout(
        timeout
    ) {
        unsigned const buffer_size = 24;
        char buffer[
            buffer_size
        ];
        static byte_random_source randomizing;
        for (
            unsigned initializing = 0;
            buffer_size > initializing;
            initializing++
        ) {
            buffer[
                initializing
            ] = randomizing(
            );
        }
        payload = std::string(
            buffer,
            buffer_size
        );
        char* identifier_buffer = reinterpret_cast<
            char*
        >(
            &identifier
        );
        for (
            unsigned initializing = 0;
            sizeof(
                identifier
            ) > initializing;
            initializing++
        ) {
            identifier_buffer[
                initializing
            ] = randomizing(
            );
        }
    }
    
    void icmp_route_trace::async_trace(
        int ttl,
        icmp_route_trace::handler_function const& response_handler
    ) {
        struct icmphdr echo_request;
        echo_request.type = ICMP_ECHO;
        echo_request.code = 0;
        echo_request.checksum = 0;
        echo_request.un.echo.id = htons(
            identifier
        );
        echo_request.un.echo.sequence = htons(
            ++sequence_number
        );
        std::string const unchecked(
            std::string(
                reinterpret_cast<
                    char const*
                >(
                    &echo_request
                ),
                sizeof(
                    echo_request
                )
            ) + payload
        );
        echo_request.checksum = ip_header_checksum(
            reinterpret_cast<
                unsigned short const*
            >(
                unchecked.data(
                )
            ),
            unchecked.size(
            )
        );
        sending = std::string(
            reinterpret_cast<
                char const*
            >(
                &echo_request
            ),
            sizeof(
                echo_request
            )
        ) + payload;
        boost::asio::ip::unicast::hops const setting(
            ttl
        );
        tracing.set_option(
            setting
        );
        boost::asio::ip::unicast::hops getting;
        tracing.get_option(
            getting
        );
        if (
            getting.value(
            ) != ttl
        ) {
            throw component_error(
                "icmp_route_trace"
            );
        }
        boost::shared_ptr<
            boost::shared_ptr<
                handler_function 
            > 
        > shared(
            new boost::shared_ptr<
                handler_function         
            >(
                new handler_function(
                    response_handler
                )
            )
        );
        tracing.async_send_to(
            boost::asio::buffer(
                sending.data(
                ),
                sending.size(
                )
            ),
            destination,
            boost::bind(
                &icmp_route_trace::handle_send,
                this,
                shared,
                boost::asio::placeholders::error
            )
        );
        expiring.expires_from_now(
            timeout
        );
        expiring.async_wait(
            boost::bind(
                &icmp_route_trace::handle_timeout,
                this,
                shared,
                boost::asio::placeholders::error
            )
        );
    }   
    
    void icmp_route_trace::handle_send(
        boost::shared_ptr<
            boost::shared_ptr<
                icmp_route_trace::handler_function
            >
        > const& response_handler,
        boost::system::error_code const& error
    ) {
        if (
            boost::asio::error::operation_aborted == error
        ) {
            return;
        }
        if (
            error
        ) {
            expiring.cancel(
            );
            if (
                !*response_handler
            ) {
                return;
            }
            (
                **response_handler
            )(
                error,
                boost::asio::ip::address(
                )
            );
            response_handler->reset(
            );
            return;
        }
        boost::asio::socket_base::message_flags const flags = 0;
        tracing.async_receive(
            boost::asio::buffer(
                receiving,
                receiving_size
            ),
            flags,
            boost::bind(
                &icmp_route_trace::handle_receive,
                this,
                response_handler,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }   
    
    void icmp_route_trace::handle_timeout(
        boost::shared_ptr<
            boost::shared_ptr<
                icmp_route_trace::handler_function 
            > 
        > const& response_handler,
        boost::system::error_code const& error
    ) {
        if (
            boost::asio::error::operation_aborted == error
        ) {
            return;
        }
        tracing.cancel(
        );
        if (
            !*response_handler
        ) { 
            return;
        }
        if (
            error
        ) {
            (
                **response_handler
            )(
                error,
                boost::asio::ip::address(
                )
            );
            response_handler->reset(
            );
            return;
        }
        (
            **response_handler
        )(
            boost::system::error_code(
                boost::system::errc::timed_out,
                boost::system::get_generic_category(
                )
            ),
            boost::asio::ip::address(
            )
        );
        response_handler->reset(
        );
    }   
    
    void icmp_route_trace::handle_receive(
        boost::shared_ptr<
            boost::shared_ptr<
                icmp_route_trace::handler_function 
            > 
        > const& response_handler,
        boost::system::error_code const& error,
        size_t bytes
    ) {
        if (
            boost::asio::error::operation_aborted == error
        ) {
            return;
        }
        expiring.cancel(
        );
        if (
            error
        ) {
            if (
                !*response_handler
            ) { 
                return;
            }
            (
                **response_handler
            )(
                error,
                boost::asio::ip::address(
                )
            );
            response_handler->reset(
            );
            return;
        }
        struct iphdr const* header;
        if (
            sizeof(
                *header
            ) > bytes
        ) {
            throw component_error(
                "icmp_route_trace"
            );
        }
        header = reinterpret_cast<
            struct iphdr const*
        >(
            receiving
        );
        boost::asio::ip::address_v4 remote(
            ntohl(
                header->saddr
            )
        );
        if (
            !*response_handler
        ) { 
            return;
        }
        (
            **response_handler
        )(
            boost::system::error_code(
            ),
            remote
        );
        response_handler->reset(
        );
    }
}
