/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef MEMORY_BASE_BIO_INCLUDED
#define MEMORY_BASE_BIO_INCLUDED

namespace @qlib@ {
    
    
    class memory_base_bio {
    public:
        ~memory_base_bio(
        );
        BIO* get_data(
        ) const;
        template<
            typename string
        >
        void export_data(
            string& destination
        );
        void flush(
        );
    protected:
        memory_base_bio(
            BIO* data
        );
    private:
        BIO* data;
    };
    
    template<
        typename string
    >
    void memory_base_bio::export_data(
        string& destination
    ) {
        char* characters;
        long data_length = BIO_get_mem_data(
            data,
            &characters
        );
        destination.replace(
            0,
            destination.size(
            ),
            characters,
            data_length
        );
    }
}

#endif
