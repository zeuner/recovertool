/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef RESOLVER_DECODING_SESSION_INCLUDED
#define RESOLVER_DECODING_SESSION_INCLUDED

#include <boost/function.hpp>
#include <string>
#include <boost/fusion/container/vector.hpp>
#include <boost/fusion/include/vector.hpp>
#include <boost/fusion/container/vector/vector_fwd.hpp>
#include <boost/asio/ip/address_v4.hpp>
#include <boost/fusion/include/vector_fwd.hpp>
#include <list>
namespace @qlib@ {
    
    
    class resolver_decoding_session :
    public resolver_session,
    protected ip_address_decoding {
    public:
        typedef boost::fusion::vector<
            std::string,
            unsigned,
            unsigned,
            unsigned,
            unsigned,
            std::string
        > mx_resource_record;
        typedef boost::fusion::vector<
            std::string,
            unsigned,
            unsigned,
            unsigned,
            std::string
        > ptr_resource_record;
        typedef boost::fusion::vector<
            std::string,
            unsigned,
            unsigned,
            unsigned,
            boost::asio::ip::address_v4
        > a_resource_record;
        typedef boost::fusion::vector<
            std::string,
            unsigned,
            unsigned,
            unsigned,
            boost::asio::ip::address_v6
        > aaaa_resource_record;
        resolver_decoding_session(
            boost::asio::io_service& communicating,
            std::list<
                boost::asio::ip::address
            > const& dns_servers,
            boost::asio::ip::udp::endpoint const& local_endpoint_v4 = boost::asio::ip::udp::endpoint(
                boost::asio::ip::udp::v4(
                ),
                0
            ),
            boost::asio::ip::udp::endpoint const& local_endpoint_v6 = boost::asio::ip::udp::endpoint(
                boost::asio::ip::udp::v6(
                ),
                0
            )
        );
    private:
        typedef boost::function<
            void(
                boost::system::error_code const& error,
                std::string const& raw_result,
                std::list<
                    mx_resource_record
                > const& records
            )
        > handling_mx_result;
        typedef boost::function<
            void(
                boost::system::error_code const& error,
                std::string const& raw_result,
                std::list<
                    ptr_resource_record
                > const& records
            )
        > handling_ptr_result;
        typedef boost::function<
            void(
                boost::system::error_code const& error,
                std::string const& raw_result,
                std::list<
                    a_resource_record
                > const& records
            )
        > handling_a_result;
        typedef boost::function<
            void(
                boost::system::error_code const& error,
                std::string const& raw_result,
                std::list<
                    aaaa_resource_record
                > const& records
            )
        > handling_aaaa_result;
    public:
        void async_resolve_mx(
            char const* domain_name,
            handling_mx_result const& result_handler
        );
        void async_resolve_ptr(
            char const* address,
            handling_ptr_result const& result_handler
        );
        void async_resolve_a(
            char const* domain_name,
            handling_a_result const& result_handler
        );
        void async_resolve_aaaa(
            char const* domain_name,
            handling_aaaa_result const& result_handler
        );
        void async_resolve_mx(
            std::string const& domain_name,
            handling_mx_result const& result_handler
        );
        void async_resolve_ptr(
            boost::asio::ip::address_v4 const& address,
            handling_ptr_result const& result_handler
        );
        void async_resolve_a(
            std::string const& domain_name,
            handling_a_result const& result_handler
        );
        void async_resolve_aaaa(
            std::string const& domain_name,
            handling_aaaa_result const& result_handler
        );
        void handle_resolve_mx(
            boost::system::error_code const& error,
            std::string const& raw_result,
            std::list<
                resource_record
            > const& records,
            handling_mx_result const& result_handler
        );
        void handle_resolve_ptr(
            boost::system::error_code const& error,
            std::string const& raw_result,
            std::list<
                resource_record
            > const& records,
            handling_ptr_result const& result_handler
        );
        void handle_resolve_a(
            boost::system::error_code const& error,
            std::string const& raw_result,
            std::list<
                resource_record
            > const& records,
            handling_a_result const& result_handler
        );
        void handle_resolve_aaaa(
            boost::system::error_code const& error,
            std::string const& raw_result,
            std::list<
                resource_record
            > const& records,
            handling_aaaa_result const& result_handler
        );
    private:
        class a_record_resolution_handler {
        public:
            a_record_resolution_handler(
                resolver_decoding_session* resolver,
                resolver_decoding_session::handling_a_result const& result_handler
            );
            void operator(
            )(
                boost::system::error_code const& error,
                std::string const& raw_result,
                std::list<
                    resource_record
                > const& records
            );
        private:
            resolver_decoding_session* resolver;
            resolver_decoding_session::handling_a_result result_handler;
        };
        class mx_record_resolution_handler {
        public:
            mx_record_resolution_handler(
                resolver_decoding_session* resolver,
                handling_mx_result const& result_handler
            );
            void operator(
            )(
                boost::system::error_code const& error,
                std::string const& raw_result,
                std::list<
                    resource_record
                > const& records
            );
        private:
            resolver_decoding_session* resolver;
            handling_mx_result result_handler;
        };
        class ptr_record_resolution_handler {
        public:
            ptr_record_resolution_handler(
                resolver_decoding_session* resolver,
                handling_ptr_result const& result_handler
            );
            void operator(
            )(
                boost::system::error_code const& error,
                std::string const& raw_result,
                std::list<
                    resource_record
                > const& records
            );
        private:
            resolver_decoding_session* resolver;
            handling_ptr_result result_handler;
        };
        class aaaa_record_resolution_handler {
        public:
            aaaa_record_resolution_handler(
                resolver_decoding_session* resolver,
                resolver_decoding_session::handling_aaaa_result const& result_handler
            );
            void operator(
            )(
                boost::system::error_code const& error,
                std::string const& raw_result,
                std::list<
                    resource_record
                > const& records
            );
        private:
            resolver_decoding_session* resolver;
            resolver_decoding_session::handling_aaaa_result result_handler;
        };
    };
}

#endif
