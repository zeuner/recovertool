/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_tcp_socket.h"
#include <boost/asio/ip/address_v4.hpp>
#include <boost/asio/placeholders.hpp>
#include "@qlib@_system_log.h"
#include <vector>
#include <boost/fusion/sequence/intrinsic/at_c.hpp>
#include <boost/bind.hpp>
#include <boost/fusion/include/at_c.hpp>
namespace @qlib@ {
    
    
    tcp_socket::tcp_socket(
        boost::asio::io_service& communicating,
        resolver_decoding_session* resolving
    ) :
    implementation(
        communicating
    ),
    resolving(
        resolving
    ) {
    }
    
    tcp_socket::tcp_socket(
        std::pair<
            boost::asio::io_service*,
            resolver_decoding_session*
        > const& initializer
    ) :
    implementation(
        *initializer.first
    ),
    resolving(
        initializer.second
    ) {
    }
    
    tcp_socket::tcp_socket(
        boost::asio::io_service& communicating,
        resolver_decoding_session* resolving,
        boost::asio::ip::tcp::endpoint const& local
    ) :
    implementation(
        communicating,
        local
    ),
    resolving(
        resolving
    ) {
    }
    
    void tcp_socket::async_connect(
        boost::asio::ip::tcp::endpoint const& endpoint,
        boost::function<
            void(
                boost::system::error_code const& error
            )
        > connect_handler
    ) {
        implementation.async_connect(
            endpoint,
            connect_handler
        );
    }
    
    void tcp_socket::async_connect(
        std::string const& hostname,
        unsigned short port,
        boost::function<
            void(
                boost::system::error_code const& error
            )
        > const& connect_handler
    ) {
        boost::system::error_code error;
        boost::asio::ip::address_v4 const numeric(
            boost::asio::ip::address_v4::from_string(
                hostname,
                error
            )
        );
        if (
            error
        ) {
            resolving->async_resolve_a(
                hostname,
                boost::bind(
                    &tcp_socket::handle_resolve,
                    this,
                    connect_handler,
                    boost::asio::placeholders::error,
                    resolver_decoding_session::records_result_argument,
                    hostname,
                    port
                )
            );
        } else {
            async_connect(
                boost::asio::ip::tcp::endpoint(
                    numeric,
                    port
                ),
                connect_handler
            );
        }
    }
    
    void tcp_socket::close(
    ) {
        implementation.close(
        );
    }
    
    void tcp_socket::cancel(
    ) {
        implementation.cancel(
        );
    }
    
    boost::asio::io_service& tcp_socket::get_io_service(
    ) {
        return implementation.get_io_service(
        );
    }
    
    void tcp_socket::async_read_some(
        boost::asio::mutable_buffer const& destination,
        boost::function<
            void(
                boost::system::error_code const& error,
                size_t received
            )
        > const& read_handler
    ) {
        implementation.async_read_some(
            boost::asio::mutable_buffers_1(
                destination
            ),
            read_handler
        );
    }
    
    void tcp_socket::async_write_some(
        boost::asio::mutable_buffer const& source,
        boost::function<
            void(
                boost::system::error_code const& error,
                size_t sent
            )
        > const& write_handler
    ) {
        implementation.async_write_some(
            boost::asio::mutable_buffers_1(
                source
            ),
            write_handler
        );
    }
    
    void tcp_socket::async_write_some(
        boost::asio::const_buffer const& source,
        boost::function<
            void(
                boost::system::error_code const& error,
                size_t sent
            )
        > const& write_handler
    ) {
        implementation.async_write_some(
            boost::asio::const_buffers_1(
                source
            ),
            write_handler
        );
    }
    
    boost::asio::ip::tcp::endpoint tcp_socket::local_endpoint(
    ) const {
        return implementation.local_endpoint(
        );
    }
    
    void tcp_socket::handle_resolve(
        boost::function<
            void(
                boost::system::error_code const& error
            )
        > connect_handler,
        boost::system::error_code const& error,
        std::list<
            resolver_decoding_session::a_resource_record
        > const& records,
        std::string hostname,
        unsigned short port
    ) {
        if (
            error
        ) {
            connect_handler(
                error
            );
            return;
        }
        if (
            records.empty(
            )
        ) {
            system_log.log_error(
                "host not found: " + hostname
            );
            connect_handler(
                boost::system::error_code(
                    dns_error_codes::host_not_found,
                    dns_category(
                    )
                )
            );
            return;
        }
        std::vector<
            resolver_decoding_session::a_resource_record
        > choosable(
            records.begin(
            ),
            records.end(
            )
        );
        static integer_random_source choosing(
            0x10000
        );
        int choice = static_cast<
            long long
        >(
            choosing(
            )
        ) * choosable.size(
        ) / 0x10000;
        async_connect(
            boost::asio::ip::tcp::endpoint(
                boost::fusion::at_c<
                    4
                >(
                    choosable[
                        choice
                    ]
                ),
                port
            ),
            connect_handler
        );
    }
}
