/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef FORWARDING_SERVER_SOCKET_INCLUDED
#define FORWARDING_SERVER_SOCKET_INCLUDED

#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>
namespace @qlib@ {
    
    
    template<
        typename implementing
    >
    class forwarding_server_socket :
    public generic_server_socket {
    public:
        forwarding_server_socket(
            boost::shared_ptr<
                implementing
            > implementation
        );
        void async_start(
            boost::function<
                void(
                    boost::system::error_code const& error
                )
            > start_handler
        );
        lowest_layer_type& lowest_layer(
        );
        boost::asio::io_service& get_io_service(
        );
        void async_read_some(
            boost::asio::mutable_buffers_1 const& destination,
            boost::function<
                void(
                    boost::system::error_code const& error,
                    size_t received
                )
            > const& read_handler
        );
        void async_write_some(
            boost::asio::mutable_buffers_1 const& source,
            boost::function<
                void(
                    boost::system::error_code const& error,
                    size_t sent
                )
            > const& write_handler
        );
        void async_write_some(
            boost::asio::const_buffers_1 const& source,
            boost::function<
                void(
                    boost::system::error_code const& error,
                    size_t sent
                )
            > const& write_handler
        );
    private:
        boost::shared_ptr<
            implementing
        > implementation;
    };
    
    template<
        typename implementing
    >
    forwarding_server_socket<
        implementing
    >::forwarding_server_socket(
        boost::shared_ptr<
            implementing
        > implementation
    ) :
    implementation(
        implementation
    ) {
    }
    
    template<
        typename implementing
    >
    void forwarding_server_socket<
        implementing
    >::async_start(
        boost::function<
            void(
                boost::system::error_code const& error
            )
        > start_handler
    ) {
        implementation->async_start(
            start_handler
        );
    }
    
    template<
        typename implementing
    >
    typename forwarding_server_socket<
        implementing
    >::lowest_layer_type& forwarding_server_socket<
        implementing
    >::lowest_layer(
    ) {
        return implementation->lowest_layer(
        );
    }
    
    template<
        typename implementing
    >
    boost::asio::io_service& forwarding_server_socket<
        implementing
    >::get_io_service(
    ) {
        return implementation->get_io_service(
        );
    }
    
    template<
        typename implementing
    >
    void forwarding_server_socket<
        implementing
    >::async_read_some(
        boost::asio::mutable_buffers_1 const& destination,
        boost::function<
            void(
                boost::system::error_code const& error,
                size_t received
            )
        > const& read_handler
    ) {
        implementation->async_read_some(
            destination,
            read_handler
        );
    }
    
    template<
        typename implementing
    >
    void forwarding_server_socket<
        implementing
    >::async_write_some(
        boost::asio::mutable_buffers_1 const& source,
        boost::function<
            void(
                boost::system::error_code const& error,
                size_t sent
            )
        > const& write_handler
    ) {
        implementation->async_write_some(
            source,
            write_handler
        );
    }
    
    template<
        typename implementing
    >
    void forwarding_server_socket<
        implementing
    >::async_write_some(
        boost::asio::const_buffers_1 const& source,
        boost::function<
            void(
                boost::system::error_code const& error,
                size_t sent
            )
        > const& write_handler
    ) {
        implementation->async_write_some(
            source,
            write_handler
        );
    }
}

#endif
