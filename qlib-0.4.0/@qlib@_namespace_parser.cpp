/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_namespace_parser.h"
#include <boost/fusion/sequence/intrinsic/at_c.hpp>
#include <stdexcept>
#include <boost/fusion/include/at_c.hpp>
#include "@qlib@_component_error.h"
#include <list>
#include "@qlib@_system_log.h"
namespace @qlib@ {
    
    
    namespace_parser::namespace_parser(
        parser_state_interface* initial_state
    ) :
    current_state(
        initial_state
    ),
    failed(
        false
    ) {
        set_substitute_entities(
        );
    }
    
    boost::shared_ptr<
        parser_state_interface
    > namespace_parser::get_state(
    ) const {
        return current_state;
    }
    
    void namespace_parser::set_state(
        boost::shared_ptr<
            parser_state_interface
        > new_state
    ) {
        current_state = new_state;
    }
    
    void namespace_parser::parse_chunk(
        Glib::ustring const& pending
    ) {
        xmlpp::SaxParser::parse_chunk(
            pending
        );
        if (
            failed
        ) {
            failed = false;
            throw std::runtime_error(
                "semantic error in parsed XML data"
            );
        }
    }
    
    void namespace_parser::set_state(
        parser_state_interface* new_state
    ) {
        current_state.reset(
            new_state
        );
    }
    
    void namespace_parser::on_start_document(
    ) {
        if (
            !failed
        ) {
        }
    }
    
    void namespace_parser::on_end_document(
    ) {
        if (
            !failed
        ) {
        }
    }
    
    boost::fusion::vector<
        std::string,
        std::string,
        std::string
    > namespace_parser::expand_namespace(
        std::string const& prefixed
    ) {
        boost::shared_ptr<
            std::map<
                std::string,
                std::string
            >
        > active_map = namespace_prefixes.top(
        );
        std::string::size_type colon = prefixed.find(
            ":"
        );
        std::string local_name;
        std::string namespace_prefix;
        if (
            std::string::npos == colon
        ) {
            local_name = prefixed;
            namespace_prefix = "";
        } else {
            local_name = std::string(
                prefixed.begin(
                ) + colon + 1,
                prefixed.end(
                )
            );
            namespace_prefix = std::string(
                prefixed.begin(
                ),
                prefixed.begin(
                ) + colon
            );
        }
        std::string const& namespace_uri = (
            *active_map
        )[
            namespace_prefix
        ];
        return boost::fusion::vector<
            std::string,
            std::string,
            std::string
        >(
            local_name,
            namespace_prefix,
            namespace_uri
        );
    }
    
    void namespace_parser::on_start_element(
        const Glib::ustring& unicode_name,
        const AttributeList& properties
    ) {
        if (
            !failed
        ) {
            bool map_writable;
            boost::shared_ptr<
                std::map<
                    std::string,
                    std::string
                >
            > active_map;
            std::list<
                std::pair<
                    std::string,
                    std::string
                >
            > semantic_properties;
            if (
                namespace_prefixes.empty(
                )
            ) {
                map_writable = true;
                active_map.reset(
                    new std::map< 
                        std::string,
                        std::string
                    >(
                    )
                );
            } else {
                map_writable = false;
                active_map = namespace_prefixes.top(
                );
            }
            static const std::string xmlns(
                "xmlns"
            );
            static const std::string xmlns_colon(
                "xmlns:"
            );
            for (
                xmlpp::SaxParser::AttributeList::const_iterator current = properties.begin(
                );
                properties.end(
                ) != current;
                current++
            ) {
                std::string const current_name(
                    current->name
                );
                bool defining_prefix = false;
                std::string prefix;
                if (
                    xmlns == current_name
                ) {
                    defining_prefix = true;
                    prefix = "";
                } else if (
                    (
                        current_name.length(
                        ) >= xmlns_colon.length(
                        )
                    ) && (
                        xmlns_colon == std::string(
                            current_name.begin(
                            ),
                            current_name.begin(
                            ) + xmlns_colon.length(
                            )
                        )
                    )
                ) {
                    defining_prefix = true;
                    prefix = std::string(
                        current_name.begin(
                        ) + xmlns_colon.length(
                        ),
                        current_name.end(
                        )
                    );
                }
                if (
                    defining_prefix
                ) {
                    if (
                        !map_writable
                    ) {
                        active_map.reset(
                            new std::map<
                                std::string,
                                std::string
                            >(
                                *active_map
                            )
                        );
                        map_writable = true;;
                    }
                    (
                        *active_map
                    )[
                        prefix
                    ] = current->value;
                }
                semantic_properties.push_back(
                    std::pair<
                        std::string,
                        std::string
                    >(
                        current_name,
                        current->value
                    )
                );
            }
            std::string const name(
                unicode_name
            );
            namespace_prefixes.push(
                active_map
            );
            boost::fusion::vector<
                std::string,
                std::string,
                std::string
            > const& expanded = expand_namespace(
                name
            );
            std::string const& local_name = boost::fusion::at_c<
                0
            >(
                expanded
            );
            std::string const& namespace_prefix = boost::fusion::at_c<
                1
            >(
                expanded
            );
            std::string const& namespace_uri = boost::fusion::at_c<
                2
            >(
                expanded
            );
            try {
                current_state->on_start_element(
                    local_name,
                    namespace_prefix,
                    namespace_uri,
                    semantic_properties,
                    namespace_prefixes.top(
                    )
                );
            } catch (
                std::exception& caught
            ) {
                system_log.log_error(
                    caught.what(
                    )
                );
                failed = true;
            }
        }
    }
    
    void namespace_parser::on_end_element(
        const Glib::ustring& name
    ) {
        if (
            !failed
        ) {
            try {
                boost::fusion::vector<
                    std::string,
                    std::string,
                    std::string
                > const& expanded = expand_namespace(
                    name
                );
                std::string const& local_name = boost::fusion::at_c<
                    0
                >(
                    expanded
                );
                std::string const& namespace_prefix = boost::fusion::at_c<
                    1
                >(
                    expanded
                );  
                std::string const& namespace_uri = boost::fusion::at_c<
                    2
                >(
                    expanded
                );
                namespace_prefixes.pop(
                );
                current_state->on_end_element(
                    local_name,
                    namespace_prefix,
                    namespace_uri
                );
            } catch (
                std::exception& caught
            ) {
                system_log.log_error(
                    caught.what(
                    )
                );
                failed = true;
            }   
        }
    }
    
    void namespace_parser::on_characters(
        const Glib::ustring& characters
    ) {
        if (
            !failed
        ) {
            try {
                current_state->on_characters(
                    characters
                );
            } catch (
                std::exception& caught
            ) {
                system_log.log_error(
                    caught.what(
                    )
                );
                failed = true;
            }
        }
    }
    
    void namespace_parser::on_comment(
        const Glib::ustring& text
    ) {
        if (
            !failed
        ) {
            try {
            } catch (
                std::exception& caught
            ) {
                system_log.log_error(
                    caught.what(
                    )
                );
                failed = true;
            }
        }
    }
    
    void namespace_parser::on_warning(
        const Glib::ustring& text
    ) {
        if (
            !failed
        ) {
            try {
                throw component_error(
                    "namespace_parser"
                );
            } catch (
                std::exception& caught
            ) {
                system_log.log_error(
                    caught.what(
                    )
                );
                failed = true;
            }
        }
    }
    
    void namespace_parser::on_error(
        const Glib::ustring& text
    ) {
        if (
            !failed
        ) {
            try {
                throw component_error(
                    "namespace_parser"
                );
            } catch (
                std::exception& caught
            ) {
                system_log.log_error(
                    caught.what(
                    )
                );
                failed = true;
            }
        }
    }
    
    void namespace_parser::on_fatal_error(
        const Glib::ustring& text
    ) {
        if (
            !failed
        ) {
            try {
                throw component_error(
                    "namespace_parser"
                );
            } catch (
                std::exception& caught
            ) {
                system_log.log_error(
                    caught.what(
                    )
                );
                failed = true;
            }
        }
    }
}
