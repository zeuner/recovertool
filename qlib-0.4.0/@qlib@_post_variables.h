/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef POST_VARIABLES_INCLUDED
#define POST_VARIABLES_INCLUDED

#include "@qlib@_component_error.h"
#include <map>
namespace @qlib@ {
    
    
    template<
        typename string
    >
    std::map<
        string,
        string
    > post_variables(
        string const& encoded
    );
    
    template<
        typename string
    >
    std::map<
        string,
        string
    > post_variables(
        string const& encoded
    ) {
        std::map<
            string,
            string
        > result;
        typename string::size_type before = 0;
        do {
            typename string::size_type after = encoded.find(
                '&',
                before
            );
            bool done = false;
            if (
                string::npos == after
            ) {
                after = encoded.size(
                );
                done = true;
            }
            typename string::size_type const delimiter = encoded.find(
                '=',
                before
            );
            if (
                (
                    string::npos == delimiter
                ) || (
                    delimiter > after
                )
            ) {
                throw component_error(
                    "post_variables"
                );
            }
            result[
                urldecoded(
                    string(
                        encoded.begin(
                        ) + before,
                        encoded.begin(
                        ) + delimiter
                    )
                )
            ] = urldecoded(
                string(
                    encoded.begin(
                    ) + delimiter + 1,
                    encoded.begin(
                    ) + after
                )
            );
            if (
                done
            ) {
                break;
            }
            before = after + 1;
        } while (
            true
        );
        return result;
    }
}

#endif
