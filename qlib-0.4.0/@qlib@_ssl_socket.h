/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef SSL_SOCKET_INCLUDED
#define SSL_SOCKET_INCLUDED

#include <string>
#include <boost/asio/placeholders.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>
namespace @qlib@ {
    
    
    template<
        typename socket
    >
    class ssl_socket :
    public boost::asio::ssl::stream<
        socket&
    > {
    public:
        ssl_socket(
            socket& next_layer,
            boost::asio::ssl::context& ssl_context
        );
        template<
            typename handling
        >
        void async_connect(
            std::string const& host,
            unsigned short port,
            handling handler
        );
        void cancel(
        );
    protected:
        void handle_connect(
            boost::system::error_code const& error
        );
        void handle_handshake(
            boost::system::error_code const& error
        );
    private:
        boost::function<
            void(
                boost::system::error_code const&
            )
        > connect_handler;
    };
    
    template<
        typename socket
    >
    template<
        typename handling
    >
    void ssl_socket<
        socket
    >::async_connect(
        std::string const& host,
        unsigned short port,
        handling handler
    ) {
        connect_handler = handler;
        this->lowest_layer(
        ).async_connect(
            host,
            port,
            boost::bind(
                &ssl_socket::handle_connect,
                this,
                boost::asio::placeholders::error
            )
        );
    }
    
    template<
        typename socket
    >
    ssl_socket<
        socket
    >::ssl_socket(
        socket& next_layer,
        boost::asio::ssl::context& ssl_context
    ) :
    boost::asio::ssl::stream<
        socket&
    >(
        next_layer,
        ssl_context
    ) {
    }
    
    template<
        typename socket
    >
    void ssl_socket<
        socket
    >::cancel(
    ) {
        this->next_layer(
        ).cancel(
        );
    }
    
    template<
        typename socket
    >
    void ssl_socket<
        socket
    >::handle_handshake(
        boost::system::error_code const& error
    ) {
        connect_handler(
            error
        );
    }
    
    template<
        typename socket
    >
    void ssl_socket<
        socket
    >::handle_connect(
        boost::system::error_code const& error
    ) {
        if (
            !error
        ) {
            this->async_handshake(
                boost::asio::ssl::stream_base::client,
                boost::bind(
                    &ssl_socket::handle_handshake,
                    this,
                    boost::asio::placeholders::error
                )
            );
            return;
        }
        connect_handler(
            error
        );
    }
}

#endif
