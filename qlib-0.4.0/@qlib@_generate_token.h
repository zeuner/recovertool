/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef GENERATE_TOKEN_INCLUDED
#define GENERATE_TOKEN_INCLUDED

#include <string>
namespace @qlib@ {
    
    
    template<
        typename string
    >
    void generate_token(
        int size,
        string& destination
    );
    
    template<
        typename string
    >
    void generate_token(
        int size,
        string& destination
    ) {
        static std::string const allowed(
            "01234567"
            "89abcdef"
            "ghijklmn"
            "opqrstuv"
        );
        static integer_random_source chooser(
            allowed.size(
            )
        );
        char writable[
            size
        ];
        for (
            int digit = 0;
            size > digit;
            digit++
        ) {
            writable[
                digit
            ] = allowed[
                chooser(
                )
            ];
        }
        destination = string(
            writable,
            size
        );
    }
}

#endif
