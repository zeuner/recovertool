/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_delimiter_splitted.h"
namespace @qlib@ {
    
    
    std::list<
        std::string
    > delimiter_splitted(
        std::string const& delimiter,
        std::string const& splitted
    ) {
        std::list<
            std::string
        > result;
        std::string::size_type begin = 0;
        do {
            std::string::size_type const end = splitted.find(
                delimiter,
                begin
            );
            if (
                std::string::npos == end
            ) {
                result.push_back(
                    std::string(
                        splitted.begin(
                        ) + begin,
                        splitted.end(
                        )
                    )
                );
                break;
            }
            result.push_back(
                std::string(
                    splitted.begin(
                    ) + begin,
                    splitted.begin(
                    ) + end
                )
            );
            begin = end + delimiter.size(
            );
        } while (
            true
        );
        return result;
    }
}
