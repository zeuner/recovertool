/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef XSLT_STYLESHEET_INCLUDED
#define XSLT_STYLESHEET_INCLUDED

#include <boost/noncopyable.hpp>
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    class xslt_stylesheet :
    private boost::noncopyable {
    public:
        template<
            typename string
        >
        xslt_stylesheet(
            string const& text
        );
        ~xslt_stylesheet(
        );
        xsltStylesheetPtr get_data(
        ) const;
        xmlDocPtr transformed(
            xml_document const& raw
        ) const;
    private:
        xsltStylesheetPtr data;
    };
    
    template<
        typename string
    >
    xslt_stylesheet::xslt_stylesheet(
        string const& text
    ) {
        xmlDocPtr const xml = xmlParseMemory(
            reinterpret_cast<
                char const*
            >(
                text.data(
                )
            ),
            text.size(
            )
        );
        if (
            NULL == xml
        ) {
            throw component_error(
                "xslt_stylesheet"
            );
        }
        data = xsltParseStylesheetDoc(
            xml
        );
        if (
            NULL == data
        ) {
            xmlFreeDoc(
                xml
            );
            throw component_error(
                "xslt_stylesheet"
            );  
        }
    }
}

#endif
