/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef SMTP_SOCKET_INCLUDED
#define SMTP_SOCKET_INCLUDED

#include <boost/asio/ip/tcp.hpp>
#include <boost/function.hpp>
#include <string>
#include <list>
#include <boost/fusion/container/vector.hpp>
#include <boost/fusion/include/vector_fwd.hpp>
#include <boost/fusion/include/vector.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/fusion/container/vector/vector_fwd.hpp>
#include <boost/asio/ip/address_v4.hpp>
namespace @qlib@ {
    
    
    class smtp_socket :
    public boost::asio::ip::tcp::socket {
    public:
        smtp_socket(
            boost::asio::io_service& io_service,
            std::string const& hostname
        );
        smtp_socket(
            boost::asio::io_service& io_service,
            std::string const& hostname,
            boost::asio::ip::tcp::endpoint const& local_endpoint
        );
        ~smtp_socket(
        );
        void async_connect(
            boost::asio::ip::address_v4 const& destination,
            boost::function<
                void(
                )
            > const& disconnect_handler
        );
        void async_send(
            std::string const& envelope_from,
            std::string const& envelope_to,
            std::string const& data,
            boost::function<
                void(
                    bool
                )
            > result_handler
        );
        bool get_idle(
        ) const;
    protected:
        void handle_connect(
            boost::shared_ptr<
                boost::function<
                    void(
                    )
                >
            > disconnect_handler,
            boost::system::error_code const& error
        );
        void connected(
            boost::shared_ptr<
                boost::function<
                    void(
                    )
                >
            > disconnect_handler
        );
        void handle_greeting_read(
            boost::shared_ptr<
                boost::function<
                    void(
                    )
                >
            > disconnect_handler,
            boost::system::error_code const& error,
            size_t transferred
        );
        void handle_greeting_write(
            boost::shared_ptr<
                boost::function<
                    void(
                    )
                >
            > disconnect_handler,
            boost::system::error_code const& error
        );
        void handle_greeting_response_read(
            boost::shared_ptr<
                boost::function<
                    void(
                    )
                >
            > disconnect_handler,
            boost::system::error_code const& error,
            size_t transferred
        );
        void handle_from_write(
            boost::shared_ptr<
                boost::function<
                    void(
                    )
                >
            > disconnect_handler,
            boost::system::error_code const& error
        );
        void handle_from_response_read(
            boost::shared_ptr<
                boost::function<
                    void(
                    )
                >
            > disconnect_handler,
            boost::system::error_code const& error,
            size_t transferred
        );
        void handle_to_write(
            boost::shared_ptr<
                boost::function<
                    void(
                    )
                >
            > disconnect_handler,
            boost::system::error_code const& error
        );
        void handle_to_response_read(
            boost::shared_ptr<
                boost::function<
                    void(
                    )
                >
            > disconnect_handler,
            boost::system::error_code const& error,
            size_t transferred
        );
        void handle_data_command_write(
            boost::shared_ptr<
                boost::function<
                    void(
                    )
                >
            > disconnect_handler,
            boost::system::error_code const& error
        );
        void handle_data_command_response_read(
            boost::shared_ptr<
                boost::function<
                    void(
                    )
                >
            > disconnect_handler,
            boost::system::error_code const& error,
            size_t transferred
        );
        void handle_body_write(
            boost::shared_ptr<
                boost::function<
                    void(
                    )
                >
            > disconnect_handler,
            boost::system::error_code const& error
        );
        void handle_body_response_read(
            boost::shared_ptr<
                boost::function<
                    void(
                    )
                >
            > disconnect_handler,
            boost::system::error_code const& error,
            size_t transferred
        );
        void async_process_send(
        );
    private:
        enum {
            input_size = 0x400
        };
        char reading_data[
            input_size
        ];
        std::string input;
        std::string writing_data;
        boost::shared_ptr<
            boost::function<
                void(
                )
            >
        > disconnect_handler;
        typedef boost::fusion::vector<
            std::string,
            std::string,
            std::string,
            boost::function<
                void(
                    bool
                )
            >
        > sending_record;
        std::list<
            sending_record
        > pending_sends;
        bool idle;
        std::string hostname;
    };
}

#endif
