/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_database_configuration_parser_state.h"
namespace @qlib@ {
    
    
    database_configuration_parser_state::database_configuration_parser_state(
        database_access_configuration& configuration
    ) :
    configuration(
        configuration
    ) {
    }
    
    void database_configuration_parser_state::on_start_element(
        const std::string& local_name,
        const std::string& namespace_prefix,
        const std::string& namespace_uri,
        const std::list<
            std::pair<
                std::string,
                std::string
            >
        >& properties,
        boost::shared_ptr<
            std::map<
                std::string,
                std::string
            > const
        > namespaces
    ) {
        if (
            (
                configuration_namespace == namespace_uri
            ) && (
                "database" == local_name
            )
        ) {
            std::string hostname;
            std::string database;
            std::string username;
            std::string password;
            for (
                std::list<
                    std::pair<
                        std::string,
                        std::string
                    >
                >::const_iterator traversed = properties.begin(
                );
                properties.end(
                ) != traversed;
                traversed++
            ) {
                if (
                    "hostname" == traversed->first
                ) {
                    hostname = traversed->second;
                } else if (
                    "database" == traversed->first
                ) {
                    database = traversed->second;
                } else if (
                    "username" == traversed->first
                ) { 
                    username = traversed->second;
                } else if (
                    "password" == traversed->first
                ) { 
                    password = traversed->second;
                }
            }
            configuration.set_database(
                hostname,
                database,
                username,
                password
            );
        }
    }
    
    void database_configuration_parser_state::on_end_element(
        const std::string& local_name,
        const std::string& namespace_prefix,
        const std::string& namespace_uri
    ) {
    }
    
    void database_configuration_parser_state::on_characters(
        const std::string& characters
    ) {
    }
}
