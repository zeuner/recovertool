/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_memory_base_bio.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    memory_base_bio::memory_base_bio(
        BIO* data
    ) :
    data(
        data
    ) {
        if (
            NULL == data
        ) {
            throw component_error(
                "memory_base_bio"
            );
        }
    }
    
    memory_base_bio::~memory_base_bio(
    ) {
        BIO_free(
            data
        );
    }
    
    BIO* memory_base_bio::get_data(
    ) const {
        return data;
    }
    
    void memory_base_bio::flush(
    ) {
        if (
            0 > BIO_reset(
                data
            )
        ) {
            throw component_error(
                "memory_base_bio"
            );
        }
    }
}
