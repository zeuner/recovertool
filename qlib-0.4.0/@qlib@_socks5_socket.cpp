/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_socks5_socket.h"
#include "@qlib@_stringified.h"
#include <boost/bind.hpp>
#include "@qlib@_component_error.h"
#include <boost/asio/write.hpp>
#include <boost/asio/placeholders.hpp>
#include <boost/asio/ip/address_v4.hpp>
namespace @qlib@ {
    
    
    socks5_socket::socks5_socket(
        boost::asio::io_service& communicating,
        boost::asio::ip::tcp::endpoint const& proxy
    ) :
    tcp_socket(
        communicating,
        NULL
    ),
    proxy(
        proxy
    ) {
    }
    
    void socks5_socket::async_connect(
        boost::asio::ip::tcp::endpoint const& endpoint,
        boost::function<
            void(
                boost::system::error_code const& error
            )
        > connect_handler
    ) {
        this->connect_handler = connect_handler;
        tcp_socket::async_connect(
            proxy,
            boost::bind(
                &socks5_socket::handle_socks_connect_resolved,
                this,
                boost::asio::placeholders::error,
                endpoint
            )
        );
    }
    
    void socks5_socket::async_connect(
        std::string const& hostname,
        unsigned short port,
        boost::function<
            void(
                boost::system::error_code const& error
            )
        > const& connect_handler
    ) {
        this->connect_handler = connect_handler;
        tcp_socket::async_connect(
            proxy,
            boost::bind(
                &socks5_socket::handle_socks_connect_resolving,
                this,
                boost::asio::placeholders::error,
                hostname,
                port
            )
        );
    }
    
    boost::asio::ip::tcp::endpoint socks5_socket::local_endpoint(
    ) const {
        return local;
    }
    
    void socks5_socket::handle_socks_connect_resolved(
        boost::system::error_code const& error,
        boost::asio::ip::tcp::endpoint endpoint
    ) {
        if (
            error
        ) {
            connect_handler(
                error
            );
            return;
        }
        boost::asio::ip::address_v4::bytes_type address_bytes;
        address_bytes = endpoint.address(
        ).to_v4(
        ).to_bytes(
        );
        unsigned short port_high = endpoint.port(
        ) >> 8;
        unsigned short port_low = endpoint.port(
        ) & 0xff;
        connect_command = std::string(
            "\x05\x01\x00\x05\x01\x00\x01",
            7
        ) + static_cast<
            char
        >(
            address_bytes[
                0
            ]
        ) + static_cast<
            char
        >(
            address_bytes[
                1
            ]
        ) + static_cast<
            char
        >(
            address_bytes[
                2
            ]
        ) + static_cast<
            char
        >(
            address_bytes[
                3
            ]
        ) + static_cast<
            char
        >(
            port_high
        ) + static_cast<
            char
        >(
            port_low
        );
        boost::asio::async_write(
            *this,
            boost::asio::buffer(
                connect_command.data(
                ),
                connect_command.size(
                )
            ),
            boost::bind(
                &socks5_socket::handle_socks_write,
                this,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
    
    void socks5_socket::handle_socks_connect_resolving(
        boost::system::error_code const& error,
        std::string hostname,
        unsigned short port
    ) {
        if (
            error
        ) {
            connect_handler(
                error
            );
            return;
        }
        unsigned short port_high = port >> 8;
        unsigned short port_low = port & 0xff;
        connect_command = std::string(
            "\x05\x01\x00\x05\x01\x00\x03",
            7
        ) + static_cast<
            char
        >(
            hostname.size(
            )
        ) + hostname + static_cast<
            char
        >(
            port_high
        ) + static_cast<
            char
        >(
            port_low
        );
        boost::asio::async_write(
            *this,
            boost::asio::buffer(
                connect_command.data(
                ),
                connect_command.size(
                )
            ),
            boost::bind(
                &socks5_socket::handle_socks_write,
                this,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
    
    void socks5_socket::handle_socks_write(
        boost::system::error_code const& error,
        size_t transferred
    ) {
        if (
            error
        ) {
            connect_handler(
                error
            );
            return;
        }
        if (
            connect_command.size(
            ) != transferred
        ) {
            throw component_error(
                "socks5_socket"
            );
        }
        boost::asio::async_read(
            *this,
            boost::asio::buffer(
                connect_response,
                connect_response_size
            ),
            boost::bind(
                &socks5_socket::handle_socks_read,
                this,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
    
    void socks5_socket::handle_socks_read(
        boost::system::error_code const& error,
        size_t transferred
    ) {
        if (
            error
        ) {
            connect_handler(
                error
            );
            return;
        }
        if (
            connect_response_size != transferred
        ) {
            throw component_error(
                "socks5_socket"
            );
        }
        boost::asio::ip::address_v4 ip;
        ip = boost::asio::ip::address_v4::from_string(
            stringified(
                static_cast<
                    unsigned
                >(
                    static_cast<
                        unsigned char
                    >(
                        connect_response[
                            6
                        ]
                    )
                )
            ) + "." + stringified(
                static_cast<
                    unsigned
                >(
                    static_cast<
                        unsigned char
                    >(
                        connect_response[
                            7
                        ]
                    )   
                )
            ) + "." + stringified(
                static_cast<
                    unsigned
                >(
                    static_cast<
                        unsigned char
                    >(
                        connect_response[
                            8
                        ]
                    )   
                )
            ) + "." + stringified(
                static_cast<
                    unsigned
                >(
                    static_cast<
                        unsigned char
                    >(
                        connect_response[
                            9
                        ]
                    )   
                )
            )
        );
        unsigned short port = static_cast<
            unsigned char
        >(
            connect_response[
                10
            ]
        );
        port <<= 8;
        port += static_cast<
            unsigned char
        >(
            connect_response[
                11
            ]
        );
        local = boost::asio::ip::tcp::endpoint(
            ip,
            port
        );
        connect_handler(
            error
        );
    }
}
