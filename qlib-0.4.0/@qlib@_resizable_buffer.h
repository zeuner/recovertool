/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef RESIZABLE_BUFFER_INCLUDED
#define RESIZABLE_BUFFER_INCLUDED

#include "@qlib@_throw_exception.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    template<
        typename storing,
        typename stored = char
    >
    class resizable_buffer;
    
    template<
        typename storing,
        typename stored
    >
    class resizable_buffer :
    private storing {
    public:
        resizable_buffer(
        );
        resizable_buffer(
            size_t size
        );
        template<
            typename iterator
        >
        resizable_buffer(
            iterator from,
            iterator to
        );
        ~resizable_buffer(
        );
        stored* get_data(
        ) const;
        size_t get_size(
        ) const;
        void resize(
            size_t size
        );
    private:
        stored* data;
        size_t size;
    };
    
    template<
        typename storing,
        typename stored
    >
    resizable_buffer<
        storing,
        stored
    >::resizable_buffer(
    ) :
    data(
        NULL
    ),
    size(
        0
    ) {
    }
    
    template<
        typename storing,
        typename stored
    >
    resizable_buffer<
        storing,
        stored
    >::resizable_buffer(
        size_t size
    ) :
    data(
        static_cast<
            stored*
        >(
            storing::malloc(
                size * sizeof(
                    stored
                )
            )
        )
    ),
    size(
        size
    ) {
        if (
            NULL == data
        ) {
            throw_exception(
                component_error(
                    "resizable_buffer"
                )
            );
        }
    }
    
    template<
        typename storing,
        typename stored
    >
    template<
        typename iterator
    >
    resizable_buffer<
        storing,
        stored
    >::resizable_buffer(
        iterator from,
        iterator to
    ) :
    data(
        static_cast<
            stored*
        >(
            storing::malloc(
                (
                    to - from
                ) * sizeof(
                    stored
                )
            )
        )
    ),
    size(
        to - from
    ) {
        if (
            NULL == data
        ) {
            throw_exception(
                component_error(
                    "resizable_buffer"
                )
            );
        }
        std::copy(
            from,
            to,
            data
        );
    }
    
    template<
        typename storing,
        typename stored
    >
    resizable_buffer<
        storing,
        stored
    >::~resizable_buffer(
    ) {
        storing::free(
            data
        );
    }
    
    template<
        typename storing,
        typename stored
    >
    stored* resizable_buffer<
        storing,
        stored
    >::get_data(
    ) const {
        return data;
    }
    
    template<
        typename storing,
        typename stored
    >
    size_t resizable_buffer<
        storing,
        stored
    >::get_size(
    ) const {
        return size;
    }
    
    template<
        typename storing,
        typename stored
    >
    void resizable_buffer<
        storing,
        stored
    >::resize(
        size_t size
    ) {
        stored* new_data = static_cast<
            stored*
        >(
            storing::realloc(
                data,
                size * sizeof(
                    stored
                )
            )
        );
        if (
            (
                NULL == new_data
            ) && (
                0 != size
            )
        ) {
            throw_exception(
                component_error(
                    "resizable_buffer"
                )
            );
        }
        data = new_data;
        this->size = size;
    }
}

#endif
