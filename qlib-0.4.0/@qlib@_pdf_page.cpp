/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_pdf_page.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    pdf_page::pdf_page(
        pdf_document& document
    ) :
    native(
        document.add_page(
        )
    ) {
    }
    
    float pdf_page::width(
    ) {
        return HPDF_Page_GetWidth(
            native
        );
    }
    
    float pdf_page::height(
    ) {
        return HPDF_Page_GetHeight(
            native
        );
    }
    
    void pdf_page::write_text(
        pdf_font const& font,
        std::string const& text,
        int size,
        float width,
        float height
    ) {
        if (
            0.0 > height
        ) {
            throw component_error(
                "pdf_page"
            );
        }
        HPDF_Page_SetFontAndSize(
            native,
            font.get_native(
            ),
            size
        );
        HPDF_Page_BeginText(
            native
        );
        HPDF_Page_MoveTextPos(
            native,
            width,
            height
        );
        HPDF_Page_ShowText(
            native,
            text.c_str(
            )
        );
        HPDF_Page_EndText(
            native
        );
    }
    
    void pdf_page::write_line(
        float line_width,
        float width_begin,
        float height_begin,
        float width_end,
        float height_end
    ) {
        HPDF_Page_SetLineWidth(
            native,
            line_width
        );
        HPDF_Page_MoveTo(
            native,
            width_begin,
            height_begin
        );
        HPDF_Page_LineTo(
            native,
            width_end,
            height_end
        );
        HPDF_Page_Stroke(
            native
        );
    }
}
