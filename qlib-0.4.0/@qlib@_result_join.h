/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef RESULT_JOIN_INCLUDED
#define RESULT_JOIN_INCLUDED

#include <boost/function.hpp>
namespace @qlib@ {
    
    
    template<
        typename first_type,
        typename second_type
    >
    class result_join {
    public:
        result_join(
            boost::function<
                void(
                    boost::system::error_code const& error,
                    std::pair<
                        first_type,
                        second_type
                    > const& result
                )
            > const& continuation
        );
        void handle_first(
            boost::system::error_code const& error,
            first_type const& result
        );
        void handle_second(
            boost::system::error_code const& error,
            second_type const& result
        );
    private:
        bool first_received;
        bool second_received;
        std::pair<
            first_type,
            second_type
        > result;
        boost::function<
            void(
                boost::system::error_code const& error,
                std::pair<
                    first_type,
                    second_type
                > const& result
            )
        > continuation;
    };
    
    template<
        typename first_type,
        typename second_type
    >
    result_join<
        first_type,
        second_type
    >::result_join(
        boost::function<
            void(
                boost::system::error_code const& error,
                std::pair<
                    first_type,
                    second_type
                > const& result
            )
        > const& continuation
    ) :
    first_received(
        false
    ),
    second_received(
        false
    ),
    continuation(
        continuation
    ) {
    }
    
    template<
        typename first_type,
        typename second_type
    >
    void result_join<
        first_type,
        second_type
    >::handle_first(
        boost::system::error_code const& error,
        first_type const& result
    ) {
        if (
            error
        ) {
            continuation(
                error,
                this->result
            );
            return;
        }
        this->result.first = result;
        if (
            second_received
        ) {
            continuation(
                error,
                this->result
            );
            return;
        }
        first_received = true;
    }
    
    template<
        typename first_type,
        typename second_type
    >
    void result_join<
        first_type,
        second_type
    >::handle_second(
        boost::system::error_code const& error,
        second_type const& result
    ) {
        if (
            error
        ) {
            continuation(
                error,
                this->result
            );
            return;
        }
        this->result.second = result;
        if (
            first_received
        ) {
            continuation(
                error,
                this->result
            );
            return;
        }
        second_received = true;
    }
}

#endif
