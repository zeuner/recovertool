/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef OBJECT_NUMBERING_INCLUDED
#define OBJECT_NUMBERING_INCLUDED

#include "@qlib@_serialized.h"
#include "@qlib@_deserialize.h"
#include <boost/noncopyable.hpp>
#include "@qlib@_component_error.h"
#include <map>
namespace @qlib@ {
    
    
    template<
        typename stored
    >
    class object_numbering :
    private boost::noncopyable {
    public:
        void flush(
        );
        unsigned get_next_number(
        ) const;
        unsigned get_number(
            stored const& object
        );
        stored const& get_object(
            unsigned number
        ) const;
    private:
        std::map<
            unsigned,
            stored const*
        > number_to_object;
        std::map<
            stored,
            unsigned
        > object_to_number;
    };
    
    template<
        typename stored
    >
    void object_numbering<
        stored
    >::flush(
    ) {
        number_to_object.clear(
        );
        object_to_number.clear(
        );
    }
    
    template<
        typename stored
    >
    unsigned object_numbering<
        stored
    >::get_next_number(
    ) const {
        unsigned next_number;
        if (
            object_to_number.empty(
            )
        ) {
            next_number = 0;
        } else {
            typename std::map<
                unsigned,
                stored const*
            >::const_reverse_iterator last = number_to_object.rbegin(
            );
            next_number = last->first + 1;
        }
        return next_number;
    }
    
    template<
        typename stored
    >
    unsigned object_numbering<
        stored
    >::get_number(
        stored const& object
    ) {
        typename std::map<
            stored,
            unsigned
        >::const_iterator found = object_to_number.find(
            object
        );
        if (
            object_to_number.end(
            ) != found
        ) {
            return found->second;
        }
        unsigned const new_number = get_next_number(
        );
        object_to_number[
            object
        ] = new_number;
        number_to_object[
            new_number
        ] = &(
            object_to_number.find(
                object
            )->first
        );
        return new_number;
    }
    
    template<
        typename stored
    >
    stored const& object_numbering<
        stored
    >::get_object(
        unsigned number
    ) const {
        return *(
            number_to_object.at(
                number
            )
        );
    }
    
    template<
        typename stored,
        typename storing
    >
    class serialized_implementation<
        object_numbering<
            stored
        >,
        storing
    > {
    public:
        static void apply(
            object_numbering<
                stored
            > const& raw,
            storing& result
        );
    };
    
    template<
        typename stored
    >
    class deserialize_implementation<
        object_numbering<
            stored
        >
    > {
    public:
        template<
            typename iterator
        >
        static void apply(
            iterator& from,
            iterator const& to,
            object_numbering<
                stored
            >& raw,
            bool& short_data
        );
    };
    
    template<
        typename stored,
        typename storing
    >
    void serialized_implementation<
        object_numbering<
            stored
        >,
        storing
    >::apply(
        object_numbering<
            stored
        > const& raw,
        storing& result
    ) {
        unsigned const bound = raw.get_next_number(
        );
        serialized(
            bound,
            result
        );
        for (
            unsigned serializing = 0;
            bound > serializing;
            serializing++
        ) {
            serialized(
                raw.get_object(
                    serializing
                ),
                result
            );
        }
    }
    
    template<
        typename stored
    >
    template<
        typename iterator
    >
    void deserialize_implementation<
        object_numbering<
            stored
        >
    >::apply(
        iterator& from,
        iterator const& to,
        object_numbering<
            stored
        >& raw,
        bool& short_data
    ) {
        raw.flush(
        );
        unsigned bound;
        deserialize(
            from,
            to,
            bound,
            short_data
        );
        if (
            short_data
        ) {
            return;
        }
        for (
            unsigned deserializing = 0;
            bound > deserializing;
            deserializing++
        ) {
            stored value;
            deserialize(
                from,
                to,
                value,
                short_data
            );
            if (
                short_data
            ) {
                return;
            }
            if (
                deserializing != raw.get_number(
                    value
                )
            ) {
                throw component_error(
                    "object_numbering"
                );
            }
        }
    }
}

#endif
