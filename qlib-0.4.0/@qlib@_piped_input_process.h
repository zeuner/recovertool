/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef PIPED_INPUT_PROCESS_INCLUDED
#define PIPED_INPUT_PROCESS_INCLUDED

#include <vector>
#include <string>
namespace @qlib@ {
    
    
    class piped_input_process {
    public:
        piped_input_process(
            std::vector<
                std::string
            > const& command_line
        );
        ~piped_input_process(
        );
        int get_input(
            char* buffer,
            size_t size
        );
        bool input_ready(
            int seconds
        );
        void terminate(
        );
        void track_exit_status(
            int& exit_status
        );
    private:
        int process_output;
        pid_t child;
        int* exit_status;
    };
}

#endif
