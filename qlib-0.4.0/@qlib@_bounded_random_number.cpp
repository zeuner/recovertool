/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_bounded_random_number.h"
#include <map>
#include <boost/shared_ptr.hpp>
namespace @qlib@ {
    
    
    unsigned bounded_random_number(
        unsigned bound
    ) {
        typedef std::map<
            unsigned,
            boost::shared_ptr<
                integer_random_source
            >
        > generator_map;
        generator_map generators;
        generator_map::const_iterator found = generators.find(
            bound
        );
        if (
            generators.end(
            ) == found
        ) {
            boost::shared_ptr<
                integer_random_source
            > instantiated(
                new integer_random_source(
                    bound
                )
            );
            generators[
                bound
            ] = instantiated;
            return (
                *instantiated
            )(
            );
        } else {
            return (
                *found->second
            )(
            );
        }
    }
}
