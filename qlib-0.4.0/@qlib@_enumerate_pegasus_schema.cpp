/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_enumerate_pegasus_schema.h"
#include "@qlib@_component_error.h"
#include <string>
namespace @qlib@ {
    
    
    void enumerate_pegasus_schema(
        PEGASUS_NAMESPACE(
            CIMClient
        )& cim_client,
        PEGASUS_NAMESPACE(
            CIMNamespaceName
        ) const& cim_namespace,
        std::list<
            PEGASUS_NAMESPACE(
                CIMObjectPath
            )
        > const& names,
        boost::function<
            void(
                PEGASUS_NAMESPACE(
                    CIMObjectPath
                ) const&
            )
        > const& object_processing,
        boost::function<
            void(
                PEGASUS_NAMESPACE(
                    CIMObjectPath
                ) const&
            )
        > const& association_processing,
        std::set<
            PEGASUS_NAMESPACE(
                String
            )
        > const& blacklist
    ) {
        std::set<
            std::string
        > encountered_objects;
        std::set<
            std::string
        > encountered_associations;
        std::list<
            PEGASUS_NAMESPACE(
                CIMObjectPath
            )
        > processing_objects;
        std::list<
            PEGASUS_NAMESPACE(
                CIMObjectPath
            )
        > processing_associations;
        for (
            std::list<
                PEGASUS_NAMESPACE(
                    CIMObjectPath
                )
            >::const_iterator processed = names.begin(
            );
            names.end(
            ) != processed;
            processed++
        ) {
            std::string const name = pegasus_to_cpp(
                processed->toString(
                )
            );
            if (
                (
                    encountered_objects.end(
                    ) == encountered_objects.find(
                        name
                    )
                ) && (
                    blacklist.end(
                    ) == blacklist.find(
                        processed->getClassName(
                        ).getString(
                        )
                    )
                )
            ) {
                object_processing(
                    *processed
                );
                encountered_objects.insert(
                    name
                );
            }
            PEGASUS_NAMESPACE(
                Array
            )<
                PEGASUS_NAMESPACE(
                    CIMObjectPath
                )
            > reference_names = cim_client.referenceNames(
                cim_namespace,
                *processed
            );
            for (
                PEGASUS_NAMESPACE(
                    Sint32
                ) traversed = reference_names.size(
                ) - 1;
                0 <= traversed;
                traversed--
            ) {
                PEGASUS_NAMESPACE(
                    CIMObjectPath
                ) const& path = reference_names[
                    traversed
                ];
                std::string const name = pegasus_to_cpp(
                    path.toString(
                    )
                );
                if (
                    (
                        encountered_associations.end(
                        ) == encountered_associations.find(
                            name
                        )
                    ) && (
                        blacklist.end(
                        ) == blacklist.find(
                            path.getClassName(
                            ).getString(
                            )
                        )
                    )
                ) {
                    association_processing(
                        path
                    );
                    encountered_associations.insert(
                        name
                    );
                    processing_associations.push_back(
                        path
                    );
                }
            }
        }
        while (
            !(
                processing_associations.empty(
                ) && processing_objects.empty(
                )
            )
        ) {
            while (
                !processing_associations.empty(
                )
            ) {
                PEGASUS_NAMESPACE(
                    Array
                )<
                    PEGASUS_NAMESPACE(
                        CIMKeyBinding
                    )
                > key_bindings = processing_associations.front(
                ).getKeyBindings(
                );
                for (
                    PEGASUS_NAMESPACE(
                        Sint32
                    ) traversed = key_bindings.size(
                    ) - 1;
                    0 <= traversed;
                    traversed--
                ) {
                    PEGASUS_NAMESPACE(
                        CIMKeyBinding
                    ) const& binding = key_bindings[
                        traversed
                    ];
                    if (
                        PEGASUS_NAMESPACE(
                            CIMKeyBinding
                        )::REFERENCE != binding.getType(
                        )
                    ) {
                        throw component_error(
                            "enumerate_pegasus_schema"
                        );
                    }
                    PEGASUS_NAMESPACE(
                        CIMObjectPath
                    ) referenced(
                        binding.getValue(
                        )
                    );
                    std::string const name = pegasus_to_cpp(
                        referenced.toString(
                        )
                    );
                    if (
                        (
                            encountered_objects.end(
                            ) == encountered_objects.find(
                                name
                            )
                        ) && (
                            blacklist.end(
                            ) == blacklist.find(
                                referenced.getClassName(
                                ).getString(
                                )
                            )
                        )
                    ) {
                        object_processing(
                            referenced
                        );
                        encountered_objects.insert(
                            name
                        );
                        processing_objects.push_back(
                            referenced
                        );
                    }
                }
                processing_associations.pop_front(
                );
            }
            while (
                !processing_objects.empty(
                )
            ) {
                std::string const name = pegasus_to_cpp(
                    processing_objects.front(
                    ).toString(
                    )
                );
                PEGASUS_NAMESPACE(
                    Array
                )<
                    PEGASUS_NAMESPACE(
                        CIMObjectPath
                    )
                > reference_names = cim_client.referenceNames(
                    cim_namespace,
                    processing_objects.front(
                    )
                );
                processing_objects.pop_front(
                );
                for (
                    PEGASUS_NAMESPACE(
                        Sint32
                    ) traversed = reference_names.size(
                    ) - 1;
                    0 <= traversed;
                    traversed--
                ) {
                    PEGASUS_NAMESPACE(
                        CIMObjectPath
                    ) const& path = reference_names[
                        traversed
                    ];
                    std::string const name = pegasus_to_cpp(
                        path.toString(
                        )
                    );
                    if (
                        (
                            encountered_associations.end(
                            ) == encountered_associations.find(
                                name
                            )
                        ) && (
                            blacklist.end(
                            ) == blacklist.find(
                                path.getClassName(
                                ).getString(
                                )
                            )
                        )
                    ) {
                        association_processing(
                            path
                        );
                        encountered_associations.insert(
                            name
                        );
                        processing_associations.push_back(
                            path
                        );
                    }
                }
            }
        }
    }
}
