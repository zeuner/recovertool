/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef POSIX_TIME_INCLUDED
#define POSIX_TIME_INCLUDED

namespace @qlib@ {
    
    class posix_time :
    public timeval {
    };
    std::ostream& operator <<(
        std::ostream& destination,
        posix_time const& written
    );
    namespace asio {
        template<
        >
        struct time_traits<
            @qlib@::posix_time
        > {
            typedef @qlib@::posix_time time_type;
            typedef @qlib@::posix_duration duration_type;
            static boost::posix_time::time_duration to_posix_duration(
                duration_type const& raw
            );
            static bool less_than(
                time_type const& less,
                time_type const& more
            );
            static duration_type subtract(
                time_type const& base,
                time_type const& subtracted
            );
            static time_type add(
                time_type const& base,
                duration_type const& added
            );
            static time_type now(
            );
        };
    }
}

#endif
