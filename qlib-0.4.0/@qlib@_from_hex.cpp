/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_from_hex.h"
#include "@qlib@_throw_exception.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    std::string from_hex(
        std::string const& hex
    ) {
        unsigned char significance = 16;
        unsigned char assembled = 0;
        std::string result;
        for (
            std::string::const_iterator processing = hex.begin(
            );
            hex.end(
            ) != processing;
            processing++
        ) {
            unsigned char digit;
            if (
                (
                    '0' <= *processing
                ) && (
                    *processing <= '9'
                )
            ) {
                digit = *processing - '0';
            } else if (
                (   
                    'a' <= *processing
                ) && (
                    *processing <= 'f'
                )
            ) { 
                digit = 10 + (*processing - 'a');
            } else if (
                (
                    'A' <= *processing
                ) && (
                    *processing <= 'F'
                )
            ) {
                digit = 10 + (*processing - 'A');
            } else {
                throw_exception(
                    component_error(
                        "from_hex"
                    )
                );
            }
            assembled += significance * digit;
            significance = 16 / significance;
            if (
                16 == significance
            ) {
                char const added = assembled;
                result += added;
                assembled = 0;
            }
        }
        return result;
    }
}
