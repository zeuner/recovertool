/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef HTTP_SERVER_ACCEPTOR_INCLUDED
#define HTTP_SERVER_ACCEPTOR_INCLUDED

#include <boost/asio/placeholders.hpp>
#include "@qlib@_http_category.h"
#include <unistd.h>
#include "@qlib@_http_peer_session.h"
#include "@qlib@_system_log.h"
#include <boost/bind/bind.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <list>
#include <vector>
#include <string>
#include "@qlib@_http_server_request.h"
#include "@qlib@_throw_exception.h"
#include "@qlib@_downcased.h"
#include <boost/asio/write.hpp>
#include "@qlib@_http_peer_response.h"
#include "@qlib@_http_error_codes.h"
#include <map>
#include "@qlib@_http_fixed_length_extractor.h"
#include <boost/function.hpp>
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    template<
        typename transmitting = boost::asio::ip::tcp::socket,
        typename allocating = std::allocator<
            char
        >
    >
    class http_server_acceptor;
    
    template<
        typename transmitting,
        typename allocating
    >
    class http_server_acceptor :
    public http_peer_session<
        transmitting,
        allocating
    > {
    public:
        template<
            typename initializing
        >
        http_server_acceptor(
            initializing& initializer,
            typename http_server_acceptor<
                transmitting,
                allocating
            >::string::size_type maximum_data = 0x400
        );
        typedef http_server_request<
            http_server_acceptor<
                transmitting,
                allocating
            >
        > request_type;
        typedef http_peer_response<
            http_server_acceptor<
                transmitting,
                allocating
            >
        > response_type;
        void async_accept(
            request_type& accepted,
            boost::function<
                bool(
                    boost::system::error_code const& error
                )
            > const& accept_handler
        );
        void async_respond(
            request_type& accepted,
            response_type const& response,
            boost::function<
                void(
                    boost::system::error_code const& error
                )
            > const& response_handler
        );
        void skip_data(
            request_type& accepted
        );
        void continue_accept_handler(
        );
        enum state_type {
            reading_method,
            skipping_resource_space,
            reading_resource,
            skipping_http_version_space,
            reading_http_version_head,
            reading_http_version_tail,
            finish_header,
            reading_header_key,
            skipping_header_value_space,
            reading_header_value,
            finishing_request_headers,
            reading_data,
            skipping_data,
        };
        enum accept_continuation_type {
            no_operation,
            deferred_handler_pending,
        };
        static bool parse_http(
            request_type* accepted,
            typename http_server_acceptor<
                transmitting,
                allocating
            >::string& to_parse,
            typename http_server_acceptor<
                transmitting,
                allocating
            >::string& resource,
            typename http_server_acceptor<
                transmitting,
                allocating
            >::string& data,
            typename http_server_acceptor<
                transmitting,
                allocating
            >::string& header_key,
            typename http_server_acceptor<
                transmitting,
                allocating
            >::string& header_value,
            std::map<
                typename http_server_acceptor<
                    transmitting,
                    allocating
                >::string,
                typename http_server_acceptor<
                    transmitting,
                    allocating
                >::string
            >& headers,
            boost::function<
                void(
                )
            > const& closer,
            boost::function<
                bool(
                    boost::system::error_code const& error
                )
            > const& accept_handler_invoker,
            boost::function<
                bool(
                )
            > const& deferred_response_handler_invoker,
            enum state_type& state,
            enum accept_continuation_type& accept_continuation,
            std::shared_ptr<
                http_fixed_length_extractor<
                    typename http_server_acceptor<
                        transmitting,
                        allocating
                    >::string
                >
            > data_extractor,
            std::string::const_iterator& matched,
            std::vector<
                std::string
            >::const_iterator& checked_method,
            std::list<
                request_type*
            >& unresponded,
            typename http_server_acceptor<
                transmitting,
                allocating
            >::string::size_type const& maximum_data
        );
        void close_link(
        );
        static std::vector<
            std::string
        > const& get_methods(
        );
    protected:
        bool invoke_accept_handler(
            boost::system::error_code const& error
        );
        void invoke_response_handler(
            boost::system::error_code const& error
        );
        bool invoke_deferred_response_handler(
        );
        void handle_write(
            boost::system::error_code const& error,
            bool response_finished,
            request_type* request
        );
        void handle_read(
            boost::system::error_code error = boost::system::error_code(
            ),
            size_t bytes = 0
        );
        bool parse_data(
        );
    private:
        request_type* accepted;
        request_type* response_deferring_request;
        boost::function<
            bool(
                boost::system::error_code const& error
            )
        > accept_handler;
        boost::function<
            void(
                boost::system::error_code const& error
            )
        > response_handler;
        boost::function<
            void(
            )
        > deferred_response_handler;
        enum {
            input_size = 0x400
        };
        char input[
            input_size
        ];
        enum state_type state;
        enum accept_continuation_type accept_continuation;
        typename http_server_acceptor<
            transmitting,
            allocating
        >::string output;
        static class method_vector :
        public std::vector<
            std::string
        > {
        public:
            method_vector(
            );
        } const methods;
        static std::string const http_version_head;
        static std::string const line_terminator;
        typename method_vector::const_iterator checked_method;
        std::string::const_iterator matched;
        typename http_server_acceptor<
            transmitting,
            allocating
        >::string resource;
        typename http_server_acceptor<
            transmitting,
            allocating
        >::string header_key;
        typename http_server_acceptor<
            transmitting,
            allocating
        >::string header_value;
        std::map<
            typename http_server_acceptor<
                transmitting,
                allocating
            >::string,
            typename http_server_acceptor<
                transmitting,
                allocating
            >::string
        > headers;
        typename http_server_acceptor<
            transmitting,
            allocating
        >::string data;
        std::shared_ptr<
            http_fixed_length_extractor<
                typename http_server_acceptor<
                    transmitting,
                    allocating
                >::string
            >
        > data_extractor;
        typename http_server_acceptor<
            transmitting,
            allocating
        >::string::size_type const maximum_data;
        typename http_server_acceptor<
            transmitting,
            allocating
        >::string to_parse;
        std::list<
            request_type*
        > unresponded;
    };
    
    template<
        typename transmitting,
        typename allocating
    >
    template<
        typename initializing
    >
    http_server_acceptor<
        transmitting,
        allocating
    >::http_server_acceptor(
        initializing& initializer,
        typename http_server_acceptor<
            transmitting,
            allocating
        >::string::size_type maximum_data
    ) :
    http_peer_session<
        transmitting,
        allocating
    >(
        initializer
    ),
    accepted(
        NULL
    ),
    response_deferring_request(
        NULL
    ),
    state(
        reading_method
    ),
    accept_continuation(
        no_operation
    ),
    checked_method(
        methods.begin(
        )
    ),
    matched(
        checked_method->begin(
        )
    ),
    maximum_data(
        maximum_data
    ) {
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    void http_server_acceptor<
        transmitting,
        allocating
    >::async_accept(
        typename http_server_acceptor<
            transmitting,
            allocating
        >::request_type& accepted,
        boost::function<
            bool(
                boost::system::error_code const& error
            )
        > const& accept_handler
    ) {
        this->accepted = &accepted;
        this->accept_handler = accept_handler;
        if (
            to_parse.empty(
            )
        ) {
            this->async_read_some(
                boost::asio::buffer(
                    input,
                    input_size
                ),
                boost::bind(
                    &http_server_acceptor::handle_read,
                    this,
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred
                )
            );
        } else {
            boost::asio::post(
                this->get_executor(
                ),
                boost::bind(
                    &http_server_acceptor::handle_read,
                    this,
                    boost::system::error_code(
                    ),
                    0
                )
            );
        }
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    void http_server_acceptor<
        transmitting,
        allocating
    >::async_respond(
        request_type& accepted,
        response_type const& response,
        boost::function<
            void(
                boost::system::error_code const& error
            )
        > const& response_handler
    ) {
        if (
            &accepted != unresponded.front(
            )
        ) {
            throw_exception(
                component_error(
                    "http_server_acceptor"
                )
            );
        }
        if (
            response.get_finished(
            )
        ) {
            unresponded.pop_front(
            );
        }
        this->response_handler = response_handler;
        output.clear(
        );
        if (
            !accepted.get_response_headers_sent(
            )
        ) {
            output += "HTTP/1.1 " + typename http_server_acceptor<
                transmitting,
                allocating
            >::string(
                response.get_status(
                ).begin(
                ),
                response.get_status(
                ).end(
                )
            ) + "\r\n";
            for (
                typename std::map<
                    typename http_server_acceptor<
                        transmitting,
                        allocating
                    >::string,
                    typename http_server_acceptor<
                        transmitting,
                        allocating
                    >::string
                >::const_iterator adding = response.get_headers(
                ).begin(
                );
                response.get_headers(
                ).end(
                ) != adding;
                adding++
            ) {
                output += adding->first + ": " + adding->second + "\r\n";
            }
            accepted.set_response_headers_sent(
            );
            output += "\r\n";
        }
        output += response.get_data(
        );
        boost::asio::async_write(
            *this,
            boost::asio::buffer(
                output.data(
                ),
                output.size(
                )
            ),
            boost::bind(
                &http_server_acceptor::handle_write,
                this,
                boost::asio::placeholders::error,
                response.get_finished(
                ),
                &accepted
            )
        );
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    void http_server_acceptor<
        transmitting,
        allocating
    >::skip_data(
        request_type& accepted
    ) {
        if (
            reading_data != state
        ) {
            throw_exception(
                component_error(
                    "http_server_acceptor"
                )
            );
        }
        state = skipping_data;
        unresponded.push_back(
            &accepted
        );
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    bool http_server_acceptor<
        transmitting,
        allocating
    >::invoke_accept_handler(
        boost::system::error_code const& error
    ) {
        boost::function<
            bool(
                boost::system::error_code const& error
            )
        > accept_handler = this->accept_handler;
        this->accept_handler.clear(
        );
        return accept_handler(
            error
        );
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    void http_server_acceptor<
        transmitting,
        allocating
    >::invoke_response_handler(
        boost::system::error_code const& error
    ) {
        boost::function<
            void(
                boost::system::error_code const& error
            )
        > response_handler = this->response_handler;
        this->response_handler.clear(
        );
        response_handler(
            error
        );
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    void http_server_acceptor<
        transmitting,
        allocating
    >::close_link(
    ) {
        this->lowest_layer(
        ).close(
        );
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    std::vector<
        std::string
    > const& http_server_acceptor<
        transmitting,
        allocating
    >::get_methods(
    ) {
        return methods;
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    bool http_server_acceptor<
        transmitting,
        allocating
    >::invoke_deferred_response_handler(
    ) {
        if (
            (
                NULL != response_deferring_request
            ) && response_deferring_request->get_responded(
            )
        ) {
            this->close_link(
            );
            boost::function<
                void(
                )
            > deferred_response_handler = this->deferred_response_handler;
            this->deferred_response_handler.clear(
            );
            response_deferring_request = NULL;
            deferred_response_handler(
            );
            return true;
        }
        return false;
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    void http_server_acceptor<
        transmitting,
        allocating
    >::handle_write(
        boost::system::error_code const& error,
        bool response_finished,
        request_type* request
    ) {
        if (
            error
        ) {
            this->invoke_response_handler(
                error
            );
            return;
        }
        if (
            response_finished
        ) {
            if (
                !request->get_finished(
                )
            ) {
                request->set_responded(
                );
                response_deferring_request = request;
                deferred_response_handler = boost::bind(
                    &http_server_acceptor::invoke_response_handler,
                    this,
                    error
                );
                this->async_read_some(
                    boost::asio::buffer(
                        input,
                        input_size
                    ),
                    boost::bind(
                        &http_server_acceptor::handle_read,
                        this,
                        boost::asio::placeholders::error,
                        boost::asio::placeholders::bytes_transferred
                    )
                );
                return;
            }
            this->close_link(
            );
        }
        this->invoke_response_handler(
            error
        );
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    bool http_server_acceptor<
        transmitting,
        allocating
    >::parse_http(
        request_type* accepted,
        typename http_server_acceptor<
            transmitting,
            allocating
        >::string& to_parse,
        typename http_server_acceptor<
            transmitting,
            allocating
        >::string& resource,
        typename http_server_acceptor<
            transmitting,
            allocating
        >::string& data,
        typename http_server_acceptor<
            transmitting,
            allocating
        >::string& header_key,
        typename http_server_acceptor<
            transmitting,
            allocating
        >::string& header_value,
        std::map<
            typename http_server_acceptor<
                transmitting,
                allocating
            >::string,
            typename http_server_acceptor<
                transmitting,
                allocating
            >::string
        >& headers,
        boost::function<
            void(
            )
        > const& closer,
        boost::function<
            bool(
                boost::system::error_code const& error
            )
        > const& accept_handler_invoker,
        boost::function<
            bool(
            )
        > const& deferred_response_handler_invoker,
        enum state_type& state,
        enum accept_continuation_type& accept_continuation,
        std::shared_ptr<
            http_fixed_length_extractor<
                typename http_server_acceptor<
                    transmitting,
                    allocating
                >::string
            >
        > data_extractor,
        std::string::const_iterator& matched,
        std::vector<
            std::string
        >::const_iterator& checked_method,
        std::list<
            request_type*
        >& unresponded,
        typename http_server_acceptor<
            transmitting,
            allocating
        >::string::size_type const& maximum_data
    ) {
        typename http_server_acceptor<
            transmitting,
            allocating
        >::string::const_iterator processed = to_parse.begin(
        );
        bool rechecking = false;
        while (
            (
                to_parse.end(
                ) != processed
            ) || rechecking
        ) {
            rechecking = false;
            switch (
                state
            ) {
            case reading_method:
                if (
                    *processed == *matched
                ) {
                    processed++;
                    matched++;
                    if (
                        checked_method->end(
                        ) == matched
                    ) {
                        accepted->set_method(
                            *checked_method
                        );
                        state = skipping_resource_space;
                    }
                    continue;
                }
                if (
                    checked_method->begin(
                    ) != matched
                ) {
                    closer(
                    );
                    accept_continuation = no_operation;
                    accept_handler_invoker(
                        boost::system::error_code(
                            http_error_codes::invalid_method,
                            http_category(
                            )
                        )
                    );
                    return false;
                }
                checked_method++;
                if (
                    methods.end(
                    ) == checked_method
                ) {
                    closer(
                    );
                    accept_continuation = no_operation;
                    accept_handler_invoker(
                        boost::system::error_code(
                            http_error_codes::invalid_method,
                            http_category(
                            )
                        )
                    );
                    return false;
                }
                matched = checked_method->begin(
                );
                continue;
            case skipping_resource_space:
                if (
                    ' ' != *processed
                ) {
                    closer(
                    );
                    accept_continuation = no_operation;
                    accept_handler_invoker(
                        boost::system::error_code(
                            http_error_codes::invalid_method,
                            http_category(
                            )
                        )
                    );
                    return false;
                }
                processed++;
                state = reading_resource;
                continue;
            case reading_resource:
                if (
                    true
                ) {
                    typename http_server_acceptor<
                        transmitting,
                        allocating
                    >::string::const_iterator start = processed;
                    while (
                        (
                            to_parse.end(
                            ) != processed
                        ) && (
                            ' ' != *processed
                        )
                    ) {
                        processed++;
                    }
                    resource += typename http_server_acceptor<
                        transmitting,
                        allocating
                    >::string(
                        start,
                        processed
                    );
                    if (
                        ' ' == *processed
                    ) {
                        accepted->set_resource(
                            resource
                        );
                        state = skipping_http_version_space;
                    }
                    continue;
                }
            case skipping_http_version_space:
                if (
                    ' ' != *processed
                ) {
                    throw_exception(
                        component_error(
                            "http_server_acceptor"
                        )
                    );
                }
                processed++;
                matched = http_version_head.begin(
                );
                state = reading_http_version_head;
                continue;
            case reading_http_version_head:
                if (
                    *processed == *matched
                ) {
                    processed++;
                    matched++;
                    if (
                        http_version_head.end(
                        ) == matched
                    ) {
                        state = reading_http_version_tail;
                    }
                    continue;
                }
                closer(
                );
                accept_continuation = no_operation;
                accept_handler_invoker(
                    boost::system::error_code(
                        http_error_codes::invalid_version,
                        http_category(
                        )
                    )
                );
                return false;
            case reading_http_version_tail:
                if (
                    http_server_acceptor<
                        transmitting,
                        allocating
                    >::string::npos != typename http_server_acceptor<
                        transmitting,
                        allocating
                    >::string(
                        "01"
                    ).find(
                        *processed
                    )
                ) {
                    processed++;
                    matched = line_terminator.begin(
                    );
                    state = finish_header;
                    continue;
                }
                closer(
                );
                accept_continuation = no_operation;
                accept_handler_invoker(
                    boost::system::error_code(
                        http_error_codes::invalid_version,
                        http_category(
                        )
                    )
                );
                return false;
            case finish_header:
                if (
                    *processed == *matched
                ) {
                    processed++;
                    matched++;
                    if (
                        line_terminator.end(
                        ) == matched
                    ) {
                        state = reading_header_key;
                    }
                    continue;
                }
                closer(
                );
                accept_continuation = no_operation;
                accept_handler_invoker(
                    boost::system::error_code(
                        http_error_codes::broken_line_terminator,
                        http_category(
                        )
                    )
                );
                return false;
            case reading_header_key:
                if (
                    *processed == line_terminator[
                        0
                    ]
                ) {
                    matched = line_terminator.begin(
                    );
                    state = finishing_request_headers;
                    continue;
                }
                if (
                    true
                ) {
                    typename http_server_acceptor<
                        transmitting,
                        allocating
                    >::string::const_iterator start = processed;
                    while (
                        (
                            to_parse.end(
                            ) != processed
                        ) && (
                            ':' != *processed
                        )
                    ) {
                        processed++;
                    }
                    header_key += typename http_server_acceptor<
                        transmitting,
                        allocating
                    >::string(
                        start,
                        processed
                    );
                    if (
                        ':' == *processed
                    ) {
                        processed++;
                        state = skipping_header_value_space;
                    }
                    continue;
                }
            case skipping_header_value_space:
                if (
                    ' ' == *processed
                ) {
                    processed++;
                    continue;
                }
                state = reading_header_value;
                continue;
            case reading_header_value:
                if (
                    true
                ) {
                    typename http_server_acceptor<
                        transmitting,
                        allocating
                    >::string::const_iterator start = processed;
                    while (
                        (
                            to_parse.end(
                            ) != processed
                        ) && (
                            line_terminator[
                                0
                            ] != *processed
                        )
                    ) {
                        processed++;
                    }
                    header_value += typename http_server_acceptor<
                        transmitting,
                        allocating
                    >::string(
                        start,
                        processed
                    );
                    if (
                        line_terminator[
                            0
                        ] == *processed
                    ) {
                        headers[
                            downcased(
                                header_key
                            )
                        ] = header_value;
                        header_key.clear(
                        );
                        header_value.clear(
                        );
                        matched = line_terminator.begin(
                        );
                        state = finish_header;
                    }
                    continue;
                }
            case finishing_request_headers:
                if (
                    *processed == *matched
                ) {
                    processed++;
                    matched++;
                    if (
                        line_terminator.end(
                        ) == matched
                    ) {
                        accepted->set_headers(
                            headers
                        );
                        typename std::map<
                            typename http_server_acceptor<
                                transmitting,
                                allocating
                            >::string,
                            typename http_server_acceptor<
                                transmitting,
                                allocating
                            >::string
                        >::const_iterator content_length = headers.find(
                            "content-length"
                        );
                        if (
                            headers.end(
                            ) == content_length
                        ) {
                            data_extractor.reset(
                                new http_fixed_length_extractor<
                                    typename http_server_acceptor<
                                        transmitting,
                                        allocating
                                    >::string
                                >(
                                    0
                                )
                            );
                        } else {
                            data_extractor.reset(
                                new http_fixed_length_extractor<
                                    typename http_server_acceptor<
                                        transmitting,
                                        allocating
                                    >::string
                                >(
                                    atoi(
                                        content_length->second.c_str(
                                        )
                                    )
                                )
                            );
                        }
                        rechecking = true;
                        if (
                            data_extractor->done(
                            )
                        ) {
                            state = reading_method;
                            accepted->set_data(
                                std::string(
                                )
                            );
                            resource.clear(
                            );
                            headers.clear(
                            );
                            header_key.clear(
                            );
                            header_value.clear(
                            );
                            data.clear(
                            );
                            accepted->set_finished(
                                true
                            );
                            if (
                                !accepted->get_responded(
                                )
                            ) {
                                unresponded.push_back(
                                    accepted
                                );
                                if (
                                    !accept_handler_invoker(
                                        boost::system::error_code(
                                        )
                                    )
                                ) {
                                    accept_continuation = deferred_handler_pending;
                                    return false;
                                }
                            }
                            checked_method = methods.begin(
                            );
                            matched = checked_method->begin(
                            );
                        } else {
                            state = reading_data;
                        }
                    }
                    continue;
                }
                closer(
                );
                accept_continuation = no_operation;
                accept_handler_invoker(
                    boost::system::error_code(
                        http_error_codes::broken_header_terminator,
                        http_category(
                        )
                    )
                );
                return false;
            case reading_data:
                while (
                    (
                        to_parse.end(
                        ) != processed
                    ) || (
                        !(
                            !data_extractor->done(
                            ) && (
                                data.size(
                                ) < maximum_data
                            )
                        )
                    )
                ) {
                    if (
                        !data_extractor->done(
                        ) && (
                            data.size(
                            ) < maximum_data
                        )
                    ) {
                        typename http_server_acceptor<
                            transmitting,
                            allocating
                        >::string::size_type available = to_parse.end(
                        ) - processed;
                        typename http_server_acceptor<
                            transmitting,
                            allocating
                        >::string::size_type const allowed(
                            maximum_data - data.size(
                            )
                        );
                        if (
                            allowed < available
                        ) {
                            available = allowed;
                        }
                        data += data_extractor->extract(
                            processed,
                            processed + available
                        );
                        continue;
                    }
                    accepted->set_data(
                        data
                    );
                    resource.clear(
                    );
                    headers.clear(
                    );
                    header_key.clear(
                    );
                    header_value.clear(
                    );
                    data.clear(
                    );
                    if (
                        data_extractor->done(
                        )
                    ) {
                        accepted->set_finished(
                            true
                        );
                        state = reading_method;
                        if (
                            !accepted->get_responded(
                            )
                        ) {
                            unresponded.push_back(
                                accepted
                            );
                        }
                    } else {
                        accepted->set_finished(
                            false
                        );
                    }
                    checked_method = methods.begin(
                    );
                    matched = checked_method->begin(
                    );
                    to_parse = typename http_server_acceptor<
                        transmitting,
                        allocating
                    >::string(
                        processed,
                        typename http_server_acceptor<
                            transmitting,
                            allocating
                        >::string::const_iterator(
                            to_parse.end(
                            )
                        )
                    );
                    if (
                        !accepted->get_responded(
                        )
                    ) {
                        if (
                            !accept_handler_invoker(
                                boost::system::error_code(
                                )
                            )
                        ) {
                            accept_continuation = deferred_handler_pending;
                            return false;
                        }
                    }
                    if (
                        data_extractor->done(
                        )
                    ) {
                        deferred_response_handler_invoker(
                        );
                    }
                    return false;
                }
                continue;
            case skipping_data:
                if (
                    data_extractor->done(
                    )
                ) {
                    if (
                        deferred_response_handler_invoker(
                        )
                    ) {
                        return false;
                    }
                    state = reading_method;
                } else {
                    typename http_server_acceptor<
                        transmitting,
                        allocating
                    >::string::size_type available = to_parse.end(
                    ) - processed;
                    data_extractor->extract(
                        processed,
                        processed + available
                    );
                    if (
                        data_extractor->done(
                        )
                    ) {
                        if (
                            deferred_response_handler_invoker(
                            )
                        ) {
                            return false;
                        }
                    }
                }
                continue;
            default:
                throw_exception(
                    component_error(
                        "http_server_acceptor"
                    )
                );
            }
        }
        to_parse = typename http_server_acceptor<
            transmitting,
            allocating
        >::string(
            processed,
            typename http_server_acceptor<
                transmitting,
                allocating
            >::string::const_iterator(
                to_parse.end(
                )
            )
        );
        return true;
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    bool http_server_acceptor<
        transmitting,
        allocating
    >::parse_data(
    ) {
        return this->parse_http(
            accepted,
            to_parse,
            resource,
            data,
            header_key,
            header_value,
            headers,
            boost::bind(
                &http_server_acceptor::close_link,
                this
            ),
            boost::bind(
                &http_server_acceptor::invoke_accept_handler,
                this,
                boost::placeholders::_1
            ),
            boost::bind(
                &http_server_acceptor::invoke_deferred_response_handler,
                this
            ),
            state,
            accept_continuation,
            data_extractor,
            matched,
            checked_method,
            unresponded,
            maximum_data
        );
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    void http_server_acceptor<
        transmitting,
        allocating
    >::continue_accept_handler(
    ) {
        switch (
            accept_continuation
        ) {
        case no_operation:
            break;
        case deferred_handler_pending:
            accept_continuation = no_operation;
            if (
                data_extractor->done(
                )
            ) {
                this->invoke_deferred_response_handler(
                );
            }
        }
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    void http_server_acceptor<
        transmitting,
        allocating
    >::handle_read(
        boost::system::error_code error,
        size_t bytes
    ) {
        if (
            boost::asio::error::operation_aborted == error
        ) {
            return;
        }
        if (
            error
        ) {
            if (
                !this->invoke_deferred_response_handler(
                )
            ) {
                accept_continuation = no_operation;
                this->invoke_accept_handler(
                    error
                );
            }
            return;
        }
    #ifdef QLIB_NO_EXCEPTIONS
        do {
    #else
        try {
    #endif
            to_parse += typename http_server_acceptor<
                transmitting,
                allocating
            >::string(
                input,
                bytes
            );
            if (
                !this->parse_data(
                )
            ) {
                return;
            }
    #ifdef QLIB_NO_EXCEPTIONS
        } while (
            false
        );
    #else
        } catch (
            std::exception& caught
        ) {
            this->close_link(
            );
            accept_continuation = no_operation;
            system_log.log_error(
                std::string(
                    "unexpected condition in http_server_acceptor: "
                ) + caught.what(
                )
            );
            this->invoke_accept_handler(
                boost::system::error_code(
                    http_error_codes::parser_error,
                    http_category(
                    )
                )
            );
            return;
        }
    #endif
        this->async_read_some(
            boost::asio::buffer(
                input,
                input_size
            ),
            boost::bind(
                &http_server_acceptor::handle_read,
                this,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    http_server_acceptor<
        transmitting,
        allocating
    >::method_vector::method_vector(
    ) :
    std::vector<
        std::string
    >(
        2
    ) {
        (
            *this
        )[
            0
        ] = "GET";
        (
            *this
        )[
            1
        ] = "POST";
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    typename http_server_acceptor<
        transmitting,
        allocating
    >::method_vector const http_server_acceptor<
        transmitting,
        allocating
    >::methods;
    
    template<
        typename transmitting,
        typename allocating
    >
    std::string const http_server_acceptor<
        transmitting,
        allocating
    >::http_version_head = "HTTP/1.";
    
    template<
        typename transmitting,
        typename allocating
    >
    std::string const http_server_acceptor<
        transmitting,
        allocating
    >::line_terminator = "\r\n";
}

#endif
