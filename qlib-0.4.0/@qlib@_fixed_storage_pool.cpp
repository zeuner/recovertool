/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_fixed_storage_pool.h"
#include "@qlib@_component_error.h"
#include "@qlib@_throw_exception.h"
namespace @qlib@ {
    
    
    fixed_storage_pool::fixed_storage_pool(
        size_t element_size,
        size_t count
    ) :
    element_size(
        element_size
    ),
    allocated(
        static_cast<
            char*
        >(
            malloc(
                element_size * count
            )
        )
    ),
    last_unused(
        allocated + (
            element_size * (
                count - 1
            )
        )
    ),
    free_list(
        count
    ),
    first_free(
        free_list.begin(
        )
    ),
    last_free(
        free_list.begin(
        )
    ) {
        if (
            NULL == allocated
        ) {
            throw_exception(
                component_error(
                    "fixed_storage_pool"
                )
            );
        }
    }
    
    fixed_storage_pool::~fixed_storage_pool(
    ) {
        free(
            allocated
        );
    }
    
    void* fixed_storage_pool::get(
    ) {
        if (
            first_free != last_free
        ) {
            last_free--;
            return *last_free;
        }
        if (
            allocated <= last_unused
        ) {
            void* result = last_unused;
            last_unused -= element_size;
            return result;
        }
        return NULL;
    }
    
    void fixed_storage_pool::put(
        void* freed
    ) {
        *last_free = freed;
        last_free++;
    }
}
