/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef RESOLVING_AAAA_RECORD_INCLUDED
#define RESOLVING_AAAA_RECORD_INCLUDED

#include <vector>
#include <boost/function.hpp>
#include <list>
#include <boost/shared_ptr.hpp>
#include <string>
namespace @qlib@ {
    
    
    class resolving_aaaa_record {
    public:
        static inline boost::arg<
            2
        > resolved_result_argument(
        );
        resolving_aaaa_record(
            resolver_decoding_session& resolving,
            std::string const& resolved,
            unsigned max_ttl = 3600
        );
    private:
        typedef boost::function<
            void(
                boost::system::error_code const& error,
                boost::asio::ip::address_v6 const& resolved_result
            )
        > handling_result;
    public:
        void async_resolve(
            handling_result const& result_handler
        );
    protected:
        void async_refresh(
        );
        void handle_resolution(
            boost::system::error_code const& error,
            std::list<
                resolver_decoding_session::aaaa_resource_record
            > const& records
        );
        void handle_expiry(
            boost::system::error_code const& error
        );
    private:
        boost::asio::io_service& communicating;
        resolver_decoding_session& resolving;
        char const* resolved;
        boost::shared_ptr<
            std::vector<
                resolver_decoding_session::aaaa_resource_record
            >
        > records;
        boost::shared_ptr<
            integer_random_source
        > chooser;
        std::list<
            handling_result
        > pending_requests;
        boost::asio::deadline_timer expiring;
        int choices;
        unsigned const max_ttl;
        class result_binder {
        public:
            result_binder(
                resolving_aaaa_record::handling_result const& result_handler,
                boost::system::error_code const& error,
                boost::asio::ip::address_v6 const& resolved_result
            );
            void operator(
            )(
            );
        private:
            resolving_aaaa_record::handling_result result_handler;
            boost::system::error_code error;
            boost::asio::ip::address_v6 resolved_result;
        };
    };
    
    boost::arg<
        2
    > resolving_aaaa_record::resolved_result_argument(
    ) {
        return boost::arg<
            2
        >(
        );
    }
}

#endif
