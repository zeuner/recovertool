/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef RING_BUFFER_INCLUDED
#define RING_BUFFER_INCLUDED

#include <vector>
namespace @qlib@ {
    
    
    template<
        typename storable
    >
    class ring_buffer {
    public:
        ring_buffer(
            typename std::vector<
                storable
            >::size_type size
        );
        storable const& get_last(
        ) const;
        void put(
            storable const& stored
        );
        typedef storable value_type;
    private:
        std::vector<
            storable
        > storage;
        typename std::vector<
            storable
        >::iterator current;
    };
    
    template<
        typename storable
    >
    ring_buffer<
        storable
    >::ring_buffer(
        typename std::vector<
            storable
        >::size_type size
    ) :
    storage(
        size
    ),
    current(
        storage.begin(
        )
    ) {
    }
    
    template<
        typename storable
    >
    storable const& ring_buffer<
        storable
    >::get_last(
    ) const {
        return *current;
    }
    
    template<
        typename storable
    >
    void ring_buffer<
        storable
    >::put(
        storable const& stored
    ) {
        current++;
        if (
            storage.end(
            ) == current
        ) {
            current = storage.begin(
            );
        }
        *current = stored;
    }
}

#endif
