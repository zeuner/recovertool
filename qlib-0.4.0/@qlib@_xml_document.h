/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef XML_DOCUMENT_INCLUDED
#define XML_DOCUMENT_INCLUDED

#include "@qlib@_component_error.h"
#include <boost/noncopyable.hpp>
namespace @qlib@ {
    
    
    class xml_document :
    private boost::noncopyable {
    public:
        xml_document(
        );
        template<
            typename string
        >
        xml_document(
            string const& text
        );
        template<
            typename string
        >
        xml_document(
            string const& text,
            string const& url,
            bool validated = false
        );
        xml_document(
            xmlDocPtr data
        );
        ~xml_document(
        );
        template<
            typename string
        >
        void export_text(
            string& text
        ) const;
        xmlNodePtr get_root_node(
        ) const;
        xmlDocPtr get_data(
        ) const;
    private:
        xmlDocPtr data;
    };
    
    template<
        typename string
    >
    xml_document::xml_document(
        string const& text
    ) :
    data(
        xmlParseMemory(
            reinterpret_cast<
                char const*
            >(
                text.data(
                )
            ),
            text.size(
            )
        )
    ) {
        if (
            NULL == data
        ) {
            throw component_error(
                "xml_document"
            );
        }
    }
    
    template<
        typename string
    >
    xml_document::xml_document(
        string const& text,
        string const& url,
        bool validated
    ) :
    data(
        xmlReadMemory(
            reinterpret_cast<
                char const*
            >(
                text.data(
                )
            ),
            text.size(
            ),
            reinterpret_cast<
                char const*
            >(
                url.c_str(
                )
            ),
            validated ? XML_PARSE_DTDVALID : 0
        )
    ) {
        if (
            NULL == data
        ) {
            throw component_error(
                "xml_document"
            );
        }
    }
    
    template<
        typename string
    >
    void xml_document::export_text(
        string& text
    ) const {
        xmlChar* text_data = NULL;
        int text_size;
        xmlDocDumpMemory(
            data,
            &text_data,
            &text_size
        );
        if (
            NULL == text_data
        ) {
            throw component_error(
                "xml_document"
            );
        }
        text = string(
            text_data,
            text_size
        );
        xmlFree(
            text_data
        );
    }
}

#endif
