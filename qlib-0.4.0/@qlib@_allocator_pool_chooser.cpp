/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_allocator_pool_chooser.h"
namespace @qlib@ {
    
    
    char* allocator_pool_chooser<
        1073741824
    >::allocate(
        long required
    ) {
        if (
            1073741824 < required
        ) {
            throw component_error(
                "allocator_pool_chooser"
            );
        }
        buffer_type* containing = allocator.allocate(
            1
        );
        char* result = containing->get_data(
        );
        if (
            reinterpret_cast<
                char*
            >(
                containing
            ) != result
        ) {
            throw component_error(
                "allocator_pool_chooser"
            );
        }
        return result;
    }
    
    void allocator_pool_chooser<
        1073741824
    >::deallocate(
        char* deallocated,
        long required
    ) {
        if (
            1073741824 < required
        ) {
            throw component_error(
                "allocator_pool_chooser"
            );
        }
        allocator.deallocate(
            reinterpret_cast<
                buffer_type*
            >(
                deallocated
            ),
            1
        );
    }
}
