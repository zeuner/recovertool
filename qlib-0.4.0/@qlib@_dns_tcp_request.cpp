/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_dns_tcp_request.h"
#include <boost/asio/write.hpp>
#include <boost/asio/placeholders.hpp>
#include <boost/bind.hpp>
namespace @qlib@ {
    
    
    dns_tcp_request::dns_tcp_request(
        boost::asio::io_service& io_service
    ) :
    boost::asio::ip::tcp::socket(
        io_service
    ) {
    }
    
    dns_tcp_request::dns_tcp_request(
        boost::asio::io_service& io_service,
        boost::asio::ip::tcp::endpoint const& local_endpoint
    ) :
    boost::asio::ip::tcp::socket(
        io_service,
        local_endpoint
    ) {
    }
    
    void dns_tcp_request::async_request(
        boost::asio::ip::address const& destination,
        std::string const& request,
        boost::function<
            void(
                boost::system::error_code const& error,
                std::string const& response
            )
        > const& result_handler
    ) {
        this->result_handler = result_handler;
        int upper = (
            request.size(
            ) >> 8
        ) & 0xff;
        int lower = request.size(
        ) & 0xff;
        char upper_character = upper;
        char lower_character = lower;
        writing_data += upper_character;
        writing_data += lower_character;
        writing_data += request;
        boost::asio::ip::tcp::endpoint endpoint(
            destination,
            53
        );
        boost::asio::ip::tcp::socket::async_connect(
            endpoint,
            boost::bind(
                &dns_tcp_request::handle_connect,
                this,
                boost::asio::placeholders::error
            )
        );
    }
    
    void dns_tcp_request::handle_connect(
        boost::system::error_code const& error
    ) {
        boost::asio::async_write(
            *this,
            boost::asio::buffer(
                writing_data.data(
                ),
                writing_data.size(
                )
            ),
            boost::bind(
                &dns_tcp_request::handle_write,
                this,
                boost::asio::placeholders::error
            )
        );
    }
    
    void dns_tcp_request::handle_write(
        boost::system::error_code const& error
    ) {
        boost::asio::async_read(
            *this,
            boost::asio::buffer(
                reading_data,
                2
            ),
            boost::asio::transfer_at_least(
                2
            ),
            boost::bind(
                &dns_tcp_request::handle_size_read,
                this,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
    
    void dns_tcp_request::handle_size_read(
        boost::system::error_code const& error,
        size_t transferred
    ) {
        if (
            error
        ) {
            result_handler(
                error,
                std::string(
                )
            );
            delete this;
            return;
        }
        if (
            2 != transferred
        ) {
            result_handler(
                boost::system::error_code(
                    boost::system::errc::protocol_error,
                    boost::system::get_generic_category(
                    )
                ),
                std::string(
                )
            );
            delete this;
            return;
        }
        unsigned char upper = reading_data[
            0
        ];
        unsigned char lower = reading_data[
            0
        ];
        input_size = (
            upper << 8
        ) + lower;
        async_read_some(
            boost::asio::buffer(
                reading_data,
                buffer_size
            ),
            boost::bind(
                &dns_tcp_request::handle_data_read,
                this,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
    
    void dns_tcp_request::handle_data_read(
        boost::system::error_code const& error,
        size_t transferred
    ) {
        if (
            error
        ) {
            result_handler(
                error,
                std::string(
                )
            );
            delete this;
            return;
        }
        if (
            input_size < transferred
        ) {
            transferred = input_size;
        }
        input += std::string(
            reading_data,
            transferred
        );
        input_size -= transferred;
        if (
            0 == input_size
        ) {
            result_handler(
                boost::system::error_code(
                ),
                input
            );
            delete this;
            return;
        }
        async_read_some(
            boost::asio::buffer(
                reading_data,
                buffer_size
            ),
            boost::bind(
                &dns_tcp_request::handle_data_read,
                this,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
}
