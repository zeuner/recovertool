/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef HTTP_PEER_RESPONSE_INCLUDED
#define HTTP_PEER_RESPONSE_INCLUDED

#include "@qlib@_http_peer_message.h"
#include <string>
namespace @qlib@ {
    
    
    template<
        typename transmitting
    >
    class http_peer_response :
    public http_peer_message<
        transmitting
    > {
    public:
        http_peer_response(
            transmitting& transmitter
        );
        void set_status(
            std::string const& status
        );
        std::string const& get_status(
        ) const;
    private:
        std::string status;
    };
    
    template<
        typename transmitting
    >
    http_peer_response<
        transmitting
    >::http_peer_response(
        transmitting& transmitter
    ) :
    http_peer_message<
        transmitting
    >(
        transmitter
    ) {
    }
    
    template<
        typename transmitting
    >
    void http_peer_response<
        transmitting
    >::set_status(
        std::string const& status
    ) {
        this->status = status;
    }
    
    template<
        typename transmitting
    >
    std::string const& http_peer_response<
        transmitting
    >::get_status(
    ) const {
        return status;
    }
}

#endif
