/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_quoted_printable_encoded.h"
#include <list>
namespace @qlib@ {
    
    
    std::string quoted_printable_encoded(
        std::string const& raw
    ) {
        if (
            raw.empty(
            )
        ) {
            return raw;
        }
        std::string result = raw;
        if (
            '\n' != result[
                result.size(
                ) - 1
            ]
        ) {
            result += "\n";
        }
        std::list<
            std::string
        > lines;
        std::string::size_type last = 0;
        while (
            true
        ) {
            std::string::size_type const next = result.find(
                '\n',
                last
            );
            if (
                std::string::npos == next
            ) {
                break;
            }
            lines.push_back(
                std::string(
                    result.begin(
                    ) + last,
                    result.begin(
                    ) + next
                )
            );
            last = next + 1;
        }
        result.clear(
        );
        for (
            std::list<
                std::string
            >::const_iterator processing = lines.begin(
            );
            lines.end(
            ) != processing;
            processing++
        ) {
            static std::string const end_allowed(
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                "abcdefghijklmnopqrstuvwxyz"
                "0123456789"
                "+/"
            );
            static std::string const intermediate_allowed = end_allowed + " ";
            if (
                processing->empty(
                )
            ) {
                result += "\n";
                continue;
            }
            std::string const* allowed = &end_allowed;
            std::string processed;
            for (
                std::string::const_iterator traversing = processing->end(
                );
                ;
            ) {
                traversing--;
                if (
                    std::string::npos == allowed->find(
                        *traversing
                    )
                ) {
                    processed = quoted_printable_character(
                        *traversing
                    ) + processed;
                } else {
                    processed = *traversing + processed;
                }
                if (
                    processing->begin(
                    ) == traversing
                ) {
                    break;
                }
                allowed = &intermediate_allowed;
            }
            if (
                "." == processed
            ) {
                processed = quoted_printable_character(
                    processed[
                        0
                    ]
                );
            } else if (
                76 <= processed.size(
                )
            ) {
                std::string split;
                std::string::size_type done = 0;
                while (
                    true
                ) {
                    std::string::size_type next = done + 76;
                    if (
                        processed.size(
                        ) < next
                    ) {
                        split += std::string(
                            processed.begin(
                            ) + done,
                            processed.end(
                            )
                        );
                        break;
                    }
                    std::string::size_type const cut = processed.find(
                        '=',
                        next - 3
                    );
                    if (
                        std::string::npos == cut
                    ) {
                    } else if (
                        cut < next
                    ) {
                        next = cut;
                    }
                    split += std::string(
                        processed.begin(
                        ) + done,
                        processed.begin(
                        ) + next
                    ) + "=\r\n";
                    done = next;
                }
                processed = split;
            }
            result += processed + "\r\n";
        }
        return result;
    }
}
