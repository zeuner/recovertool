/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_xml_compare_structure.h"
namespace @qlib@ {
    
    
    int xml_compare_structure(
        xmlNodePtr smaller,
        xmlNodePtr larger
    ) {
        int const names = strcmp(
            reinterpret_cast<
                const char*
            >(
                smaller->name
            ),
            reinterpret_cast<
                const char*
            >(
                larger->name
            )
        );
        if (
            0 != names
        ) {
            return names;
        }
        if (
            NULL == smaller->ns
        ) {
            if (
                NULL != larger->ns
            ) {
                return -1;
            }
        } else {
            if (
                NULL == larger->ns
            ) {
                return 1;
            }
        }
        int const namespaces = strcmp(
            reinterpret_cast<
                const char*
            >(
                smaller->ns->href
            ),
            reinterpret_cast<
                const char*
            >(
                larger->ns->href
            )
        );
        if (
            0 != namespaces
        ) {
            return namespaces;
        }
        xmlNodePtr smaller_child = smaller->children;
        xmlNodePtr larger_child = larger->children;
        while (
            true
        ) {
            while (
                (
                    NULL != smaller_child
                ) &&(
                    XML_ELEMENT_NODE != smaller_child->type
                )
            ) {
                smaller_child = smaller_child->next;
            }
            while (
                (
                    NULL != larger_child
                ) &&(
                    XML_ELEMENT_NODE != larger_child->type
                )
            ) {
                larger_child = larger_child->next;
            }
            if (
                NULL == smaller_child
            ) {
                if (
                    NULL != larger_child
                ) {
                    return -1;
                } else {
                    break;
                }
            } else {
                if (
                    NULL == larger_child
                ) {
                    return 1;
                }
            }
            int const children = xml_compare_structure(
                smaller_child,
                larger_child
            );
            if (
                0 != children
            ) {
                return children;
            }
            smaller_child = smaller_child->next;
            larger_child = larger_child->next;
        }
        return 0;
    }
}
