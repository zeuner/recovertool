/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_pdf_document.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    pdf_document::pdf_document(
    ) :
    native(
        HPDF_New(
            pdf_document::error_handler,
            NULL
        )
    ) {
    }
    
    pdf_document::~pdf_document(
    ) {
        HPDF_Free(
            native
        );
    }
    
    std::string pdf_document::get_data(
    ) {
        HPDF_AddPage(
            native
        );
        if (
            HPDF_OK != HPDF_SaveToStream(
                native
            )
        ) {
            throw component_error(
                "pdf_document"
            );
        }
        HPDF_UINT32 size = HPDF_GetStreamSize(
            native
        );
        HPDF_BYTE data[
            size
        ];
        HPDF_STATUS status = HPDF_ReadFromStream(
            native,
            data,
            &size
        );
        if (
            (
                HPDF_OK != status
            ) && (
                HPDF_STREAM_EOF != status
            )
        ) {
            throw component_error(
                "pdf_document"
            );
        }
        return std::string(
            reinterpret_cast<
                char const*
            >(
                data
            ),
            size
        );
    }
    
    HPDF_Page pdf_document::add_page(
    ) {
        return HPDF_AddPage(
            native
        );
    }
    
    HPDF_Font pdf_document::get_font(
        std::string const& name
    ) {
        return HPDF_GetFont(
            native,
            name.c_str(
            ),
            NULL
        );
    }
    
    void pdf_document::error_handler(
        HPDF_STATUS error_number,
        HPDF_STATUS detail_number,
        void* user_data
    ) {
        throw component_error(
            "pdf_document"
        );
    }
}
