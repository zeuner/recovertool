/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef PACKET_CAPTURE_SOCKET_INCLUDED
#define PACKET_CAPTURE_SOCKET_INCLUDED

#include <string>
namespace @qlib@ {
    
    
    class packet_capture_socket :
    public packet_capture_wrapper,
    public boost::asio::basic_raw_socket<
        packet_raw_protocol
    > {
    public:
        packet_capture_socket(
            boost::asio::io_service& communicating,
            std::string const& interface,
            bool promiscuous = true,
            size_t snapshot_length = 0x1000,
            size_t buffer_size = 0x200000,
            int timeout_milliseconds = -1
        );
    };
}

#endif
