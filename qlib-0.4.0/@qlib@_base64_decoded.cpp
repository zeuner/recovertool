/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_base64_decoded.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    std::string base64_decoded(
        std::string const& raw
    ) {
        std::string const characters(
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            "abcdefghijklmnopqrstuvwxyz"
            "0123456789"   
            "+/"
        );
        std::pair<
            char,
            int
        > decoding[
            0x100
        ];
        for (
            std::string::size_type decoded_bits = 0;
            characters.size(
            ) > decoded_bits;
            decoded_bits++
        ) {
            decoding[
                static_cast<
                    int
                >(
                    characters[
                        decoded_bits
                    ]
                )
            ] = std::make_pair(
                decoded_bits,
                6
            );
        }
        decoding[
            static_cast<
                int
            >(
                '='
            )
        ] = std::make_pair(
            0,
            -2
        );
        decoding[
            static_cast<
                int 
            >(
                '\r'
            )
        ] = std::make_pair(
            0,
            0
        );
        decoding[
            static_cast<
                int 
            >(
                '\n'
            )
        ] = std::make_pair(
            0,
            0
        );
        std::string decoded_data;
        int bits = 0;
        char this_byte = 0;
        for (
            std::string::const_iterator traversed = raw.begin(
            );
            raw.end(
            ) != traversed;
            traversed++
        ) {
            std::pair<
                char,
                int
            > const& this_decoded = decoding[
                static_cast<
                    int
                >(
                    *traversed
                )
            ];
            bits += this_decoded.second;
            if (
                8 <= bits
            ) {
                bits -= 8;
                this_byte += this_decoded.first >> bits;
                decoded_data += this_byte;
                this_byte = 0;
            }
            this_byte += this_decoded.first << (
                8 - bits
            );
        }
        if (
            0 != bits
        ) {
            throw component_error(
                "base64_decoded"
            );
        }
        return decoded_data;
    }
}
