/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef SOAP_TYPED_INCLUDED
#define SOAP_TYPED_INCLUDED

#include <list>
#include <vector>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <string>
namespace @qlib@ {
    
    
    class soap_typed :
    private boost::noncopyable {
    public:
        template<
            typename element_type
        >
        void add_element(
            std::string const& element_name,
            std::string const& element_namespace,
            element_type const& added
        );
        void dump_to_xml(
            xmlNodePtr augmented
        ) const;
        void parse_xml(
            xmlNodePtr const parsed
        );
        typedef boost::variant<
            boost::shared_ptr<
                soap_typed
            >,
            std::string,
            long,
            float,
            double
        > element_value;
        std::vector<
            element_value
        > matching_elements(
            std::string const& element_name,
            std::string const& element_namespace
        );
        void set_value_type(
            std::string const& local,
            std::string const& ns
        );
    protected:
        soap_typed(
            std::list<
                soap_service::element_type
            > const& type
        );
        soap_service::type_descriptor const& next_element_type(
            std::string const& element_name,
            std::string const& element_namespace
        );
        void add_with_type(
            std::string const& element_name,
            std::string const& element_namespace,
            soap_service::type_descriptor const& type,
            long value
        );
        void add_with_type(
            std::string const& element_name,
            std::string const& element_namespace,
            soap_service::type_descriptor const& type,
            float value
        );
        void add_with_type(
            std::string const& element_name,
            std::string const& element_namespace,
            soap_service::type_descriptor const& type,
            double value
        );
        void add_with_type(
            std::string const& element_name,
            std::string const& element_namespace,
            soap_service::type_descriptor const& type,
            std::string const& value
        );
        void add_with_type(
            std::string const& element_name,
            std::string const& element_namespace,
            soap_service::type_descriptor const& type,
            boost::shared_ptr<
                soap_typed
            > const& value
        );
        static xmlNsPtr namespace_for_child(
            std::string const& href,
            xmlNodePtr parent
        );
    private:
        class dumping_visitor :
        public boost::static_visitor<
            void
        > {
        public:
            dumping_visitor(
                xmlNodePtr augmented
            );
            void operator(
            )(
                std::string const& visited
            ) const;
            void operator(
            )(
                long visited
            ) const;
            void operator(
            )(
                float visited
            ) const;
            void operator(
            )(
                double visited
            ) const;
            void operator(
            )(
                boost::shared_ptr<
                    soap_typed
                > const& visited
            ) const;
        private:
            xmlNodePtr augmented;
        };
        class parsing_visitor :
        public boost::static_visitor<
            void
        > {
        public:
            parsing_visitor(
                std::string const& local,
                std::string const& ns,
                xmlNodePtr const parsed,
                soap_typed& augmented
            );
            void operator(
            )(
                std::list<
                    soap_service::element_type
                > const& visited
            ) const;
            void operator(
            )(
                std::string const& visited
            ) const;
        private:
            std::string local;
            std::string ns;
            xmlNodePtr const parsed;
            soap_typed& augmented;
        };
        std::list<
            soap_service::element_type
        > const& type;
        boost::shared_ptr<
            soap_service::element_identification
        > value_type;
        std::list<
            soap_service::element_type
        >::const_iterator current_element;
        int element_count;
        std::list<
            std::pair<
                soap_service::element_identification,
                element_value
            >
        > elements;
    };
    
    template<
        typename element_type
    >
    void soap_typed::add_element(
        std::string const& element_name,
        std::string const& element_namespace,
        element_type const& added
    ) {
        this->add_with_type(
            element_name,
            element_namespace,
            this->next_element_type(
                element_name,
                element_namespace
            ),
            added
        );
    }
}

#endif
