/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_pcap_dump_reader.h"
#include <unistd.h>
#include <stdexcept>
namespace @qlib@ {
    
    
    pcap_dump_reader::pcap_dump_reader(
        std::string const& filename
    ) :
    handle(
        pcap_open_offline(
            filename.c_str(
            ),
            error_buffer
        )
    ) {
        if (
            NULL == handle
        ) {
            throw std::runtime_error(
                std::string(
                    "pcap_dump_reader: pcap_open_offline: "
                ) + error_buffer
            );
        }
    }
    
    pcap_dump_reader::~pcap_dump_reader(
    ) {
        pcap_close(
            handle
        );
    }
    
    bool pcap_dump_reader::get_packet(
        u_char const*& data,
        bpf_u_int32& size
    ) {
        struct pcap_pkthdr header;
        if (
            NULL == (
                data = pcap_next(
                    handle,
                    &header
                )
            )
        ) {
            return false;
        }
        size = header.caplen;
        return true;
    }
}
