/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_low_pass_filter_series.h"
namespace @qlib@ {
    
    
    std::list<
        amplitude_value 
    > low_pass_filter_series(
        time_value sample_rate,
        time_value cutoff,
        int size
    ) {
        std::list<
            amplitude_value
        > result;
        static time_value const pi = 4.0 * std::atan(
            1.0
        );
        for (
            int i = -size;
            size >= i;
            i++
        ) {
            real_number const i_real = i;
            result.push_front(
                (
                    i == 0
                ) ? (
                    real_number(
                        2
                    ) * cutoff / sample_rate
                ) : (
                    sin(
                        real_number(
                            2
                        ) * pi * i_real * cutoff / sample_rate
                    ) / (
                        pi * i_real
                    )
                )
            );
        }
        return result;
    }
}
