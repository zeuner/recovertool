/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_ip_layer_link.h"
#include <cstring>
namespace @qlib@ {
    
    
    ip_layer_link::ip_layer_link(
        in_addr_t source_ip,
        in_addr_t destination_ip,
        uint8_t transport_protocol
    ) :
    identification(
        choose_short(
        )
    ) {
        memset(
            &native_header,
            0,
            sizeof(
                native_header
            )
        );
        native_header.ihl = 0x5;
        native_header.version = 0x4;
        native_header.tos = 0x0;
        native_header.frag_off = htons(
            0x4000
        );
        native_header.ttl = 64;
        native_header.protocol = transport_protocol;
        native_header.saddr = source_ip;
        native_header.daddr = destination_ip;
        native_header.check = 0x0;
    }
    
    void ip_layer_link::write_header(
        native_type& written,
        size_t payload_size
    ) {
        memcpy(
            &written,
            &native_header,
            sizeof(
                native_header
            )
        );
        written.id = htons(
            identification++
        );
        written.tot_len = htons(
            sizeof(
                native_header
            ) + payload_size
        );
        written.check = ip_header_checksum(
            reinterpret_cast<
                unsigned short*
            >(
                &written
            ),
            sizeof(
                written
            )
        );
    }
    
    in_addr_t ip_layer_link::get_source_ip(
    ) const {
        return native_header.saddr;
    }
    
    in_addr_t ip_layer_link::get_destination_ip(
    ) const {
        return native_header.daddr;
    }
    
    uint16_t ip_layer_link::get_protocol(
    ) {
        return ETH_P_IP;
    }
    
    integer_random_source ip_layer_link::choose_short(
        1 << 16
    );
}
