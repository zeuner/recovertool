/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_spectrally_inverted.h"
namespace @qlib@ {
    
    
    std::list<
        amplitude_value 
    > spectrally_inverted(
        std::list<
            amplitude_value
        > inverting
    ) {
        int const center = (
            inverting.size(
            ) - 1
        ) / 2;
        int i = 0;
        for (
            std::list<
                amplitude_value
            >::iterator adapting = inverting.begin(
            );
            inverting.end(
            ) != adapting;
            adapting++,
            i++
        ) {
            *adapting *= -1;
            if (
                center == i
            ) {
                *adapting += 1;
            }
        }
        return inverting;
    }
}
