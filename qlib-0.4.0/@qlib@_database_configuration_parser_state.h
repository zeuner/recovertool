/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef DATABASE_CONFIGURATION_PARSER_STATE_INCLUDED
#define DATABASE_CONFIGURATION_PARSER_STATE_INCLUDED

#include <map>
#include "@qlib@_parser_state_interface.h"
#include <boost/shared_ptr.hpp>
#include <string>
#include <list>
namespace @qlib@ {
    
    
    class database_configuration_parser_state :
    public parser_state_interface {
    public:
       database_configuration_parser_state(
            database_access_configuration& configuration
        );
        void on_start_element(
            const std::string& local_name,
            const std::string& namespace_prefix,
            const std::string& namespace_uri,
            const std::list<
                std::pair<
                    std::string,
                    std::string
                >
            >& properties,
            boost::shared_ptr<
                std::map<
                    std::string,
                    std::string
                > const
            > namespaces
        );
        void on_end_element(
            const std::string& local_name,
            const std::string& namespace_prefix,
            const std::string& namespace_uri
        );
        void on_characters(
            const std::string& characters
        );
    private:
        database_access_configuration& configuration;
    };
}

#endif
