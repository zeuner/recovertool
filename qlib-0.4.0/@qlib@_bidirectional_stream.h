/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef BIDIRECTIONAL_STREAM_INCLUDED
#define BIDIRECTIONAL_STREAM_INCLUDED

#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/noncopyable.hpp>
#include <unistd.h>
#include <boost/asio/placeholders.hpp>
#include <boost/asio/write.hpp>
namespace @qlib@ {
    
    
    template<
        typename top_connected,
        typename bottom_connected,
        unsigned buffer_size = 0x400
    >
    class bidirectional_stream;
    
    template<
        typename top_connected,
        typename bottom_connected,
        unsigned buffer_size
    >
    class bidirectional_stream :
    private boost::noncopyable {
    public:
        bidirectional_stream(
            top_connected& top,
            bottom_connected& bottom,
            boost::function<
                void(
                )
            > const& shutdown_handler
        );
    protected:
        void handle_upstream_read(
            boost::system::error_code const& error,
            size_t bytes
        );
        void handle_downstream_read(
            boost::system::error_code const& error,
            size_t bytes
        );
        void handle_upstream_write(
            boost::system::error_code const& error
        );
        void handle_downstream_write(
            boost::system::error_code const& error
        );
    private:
        top_connected& top;
        bottom_connected& bottom;
        char upstream_data[
            buffer_size
        ];
        char downstream_data[
            buffer_size
        ];
        boost::function<
            void(
            )
        > shutdown_handler;
        bool done;
    };
    
    template<
        typename top_connected,
        typename bottom_connected,
        unsigned buffer_size
    >
    bidirectional_stream<
        top_connected,
        bottom_connected,
        buffer_size
    >::bidirectional_stream(
        top_connected& top,
        bottom_connected& bottom,
        boost::function<
            void(
            )
        > const& shutdown_handler
    ) :
    top(
        top
    ),
    bottom(
        bottom
    ),
    shutdown_handler(
        shutdown_handler
    ),
    done(
        false
    ) {
        bottom.async_read_some(
            boost::asio::buffer(
                upstream_data,
                buffer_size
            ),
            boost::bind(
                &bidirectional_stream<
                    top_connected,
                    bottom_connected,
                    buffer_size
                >::handle_upstream_read,
                this,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
        top.async_read_some(
            boost::asio::buffer(
                downstream_data,
                buffer_size
            ),
            boost::bind(
                &bidirectional_stream<
                    top_connected,
                    bottom_connected,
                    buffer_size
                >::handle_downstream_read,
                this,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
    
    template<
        typename top_connected,
        typename bottom_connected,
        unsigned buffer_size
    >
    void bidirectional_stream<
        top_connected,
        bottom_connected,
        buffer_size
    >::handle_upstream_read(
        boost::system::error_code const& error,
        size_t bytes
    ) {
        if (
            done
        ) {
            shutdown_handler(
            );
            return;
        }
        if (
            error
        ) {
            done = true;
            top.close(
            );
            return;
        }
        boost::asio::async_write(
            top,
            boost::asio::buffer(
                upstream_data,
                bytes
            ),
            boost::bind(
                &bidirectional_stream<
                    top_connected,
                    bottom_connected,
                    buffer_size
                >::handle_upstream_write,
                this,
                boost::asio::placeholders::error
            )
        );
    }
    
    template<
        typename top_connected,
        typename bottom_connected,
        unsigned buffer_size
    >
    void bidirectional_stream<
        top_connected,
        bottom_connected,
        buffer_size
    >::handle_downstream_read(
        boost::system::error_code const& error,
        size_t bytes
    ) {
        if (
            done
        ) {
            shutdown_handler(
            );
            return;
        }
        if (
            error
        ) {
            done = true;
            bottom.close(
            );
            return;
        }
        boost::asio::async_write(
            bottom,
            boost::asio::buffer(
                downstream_data,
                bytes
            ),
            boost::bind(
                &bidirectional_stream<
                    top_connected,
                    bottom_connected,
                    buffer_size
                >::handle_downstream_write,
                this,
                boost::asio::placeholders::error
            )
        );
    }
    
    template<
        typename top_connected,
        typename bottom_connected,
        unsigned buffer_size
    >
    void bidirectional_stream<
        top_connected,
        bottom_connected,
        buffer_size
    >::handle_upstream_write(
        boost::system::error_code const& error
    ) {
        if (
            done
        ) {
            shutdown_handler(
            );
            return;
        }
        if (
            error
        ) {
            done = true;
            bottom.close(
            );
            return;
        }
        bottom.async_read_some(
            boost::asio::buffer(
                upstream_data,
                buffer_size
            ),
            boost::bind(
                &bidirectional_stream<
                    top_connected,
                    bottom_connected,
                    buffer_size
                >::handle_upstream_read,
                this,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
    
    template<
        typename top_connected,
        typename bottom_connected,
        unsigned buffer_size
    >
    void bidirectional_stream<
        top_connected,
        bottom_connected,
        buffer_size
    >::handle_downstream_write(
        boost::system::error_code const& error
    ) {
        if (
            done
        ) {
            shutdown_handler(
            );
            return;
        }
        if (
            error
        ) {
            done = true;
            top.close(
            );
            return;
        }
        top.async_read_some(
            boost::asio::buffer(
                downstream_data,
                buffer_size
            ),
            boost::bind(
                &bidirectional_stream<
                    top_connected,
                    bottom_connected,
                    buffer_size
                >::handle_downstream_read,
                this,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
}

#endif
