/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef HTTP_SERVER_REQUEST_INCLUDED
#define HTTP_SERVER_REQUEST_INCLUDED

#include "@qlib@_http_peer_request.h"
#include <boost/function.hpp>
namespace @qlib@ {
    
    
    template<
        typename accepting
    >
    class http_server_request :
    public http_peer_request<
        accepting
    > {
    public:
        http_server_request(
            accepting& acceptor
        );
        void async_respond(
            typename http_server_request<
                accepting
            >::response_type const* response,
            boost::function<
                void(
                    boost::system::error_code const& error
                )
            > const& response_handler
        );
        bool get_response_headers_sent(
        ) const;
        void set_response_headers_sent(
        );
        bool get_responded(
        ) const;
        void set_responded(
        );
    private:
        bool response_headers_sent;
        bool responded;
    };
    
    template<
        typename accepting
    >
    http_server_request<
        accepting
    >::http_server_request(
        accepting& acceptor
    ) :
    http_peer_request<
        accepting
    >(
        acceptor
    ),
    response_headers_sent(
        false
    ),
    responded(
        false
    ) {
    }
    
    template<
        typename accepting
    >
    void http_server_request<
        accepting
    >::async_respond(
        typename http_server_request<
            accepting
        >::response_type const* response,
        boost::function<
            void(
                boost::system::error_code const& error
            )
        > const& response_handler
    ) {
        this->get_transmitter(
        ).async_respond(
            *this,
            *response,
            response_handler
        );
    }
    
    template<
        typename accepting
    >
    bool http_server_request<
        accepting
    >::get_response_headers_sent(
    ) const {
        return response_headers_sent;
    }
    
    template<
        typename accepting
    >
    void http_server_request<
        accepting
    >::set_response_headers_sent(
    ) {
        response_headers_sent = true;
    }
    
    template<
        typename accepting
    >
    bool http_server_request<
        accepting
    >::get_responded(
    ) const {
        return responded;
    }
    
    template<
        typename accepting
    >
    void http_server_request<
        accepting
    >::set_responded(
    ) {
        responded = true;
    }
}

#endif
