/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_stripped_trailing_spaces.h"
namespace @qlib@ {
    
    
    std::string stripped_trailing_spaces(
        std::string const& raw
    ) {
        std::string::const_iterator last = raw.begin(
        ) - 1;
        for (
            std::string::const_iterator scanning = raw.begin(
            );
            raw.end(
            ) != scanning;
            scanning++
        ) {
            if (
                ' ' != *scanning
            ) {
                last = scanning;
            }
        }
        return std::string(
            raw.begin(
            ),
            last + 1
        );
    }
}
