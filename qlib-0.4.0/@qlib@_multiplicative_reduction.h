/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef MULTIPLICATIVE_REDUCTION_INCLUDED
#define MULTIPLICATIVE_REDUCTION_INCLUDED

namespace @qlib@ {
    
    
    template<
        typename valued
    >
    class multiplicative_reduction {
    public:
        static valued initial_value(
        );
        static valued reduced(
            valued one,
            valued other
        );
        static void reduce(
            valued& one,
            valued other
        );
    };
    
    template<
        typename valued
    >
    valued multiplicative_reduction<
        valued
    >::initial_value(
    ) {
        return real_number(
            1
        );
    }
    
    template<
        typename valued
    >
    valued multiplicative_reduction<
        valued
    >::reduced(
        valued one,
        valued other
    ) {
        return one * other;
    }
    
    template<
        typename valued
    >
    void multiplicative_reduction<
        valued
    >::reduce(
        valued& one,
        valued other
    ) {
        one *= other;
    }
}

#endif
