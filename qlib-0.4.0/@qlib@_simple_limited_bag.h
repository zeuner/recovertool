/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef SIMPLE_LIMITED_BAG_INCLUDED
#define SIMPLE_LIMITED_BAG_INCLUDED

#include <vector>
#include "@qlib@_component_error.h"
#include <boost/noncopyable.hpp>
namespace @qlib@ {
    
    
    template<
        typename element_type
    >
    class simple_limited_bag :
    private boost::noncopyable {
    public:
        simple_limited_bag(
            size_t size = 0
        );
        void resize(
            size_t size
        );
        class iterator {
        public:
            iterator(
                element_type* position,
                element_type* elements_start,
                element_type* elements_end
            );
            bool operator!=(
                iterator const& other
            ) const;
            iterator const& operator++(
            );
            element_type& operator*(
            );
        private:
            element_type* position;
            element_type* elements_start;
            element_type* elements_end;
        };
        iterator insert(
            element_type const& inserted
        );
        void erase(
            iterator erased
        );
        iterator begin(
        ) const;
        iterator end(
        ) const;
        bool empty(
        ) const;
    private:
        std::vector<
            element_type
        > elements;
        element_type* elements_start;
        element_type* elements_end;
        element_type* first_element;
        element_type* last_element;
    };
    
    template<
        typename element_type
    >
    simple_limited_bag<
        element_type
    >::simple_limited_bag(
        size_t size
    ) :
    elements(
        size + 1
    ),
    elements_start(
        elements.data(
        )
    ),
    elements_end(
        elements.data(
        ) + (
            size + 1
        )
    ),
    first_element(
        elements.data(
        )
    ),
    last_element(
        elements.data(
        )
    ) {
    }
    
    template<
        typename element_type
    >
    void simple_limited_bag<
        element_type
    >::resize(
        size_t size
    ) {
        if (
            !empty(
            )
        ) {
            throw component_error(
                "simple_limited_bag"
            );
        }
        elements.resize(
            size + 1
        );
        elements_start = elements.data(
        );
        elements_end = elements.data(
        ) + (
            size + 1
        );
        first_element = elements.data(
        );
        last_element = elements.data(
        );
    }
    
    template<
        typename element_type
    >
    simple_limited_bag<
        element_type
    >::simple_limited_bag::iterator::iterator(
        element_type* position,
        element_type* elements_start,
        element_type* elements_end
    ) :
    position(
        position
    ),
    elements_start(
        elements_start
    ),
    elements_end(
        elements_end
    ) {
    }
    
    template<
        typename element_type
    >
    bool simple_limited_bag<
        element_type
    >::iterator::operator!=(
        iterator const& other
    ) const {
        return position != other.position;
    }
    
    template<
        typename element_type
    >
    typename simple_limited_bag<
        element_type
    >::iterator const& simple_limited_bag<
        element_type
    >::iterator::operator++(
    ) {
        position++;
        if (
            elements_end == position
        ) {
            position = elements_start;
        }
        return *this;
    }
    
    template<
        typename element_type
    >
    element_type& simple_limited_bag<
        element_type
    >::iterator::operator*(
    ) {
        return *position;
    }
    
    template<
        typename element_type
    >
    typename simple_limited_bag<
        element_type
    >::iterator simple_limited_bag<
        element_type
    >::insert(
        element_type const& inserted
    ) {
        *last_element = inserted;
        iterator const result(
            last_element,
            elements_start,
            elements_end
        );
        last_element++;
        if (
            elements_end == last_element
        ) {
            last_element = elements_start;
        }
        if (
            last_element == first_element
        ) {
            throw component_error(
                "simple_limited_bag"
            );
        }
        return result;
    }
    
    template<
        typename element_type
    >
    void simple_limited_bag<
        element_type
    >::erase(
        typename simple_limited_bag<
            element_type
        >::iterator erased
    ) {
        if (
            last_element == first_element
        ) {
            throw component_error(
                "simple_limited_bag"
            );
        }
        *erased = *first_element;
        first_element++;
        if (
            elements_end == first_element
        ) {
            first_element = elements_start;
        }
    }
    
    template<
        typename element_type
    >
    typename simple_limited_bag<
        element_type
    >::iterator simple_limited_bag<
        element_type
    >::begin(
    ) const {
        return iterator(
            first_element,
            elements_start,
            elements_end
        );
    }
    
    template<
        typename element_type
    >
    typename simple_limited_bag<
        element_type
    >::iterator simple_limited_bag<
        element_type
    >::end(
    ) const {
        return iterator(
            last_element,
            elements_start,
            elements_end
        );
    }
    
    template<
        typename element_type
    >
    bool simple_limited_bag<
        element_type
    >::empty(
    ) const {
        return first_element == last_element;
    }
}

#endif
