/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_with_changed_working_directory.h"
#include "@qlib@_system_log.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    with_changed_working_directory::with_changed_working_directory(
        std::string const& path
    ) {
        if (
            NULL == getcwd(
                old_cwd,
                sizeof(
                    old_cwd
                )
            )
        ) {
            throw component_error(
                "with_changed_working_directory"
            );
        }
        if (
            0 > chdir(
                path.c_str(
                )
            )
        ) {
            throw component_error(
                "with_changed_working_directory"
            );
        }
    }
    
    with_changed_working_directory::~with_changed_working_directory(
    ) {
        if (
            0 > chdir(
                old_cwd
            )
        ) {
            system_log.log_error(
                "could not switch back to directory " + std::string(
                    old_cwd
                )
            );
        }
    }
}
