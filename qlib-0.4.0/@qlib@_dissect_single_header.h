/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef DISSECT_SINGLE_HEADER_INCLUDED
#define DISSECT_SINGLE_HEADER_INCLUDED

#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    template<
        typename string
    >
    void dissect_single_header(
        string const& header,
        string& key,
        string& value
    );
    
    template<
        typename string
    >
    void dissect_single_header(
        string const& header,
        string& key,
        string& value
    ) {
        typename string::size_type at_delimiter = header.find(
            ':'
        );
        if (
            string::npos == at_delimiter
        ) {
            throw component_error(
                "dissect_single_header"
            );
        }
        key = string(
            header.begin(
            ),
            header.begin(
            ) + at_delimiter
        );
        at_delimiter++;
        while (
            ' ' == header[
                at_delimiter
            ]
        ) {
            at_delimiter++;
        }
        value = string(
            header.begin(
            ) + at_delimiter,
            header.end(
            )
        );
    }
}

#endif
