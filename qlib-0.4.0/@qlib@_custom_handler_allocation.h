/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef CUSTOM_HANDLER_ALLOCATION_INCLUDED
#define CUSTOM_HANDLER_ALLOCATION_INCLUDED

#include <vector>
namespace @qlib@ {
    
    
    class custom_handler_allocation {
    public:
        template<
            typename handling
        >
        class allocation_handler;
        template<
            typename handling
        >
        static allocation_handler<
            handling
        > wrap(
            handling wrapped
        );
    };
    
    template<
        typename handling
    >
    void* asio_handler_allocate(
        std::size_t size,
        custom_handler_allocation::allocation_handler<
            handling
        >* this_handler
    );
    
    template<
        typename handling
    >
    void asio_handler_deallocate(
        void* deallocated,
        std::size_t size,
        custom_handler_allocation::allocation_handler<
            handling
        >* this_handler
    );
    
    template<
        typename handling
    >
    class custom_handler_allocation::allocation_handler {
    public:
        allocation_handler(
            handling handler
        );
        void operator(
        )(
        );
        template<
            typename passable1
        >
        void operator(
        )(
            passable1 argument1
        );
        template<
            typename passable1,
            typename passable2
        >
        void operator(
        )(
            passable1 argument1,
            passable2 argument2
        );
        template<
            typename passable1,
            typename passable2,
            typename passable3
        >
        void operator(
        )(
            passable1 argument1,
            passable2 argument2,
            passable3 argument3
        );
        friend void* asio_handler_allocate<
        >(
            std::size_t size,
            allocation_handler<
                handling
            >* this_handler
        );
        friend void asio_handler_deallocate<
        >(
            void* deallocated,
            std::size_t size,
            allocation_handler<
                handling
            >* this_handler
        );
        void* operator new(
            size_t size
        );
        void* operator new(
            size_t size,
            void* placement
        );
        void operator delete(
            void* deleted
        );
    private:
        static flushed_pointer_container<
            std::vector<
                growing_storage_pool*
            >
        > memory_supply;
        handling handler;
    };
    
    template<
        typename handling
    >
    custom_handler_allocation::allocation_handler<
        handling
    >::allocation_handler(
        handling handler
    ) :
    handler(
        handler
    ) {
    }
    
    template<
        typename handling
    >
    void custom_handler_allocation::allocation_handler<
        handling
    >::operator(
    )(
    ) {
        handler(
        );
    }
    
    template<
        typename handling
    >
    template<
        typename passable1
    >
    void custom_handler_allocation::allocation_handler<
        handling
    >::operator(
    )(
        passable1 argument1
    ) {
        handler(
            argument1
        );
    }
    
    template<
        typename handling
    >
    template<
        typename passable1,
        typename passable2
    >
    void custom_handler_allocation::allocation_handler<
        handling
    >::operator(
    )(
        passable1 argument1,
        passable2 argument2
    ) {
        handler(
            argument1,
            argument2
        );
    }
    
    template<
        typename handling
    >
    template<
        typename passable1,
        typename passable2,
        typename passable3
    >
    void custom_handler_allocation::allocation_handler<
        handling
    >::operator(
    )(
        passable1 argument1,
        passable2 argument2,
        passable3 argument3
    ) {
        handler(
            argument1,
            argument2,
            argument3
        );
    }
    
    template<
        typename handling
    >
    void* asio_handler_allocate(
        std::size_t size,
        custom_handler_allocation::allocation_handler<
            handling
        >* this_handler
    ) {
        if (
            size >= custom_handler_allocation::allocation_handler<
                handling
            >::memory_supply.size(
            )
        ) {
            custom_handler_allocation::allocation_handler<
                handling
            >::memory_supply.resize(
                size + 1,
                NULL
            );
        }
        growing_storage_pool*& applicable = custom_handler_allocation::allocation_handler<
            handling
        >::memory_supply[
            size
        ];
        if (
            NULL == applicable
        ) {
            applicable = new growing_storage_pool(
                size + sizeof(
                    growing_storage_pool*
                )
            );
        }
        growing_storage_pool** marking = static_cast<
            growing_storage_pool**
        >(
            applicable->get(
            )
        );
        *marking = applicable;
        return marking + 1;
    }
    
    template<
        typename handling
    >
    void asio_handler_deallocate(
        void* deallocated,
        std::size_t size,
        custom_handler_allocation::allocation_handler<
            handling
        >* this_handler
    ) {
        growing_storage_pool** data = static_cast<
            growing_storage_pool**
        >(
            deallocated
        );
        growing_storage_pool** applicable = data - 1;
        (
            *applicable
        )->put(
            applicable
        );
    }
    
    template<
        typename handling
    >
    void* custom_handler_allocation::allocation_handler<
        handling
    >::operator new(
        size_t size
    ) {
        if (
            size >= memory_supply.size(
            )
        ) {
            memory_supply.resize(
                size + 1,
                NULL
            );
        }
        growing_storage_pool*& applicable = memory_supply[
            size
        ];
        if (
            NULL == applicable
        ) {
            applicable = new growing_storage_pool(
                size + sizeof(
                    growing_storage_pool*
                )
            );
        }
        growing_storage_pool** marking = static_cast<
            growing_storage_pool**
        >(
            applicable->get(
            )
        );
        *marking = applicable;
        return marking + 1;
    }
    
    template<
        typename handling
    >
    void* custom_handler_allocation::allocation_handler<
        handling
    >::operator new(
        size_t size,
        void* placement
    ) {
        return placement;
    }
    
    template<
        typename handling
    >
    void custom_handler_allocation::allocation_handler<
        handling
    >::operator delete(
        void* deleted
    ) {
        growing_storage_pool** data = static_cast<
            growing_storage_pool**
        >(
            deleted
        );
        growing_storage_pool** applicable = data - 1;
        (
            *applicable
        )->put(
            applicable
        );
    }
    
    template<
        typename handling
    >
    custom_handler_allocation::allocation_handler<
        handling
    > custom_handler_allocation::wrap(
        handling wrapped
    ) {
        return custom_handler_allocation::allocation_handler<
            handling
        >(
            wrapped
        );
    }
    
    template<
        typename handling
    >
    flushed_pointer_container<
        std::vector<
            growing_storage_pool*
        >
    > custom_handler_allocation::allocation_handler<
        handling
    >::memory_supply;
}

#endif
