/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_fixed_point_number.h"
#include <sstream>
namespace @qlib@ {
    
    
    fixed_point_number::fixed_point_number(
        int initializer
    ) :
    value(
        static_cast<
            int64_t
        >(
            initializer
        ) * 1000
    ) {
    }
    
    fixed_point_number::fixed_point_number(
        long initializer
    ) :
    value(
        static_cast<
            int64_t
        >(
            initializer
        ) * 1000
    ) {
    }
    
    fixed_point_number::fixed_point_number(
        long long initializer
    ) :
    value(
        static_cast<
            int64_t
        >(
            initializer
        ) * 1000
    ) {
    }
    
    fixed_point_number::fixed_point_number(
        unsigned initializer
    ) :
    value(
        static_cast<
            int64_t
        >(
            initializer
        ) * 1000
    ) {
    }
    
    fixed_point_number::fixed_point_number(
        unsigned long initializer
    ) :
    value(
        static_cast<
            int64_t
        >(
            initializer
        ) * 1000
    ) {
    }
    
    fixed_point_number::fixed_point_number(
        unsigned long long initializer
    ) :
    value(
        static_cast<
            int64_t
        >(
            initializer
        ) * 1000
    ) {
    }
    
    fixed_point_number fixed_point_number::operator+(
        fixed_point_number const& added
    ) const {
        fixed_point_number result = *this;
        result.value += added.value;
        return result;
    }
    
    fixed_point_number& fixed_point_number::operator+=(
        fixed_point_number const& added
    ) {
        value += added.value;
        return *this;
    }
    
    fixed_point_number fixed_point_number::operator-(
        fixed_point_number const& subtracted
    ) const {
        fixed_point_number result = *this;
        result.value -= subtracted.value;
        return result;
    }
    
    fixed_point_number& fixed_point_number::operator-=(
        fixed_point_number const& subtracted
    ) {
        value += subtracted.value;
        return *this;
    }
    
    fixed_point_number fixed_point_number::operator*(
        fixed_point_number const& multiplied
    ) const {
        fixed_point_number result = *this;
        result.value *= multiplied.value;
        result.value /= 1000;
        return result;
    }
    
    fixed_point_number& fixed_point_number::operator*=(
        fixed_point_number const& multiplied
    ) {
        value *= multiplied.value;
        value /= 1000;
        return *this;
    }
    
    fixed_point_number fixed_point_number::operator/(
        fixed_point_number const& divisor
    ) const {
        fixed_point_number result = *this;
        result.value *= 1000;
        result.value /= divisor.value;
        return result;
    }
    
    fixed_point_number& fixed_point_number::operator/=(
        fixed_point_number const& divisor
    ) {
        value *= 1000;
        value /= divisor.value;
        return *this;
    }
    
    fixed_point_number::operator int(
    ) const {
        return value / 1000;
    }
    
    fixed_point_number::operator long(
    ) const {
        return value / 1000;
    }
    
    fixed_point_number::operator long long(
    ) const {
        return value / 1000;
    }
    
    std::string fixed_point_number::to_string(
    ) const {
        std::stringstream preparing;
        preparing << value;
        std::string raw = preparing.str(
        );
        while (
            4 > raw.size(
            )
        ) {
            raw = "0" + raw;
        }
        raw = std::string(
            raw.begin(
            ),
            raw.end(
            ) - 3
        ) + "." + std::string(
            raw.end(
            ) - 3,
            raw.end(
            )
        );
        while (
            '0' == raw[
                raw.size(
                ) - 1
            ]
        ) {
            raw = std::string(
                raw.begin(
                ),
                raw.end(
                ) - 1
            );
        }
        if (
            '.' == raw[
                raw.size(
                ) - 1
            ]
        ) {
            raw = std::string(
                raw.begin(
                ),
                raw.end(
                ) - 1
            );
        }
        return raw;
    }
    
    std::ostream& operator<<(
        std::ostream& destination,
        fixed_point_number const& printed
    ) {
        return destination << printed.to_string(
        );
    }
}
