/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_netlink_socket_message.h"
#include <cstring>
namespace @qlib@ {
    
    
    netlink_socket_message::netlink_socket_message(
        netlink_message& payload
    ) {
        memset(
            &kernel,
            0,
            sizeof(
                kernel
            )
        );
        kernel.nl_family = AF_NETLINK;
        io_vector.iov_base = payload.get_data(
        );
        io_vector.iov_len = payload.get_data(
        )->nlmsg_len;
        memset(
            &data,
            0,
            sizeof(
                data
            )
        );
        data.msg_iov = &io_vector;
        data.msg_iovlen = 1;
        data.msg_name = &kernel;
        data.msg_namelen = sizeof(
            kernel
        );
    }
    
    struct msghdr const* netlink_socket_message::get_data(
    ) const {
        return &data;
    }
}
