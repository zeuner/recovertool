/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef MEMORY_MAPPED_RECEIVE_SOCKET_INCLUDED
#define MEMORY_MAPPED_RECEIVE_SOCKET_INCLUDED

#include <string>
#include "@qlib@_component_error.h"
#include <boost/bind.hpp>
#include <boost/asio/placeholders.hpp>
namespace @qlib@ {
    
    
    class memory_mapped_receive_socket {
    public:
        static inline boost::arg<
            1
        > data_argument(
        );
        static inline boost::arg<
            2
        > size_argument(
        );
        static inline boost::arg<
            3
        > truncated_argument(
        );
        static inline boost::arg<
            4
        > dropped_argument(
        );
        static inline boost::arg<
            5
        > original_size_argument(
        );
        static inline boost::arg<
            6
        > seconds_argument(
        );
        static inline boost::arg<
            7
        > microseconds_argument(
        );
        static inline boost::arg<
            8
        > reuse_argument(
        );
        static inline boost::arg<
            2
        > frames_argument(
        );
        memory_mapped_receive_socket(
            boost::asio::io_service& communicating,
            std::string const& interface,
            unsigned block_size,
            unsigned block_count,
            unsigned frame_size,
            unsigned frame_count,
            uint16_t received_protocol = ETH_P_IP,
            bool promiscuous = false
        );
        ~memory_mapped_receive_socket(
        );
        bool flush(
        );
        template<
            typename packet_processing,
            typename poll_processing
        >
        void async_receive_packets(
            packet_processing packet_processor,
            poll_processing poll_processor
        );
        template<
            typename packet_processing,
            typename poll_processing
        >
        void async_receive_async_packets(
            packet_processing packet_processor,
            poll_processing poll_processor
        );
        void reuse_frame(
            unsigned char* frame
        );
        bool can_poll_asynchronously(
        ) const;
        boost::asio::io_service& get_io_service(
        );
        boost::asio::basic_raw_socket<
            packet_raw_protocol
        >& get_handle(
        );
        size_t get_snapshot_length(
        ) const;
    private:
        struct tpacket_req buffer_request;
        unsigned char* mapped_bytes;
        boost::asio::basic_raw_socket<
            packet_raw_protocol
        >* handle;
        simple_limited_bag<
            unsigned char*
        > usable_frames;
        bool const promiscuous;
        std::string const interface;
    };
    
    boost::arg<
        1
    > memory_mapped_receive_socket::data_argument(
    ) {
        return boost::arg<
            1
        >(
        );
    }
    
    boost::arg<
        2
    > memory_mapped_receive_socket::size_argument(
    ) {
        return boost::arg<
            2
        >(
        );
    }
    
    boost::arg<
        3
    > memory_mapped_receive_socket::truncated_argument(
    ) {
        return boost::arg<
            3
        >(
        );
    }
    
    boost::arg<
        4
    > memory_mapped_receive_socket::dropped_argument(
    ) {
        return boost::arg<
            4
        >(
        );
    }
    
    boost::arg<
        5
    > memory_mapped_receive_socket::original_size_argument(
    ) {
        return boost::arg<
            5
        >(
        );
    }
    
    boost::arg<
        6
    > memory_mapped_receive_socket::seconds_argument(
    ) {
        return boost::arg<
            6
        >(
        );
    }
    
    boost::arg<
        7
    > memory_mapped_receive_socket::microseconds_argument(
    ) {
        return boost::arg<
            7
        >(
        );
    }
    
    boost::arg<
        8
    > memory_mapped_receive_socket::reuse_argument(
    ) {
        return boost::arg<
            8
        >(
        );
    }
    
    boost::arg<
        2
    > memory_mapped_receive_socket::frames_argument(
    ) {
        return boost::arg<
            2
        >(
        );
    }
    
    template<
        typename packet_processing,
        typename poll_processing
    >
    void memory_mapped_receive_socket::async_receive_packets(
        packet_processing packet_processor,
        poll_processing poll_processor
    ) {
        unsigned char* const frames_end = mapped_bytes + (
            buffer_request.tp_frame_size * buffer_request.tp_frame_nr
        );
        int found = 0;
        for (
            unsigned char* current_frame = mapped_bytes;
            frames_end > current_frame;
            current_frame += buffer_request.tp_frame_size
        ) {
            struct tpacket_hdr* header = reinterpret_cast<
                struct tpacket_hdr*
            >(
                current_frame
            );
            if (
                TP_STATUS_USER & header->tp_status
            ) {
                found++;
                packet_processor(
                    reinterpret_cast<
                        char*
                    >(
                        current_frame + header->tp_mac
                    ),
                    header->tp_len,
                    TP_STATUS_COPY & header->tp_status,
                    TP_STATUS_LOSING & header->tp_status,
                    header->tp_snaplen,
                    header->tp_sec,
                    header->tp_usec
                );
                header->tp_status = TP_STATUS_KERNEL;
            }
        }
        if (
            0 < found
        ) {
            handle->get_io_service(
            ).post(
                boost::bind(
                    poll_processor,
                    boost::system::error_code(
                    ),
                    found
                )
            );
        } else {
            handle->async_receive(
                boost::asio::null_buffers(
                ),
                boost::bind(
                    poll_processor,
                    boost::asio::placeholders::error,
                    found
                )
            );
        }
    }
    
    template<
        typename packet_processing,
        typename poll_processing
    >
    void memory_mapped_receive_socket::async_receive_async_packets(
        packet_processing packet_processor,
        poll_processing poll_processor
    ) {
        if (
            usable_frames.empty(
            )
        ) {
            throw component_error(
                "memory_mapped_receive_socket"
            );
        }
        int found = 0;
        for (
            simple_limited_bag<
                unsigned char*
            >::iterator current_frame = usable_frames.begin(
            );
            usable_frames.end(
            ) != current_frame;
            ++current_frame
        ) {
            unsigned char* current_frame_position = *current_frame;
            struct tpacket_hdr* header = reinterpret_cast<
                struct tpacket_hdr*
            >(
                current_frame_position
            );
            if (
                TP_STATUS_USER & header->tp_status
            ) {
                found++;
                usable_frames.erase(
                    current_frame
                );
                packet_processor(
                    reinterpret_cast<
                        char*
                    >(
                        current_frame_position + header->tp_mac
                    ),
                    header->tp_len,
                    TP_STATUS_COPY & header->tp_status,
                    TP_STATUS_LOSING & header->tp_status,
                    header->tp_snaplen,
                    header->tp_sec,
                    header->tp_usec,
                    current_frame_position
                );
            }
        }
        if (
            0 < found
        ) {
            handle->get_io_service(
            ).post(
                boost::bind(
                    poll_processor,
                    boost::system::error_code(
                    ),
                    found
                )
            );
        } else {
            handle->async_receive(
                boost::asio::null_buffers(
                ),
                boost::bind(
                    poll_processor,
                    boost::asio::placeholders::error,
                    found
                )
            );
        }
    }
}

#endif
