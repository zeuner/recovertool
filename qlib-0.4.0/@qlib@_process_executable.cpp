/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_process_executable.h"
#include "@qlib@_stringified.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    std::string process_executable(
    ) {
        pid_t const process_pid = getpid(
        );
        char buffer[
            0x400
        ];
        int const characters = readlink(
            (
                "/proc/" + stringified(
                    process_pid
                ) + "/exe"
            ).c_str(
            ),
            buffer,
            sizeof(
                buffer
            )
        );
        if (
            0 > characters
        ) {
            throw component_error(
                "process_executable"
            );
        }
        if (
            sizeof(
                buffer
            ) == characters
        ) {
            throw component_error(
                "process_executable"
            );
        }
        return std::string(
            buffer,
            characters
        );
    }
}
