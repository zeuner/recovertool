/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_smtp_socket.h"
#include <boost/fusion/sequence/intrinsic/at_c.hpp>
#include <boost/fusion/include/at_c.hpp>
#include <boost/asio/placeholders.hpp>
#include <boost/bind.hpp>
#include <boost/asio/write.hpp>
namespace @qlib@ {
    
    
    smtp_socket::smtp_socket(
        boost::asio::io_service& io_service,
        std::string const& hostname
    ) :
    boost::asio::ip::tcp::socket(
        io_service
    ),
    idle(
        false
    ),
    hostname(
        hostname
    ) {
    }
    
    smtp_socket::smtp_socket(
        boost::asio::io_service& io_service,
        std::string const& hostname,
        boost::asio::ip::tcp::endpoint const& local_endpoint
    ) :
    boost::asio::ip::tcp::socket(
        io_service,
        local_endpoint
    ),
    idle(
        false
    ),
    hostname(
        hostname
    ) {
    }
    
    smtp_socket::~smtp_socket(
    ) {
        while (
            !pending_sends.empty(
            )
        ) {
            get_io_service(
            ).post(
                boost::bind(
                    boost::fusion::at_c<
                        3
                    >(
                        pending_sends.front(
                        )
                    ),
                    true
                )
            );
            pending_sends.pop_front(
            );
        }
    }
    
    void smtp_socket::async_connect(
        boost::asio::ip::address_v4 const& destination,
        boost::function<
            void(
            )
        > const& disconnect_handler
    ) {
        this->disconnect_handler.reset(
            new boost::function<
                void(
                )
            >(
                disconnect_handler
            )
        );
        boost::asio::ip::tcp::endpoint endpoint(
            destination,
            25
        );
        boost::asio::ip::tcp::socket::async_connect(
            endpoint,
            boost::bind(
                &smtp_socket::handle_connect,
                this,
                this->disconnect_handler,
                boost::asio::placeholders::error
            )
        );
    }
    
    void smtp_socket::async_send(
        std::string const& envelope_from,
        std::string const& envelope_to,
        std::string const& data,
        boost::function<
            void(
                bool
            )
        > result_handler
    ) {
        pending_sends.push_back(
            sending_record(
                envelope_from,
                envelope_to,
                data,
                result_handler
            )
        );
        if (
            idle
        ) {
            idle = false;
            async_process_send(
            );
        }
    }
    
    bool smtp_socket::get_idle(
    ) const {
        return idle;
    }
    
    void smtp_socket::connected(
        boost::shared_ptr<
            boost::function<
                void(
                )
            >
        > disconnect_handler
    ) {
        async_read_some(
            boost::asio::buffer(
                reading_data,
                input_size
            ),
            boost::bind(
                &smtp_socket::handle_greeting_read,
                this,
                disconnect_handler,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
    
    void smtp_socket::handle_greeting_read(
        boost::shared_ptr<
            boost::function<
                void(
                )
            >
        > disconnect_handler,
        boost::system::error_code const& error,
        size_t transferred
    ) {
        if (
            boost::asio::error::operation_aborted == error
        ) { 
            return;
        } else if (
            error
        ) {
            (
                *disconnect_handler
            )(
            );
            return;
        }
        input += std::string(
            reading_data,
            transferred
        );
        std::string::size_type const line_end = input.find(
            '\n'
        );
        if (
            std::string::npos == line_end
        ) {
            async_read_some(
                boost::asio::buffer(
                    reading_data,
                    input_size
                ),
                boost::bind(
                    &smtp_socket::handle_greeting_read,
                    this,
                    disconnect_handler,
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred
                )
            );
            return;
        }
        if (
            !matches_prefix(
                "220",
                input
            )
        ) {
            (
                *disconnect_handler
            )(
            );
            return;
        }
        input = std::string(
            input.begin(
            ) + line_end + 1,
            input.end(
            )
        );
        writing_data += "HELO " + hostname + "\r\n";
        boost::asio::async_write(
            *this,
            boost::asio::buffer(
                writing_data.data(
                ),
                writing_data.size(
                )
            ),
            boost::bind(
                &smtp_socket::handle_greeting_write,
                this,
                disconnect_handler,
                boost::asio::placeholders::error
            )
        );
    }
    
    void smtp_socket::handle_greeting_write(
        boost::shared_ptr<
            boost::function<
                void(
                )
            >
        > disconnect_handler,
        boost::system::error_code const& error
    ) {
        if (
            boost::asio::error::operation_aborted == error
        ) { 
            return;
        } else if (
            error
        ) {
            (
                *disconnect_handler
            )(
            );
            return;
        }
        writing_data.clear(
        );
        async_read_some(
            boost::asio::buffer(
                reading_data,
                input_size
            ),
            boost::bind(
                &smtp_socket::handle_greeting_response_read,
                this,
                disconnect_handler,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
    
    void smtp_socket::handle_data_command_write(
        boost::shared_ptr<
            boost::function<
                void(
                )
            >
        > disconnect_handler,
        boost::system::error_code const& error
    ) {
        if (
            boost::asio::error::operation_aborted == error
        ) { 
            return;
        } else if (
            error
        ) {
            (
                *disconnect_handler
            )(
            );
            return;
        }
        writing_data.clear(
        );
        async_read_some(
            boost::asio::buffer(
                reading_data,
                input_size
            ),
            boost::bind(
                &smtp_socket::handle_data_command_response_read,
                this,
                disconnect_handler,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
    
    void smtp_socket::handle_data_command_response_read(
        boost::shared_ptr<
            boost::function<
                void(
                )
            >
        > disconnect_handler,
        boost::system::error_code const& error,
        size_t transferred
    ) {
        if (
            boost::asio::error::operation_aborted == error
        ) { 
            return;
        } else if (
            error
        ) {
            (
                *disconnect_handler
            )(
            );
            return;
        }
        input += std::string(
            reading_data,
            transferred
        );
        std::string::size_type const line_end = input.find(
            '\n'
        );
        if (
            std::string::npos == line_end
        ) {
            async_read_some(
                boost::asio::buffer(
                    reading_data,
                    input_size
                ),
                boost::bind(
                    &smtp_socket::handle_data_command_response_read,
                    this,
                    disconnect_handler,
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred
                )
            );
            return;
        }
        if (
            !matches_prefix(
                "354",
                input
            )
        ) {
            (
                *disconnect_handler
            )(
            );
            return;
        }
        input = std::string(
            input.begin(
            ) + line_end + 1,
            input.end(
            )
        );
        writing_data += boost::fusion::at_c<
            2
        >(
            pending_sends.front(
            )
        ) + ""
        ".\r\n";
        boost::asio::async_write(
            *this,
            boost::asio::buffer(
                writing_data.data(
                ),
                writing_data.size(
                )
            ),
            boost::bind(
                &smtp_socket::handle_body_write,
                this,
                disconnect_handler,
                boost::asio::placeholders::error
            )
        );
    }
    
    void smtp_socket::handle_to_write(
        boost::shared_ptr<
            boost::function<
                void(
                )
            >
        > disconnect_handler,
        boost::system::error_code const& error
    ) {
        if (
            boost::asio::error::operation_aborted == error
        ) { 
            return;
        } else if (
            error
        ) {
            (
                *disconnect_handler
            )(
            );
            return;
        }
        writing_data.clear(
        );
        async_read_some(
            boost::asio::buffer(
                reading_data,
                input_size
            ),
            boost::bind(
                &smtp_socket::handle_to_response_read,
                this,
                disconnect_handler,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
    
    void smtp_socket::handle_to_response_read(
        boost::shared_ptr<
            boost::function<
                void(
                )
            >
        > disconnect_handler,
        boost::system::error_code const& error,
        size_t transferred
    ) {
        if (
            boost::asio::error::operation_aborted == error
        ) { 
            return;
        } else if (
            error
        ) {
            (
                *disconnect_handler
            )(
            );
            return;
        }
        input += std::string(
            reading_data,
            transferred
        );
        std::string::size_type const line_end = input.find(
            '\n'
        );
        if (
            std::string::npos == line_end
        ) {
            async_read_some(
                boost::asio::buffer(
                    reading_data,
                    input_size
                ),
                boost::bind(
                    &smtp_socket::handle_to_response_read,
                    this,
                    disconnect_handler,
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred
                )
            );
            return;
        }
        if (
            !matches_prefix(
                "250",
                input
            )
        ) {
            (
                *disconnect_handler
            )(
            );
            return;
        }
        input = std::string(
            input.begin(
            ) + line_end + 1,
            input.end(
            )
        );
        writing_data += "DATA\r\n";
        boost::asio::async_write(
            *this,
            boost::asio::buffer(
                writing_data.data(
                ),
                writing_data.size(
                )
            ),
            boost::bind(
                &smtp_socket::handle_data_command_write,
                this,
                disconnect_handler,
                boost::asio::placeholders::error
            )
        );
    }
    
    void smtp_socket::handle_from_write(
        boost::shared_ptr<
            boost::function<
                void(
                )
            >
        > disconnect_handler,
        boost::system::error_code const& error
    ) {
        if (
            boost::asio::error::operation_aborted == error
        ) { 
            return;
        } else if (
            error
        ) {
            (
                *disconnect_handler
            )(
            );
            return;
        }
        writing_data.clear(
        );
        async_read_some(
            boost::asio::buffer(
                reading_data,
                input_size
            ),
            boost::bind(
                &smtp_socket::handle_from_response_read,
                this,
                disconnect_handler,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
    
    void smtp_socket::handle_from_response_read(
        boost::shared_ptr<
            boost::function<
                void(
                )
            >
        > disconnect_handler,
        boost::system::error_code const& error,
        size_t transferred
    ) {
        if (
            boost::asio::error::operation_aborted == error
        ) { 
            return;
        } else if (
            error
        ) {
            (
                *disconnect_handler
            )(
            );
            return;
        }
        input += std::string(
            reading_data,
            transferred
        );
        std::string::size_type const line_end = input.find(
            '\n'
        );
        if (
            std::string::npos == line_end
        ) {
            async_read_some(
                boost::asio::buffer(
                    reading_data,
                    input_size
                ),
                boost::bind(
                    &smtp_socket::handle_from_response_read,
                    this,
                    disconnect_handler,
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred
                )
            );
            return;
        }
        if (
            !matches_prefix(
                "250",
                input
            )
        ) {
            (
                *disconnect_handler
            )(
            );
            return;
        }
        input = std::string(
            input.begin(
            ) + line_end + 1,
            input.end(
            )
        );
        writing_data += "RCPT TO:<" + boost::fusion::at_c<
            1
        >(
            pending_sends.front(
            )
        ) + ">\r\n";
        boost::asio::async_write(
            *this,
            boost::asio::buffer(
                writing_data.data(
                ),
                writing_data.size(
                )
            ),
            boost::bind(
                &smtp_socket::handle_to_write,
                this,
                disconnect_handler,
                boost::asio::placeholders::error
            )
        );
    }
    
    void smtp_socket::handle_body_write(
        boost::shared_ptr<
            boost::function<
                void(
                )
            >
        > disconnect_handler,
        boost::system::error_code const& error
    ) {
        if (
            boost::asio::error::operation_aborted == error
        ) { 
            return;
        } else if (
            error
        ) {
            (
                *disconnect_handler
            )(
            );
            return;
        }
        writing_data.clear(
        );
        async_read_some(
            boost::asio::buffer(
                reading_data,
                input_size
            ),
            boost::bind(
                &smtp_socket::handle_body_response_read,
                this,
                disconnect_handler,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
    
    void smtp_socket::handle_body_response_read(
        boost::shared_ptr<
            boost::function<
                void(
                )
            >
        > disconnect_handler,
        boost::system::error_code const& error,
        size_t transferred
    ) {
        if (
            boost::asio::error::operation_aborted == error
        ) { 
            return;
        } else if (
            error
        ) {
            (
                *disconnect_handler
            )(
            );
            return;
        }
        input += std::string(
            reading_data,
            transferred
        );
        std::string::size_type const line_end = input.find(
            '\n'
        );
        if (
            std::string::npos == line_end
        ) {
            async_read_some(
                boost::asio::buffer(
                    reading_data,
                    input_size
                ),
                boost::bind(
                    &smtp_socket::handle_body_response_read,
                    this,
                    disconnect_handler,
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred
                )
            );
            return;
        }
        if (
            !matches_prefix(
                "250",
                input
            )
        ) {
            (
                *disconnect_handler
            )(
            );
            return;
        }
        input = std::string(
            input.begin(
            ) + line_end + 1,
            input.end(
            )
        );
        get_io_service(
        ).post(
            boost::bind(
                boost::fusion::at_c<
                    3
                >(
                    pending_sends.front(
                    )
                ),
                false
            )
        );
        pending_sends.pop_front(
        );
        if (
            pending_sends.empty(
            )
        ) {
            idle = true;
            return;
        }
        async_process_send(
        );
    }
    
    void smtp_socket::handle_greeting_response_read(
        boost::shared_ptr<
            boost::function<
                void(
                )
            >
        > disconnect_handler,
        boost::system::error_code const& error,
        size_t transferred
    ) {
        if (
            boost::asio::error::operation_aborted == error
        ) { 
            return;
        } else if (
            error
        ) {
            (
                *disconnect_handler
            )(
            );
            return;
        }
        input += std::string(
            reading_data,
            transferred
        );
        std::string::size_type const line_end = input.find(
            '\n'
        );
        if (
            std::string::npos == line_end
        ) {
            async_read_some(
                boost::asio::buffer(
                    reading_data,
                    input_size
                ),
                boost::bind(
                    &smtp_socket::handle_greeting_response_read,
                    this,
                    disconnect_handler,
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred
                )
            );
            return;
        }
        if (
            !matches_prefix(
                "250",
                input
            )
        ) {
            (
                *disconnect_handler
            )(
            );
            return;
        }
        input = std::string(
            input.begin(
            ) + line_end + 1,
            input.end(
            )
        );
        if (
            pending_sends.empty(
            )
        ) {
            idle = true;
            return;
        }
        async_process_send(
        );
    }
    
    void smtp_socket::async_process_send(
    ) {
        writing_data += "MAIL FROM:<" + boost::fusion::at_c<
            0
        >(
            pending_sends.front(
            )
        ) + ">\r\n";
        boost::asio::async_write(
            *this,
            boost::asio::buffer(
                writing_data.data(
                ),
                writing_data.size(
                )
            ),
            boost::bind(
                &smtp_socket::handle_from_write,
                this,
                disconnect_handler,
                boost::asio::placeholders::error
            )
        );
    }
    
    void smtp_socket::handle_connect(
        boost::shared_ptr<
            boost::function<
                void(
                )
            >
        > disconnect_handler,
        boost::system::error_code const& error
    ) {
        if (
            boost::asio::error::operation_aborted == error
        ) { 
            return;
        } else if (
            error
        ) {
            (
                *disconnect_handler
            )(
            );
            return;
        }
        connected(
            disconnect_handler
        );
    }
}
