/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef FLAT_OSCILLATION_SOURCE_INCLUDED
#define FLAT_OSCILLATION_SOURCE_INCLUDED

#include <map>
namespace @qlib@ {
    
    
    class flat_oscillation_source :
    public specialized_source<
        frequency_controlled_oscillation_source<
            amplitude_value,
            envelope_source<
                time_value
            >
        >
    > {
    public:
        flat_oscillation_source(
            std::map<
                time_value,
                time_value
            > frequency_envelope_points,
            time_value rate,
            time_value duration,
            time_value (
                *waveform
            )(
                time_value phase
            ) = &sin
        );
    private:
        typedef envelope_source<
            time_value
        > frequency_envelope_source_type;
        frequency_envelope_source_type frequency_envelope_source;
        typedef frequency_controlled_oscillation_source<
            amplitude_value,
            frequency_envelope_source_type
        > oscillating_type;
    };
}

#endif
