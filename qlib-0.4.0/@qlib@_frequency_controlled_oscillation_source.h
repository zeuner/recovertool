/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef FREQUENCY_CONTROLLED_OSCILLATION_SOURCE_INCLUDED
#define FREQUENCY_CONTROLLED_OSCILLATION_SOURCE_INCLUDED

#include <boost/noncopyable.hpp>
namespace @qlib@ {
    
    
    template<
        typename value_type,
        typename controlling,
        typename waveform_type = time_value
    >
    class frequency_controlled_oscillation_source;
    
    template<
        typename value_type,
        typename controlling,
        typename waveform_type
    >
    class frequency_controlled_oscillation_source :
    private boost::noncopyable {
    public:
        frequency_controlled_oscillation_source(
            controlling& frequency_source,
            time_value rate,
            waveform_type (
                *waveform
            )(
                time_value phase
            ) = &sin
        );
        size_t get_samples(
            value_type* first,
            value_type* last
        );
        bool is_master(
        ) const;
        bool get_sample(
            value_type& retrieved
        );
    private:
        controlling& frequency_source;
        time_value const pi;
        time_value const real_rate;
        time_value const speed;
        time_value integrated_frequency;
        waveform_type (
            *waveform
        )(
            time_value phase
        );
        time_value last_frequency;
        time_value last_increment;
    };
    
    template<
        typename value_type,
        typename controlling,
        typename waveform_type
    >
    class bulk_audio_retrieval<
        frequency_controlled_oscillation_source<
            value_type,
            controlling,
            waveform_type
        >,
        value_type
    > {
    public:
        static size_t get_samples(
            frequency_controlled_oscillation_source<
                value_type,
                controlling,
                waveform_type
            >& source,
            value_type* first,
            value_type* last
        );
    };
    
    template<
        typename value_type,
        typename controlling,
        typename waveform_type
    >
    frequency_controlled_oscillation_source<
        value_type,
        controlling,
        waveform_type
    >::frequency_controlled_oscillation_source(
        controlling& frequency_source,
        time_value rate,
        waveform_type (
            *waveform
        )(
            time_value phase
        )
    ) :
    frequency_source(
        frequency_source
    ),
    pi(
        4.0 * std::atan(
            1.0
        )
    ),
    real_rate(
        rate
    ),
    speed(
        real_number(
            2 
        ) * pi / real_rate
    ),
    integrated_frequency(
        0
    ),
    waveform(
        waveform
    ),
    last_frequency(
        0
    ),
    last_increment(
        0
    ) {
    }
    
    template<
        typename value_type,
        typename controlling,
        typename waveform_type
    >
    size_t frequency_controlled_oscillation_source<
        value_type,
        controlling,
        waveform_type
    >::get_samples(
        value_type* first,
        value_type* last
    ) {
        size_t const required_frequencies = last - first;
        time_value current_frequencies[
            required_frequencies
        ];
        size_t const available = bulk_audio_retrieval<
            controlling,
            time_value
        >::get_samples(
            frequency_source,
            current_frequencies,
            current_frequencies + required_frequencies
        );
        if (
            0 == available
        ) {
            return 0;
        }
        time_value current_increments[
            available
        ];
        for (
            int scaling = 0;
            available > scaling;
            scaling++
        ) {
            time_value current_frequency = current_frequencies[
                scaling
            ];
            if (
                last_frequency != current_frequency
            ) {
                last_frequency = current_frequency;
                last_increment = current_frequency * speed;
            }
            current_increments[
                scaling
            ] = last_increment;
        }
        time_value integrated_frequencies[
            available + 1
        ];
        integrated_frequencies[
            0
        ] = integrated_frequency;
        for (
            int summing = 0;
            available > summing;
            summing++
        ) {
            integrated_frequencies[
                summing + 1
            ] = current_increments[
                summing
            ] + integrated_frequencies[
                summing
            ];
        }
        integrated_frequency = integrated_frequencies[
            available
        ];
        std::transform(
            integrated_frequencies,
            integrated_frequencies + available,
            first,
            *waveform
        );
        return available;
    }
    
    template<
        typename value_type,
        typename controlling,
        typename waveform_type
    >
    bool frequency_controlled_oscillation_source<
        value_type,
        controlling,
        waveform_type
    >::is_master(
    ) const {
        return true;
    }
    
    template<
        typename value_type,
        typename controlling,
        typename waveform_type
    >
    bool frequency_controlled_oscillation_source<
        value_type,
        controlling,
        waveform_type
    >::get_sample(
        value_type& retrieved
    ) {
        retrieved = (
            *waveform
        )(
            integrated_frequency
        );
        time_value current_frequency;
        if (
            !frequency_source.get_sample(
                current_frequency
            )
        ) {
            return false;
        }
        if (
            last_frequency != current_frequency
        ) {
            last_frequency = current_frequency;
            last_increment = current_frequency * speed;
        }
        integrated_frequency += last_increment;
        return true;
    }
    
    template<
        typename value_type,
        typename controlling,
        typename waveform_type
    >
    size_t bulk_audio_retrieval<
        frequency_controlled_oscillation_source<
            value_type,
            controlling,
            waveform_type
        >,
        value_type
    >::get_samples(
        frequency_controlled_oscillation_source<
            value_type,
            controlling,
            waveform_type
        >& source,
        value_type* first,
        value_type* last
    ) {
        return source.get_samples(
            first,
            last
        );
    }
}

#endif
