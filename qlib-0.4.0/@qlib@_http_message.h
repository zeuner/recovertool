/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef HTTP_MESSAGE_INCLUDED
#define HTTP_MESSAGE_INCLUDED

#include <string>
namespace @qlib@ {
    
    
    class http_message {
    public:
        http_message(
            std::string const& raw
        );
        std::string const& get_headers(
        ) const;
        std::string const& get_body(
        ) const;
        static bool valid_message(
            std::string const& raw
        );
    private:
        std::string headers;
        std::string body;
        static std::string const body_delimiter;
    };
}

#endif
