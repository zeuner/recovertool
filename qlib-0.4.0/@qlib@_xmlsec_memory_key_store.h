/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef XMLSEC_MEMORY_KEY_STORE_INCLUDED
#define XMLSEC_MEMORY_KEY_STORE_INCLUDED

#include <boost/shared_ptr.hpp>
#include <map>
namespace @qlib@ {
    
    
    class xmlsec_memory_key_store {
    public:
        void add_key(
            xmlSecKeyPtr added
        );
        bool has_key(
            xml_string const& name
        ) const;
        xmlSecKeyPtr get_key(
            xml_string const& name,
            xmlSecKeyInfoCtxPtr key_info_context
        ) const;
    private:
        std::map<
            xml_string,
            boost::shared_ptr<
                xmlsec_key_wrapper
            >
        > keys;
    };
}

#endif
