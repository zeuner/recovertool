/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_fill_schema.h"
namespace @qlib@ {
    
    
    void fill_schema(
        std::map<
            std::string,
            std::string
        > const& tables,
        database_connection& database
    ) {
        std::map<
            std::string,
            std::string
        > missing_tables = tables;
        do {
            std::auto_ptr<
                database_result
            > querying(
                database.get_tables_query(
                )
            );
            database_result& tables = *querying;
            for (
                database_result::row_type current = tables.fetch_row(
                );
                current;
                current = tables.fetch_row(
                )
            ) {
                missing_tables.erase(
                    current[
                        0
                    ]
                );
            }
        } while (
            false
        );
        for (
            std::map<
                std::string,
                std::string
            >::const_iterator created = missing_tables.begin(
            );
            missing_tables.end(
            ) != created;
            created++
        ) {
            database_push(
                database,
                created->second
            );
        }
    }
}
