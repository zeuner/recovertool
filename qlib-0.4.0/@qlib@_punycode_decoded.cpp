/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_punycode_decoded.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    std::string punycode_decoded(
        std::string const& raw
    ) {
        std::string::size_type const delimiter = raw.rfind(
            '-'
        );
        std::wstring decoded;
        std::string encoded;
        if (
            std::string::npos == delimiter
        ) {
            encoded = raw;
        } else {
            decoded = std::wstring(
                raw.begin(
                ),
                raw.begin(
                ) + delimiter
            );
            encoded = std::string(
                raw.begin(
                ) + delimiter + 1,
                raw.end(
                )
            );
        }
        int n = 0x80;
        int i = 0;
        int bias = 72;
        while (
            !encoded.empty(
            )
        ) {
            int const old_i = i;
            int w = 1;
            static int const base = 36;
            int k = base;
            static int const t_min = 1;
            static int const t_max = 26;
            do {
                if (
                    encoded.empty(
                    )
                ) {
                    throw component_error(
                        "punycode_decoded"
                    );
                }
                char const consumed = encoded[
                    0
                ];
                encoded = std::string(
                    encoded.begin(
                    ) + 1,
                    encoded.end(
                    )
                );
                int digit;
                if (
                    (
                        'A' <= consumed
                    ) && (
                        'Z' >= consumed
                    )
                ) {
                    digit = consumed - 'A';
                } else if (
                    (
                        'a' <= consumed
                    ) && (
                        'z' >= consumed
                    )
                ) { 
                    digit = consumed - 'a';
                } else if (
                    (
                        '0' <= consumed
                    ) && (
                        '9' >= consumed
                    )
                ) {
                    digit = consumed - '0' + 26;
                } else {
                    throw component_error(
                        "punycode_decoded"
                    );
                }
                i += digit * w;
                int t;
                if (
                    k <= (
                        bias + t_min
                    )
                ) {
                    t = t_min;
                } else if (
                    k >= (
                        bias + t_max
                    )
                ) {
                    t = t_max;
                } else {
                    t = k - bias;
                }
                if (
                    digit < t
                ) {
                    break;
                }
                w *= base - t;
                k += base;
            } while (
                true
            );
            int delta = i - old_i;
            if (
                0 == old_i
            ) {
                delta /= 700;
            } else {
                delta /= 2;
            }
            delta += delta / (
                decoded.size(
                ) + 1
            );
            k = 0;
            while (
                delta > (
                    (
                        (
                            base - t_min
                        ) * t_max
                    ) / 2
                )
            ) {
                delta /= base - t_min;
                k += base;
            }
            bias = k + (
                (
                    (
                        base - t_min + 1
                    ) * delta
                ) / (
                    delta + 38
                )
            );
            n += i / (
                decoded.size(
                ) + 1
            );
            i %= decoded.size(
            ) + 1;
            decoded.insert(
                decoded.begin(
                ) + i,
                1,
                n
            );
            i++;
        }
        return from_wide_string(
            decoded,
            "utf-8"
        );
    }
}
