/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_check_xml_node.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    void check_xml_node(
        xmlNodePtr checked,
        xml_string const& name,
        xml_string const& name_space
    ) {
        if (
            !correct_xml_node(
                checked,
                name,
                name_space
            )
        ) {
            throw component_error(
                "check_xml_node"
            );
        }
    }
}
