/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef SOAP_ELEMENT_INCLUDED
#define SOAP_ELEMENT_INCLUDED

#include <string>
namespace @qlib@ {
    
    
    class soap_service;
    
    class soap_element :
    public soap_typed {
    public:
        soap_element(
            soap_service const& service,
            std::string const& element_name,
            std::string const& element_namespace
        );
        void dump_to_xml(
            xmlNodePtr augmented
        );
        void parse_xml(
            xmlNodePtr const parsed
        );
    private:
        std::string element_name;
        std::string element_namespace;
    };
}

#endif
