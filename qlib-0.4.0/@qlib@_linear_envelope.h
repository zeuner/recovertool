/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef LINEAR_ENVELOPE_INCLUDED
#define LINEAR_ENVELOPE_INCLUDED

#include <map>
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    template<
        typename value_type
    >
    class linear_envelope {
    public:
        linear_envelope(
            std::map<
                time_value,
                value_type
            > const& points
        );
        value_type get_value(
            time_value at
        );
    private:
        std::map<
            time_value,
            value_type
        > points;
        typename std::map<
            time_value,
            value_type
        >::const_iterator fewer;
        typename std::map<
            time_value,
            value_type
        >::const_iterator greater;
    };
    
    template<
        typename value_type
    >
    linear_envelope<
        value_type
    >::linear_envelope(
        std::map<
            time_value,
            value_type
        > const& points
    ) :
    points(
        points
    ),
    fewer(
        this->points.begin(
        )
    ),
    greater(
        this->points.begin(
        )
    ) {
    }
    
    template<
        typename value_type
    >
    value_type linear_envelope<
        value_type
    >::get_value(
        time_value at
    ) {
        if (
            (
                fewer->first > at
            ) || (
                greater->first < at
            )
        ) {
            greater = points.lower_bound(
                at
            );
            if (
                points.end(
                ) == greater
            ) {
                throw component_error(
                    "linear_envelope"
                );
            }
            if (
                at == greater->first
            ) {
                fewer == greater;
                return greater->second;
            }
            if (
                points.begin(
                ) == greater
            ) {
                throw component_error(
                    "linear_envelope"
                );
            }
            fewer = greater;
            fewer--;
        }
        if (
            at == greater->first
        ) {
            return greater->second;
        }
        return fewer->second + (
            greater->second - fewer->second
        ) * (
            at - fewer->first
        ) / (
            greater->first - fewer->first
        );
    }
}

#endif
