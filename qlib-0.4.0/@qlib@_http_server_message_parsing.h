/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef HTTP_SERVER_MESSAGE_PARSING_INCLUDED
#define HTTP_SERVER_MESSAGE_PARSING_INCLUDED

#include <string>
#include <boost/asio/ip/tcp.hpp>
#include <boost/bind.hpp>
#include <unistd.h>
#include <map>
namespace @qlib@ {
    
    
    template<
        typename transmitting = boost::asio::ip::tcp::socket,
        typename allocating = std::allocator<
            char
        >
    >
    class http_server_message_parsing;
    
    template<
        typename transmitting,
        typename allocating
    >
    class http_server_message_parsing {
    public:
        http_server_message_parsing(
        );
        bool operator(
        )(
            typename http_client_connection<
                transmitting,
                allocating
            >::response_type* response,
            typename http_client_connection<
                transmitting,
                allocating
            >::string& to_parse,
            boost::system::error_code& error
        );
    private:
        typename http_client_connection<
            transmitting,
            allocating
        >::string result_code;
        typename http_client_connection<
            transmitting,
            allocating
        >::string data;
        typename http_client_connection<
            transmitting,
            allocating
        >::string header_key;
        typename http_client_connection<
            transmitting,
            allocating
        >::string header_value;
        std::map<
            typename http_client_connection<
                transmitting,
                allocating
            >::string,
            typename http_client_connection<
                transmitting,
                allocating
            >::string
        > headers;
        enum http_client_connection<
            transmitting,
            allocating
        >::state_type state;
        typename http_client_connection<
            transmitting,
            allocating
        >::string::size_type data_required;
        std::string::const_iterator matched;
    };
    
    template<
        typename transmitting,
        typename allocating
    >
    http_server_message_parsing<
        transmitting,
        allocating
    >::http_server_message_parsing(
    ) :
    state(
        http_client_connection<
            transmitting,
            allocating
        >::reading_http_version_head
    ),
    data_required(
        0
    ),
    matched(
        http_client_connection<
            transmitting,
            allocating
        >::get_http_version_head(
        ).begin(
        )
    ) {
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    bool http_server_message_parsing<
        transmitting,
        allocating
    >::operator(
    )(
        typename http_client_connection<
            transmitting,
            allocating
        >::response_type* response,
        typename http_client_connection<
            transmitting,
            allocating
        >::string& to_parse,
        boost::system::error_code& error
    ) {
        http_stub_peer_handlers handlers;
        bool const result = http_client_connection<
            transmitting,
            allocating
        >::parse_http(
            response,
            to_parse,
            result_code,
            data,
            header_key,
            header_value,
            headers,
            boost::bind(
                &http_stub_peer_handlers::close_link,
                &handlers
            ),
            boost::bind(
                &http_stub_peer_handlers::invoke_response_handler,
                &handlers,
                _1
            ),
            state,
            data_required,
            matched,
            0x400
        );
        error = handlers.get_error(
        );
        return result;
    }
}

#endif
