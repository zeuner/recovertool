/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    component_error::component_error(
        std::string const& component
    ) :
    error_string(
        "unexpected condition in component " + component
    ),
    component(
        component
    ) {
    }
    
    component_error::~component_error(
    ) throw(
    ) {
    }
    
    const char* component_error::what(
    ) const throw(
    ) {
        return error_string.c_str(
        );
    }
    
    std::string const& component_error::get_component(
    ) const {
        return component;
    }
}
