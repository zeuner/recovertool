/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_system_dns_servers.h"
#include <string>
namespace @qlib@ {
    
    
    std::list<
        boost::asio::ip::address
    > system_dns_servers(
    ) {
        std::list<
            boost::asio::ip::address
        > result;
        std::ifstream configuration(
            "/etc/resolv.conf"
        );
        std::string line;
        while (
            std::getline(
                configuration,
                line
            )
        ) {
            std::string const directive(
                "nameserver "
            );
            if (
                matches_prefix(
                    directive,
                    line
                )
            ) {
                result.push_back(
                    boost::asio::ip::address::from_string(
                        std::string(
                            line.begin(
                            ) + directive.size(
                            ),
                            line.end(
                            )
                        )
                    )
                );
            }
        }
        return result;
    }
}
