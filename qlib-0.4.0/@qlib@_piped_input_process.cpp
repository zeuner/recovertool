/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_piped_input_process.h"
#include <cerrno>
#include <csignal>
#include "@qlib@_c_string.h"
#include <sys/wait.h>
#include <unistd.h>
#include "@qlib@_component_error.h"
#include <cstring>
#include <fcntl.h>
#include <sys/select.h>
#include "@qlib@_c_buffer.h"
#include <sys/types.h>
namespace @qlib@ {
    
    
    piped_input_process::piped_input_process(
        std::vector<
            std::string
        > const& command_line
    ) :
    exit_status(
        NULL
    ) {
        if (
            command_line.empty(
            )
        ) {
            throw component_error(
                "piped_input_process"
            );
        }
        int output[
            2
        ];
        if (
            0 > pipe(
                output
            )
        ) {
            throw component_error(
                "piped_input_process"
            );
        }
        child = fork(
        );
        if (
            0 > child
        ) {
            throw component_error(
                "piped_input_process"
            );
        }
        if (
            0 < child
        ) {
            process_output = output[
                0
            ];
            close(
                output[
                    1
                ]
            );
            close(
                0
            );
            if (
                0 > fcntl(
                    process_output,
                    F_SETFL,
                    fcntl(
                        process_output,
                        F_GETFL
                    ) | O_NONBLOCK
                )
            ) {
                throw component_error(
                    "piped_input_process"
                );
            }
        } else {
            close(
                output[
                    0
                ]
            );
            if (
                0 > dup2(
                    output[
                        1
                    ],
                    1
                )
            ) {
                throw component_error(
                    "piped_input_process"
                );
            }
            close(
                output[
                    1
                ]
            );
            c_string path(
                "PATH=/usr/bin:/bin"
            );
            char* environment[
                2
            ];
            environment[
                0
            ] = path.get_data(
            );
            environment[
                1
            ] = NULL;
            environ = environment;
            std::vector<
                c_buffer
            > argument_buffers(
                command_line.size(
                )
            );
            char* arguments[
                command_line.size(
                ) + 1
            ];
            arguments[
                command_line.size(
                )
            ] = NULL;
            for (
                size_t initializing = 0;
                command_line.size(
                ) > initializing;
                initializing++
            ) {
                argument_buffers[
                    initializing
                ].resize(
                    command_line[
                        initializing
                    ].size(
                    ) + 1
                );
                memcpy(
                    argument_buffers[
                        initializing
                    ].get_data(
                    ),
                    command_line[
                        initializing
                    ].c_str(
                    ),
                    command_line[
                        initializing
                    ].size(
                    ) + 1
                );
                arguments[
                    initializing
                ] = argument_buffers[
                    initializing
                ].get_data(
                );
            }
            execvp(
                arguments[
                    0
                ],
                arguments
            );
            throw component_error(
                "piped_input_process"
            );
        }
    }
    
    piped_input_process::~piped_input_process(
    ) {
        close(
            process_output
        );
        int status;
        waitpid(
            child,
            &status,
            0
        );
        if (
            NULL != exit_status
        ) {
            if (
                !WIFEXITED(
                    status
                )
            ) {
                return;
            }
            *exit_status = WEXITSTATUS(
                status
            );
        }
    }
    
    int piped_input_process::get_input(
        char* buffer,
        size_t size
    ) {
        int const result = read(
            process_output,
            buffer,
            size
        );
        if (
            0 < result
        ) {
            return result;
        }
        if (
            0 == result
        ) {
            return -1;
        }
        if (
            EAGAIN == errno
        ) {
            return 0;
        } else {
            return -1;
        }
    }
    
    bool piped_input_process::input_ready(
        int seconds
    ) {
        fd_set read_fds;
        struct timeval timeout_value;
        timeout_value.tv_sec = seconds;
        timeout_value.tv_usec = 0;
        FD_ZERO(
            &read_fds
        );
        FD_SET(
            process_output,
            &read_fds
        );
        int return_value = select(
            process_output + 1,
            &read_fds,
            NULL,
            NULL,
            &timeout_value
        );
        if (
            -1 == return_value
        ) {
            throw component_error(
                "piped_input_process"
            );
        }
        if (
            return_value
        ) {
            return true;
        } else {
            return false;
        }
    }
    
    void piped_input_process::terminate(
    ) {
        kill(
            child,
            SIGTERM
        );
    }
    
    void piped_input_process::track_exit_status(
        int& exit_status
    ) {
        this->exit_status = &exit_status;
    }
}
