/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_resolving_a_record.h"
#include <unistd.h>
#include <boost/fusion/include/at_c.hpp>
#include <boost/asio/placeholders.hpp>
#include <boost/fusion/sequence/intrinsic/at_c.hpp>
#include <boost/bind.hpp>
namespace @qlib@ {
    
    
    resolving_a_record::resolving_a_record(
        resolver_decoding_session& resolving,
        std::string const& resolved,
        unsigned max_ttl
    ) :
    communicating(
        resolving.get_io_service(
        )
    ),
    resolving(
        resolving
    ),
    resolved(
        interned_string(
            resolved
        )
    ),
    expiring(
        communicating
    ),
    choices(
        -1
    ),
    max_ttl(
        max_ttl
    ) {
        async_refresh(
        );
    }
    
    void resolving_a_record::async_resolve(
        handling_result const& result_handler
    ) {
        if (
            !chooser
        ) {
            pending_requests.push_back(
                result_handler
            );
            async_refresh(
            );
            return;
        }
        if (
            records->empty(
            )
        ) {
            communicating.post(
                custom_handler_allocation::wrap(
                    result_binder(
                        result_handler,
                        boost::system::error_code(
                            dns_error_codes::host_not_found,
                            dns_category(
                            )
                        ),
                        boost::asio::ip::address_v4(
                        )
                    )
                )
            );
            return;
        }
        communicating.post(
            custom_handler_allocation::wrap(
                result_binder(
                    result_handler,
                    boost::system::error_code(
                    ),
                    boost::fusion::at_c<
                        resolver_session::resource_record_data
                    >(
                        (
                            *records
                        )[
                            (
                                *chooser
                            )(
                            )
                        ]
                    )
                )
            )
        );
    }
    
    void resolving_a_record::async_refresh(
    ) {
        boost::system::error_code error;
        boost::asio::ip::address_v4 const numeric(
            boost::asio::ip::address_v4::from_string(
                resolved,
                error
            )
        );
        if (
            !error
        ) {
            std::list<
                resolver_decoding_session::a_resource_record
            > records;
            if (
                resolving.get_closed(
                )
            ) {
                handle_resolution(
                    boost::system::error_code(
                        dns_error_codes::resolver_shutdown,
                        dns_category(
                        )
                    ),
                    records
                );
                return;
            }
            resolver_decoding_session::a_resource_record serving;
            boost::fusion::at_c<
                resolver_session::resource_record_ttl
            >(
                serving
            ) = -1;
            boost::fusion::at_c<
                resolver_session::resource_record_data
            >(
                serving
            ) = numeric;
            records.push_back(
                serving
            );
            handle_resolution(
                boost::system::error_code(
                ),
                records
            );
            return;
        }
        resolving.async_resolve_a(
            resolved,
            boost::bind(
                &resolving_a_record::handle_resolution,
                this,
                boost::asio::placeholders::error,
                resolver_session::records_result_argument
            )
        );
    }
    
    void resolving_a_record::handle_resolution(
        boost::system::error_code const& error,
        std::list<
            resolver_decoding_session::a_resource_record
        > const& records
    ) {
        if (
            boost::asio::error::operation_aborted == error
        ) {
            return;
        } else if (
            error
        ) {
            std::list<
                handling_result
            > pending_requests;
            pending_requests.swap(
                this->pending_requests
            );
            for (
                std::list<
                    handling_result
                >::const_iterator processing = pending_requests.begin(
                );
                pending_requests.end(
                ) != processing;
                processing++
            ) {
                communicating.post(
                    custom_handler_allocation::wrap(
                        result_binder(
                            *processing,
                            boost::system::error_code(
                                dns_error_codes::resolver_shutdown,
                                dns_category(
                                )
                            ),
                            boost::asio::ip::address_v4(
                            )
                        )
                    )
                );
            }
            return;
        }
        this->records.reset(
            new std::vector<
                resolver_decoding_session::a_resource_record
            >(
                records.begin(
                ),
                records.end(
                )
            )
        );
        if (
            this->records->empty(
            )
        ) {
            expiring.expires_from_now(
                boost::posix_time::seconds(
                    1
                )
            );
            expiring.async_wait(
                boost::bind(
                    &resolving_a_record::handle_expiry,
                    this,
                    boost::asio::placeholders::error
                )
            );
            return;
        } else {
            int const new_choices = this->records->size(
            );
            if (
                new_choices != choices
            ) {
                chooser.reset(
                    new integer_random_source(
                        new_choices
                    )
                );
                choices = new_choices;
            }
        }
        unsigned ttl = max_ttl;
        for (
            std::vector<
                resolver_decoding_session::a_resource_record
            >::const_iterator checking = this->records->begin(
            );
            this->records->end(
            ) != checking;
            checking++
        ) {
            unsigned const current = boost::fusion::at_c<
                resolver_session::resource_record_ttl
            >(
                *checking
            );
            if (
                current < ttl
            ) {
                ttl = current;
            }
        }
        std::list<
            handling_result
        > pending_requests;
        pending_requests.swap(
            this->pending_requests
        );
        for (
            std::list<
                handling_result
            >::const_iterator processing = pending_requests.begin(
            );
            pending_requests.end(
            ) != processing;
            processing++
        ) {
            async_resolve(
                *processing
            );
        }
        if (
            1 > ttl
        ) {
            ttl = 1;
        }
        expiring.expires_from_now(
            boost::posix_time::seconds(
                ttl
            )
        );
        expiring.async_wait(
            boost::bind(
                &resolving_a_record::handle_expiry,
                this,
                boost::asio::placeholders::error
            )
        );
    }
    
    void resolving_a_record::handle_expiry(
        boost::system::error_code const& error
    ) {
        if (
            error
        ) {
            return;
        }
        async_refresh(
        );
    }
    
    resolving_a_record::result_binder::result_binder(
        resolving_a_record::handling_result const& result_handler,
        boost::system::error_code const& error,
        boost::asio::ip::address_v4 const& resolved_result
    ) :
    result_handler(
        result_handler
    ),
    error(
        error
    ),
    resolved_result(
        resolved_result
    ) {
    }
    
    void resolving_a_record::result_binder::operator(
    )(
    ) {
        result_handler(
            error,
            resolved_result
        );
    }
}
