/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_request_x509_name.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    request_x509_name::request_x509_name(
        memory_x509_request const& container
    ) :
    data(
        X509_REQ_get_subject_name(
            container.get_data(
            )
        )
    ) {
        if (
            NULL == data
        ) {
            throw component_error(
                "request_x509_name"
            );
        }
    }
    
    X509_NAME* request_x509_name::get_data(
    ) const {
        return data;
    }
}
