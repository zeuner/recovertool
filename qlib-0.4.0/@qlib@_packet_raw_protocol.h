/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef PACKET_RAW_PROTOCOL_INCLUDED
#define PACKET_RAW_PROTOCOL_INCLUDED

namespace @qlib@ {
    
    
    class packet_raw_protocol {
    public:
        packet_raw_protocol(
        );
        packet_raw_protocol(
            int protocol_value
        );
        int type(
        ) const;
        int protocol(
        ) const;
        int family(
        ) const;
        std::size_t capacity(
        ) const;
        void resize(
            std::size_t new_size
        );
        class endpoint {
        public:
            endpoint(
            );
            endpoint(
                struct sockaddr_ll link_layer
            );
            int type(
            ) const;
            int protocol(
            ) const;
            std::size_t capacity(
            ) const;
            std::size_t size(
            ) const;
            void resize(
                std::size_t new_size
            );
            boost::asio::detail::socket_addr_type const* data(
            ) const;
            boost::asio::detail::socket_addr_type* data(
            );
        private:
            union {
                boost::asio::detail::socket_addr_type base;
                struct sockaddr_ll specific;
            } native_data;
        };
    private:
        int protocol_value;
    };
}

#endif
