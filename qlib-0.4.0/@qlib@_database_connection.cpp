/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_database_connection.h"
#include "@qlib@_component_error.h"
#include <unistd.h>
#include "@qlib@_stringified.h"
#include "@qlib@_throw_exception.h"
namespace @qlib@ {
    
    
    database_connection::database_connection(
        std::string const& hostname,
        std::string const& database,
        std::string const& username,
        std::string const& password
    ) :
    handle(
        mysql_init(
            NULL
        )
    ),
    query_duration_count(
        0
    ),
    duration_flush_counter(
        0
    ),
    flushing_durations(
        false
    ) {
        if (
            NULL == handle
        ) {
            return;
        }
        connection_type connected = mysql_real_connect(
            handle,
            hostname.c_str(
            ),
            username.c_str(
            ),
            password.c_str(
            ),
            database.c_str(
            ),
            0,
            NULL,
            0
        );
        if (
            NULL == connected
        ) {
            mysql_close(
                handle
            );
            handle = connected;
            return;
        }
        try {
            do {
                std::auto_ptr<
                    database_result
                > querying(
                    get_tables_query(
                    )
                );
                database_result& tables = *querying;
                database_result::row_type existing;
                for (
                    existing = tables.fetch_row(
                    );
                    existing;
                    existing = tables.fetch_row(
                    )
                ) {
                    if (
                        std::string(
                            "database_query_duration"
                        ) == existing[
                            0
                        ]
                    ) {
                        return;
                    }
                }
            } while (
                false
            );
            database_push(
                *this,
                "CREATE TABLE database_query_duration (\n"
                "    queried text,\n"
                "    elapsed int(11),\n"
                "    key (elapsed)\n"
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8"
            );
        } catch (
            std::exception& caught
        ) {
            this->~database_connection(
            );
            throw;
        }
    }
    
    database_connection::~database_connection(
    ) {
        try {
            flush_query_durations(
                true
            );
        } catch (
            std::exception& caught
        ) {
        }
        if (
            NULL == handle
        ) {
            return;
        }
        mysql_close(
            handle
        );
    }
    
    database_result* database_connection::get_tables_query(
    ) {   
        return new database_result(
            *this,
            "SHOW TABLES"
        );
    }
    
    MYSQL_RES* database_connection::query_result(
        std::string const& query
    ) {
        database_push(
            *this,
            query
        );
        MYSQL_RES* result = mysql_store_result(  
            get_storage(
            )
        );
        if (
            NULL == result
        ) {
            throw_exception(
                component_error(
                    "database_connection"
                )
            );
        }
        return result;
    }
    
    void database_connection::flush_query_durations(
        bool force
    ) {
        if (
            flushing_durations
        ) {
            return;
        }
        flushing_durations = true;
        duration_flush_counter++;
        if (
            !force && (
                128 > duration_flush_counter
            )
        ) {
            return;
        }
        duration_flush_counter = 0;
        scoped_transaction committing(
            *this
        );
        std::list<
            std::pair<
                std::string,  
                boost::posix_time::time_duration
            >
        > query_durations;
        query_durations.swap(
            this->query_durations
        );
        for (
            std::list<
                std::pair<
                    std::string,
                    boost::posix_time::time_duration
                >
            >::const_iterator flushing = query_durations.begin(
            );
            query_durations.end(
            ) != flushing;
            flushing++
        ) {
            database_push(
                *this,
                "INSERT\n"
                "    INTO database_query_duration (queried,elapsed)\n"
                "    SELECT " + quoted(
                    flushing->first
                ) + "," + stringified(
                    flushing->second.total_milliseconds(
                    )
                )
            );
        }
        query_durations.clear(
        );
        query_durations.swap(
            this->query_durations
        );
        long cutoff;
        while (
            4096 <= query_duration_count
        ) {
            query_duration_count -= 4096;
            if (
                true
            ) {
                database_result separating(
                    *this,
                    "SELECT elapsed\n"
                    "    FROM database_query_duration\n"
                    "    ORDER BY elapsed DESC\n"
                    "    LIMIT 1"
                );
                database_result::row_type found = separating.fetch_row(
                );
                if (
                    !found
                ) {
                    break;
                }
                cutoff = atol(
                    found[
                        0
                    ]
                );
            }
            database_push(
                *this,
                "DELETE\n"
                "    FROM database_query_duration\n"
                "    WHERE elapsed>=" + stringified(
                    cutoff
                )
            );
        }
        if (
            true
        ) {
            database_result separating(
                *this,
                "SELECT elapsed\n"
                "    FROM database_query_duration\n"
                "    ORDER BY elapsed DESC\n"
                "    LIMIT 1\n"
                "    OFFSET 512"
            );
            database_result::row_type found = separating.fetch_row(
            );
            if (
                !found
            ) {
                return;
            }
            cutoff = atol(
                found[
                    0
                ]
            );
        }
        database_push(
            *this,
            "DELETE\n"
            "    FROM database_query_duration\n"
            "    WHERE elapsed<" + stringified(
                cutoff
            )
        );
        flushing_durations = false;
        committing.commit(
        );
    }
    
    void database_connection::store_query_duration(
        std::string const& query,
        boost::posix_time::time_duration const& duration
    ) {
        query_duration_count++;
        query_durations.push_back(
            std::make_pair(
                query,
                duration
            )
        );
    }
    
    void database_connection::ensure_connection(
    ) {
        if (
            0 != mysql_ping(
                get_storage(
                )
            )
        ) {
            throw_exception(
                component_error(
                    "database_connection"
                )
            );
        }
    }
    
    void database_connection::require_threads(
    ) {
        if (
            0 == mysql_thread_safe(
            )
        ) {
            throw_exception(
                component_error(
                    "database_connection"
                )
            );
        }
    }
    
    std::string database_connection::last_error(
    ) {
        return mysql_error(
            get_storage(
            )
        );
    }
    
    database_connection::connection_type database_connection::get_storage(
    ) const {
        if (
            NULL == handle
        ) {
            throw_exception(
                no_database_connected(
                )
            );
        }
        return handle;
    }
}
