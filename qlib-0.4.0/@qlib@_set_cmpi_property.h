/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef SET_CMPI_PROPERTY_INCLUDED
#define SET_CMPI_PROPERTY_INCLUDED

#include <string>
#include <boost/date_time/posix_time/ptime.hpp>
namespace @qlib@ {
    
    
    template<
        typename object_type,
        typename value_type
    >
    void set_cmpi_property(
        CMPIBroker const* broker,
        object_type* object,
        std::string const& name,
        value_type value
    );
    
    template<
        typename object_type
    >
    void set_cmpi_property(
        CMPIBroker const* broker,
        object_type* object,
        std::string const& name,
        void* value
    ) {
        uint64_t const numeric = reinterpret_cast<
            uintptr_t
        >(
            value
        );
        set_native_cmpi_property(
            object,
            name.c_str(
            ),
            reinterpret_cast<
                CMPIValue const*
            >(
                &numeric
            ),
            CMPI_uint64
        );
    }
    
    template<
        typename object_type
    >
    void set_cmpi_property(
        CMPIBroker const* broker,
        object_type* object,
        std::string const& name,
        double value
    ) {
        if (
            isnan(
                value
            )
        ) {
            return;
        }
        set_native_cmpi_property(
            object,
            name.c_str(
            ),
            reinterpret_cast<
                CMPIValue const*
            >(
                &value
            ),
            CMPI_real64
        );
    }
    
    template<
        typename object_type
    >
    void set_cmpi_property(
        CMPIBroker const* broker,
        object_type* object,
        std::string const& name,
        uint32_t value
    ) {
        set_native_cmpi_property(
            object,
            name.c_str(
            ),
            reinterpret_cast<
                CMPIValue const*
            >(
                &value
            ),
            CMPI_uint32
        );
    }
    
    template<
        typename object_type
    >
    void set_cmpi_property(
        CMPIBroker const* broker,
        object_type* object,
        std::string const& name,
        int32_t value
    ) {
        set_native_cmpi_property(
            object,
            name.c_str(
            ),
            reinterpret_cast<
                CMPIValue const*
            >(
                &value
            ),
            CMPI_sint32
        );
    }
    
    template<
        typename object_type
    >
    void set_cmpi_property(
        CMPIBroker const* broker,
        object_type* object,
        std::string const& name,
        uint64_t value
    ) {
        set_native_cmpi_property(
            object,
            name.c_str(
            ),
            reinterpret_cast<
                CMPIValue const*
            >(
                &value
            ),
            CMPI_uint64
        );
    }
    
    template<
        typename object_type
    >
    void set_cmpi_property(
        CMPIBroker const* broker,
        object_type* object,
        std::string const& name,
        std::string value
    ) {
        set_native_cmpi_property(
            object,
            name.c_str(
            ),
            reinterpret_cast<
                CMPIValue const*
            >(
                value.c_str(
                )
            ),
            CMPI_chars
        );
    }
    
    template<
        typename object_type
    >
    void set_cmpi_property(
        CMPIBroker const* broker,
        object_type* object,
        std::string const& name,
        boost::posix_time::ptime value
    ) {
        CMPIValue native;
        native.dateTime = CMNewDateTimeFromBinary(
            broker,
            (
                value - boost::posix_time::from_time_t(
                    0
                )
            ).total_microseconds(
            ),
            0,
            NULL
        );
        set_native_cmpi_property(
            object,
            name.c_str(
            ),
            &native,
            CMPI_dateTime
        );
    }
}

#endif
