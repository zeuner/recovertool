/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_attached_debugger.h"
#include "@qlib@_stringified.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    attached_debugger::attached_debugger(
        std::string const& output_file,
        std::string const& error_file,
        std::string const& script
    ) {
        pid_t const current_pid = getpid(
        );
        std::string const executable = process_executable(
        );
        pid_t const debugger_pid = fork(
        );
        if (
            0 > debugger_pid
        ) {
            throw component_error(
                "attached_debugger"
            );
        }
        if (
            0 < debugger_pid
        ) {
            sleep(
                1
            );
            return;
        }
        std::string const shell_script(
            "debug_script=`mktemp`\n"
            "echo " + shell_quoted(
                script
            ) + ">$debug_script\n"
            "gdb -batch -x $debug_script " + executable + " " + stringified(
                current_pid
            ) + "\n"
            "rm $debug_script"
        );
        if (
            !daemonized(
            )
        ) {
            exit(
                0
            );
        }
        do {
            posix_file output(
                output_file,
                O_WRONLY | O_CREAT | O_TRUNC,
                0666
            );
            posix_file error(
                error_file,
                O_WRONLY | O_CREAT | O_TRUNC,
                0666
            );
            dup2(
                output.get_data(
                ),
                1
            );
            dup2(
                error.get_data(
                ),
                2
            );
        } while (
            false
        );
        execl(
            "/bin/sh",
            "/bin/sh",
            "-c",
            shell_script.c_str(
            ),
            NULL
        );
        throw component_error(
            "attached_debugger"
        );
    }
}
