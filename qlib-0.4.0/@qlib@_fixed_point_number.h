/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef FIXED_POINT_NUMBER_INCLUDED
#define FIXED_POINT_NUMBER_INCLUDED

#include <string>
namespace @qlib@ {
    
    
    class fixed_point_number {
    public:
        fixed_point_number(
            int initializer
        );
        fixed_point_number(
            long initializer
        );
        fixed_point_number(
            long long initializer
        );
        fixed_point_number(
            unsigned initializer
        );
        fixed_point_number(
            unsigned long initializer
        );
        fixed_point_number(
            unsigned long long initializer
        );
        fixed_point_number operator+(
            fixed_point_number const& added
        ) const;
        fixed_point_number& operator+=(
            fixed_point_number const& added
        );
        fixed_point_number operator-(
            fixed_point_number const& subtracted
        ) const;
        fixed_point_number& operator-=(
            fixed_point_number const& subtracted
        );
        fixed_point_number operator*(
            fixed_point_number const& multiplied
        ) const;
        fixed_point_number& operator*=(
            fixed_point_number const& multiplied
        );
        fixed_point_number operator/(
            fixed_point_number const& divisor
        ) const;
        fixed_point_number& operator/=(
            fixed_point_number const& divisor
        );
        operator int(
        ) const;
        operator long(
        ) const;
        operator long long(
        ) const;
        std::string to_string(
        ) const;
    private:
        int64_t value;
    };
    
    std::ostream& operator<<(
        std::ostream& destination,
        fixed_point_number const& printed
    );
}

#endif
