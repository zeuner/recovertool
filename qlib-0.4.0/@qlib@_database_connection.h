/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef DATABASE_CONNECTION_INCLUDED
#define DATABASE_CONNECTION_INCLUDED

#include <list>
#include <string>
#include "@qlib@_c_string.h"
namespace @qlib@ {
    
    
    class database_result;
    
    class database_connection {
    public:
        database_connection(
            std::string const& hostname,
            std::string const& database,
            std::string const& username,
            std::string const& password
        );
        ~database_connection(
        );
        typedef MYSQL* connection_type;
        connection_type get_storage(
        ) const;
        MYSQL_RES* query_result(
            std::string const& query
        );
        void flush_query_durations(
            bool force = false
        );
        void store_query_duration(
            std::string const& query,
            boost::posix_time::time_duration const& duration
        );
        void ensure_connection(
        );
        static void require_threads(
        );
        template<
            typename string
        >
        string quoted(
            string const& raw
        );
        template<
            typename string
        >
        string quoted_binary(
            string const& raw
        );
        std::string last_error(
        );
        database_result* get_tables_query(
        );
    private:
        connection_type handle;
        std::list<
            std::pair<
                std::string,
                boost::posix_time::time_duration
            >
        > query_durations;
        long query_duration_count;
        int duration_flush_counter;
        bool flushing_durations;
    };
    
    template<
        typename string
    >
    string database_connection::quoted(
        string const& raw
    ) {
        char buffer[
            raw.size(
            ) * 2 + 1
        ];
        long required = mysql_real_escape_string(
            get_storage(
            ),
            buffer,
            raw.data(
            ),
            raw.size(
            )
        );
        return "'" + string(
            buffer,
            required
        ) + "'";
    }
    
    template<
        typename string
    >
    string database_connection::quoted_binary(
        string const& raw
    ) {
        typedef std::basic_string<
            char,
            std::char_traits<
                char
            >,
            typename string::allocator_type::template rebind<
                char
            >::other
        > char_string;
        char_string text = quoted(
            char_string(
                reinterpret_cast<
                    char const*
                >(
                    raw.data(
                    )
                ),
                raw.size(
                )
            )
        );
        return std::basic_string<
            unsigned char,
            std::char_traits<
                unsigned char
            >,
            typename string::allocator_type
        >(
            reinterpret_cast<
                unsigned char const*
            >(
                text.data(
                )
            ),
            text.size(
            )
        );
    }
}

#endif
