/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef CMPI_OID_KEY_INCLUDED
#define CMPI_OID_KEY_INCLUDED

#include <string>
namespace @qlib@ {
    
    
    class cmpi_oid_key {
    public:
        static std::string get_key_name(
        );
        typedef void* key_type;
        key_type get_key(
        );
        template<
            typename managed
        >
        static void retrieve_by_key(
            key_type key,
            managed*& retrieved
        );
    };
    
    template<
        typename managed
    >
    void cmpi_oid_key::retrieve_by_key(
        key_type key,
        managed*& retrieved
    ) {
        managed* searched = static_cast<
            managed*
        >(
            key
        );
        if (
            managed::get_registered_instances(
            ).end(
            ) == managed::get_registered_instances(
            ).find(
                searched
            )
        ) {
            retrieved = NULL;
            return;
        }
        retrieved = searched;
    }
}

#endif
