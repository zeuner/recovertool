/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef ETHERNET_LAYER_LINK_INCLUDED
#define ETHERNET_LAYER_LINK_INCLUDED

#include <string>
namespace @qlib@ {
    
    
    class ethernet_layer_link {
    public:
        ethernet_layer_link(
            std::string const& interface,
            uint16_t network_protocol
        );
        void write_header(
            struct ether_header& written
        ) const;
    private:
        struct ether_header native_header;
    };
}

#endif
