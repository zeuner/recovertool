/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef LOOKUP_TABLE_INCLUDED
#define LOOKUP_TABLE_INCLUDED

#include <boost/noncopyable.hpp>
#include <map>
namespace @qlib@ {
    
    
    template<
        typename storable
    >
    class lookup_table :
    private boost::noncopyable {
    public:
        lookup_table(
        );
        int get_key(
            storable const& value
        );
        storable const& get_value(
            int key
        ) const;
    private:
        std::map<
            storable,
            int
        > to_key;
        std::map<
            int,
            storable
        > to_value;
        int newest_key;
    };
    
    template<
        typename storable
    >
    lookup_table<
        storable
    >::lookup_table(
    ) :
    newest_key(
        0
    ) {
    }
    
    template<
        typename storable
    >
    int lookup_table<
        storable
    >::get_key(
        storable const& value
    ) {
        typename std::map<
            storable,
            int
        >::const_iterator found = to_key.find(
            value
        );
        if (
            to_key.end(
            ) == found
        ) {
            int const inserted = newest_key++;
            to_key[
                value
            ] = inserted;
            to_value[
                inserted
            ] = value;
            return inserted;
        } else {
            return found->second;
        }
    }
    
    template<
        typename storable
    >
    storable const& lookup_table<
        storable
    >::get_value(
        int key
    ) const {
        return to_value.at(
            key
        );
    }
}

#endif
