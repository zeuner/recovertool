/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef HTTP_GET_REQUEST_INCLUDED
#define HTTP_GET_REQUEST_INCLUDED

#include <string>
#include <unistd.h>
namespace @qlib@ {
    
    
    template<
        typename socket
    >
    class http_get_request :
    public stream_one_shot_request<
        socket
    > {
    public:
        template<
            typename initializing
        >
        http_get_request(
            initializing& initializer
        );
        template<
            typename initializing1,
            typename initializing2
        >
        http_get_request(
            initializing1& initializer1,
            initializing2& initializer2
        );
        template<
            typename handling
        >
        void async_request(
            std::string const& host,
            unsigned short port,
            std::string const& resource,
            std::string const& agent,
            handling handler
        );
    };
    
    template<
        typename socket
    >
    template<
        typename initializing
    >
    http_get_request<
        socket
    >::http_get_request(
        initializing& initializer
    ) :
    stream_one_shot_request<
        socket
    >(
        initializer
    ) {
    }
    
    template<
        typename socket
    >
    template<
        typename initializing1,
        typename initializing2
    >
    http_get_request<
        socket
    >::http_get_request(
        initializing1& initializer1,
        initializing2& initializer2
    ) :
    stream_one_shot_request<
        socket
    >(
        initializer1,
        initializer2
    ) {
    }
    
    template<
        typename socket
    >
    template<
        typename handling
    >
    void http_get_request<
        socket
    >::async_request(
        std::string const& host,
        unsigned short port,
        std::string const& resource,
        std::string const& agent,
        handling handler
    ) {
        stream_one_shot_request<
            socket
        >::async_request(
            host,
            port,
            "GET " + resource + " HTTP/1.0\r\n"
            "Host: " + host + "\r\n"
            "User-Agent: " + agent + "\r\n"
            "Connection: close\r\n"
            "\r\n",
            handler
        );
    }
}

#endif
