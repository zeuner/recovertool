/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef ALLOCATOR_POOL_CHOOSER_INCLUDED
#define ALLOCATOR_POOL_CHOOSER_INCLUDED

#include "@qlib@_component_error.h"
#include "@qlib@_c_buffer.h"
#include <boost/shared_ptr.hpp>
namespace @qlib@ {
    
    
    template<
        long size = 1
    >
    class allocator_pool_chooser;
    
    template<
        long size
    >
    class allocator_pool_chooser {
    public:
        char* allocate(
            long required
        );
        void deallocate(
            char* deallocated,
            long required
        );
    private:
        typedef static_buffer<
            size
        > buffer_type;
        boost::pool_allocator<
            buffer_type,
            allocator_wrapper<
                std::allocator
            >
        > allocator;
        static boost::shared_ptr<
            allocator_pool_chooser<
                size * 2
            >
        > next_size;
    };
    
    template<
    >
    class allocator_pool_chooser<
        1073741824
    > {
    public:
        char* allocate(
            long required
        );
        void deallocate(
            char* deallocated,
            long required
        );
    private:
        typedef static_buffer<
            1073741824
        > buffer_type;
        boost::pool_allocator<
            buffer_type,
            allocator_wrapper<
                std::allocator
            >
        > allocator;
    };
    
    template<
        long size
    >
    char* allocator_pool_chooser<
        size
    >::allocate(
        long required
    ) {
        if (
            size < required
        ) {
            if (
                !next_size
            ) {
                next_size.reset(
                    new allocator_pool_chooser<
                        size * 2
                    >
                );
            }
            return next_size->allocate(
                required
            );
        }
        buffer_type* containing = allocator.allocate(
            1
        );
        char* result = containing->get_data(
        );
        if (
            reinterpret_cast<
                char*
            >(
                containing
            ) != result
        ) {
            throw component_error(
                "allocator_pool_chooser"
            );
        }
        return result;
    }
    
    template<
        long size
    >
    void allocator_pool_chooser<
        size
    >::deallocate(
        char* deallocated,
        long required
    ) {
        if (
            size < required
        ) {
            if (
                !next_size
            ) {
                throw component_error(
                    "allocator_pool_chooser"
                );
            }
            next_size->deallocate(
                deallocated,
                required
            );
            return;
        }
        allocator.deallocate(
            reinterpret_cast<
                buffer_type*
            >(
                deallocated
            ),
            1
        );
    }
    
    template<
        long size
    >
    boost::shared_ptr<
        allocator_pool_chooser<
            size * 2
        >
    > allocator_pool_chooser<
        size
    >::next_size;
}

#endif
