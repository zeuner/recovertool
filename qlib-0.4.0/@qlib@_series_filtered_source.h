/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef SERIES_FILTERED_SOURCE_INCLUDED
#define SERIES_FILTERED_SOURCE_INCLUDED

#include <boost/noncopyable.hpp>
#include <list>
namespace @qlib@ {
    
    
    template<
        typename playable
    >
    class series_filtered_source :
    private boost::noncopyable {
    public:
        series_filtered_source(
            playable& source,
            std::list<
                amplitude_value
            > const& series
        );
        bool get_sample(
            amplitude_value& retrieved
        );
    private:
        playable& source;
        resizable_buffer<
            malloc_c_allocator,
            amplitude_value
        > series;
        resizable_buffer<
            malloc_c_allocator,
            amplitude_value
        > history;
        amplitude_value* next;
        amplitude_value* const end;
    };
    
    template<
        typename playable
    >
    series_filtered_source<
        playable
    >::series_filtered_source(
        playable& source,
        std::list< 
            amplitude_value 
        > const& series
    ) :
    source(
        source
    ),
    series(
        series.size(
        )
    ),
    history(
        series.size(
        )
    ),
    next(
        history.get_data(
        )
    ),
    end(
        history.get_data(
        ) + history.get_size(
        )
    ) {
        size_t initializing;
        std::list<
            amplitude_value
        >::const_iterator initializer = series.begin(
        );
        for (
            initializing = 0;
            this->series.get_size(
            ) > initializing;
            initializing++,
            initializer++
        ) {
            this->series.get_data(
            )[
                initializing
            ] = *initializer;
        }
        for (
            initializing = 0;
            history.get_size(
            ) > initializing;
            initializing++
        ) {
            history.get_data(
            )[
                initializing
            ] = 0;
        }
    }
    
    template<
        typename playable
    >
    bool series_filtered_source<
        playable
    >::get_sample(
        amplitude_value& retrieved
    ) {
        if (
            !source.get_sample(
                *next
            )
        ) {
            return false;
        }
        next++;
        if (
            end <= next
        ) {
            next = history.get_data(
            );
        }
        retrieved = 0;
        for (
            amplitude_value* accumulating = next,
            * factor = series.get_data(
            );
            end > accumulating;
            factor++,
            accumulating++
        ) {
            retrieved += (
                *accumulating
            ) * (
                *factor
            );
        }
        for (
            amplitude_value* accumulating = history.get_data(
            ),
            * factor = series.get_data(
            ) + (
                end - next
            );
            next > accumulating;
            factor++,
            accumulating++
        ) {
            retrieved += (
                *accumulating
            ) * (
                *factor
            );
        }
        return true;
    }
}

#endif
