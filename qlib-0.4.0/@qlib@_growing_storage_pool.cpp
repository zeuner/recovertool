/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_growing_storage_pool.h"
namespace @qlib@ {
    
    
    growing_storage_pool::growing_storage_pool(
        size_t element_size,
        size_t smallest_pool,
        size_t alignment
    ) :
    current_pool_size(
        smallest_pool
    ) {
        aligned_element_size = element_size;
        aligned_element_size += alignment - 1;
        aligned_element_size -= aligned_element_size % alignment;
        aligned_element_size += sizeof(
            fixed_storage_pool*
        );
        add_pool(
        );
    }
    
    void* growing_storage_pool::get(
    ) {
        void* retrieved = active_pool->get(
        );
        if (
            NULL == retrieved
        ) {
            add_pool(
            );
            retrieved = active_pool->get(
            );
        }
        fixed_storage_pool** marking = static_cast<
            fixed_storage_pool**
        >(
            retrieved
        );
        *marking = active_pool;
        return marking + 1;
    }
    
    void growing_storage_pool::put(
        void* freed
    ) {
        fixed_storage_pool** data = static_cast<
            fixed_storage_pool**
        >(
            freed
        );
        fixed_storage_pool** responsible = data - 1;
        (
            *responsible
        )->put(
            responsible
        );
    }
    
    void growing_storage_pool::add_pool(
    ) {
        fixed_storage_pool* added(
            new fixed_storage_pool(
                aligned_element_size,
                current_pool_size
            )
        );
        pools.push_back(
            added
        );
        active_pool = pools.back(
        );
        current_pool_size *= 2;
    }
}
