/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_file_lock.h"
#include <sys/file.h>
#include "@qlib@_throw_exception.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    file_lock::file_lock(
        int file,
        int operation
    ) :
    file(
        file
    ) {
        if (
            0 > flock(
                file,
                operation
            )
        ) {
            throw_exception(
                component_error(
                    "file_lock"
                )
            );
        }
    }
    
    file_lock::~file_lock(
    ) {
        flock(
            file,
            LOCK_UN
        );
    }
}
