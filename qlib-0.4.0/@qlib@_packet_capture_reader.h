/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef PACKET_CAPTURE_READER_INCLUDED
#define PACKET_CAPTURE_READER_INCLUDED

#include "@qlib@_c_string.h"
#include "@qlib@_throw_exception.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    template<
        typename producing
    >
    class packet_capture_reader {
    public:
        packet_capture_reader(
            producing& source
        );
        bool get_packet(
            unsigned char const*& data,
            size_t& size
        );
    protected:
        template<
            typename stored
        >
        bool read_value(
            stored& value
        );
    private:
        producing& source;
        std::basic_string<
            unsigned char
        > last_packet;
    };
    
    template<
        typename producing
    >
    packet_capture_reader<
        producing
    >::packet_capture_reader(
        producing& source
    ) :
    source(
        source
    ) {
        uint32_t byte_order_marker;
        if (
            !read_value(
                byte_order_marker
            )
        ) {
            throw_exception(
                component_error(
                    "packet_capture_reader"
                )
            );
        }
        if (
            0xa1b2c3d4 != byte_order_marker
        ) {
            throw_exception(
                component_error(
                    "packet_capture_reader"
                )
            );
        }
        uint16_t major_version;
        if (
            !read_value(
                major_version
            )
        ) {
            throw_exception(
                component_error(
                    "packet_capture_reader"
                )
            );
        }
        if (
            2 != major_version
        ) {
            throw_exception(
                component_error(
                    "packet_capture_reader"
                )
            );
        }
        uint16_t minor_version;
        if (
            !read_value(
                minor_version
            )
        ) {
            throw_exception(
                component_error(
                    "packet_capture_reader"
                )
            );
        }
        if (
            4 != minor_version
        ) {
            throw_exception(
                component_error(
                    "packet_capture_reader"
                )
            );
        }
        uint32_t timezone_offset;
        if (
            !read_value(
                timezone_offset
            )
        ) {
            throw_exception(
                component_error(
                    "packet_capture_reader"
                )
            );
        }
        if (
            0 != timezone_offset
        ) {
            throw_exception(
                component_error(
                    "packet_capture_reader"
                )
            );
        }
        uint32_t timestamp_accuracy;
        if (
            !read_value(
                timestamp_accuracy
            )
        ) {
            throw_exception(
                component_error(
                    "packet_capture_reader"
                )
            );
        }
        if (
            0 != timestamp_accuracy
        ) {
            throw_exception(
                component_error(
                    "packet_capture_reader"
                )
            );
        }
        uint32_t snapshot_length;
        if (
            !read_value(
                snapshot_length
            )
        ) {
            throw_exception(
                component_error(
                    "packet_capture_reader"
                )
            );
        }
        uint32_t link_layer_header_type;
        if (
            !read_value(
                link_layer_header_type
            )
        ) {
            throw_exception(
                component_error(
                    "packet_capture_reader"
                )
            );
        }
        if (
            1 != link_layer_header_type
        ) {
            throw_exception(
                component_error(
                    "packet_capture_reader"
                )
            );
        }
    }
    
    template<
        typename producing
    >
    bool packet_capture_reader<
        producing
    >::get_packet(
        unsigned char const*& data,
        size_t& size
    ) {
        uint32_t seconds;
        if (
            !read_value(
                seconds
            )
        ) {
            return false;
        }
        uint32_t microseconds;
        if (
            !read_value(
                microseconds
            )
        ) {
            return false;
        }
        uint32_t captured_size;
        if (
            !read_value(
                captured_size
            )
        ) { 
            return false;
        }
        uint32_t original_size;
        if (
            !read_value(
                original_size
            )
        ) { 
            return false;
        }
        unsigned char buffer[
            captured_size
        ];
        if (
            !source.read(
                reinterpret_cast<
                    char*
                >(
                    buffer
                ),
                captured_size
            )
        ) {
            return false;
        }
        last_packet = std::basic_string<
            unsigned char
        >(
            buffer,
            captured_size
        );
        data = last_packet.data(
        );
        size = captured_size;
        return true;
    }
    
    template<
        typename producing
    >
    template<
        typename stored
    >
    bool packet_capture_reader<
        producing
    >::read_value(
        stored& value
    ) {
        return source.read(
            reinterpret_cast<
                char*
            >(
                &value
            ),
            sizeof(
                value
            )
        );
    }
}

#endif
