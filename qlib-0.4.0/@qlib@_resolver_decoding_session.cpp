/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_resolver_decoding_session.h"
#include "@qlib@_stringified.h"
#include "@qlib@_throw_exception.h"
#include "@qlib@_component_error.h"
#include <boost/fusion/sequence/intrinsic/at_c.hpp>
#include <boost/fusion/include/at_c.hpp>
namespace @qlib@ {
    
    
    resolver_decoding_session::resolver_decoding_session(
        boost::asio::io_service& communicating,
        std::list<
            boost::asio::ip::address
        > const& dns_servers,
        boost::asio::ip::udp::endpoint const& local_endpoint_v4,
        boost::asio::ip::udp::endpoint const& local_endpoint_v6
    ) :
    resolver_session(
        communicating,
        dns_servers,
        local_endpoint_v4,
        local_endpoint_v6
    ) {
    }
    
    void resolver_decoding_session::async_resolve_mx(
        char const* domain_name,
        resolver_decoding_session::handling_mx_result const& result_handler
    ) {
        async_resolve(
            mx,
            domain_name,
            custom_handler_allocation::wrap(
                mx_record_resolution_handler(
                    this,
                    result_handler
                )
            )
        );
    }
    
    void resolver_decoding_session::async_resolve_ptr(
        char const* domain_name,
        resolver_decoding_session::handling_ptr_result const& result_handler
    ) {
        async_resolve(
            ptr,
            domain_name,
            custom_handler_allocation::wrap(
                ptr_record_resolution_handler(
                    this,
                    result_handler
                )
            )
        );
    }
    
    void resolver_decoding_session::async_resolve_a(
        char const* domain_name,
        resolver_decoding_session::handling_a_result const& result_handler
    ) {
        async_resolve(
            a,
            domain_name,
            custom_handler_allocation::wrap(
                a_record_resolution_handler(
                    this,
                    result_handler
                )
            )
        );
    }
    
    void resolver_decoding_session::async_resolve_aaaa(
        char const* domain_name,
        resolver_decoding_session::handling_aaaa_result const& result_handler
    ) {
        async_resolve(
            aaaa,
            domain_name,
            custom_handler_allocation::wrap(
                aaaa_record_resolution_handler(
                    this,
                    result_handler
                )
            )
        );
    }
    
    void resolver_decoding_session::async_resolve_mx(
        std::string const& domain_name,
        resolver_decoding_session::handling_mx_result const& result_handler
    ) {
        async_resolve_mx(
            interned_string(
                domain_name
            ),
            result_handler
        );
    }
    
    void resolver_decoding_session::async_resolve_ptr(
        boost::asio::ip::address_v4 const& address,
        resolver_decoding_session::handling_ptr_result const& result_handler
    ) {
        boost::asio::ip::address_v4::bytes_type const bytes = address.to_bytes(
        );
        std::string const domain_name = stringified(
            static_cast<
                int
            >(
                bytes[
                    3
                ]
            )
        ) + "." + stringified(
            static_cast<
                int
            >(
                bytes[
                    2
                ]
            )
        ) + "." + stringified(
            static_cast<
                int
            >(
                bytes[
                    1
                ]
            )
        ) + "." + stringified(
            static_cast<
                int
            >(
                bytes[
                    0
                ]
            )
        ) + ".in-addr.arpa";
        async_resolve_ptr(
            interned_string(
                domain_name
            ),
            result_handler
        );
    }
    
    void resolver_decoding_session::async_resolve_a(
        std::string const& domain_name,
        resolver_decoding_session::handling_a_result const& result_handler
    ) {
        async_resolve_a(
            interned_string(
                domain_name
            ),
            result_handler
        );
    }
    
    void resolver_decoding_session::async_resolve_aaaa(
        std::string const& domain_name,
        resolver_decoding_session::handling_aaaa_result const& result_handler
    ) {
        async_resolve_aaaa(
            interned_string(
                domain_name
            ),
            result_handler
        );
    }
    
    void resolver_decoding_session::handle_resolve_mx(
        boost::system::error_code const& error,
        std::string const& raw_result,
        std::list<
            resource_record
        > const& records,
        resolver_decoding_session::handling_mx_result const& result_handler
    ) {
        std::list<
            mx_resource_record
        > mx_records;
        for (
            std::list<
                resource_record
            >::const_iterator transformed = records.begin(
            );
            records.end(
            ) != transformed;
            transformed++
        ) {
            std::string::const_iterator data_begin = boost::fusion::at_c<
                4
            >(
                *transformed
            ).begin(
            );
            if (
                boost::fusion::at_c<
                    4
                >(
                    *transformed
                ).end(
                ) == data_begin
            ) {
                throw_exception(
                    component_error(
                        "resolver_decoding_session"
                    )
                );
            }
            unsigned char digit = static_cast<
                unsigned char
            >(
                *data_begin++
            );
            int priority = digit;
            priority <<= 8;
            if (
                boost::fusion::at_c<
                    4
                >(
                    *transformed
                ).end(
                ) == data_begin
            ) {
                throw_exception(
                    component_error(
                        "resolver_decoding_session"
                    )
                );
            }
            digit = static_cast<
                unsigned char
            >(
                *data_begin++
            );
            priority += digit;
            std::string mx = decode_labels(
                data_begin,
                boost::fusion::at_c<
                    4
                >(
                    *transformed
                ).end(
                ),
                raw_result.begin(
                ),
                raw_result.end(
                )
            );
            mx_records.push_back(
                mx_resource_record(
                    boost::fusion::at_c<
                        0
                    >(
                        *transformed
                    ),
                    boost::fusion::at_c<
                        1
                    >(
                        *transformed
                    ),
                    boost::fusion::at_c<
                        2
                    >(
                        *transformed
                    ),
                    boost::fusion::at_c<
                        3
                    >(
                        *transformed
                    ),
                    priority,
                    mx
                )
            );
        }
        result_handler(
            error,
            raw_result,
            mx_records
        );
    }
    
    void resolver_decoding_session::handle_resolve_ptr(
        boost::system::error_code const& error,
        std::string const& raw_result,
        std::list<
            resource_record
        > const& records,
        resolver_decoding_session::handling_ptr_result const& result_handler
    ) {
        std::list<
            ptr_resource_record
        > ptr_records;
        for (
            std::list<
                resource_record
            >::const_iterator transformed = records.begin(
            );
            records.end(
            ) != transformed;
            transformed++
        ) {
            std::string::const_iterator data_begin = boost::fusion::at_c<
                4
            >(
                *transformed
            ).begin(
            );
            std::string ptr = decode_labels(
                data_begin,
                boost::fusion::at_c<
                    4
                >(
                    *transformed
                ).end(
                ),
                raw_result.begin(
                ),
                raw_result.end(
                )
            );
            ptr_records.push_back(
                ptr_resource_record(
                    boost::fusion::at_c<
                        0
                    >(
                        *transformed
                    ),
                    boost::fusion::at_c<
                        1
                    >(
                        *transformed
                    ),
                    boost::fusion::at_c<
                        2
                    >(
                        *transformed
                    ),
                    boost::fusion::at_c<
                        3
                    >(
                        *transformed
                    ),
                    ptr
                )
            );
        }
        result_handler(
            error,
            raw_result,
            ptr_records
        );
    }
    
    void resolver_decoding_session::handle_resolve_a(
        boost::system::error_code const& error,
        std::string const& raw_result,
        std::list<
            resource_record
        > const& records,
        resolver_decoding_session::handling_a_result const& result_handler
    ) {
        std::list<
            a_resource_record
        > a_records;
        for (
            std::list<
                resource_record
            >::const_iterator transformed = records.begin(
            );
            records.end(
            ) != transformed;
            transformed++
        ) {
            if (
                a != boost::fusion::at_c<
                    resource_record_type
                >(
                    *transformed
                )
            ) {
                continue;
            }
            std::string const& ip_raw = boost::fusion::at_c<
                resource_record_data
            >(
                *transformed
            );
            if (
                4 != ip_raw.size(
                )
            ) {
                throw_exception(
                    component_error(
                        "resolver_decoding_session"
                    )
                );
            }
            a_records.push_back(
                a_resource_record(
                    boost::fusion::at_c<
                        0
                    >(
                        *transformed
                    ),
                    boost::fusion::at_c<
                        1
                    >(
                        *transformed
                    ),
                    boost::fusion::at_c<
                        2
                    >(
                        *transformed
                    ),
                    boost::fusion::at_c<
                        3
                    >(
                        *transformed
                    ),
                    raw_to_ipv4(
                        ip_raw
                    )
                )
            );
        }
        result_handler(
            error,
            raw_result,
            a_records
        );
    }
    
    void resolver_decoding_session::handle_resolve_aaaa(
        boost::system::error_code const& error,
        std::string const& raw_result,
        std::list<
            resource_record
        > const& records,
        resolver_decoding_session::handling_aaaa_result const& result_handler
    ) {
        std::list<
            aaaa_resource_record
        > aaaa_records;
        for (
            std::list<
                resource_record
            >::const_iterator transformed = records.begin(
            );
            records.end(
            ) != transformed;
            transformed++
        ) {
            std::string const& ip_raw = boost::fusion::at_c<
                4
            >(
                *transformed
            );
            if (
                16 != ip_raw.size(
                )
            ) {
                throw_exception(
                    component_error(
                        "resolver_decoding_session"
                    )
                );
            }
            aaaa_records.push_back(
                aaaa_resource_record(
                    boost::fusion::at_c<
                        0
                    >(
                        *transformed
                    ),
                    boost::fusion::at_c<
                        1
                    >(
                        *transformed
                    ),
                    boost::fusion::at_c<
                        2
                    >(
                        *transformed
                    ),
                    boost::fusion::at_c<
                        3
                    >(
                        *transformed
                    ),
                    raw_to_ipv6(
                        ip_raw
                    )
                )
            );
        }
        result_handler(
            error,
            raw_result,
            aaaa_records
        );
    }
    
    resolver_decoding_session::a_record_resolution_handler::a_record_resolution_handler(
        resolver_decoding_session* resolver,
        resolver_decoding_session::handling_a_result const& result_handler
    ) :
    resolver(
        resolver
    ),
    result_handler(
        result_handler
    ) {
    }
    
    void resolver_decoding_session::a_record_resolution_handler::operator(
    )(
        boost::system::error_code const& error,
        std::string const& raw_result,
        std::list<
            resource_record
        > const& records
    ) {
        resolver->handle_resolve_a(
            error,
            raw_result,
            records,
            result_handler
        );
    }
    
    resolver_decoding_session::mx_record_resolution_handler::mx_record_resolution_handler(
        resolver_decoding_session* resolver,
        resolver_decoding_session::handling_mx_result const& result_handler
    ) :
    resolver(
        resolver
    ),
    result_handler(
        result_handler
    ) {
    }
    
    void resolver_decoding_session::mx_record_resolution_handler::operator(
    )(
        boost::system::error_code const& error,
        std::string const& raw_result,
        std::list<
            resource_record
        > const& records
    ) {
        resolver->handle_resolve_mx(
            error,
            raw_result,
            records,
            result_handler
        );
    }
    
    resolver_decoding_session::ptr_record_resolution_handler::ptr_record_resolution_handler(
        resolver_decoding_session* resolver,
        resolver_decoding_session::handling_ptr_result const& result_handler
    ) :
    resolver(
        resolver
    ),
    result_handler(
        result_handler
    ) {
    }
    
    void resolver_decoding_session::ptr_record_resolution_handler::operator(
    )(
        boost::system::error_code const& error,
        std::string const& raw_result,
        std::list<
            resource_record
        > const& records
    ) {
        resolver->handle_resolve_ptr(
            error,
            raw_result,
            records,
            result_handler
        );
    }
    
    resolver_decoding_session::aaaa_record_resolution_handler::aaaa_record_resolution_handler(
        resolver_decoding_session* resolver,
        resolver_decoding_session::handling_aaaa_result const& result_handler
    ) :
    resolver(
        resolver
    ),
    result_handler(
        result_handler
    ) {
    }
    
    void resolver_decoding_session::aaaa_record_resolution_handler::operator(
    )(
        boost::system::error_code const& error,
        std::string const& raw_result,
        std::list<
            resource_record
        > const& records
    ) {
        resolver->handle_resolve_aaaa(
            error,
            raw_result,
            records,
            result_handler
        );
    }
}
