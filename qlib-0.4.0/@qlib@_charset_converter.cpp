/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_charset_converter.h"
#include <unistd.h>
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    charset_converter::charset_converter(
        std::string const& from,
        std::string const& to
    ) :
    data(
        iconv_open(
            to.c_str(
            ),
            from.c_str(
            )
        )
    ) {
        if (
            reinterpret_cast<
                iconv_t
            >(
                -1
            ) == data
        ) {
            throw component_error(
                "charset_converter"
            );
        }
    }
    
    charset_converter::~charset_converter(
    ) {
        iconv_close(
            data
        );
    }
    
    iconv_t charset_converter::get_data(
    ) const {
        return data;
    }
}
