/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef MEMORY_MAPPED_STRING_INCLUDED
#define MEMORY_MAPPED_STRING_INCLUDED

#include <string>
#include <boost/noncopyable.hpp>
namespace @qlib@ {
    
    class memory_mapped_string :
    private boost::noncopyable {
        template<
            typename generic_iterator
        >
        class iterator_traits {
        public:
            typedef typename generic_iterator::iterator_category iterator_category;
        };
        template<
            typename element
        >
        class iterator_traits<
            element*
        > {
        public:
            typedef void iterator_category;
        };
        template<
            typename element
        >
        class iterator_traits<
            element const*
        > {
        public:
            typedef void iterator_category;
        };
    public:
        memory_mapped_string(
            std::string const& filename
        );
        ~memory_mapped_string(
        );
        typedef size_t size_type;
        size_type size(
        ) const;
        char operator[
        ](
            size_type offset
        ) const;
        class iterator {
        public:
            typedef iterator_traits<
                std::string::iterator
            >::iterator_category iterator_category;
            typedef int difference_type;
            typedef char value_type;
            typedef char* pointer;
            typedef char& reference;
            iterator(
            );
            iterator(
                char* position
            );
            char& operator*(
            );
            bool operator<(
                iterator const& other
            ) const;
            bool operator!=(
                iterator const& other
            ) const;
            bool operator==(
                iterator const& other
            ) const;
            iterator& operator++(
            );
            iterator operator+(
                difference_type offset
            ) const;
            difference_type operator-(
                iterator other
            ) const;
        private:
            char* position;
        };
        iterator begin(
        );
        iterator end(
        );
        class const_iterator {
        public:
            typedef iterator_traits<
                std::string::const_iterator
            >::iterator_category iterator_category;
            typedef int difference_type;
            typedef char const value_type;
            typedef char const* pointer;
            typedef char const& reference;
            const_iterator(
            );
            const_iterator(
                char const* position
            );
            char const& operator*(
            );
            bool operator<(
                const_iterator const& other
            ) const;
            bool operator!=(
                const_iterator const& other
            ) const;
            bool operator==(
                const_iterator const& other
            ) const;
            const_iterator& operator++(
            );
            const_iterator operator+(
                difference_type offset
            ) const;
            difference_type operator-(
                const_iterator other
            ) const;
        private:
            char const* position;
        };
        const_iterator begin(
        ) const;
        const_iterator end(
        ) const;
    private:
        int file;
        char* data;
        size_type length;
        size_type mapped_length;
    };
    template<
    >
    char* copy(
        @qlib@::memory_mapped_string::const_iterator first,
        @qlib@::memory_mapped_string::const_iterator last,
        char* result
    );
}

#endif
