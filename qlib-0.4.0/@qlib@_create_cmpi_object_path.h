/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef CREATE_CMPI_OBJECT_PATH_INCLUDED
#define CREATE_CMPI_OBJECT_PATH_INCLUDED

#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    template<
        typename managed
    >
    CMPIObjectPath* create_cmpi_object_path(
        CMPIBroker const* broker,
        CMPIStatus& result,
        char const* cim_namespace,
        managed* instantiated
    ) {
        CMPIObjectPath* constructed = CMNewObjectPath(
            broker,
            cim_namespace,
            instantiated->get_class_name(
            ).c_str(
            ),
            &result
        );
        if (
            (
                CMPI_RC_OK != result.rc
            ) || CMIsNullObject(
                constructed
            )
        ) {
            throw component_error(
                "create_cmpi_object_path"
            );
        }
        set_cmpi_property(
            broker,
            constructed,
            instantiated->get_key_name(
            ),
            instantiated->get_key(
            )
        );
        return constructed;
    }
}

#endif
