/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef CMPI_PROPERTY_SETTER_INCLUDED
#define CMPI_PROPERTY_SETTER_INCLUDED

#include <string>
namespace @qlib@ {
    
    
    class cmpi_property_setter {
    public:
        cmpi_property_setter(
            CMPIBroker const* broker,
            CMPIInstance* augmented
        );
        template<
            typename describing
        >
        void visit_property(
            std::string const& name,
            describing value
        );
    private:
        CMPIBroker const* broker;
        CMPIInstance* augmented;
    };
    
    template<
        typename describing
    >
    void cmpi_property_setter::visit_property(
        std::string const& name,
        describing value
    ) {
        set_cmpi_property(
            broker,
            augmented,
            name,
            value
        );
    }
}

#endif
