/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_database_access_configuration.h"
namespace @qlib@ {
    
    
    void database_access_configuration::set_database(
        std::string const& hostname,
        std::string const& database,
        std::string const& username,
        std::string const& password
    ) {
        this->hostname = hostname;
        this->database = database;
        this->username = username;
        this->password = password;
    }
    
    std::string const& database_access_configuration::get_database_hostname(
    ) const {
        return hostname;
    }
    
    std::string const& database_access_configuration::get_database_database(
    ) const {
        return database;
    }
    
    std::string const& database_access_configuration::get_database_username(
    ) const {
        return username;
    }
    
    std::string const& database_access_configuration::get_database_password(
    ) const {
        return password;
    }
}
