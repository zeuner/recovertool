/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef HEX_STRING_INCLUDED
#define HEX_STRING_INCLUDED

#include "@qlib@_c_string.h"
namespace @qlib@ {
    
    
    template<
        typename string
    >
    string hex_string(
        string const& raw
    );
    
    template<
        typename character
    >
    std::basic_string<
        character
    > hex_string(
        character const* raw
    );
    
    template<
        typename string
    >
    string hex_string(
        string const& raw
    ) {
        string result;
        for (
            typename string::const_iterator processing = raw.begin(
            );
            raw.end(
            ) != processing;
            processing++
        ) {
            static char const hex[
            ] = "0123456789ABCDEF";
            unsigned char const indexing = *processing;
            result += hex[
                indexing >> 4
            ];
            result += hex[
                0xf & indexing
            ];
        }
        return result;
    }
    
    template<
        typename character
    >
    std::basic_string<
        character
    > hex_string(
        character const* raw
    ) {
        std::basic_string<
            character
        > result;
        for (
            character const* processing = raw;
            *processing;
            processing++
        ) {
            char hex[
            ] = "0123456789ABCDEF";
            unsigned char const indexing = *processing;
            result += hex[
                indexing >> 4
            ];
            result += hex[
                0xf & indexing
            ];
        }
        return result;
    }
}

#endif
