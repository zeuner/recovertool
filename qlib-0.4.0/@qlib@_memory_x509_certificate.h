/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef MEMORY_X509_CERTIFICATE_INCLUDED
#define MEMORY_X509_CERTIFICATE_INCLUDED

#include <string>
#include "@qlib@_c_string.h"
namespace @qlib@ {
    
    
    class memory_x509_certificate {
    public:
        memory_x509_certificate(
            std::string const& input
        );
        ~memory_x509_certificate(
        );
        X509* get_data(
        ) const;
    private:
        c_string password;
        X509* data;
    };
}

#endif
