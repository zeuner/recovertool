/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_piped_process.h"
#include "@qlib@_component_error.h"
#include <cstring>
#include <cerrno>
#include <unistd.h>
#include "@qlib@_c_buffer.h"
#include <fcntl.h>
#include "@qlib@_c_string.h"
namespace @qlib@ {
    
    
    piped_process::piped_process(
        std::vector<
            std::string
        > const& command_line
    ) {
        if (
            command_line.empty(
            )
        ) {
            throw component_error(
                "piped_process"
            );
        }
        int input[
            2
        ];
        if (
            0 > pipe(
                input
            )
        ) {
            throw component_error(
                "piped_process"
            );
        }
        int output[
            2
        ];
        if (
            0 > pipe(
                output
            )
        ) {
            throw component_error(
                "piped_process"
            );
        }
        child = fork(
        );
        if (
            0 > child
        ) {
            throw component_error(
                "piped_process"
            );
        }
        if (
            0 < child
        ) {
            process_input = input[
                1
            ];
            process_output = output[
                0
            ];
            close(
                input[
                    0
                ]
            );
            close(
                output[
                    1
                ]
            );
            if (
                0 > fcntl(
                    process_output,
                    F_SETFL,
                    fcntl(
                        process_output,
                        F_GETFL
                    ) | O_NONBLOCK
                )
            ) {
                throw component_error(
                    "piped_process"
                );
            }
        } else {
            close(
                input[
                    1
                ]
            );
            close(
                output[
                    0
                ]
            );
            if (
                0 > dup2(
                    input[
                        0
                    ],
                    0
                )
            ) {
                throw component_error(
                    "piped_process"
                );
            }
            close(
                input[
                    0
                ]
            );
            if (
                0 > dup2(
                    output[
                        1
                    ],
                    1
                )
            ) {
                throw component_error(
                    "piped_process"
                );
            }
            close(
                output[
                    1
                ]
            );
            c_string path(
                "PATH=/usr/bin:/bin"
            );
            char* environment[
                2
            ];
            environment[
                0
            ] = path.get_data(
            );
            environment[
                1
            ] = NULL;
            environ = environment;
            std::vector<
                c_buffer
            > argument_buffers(
                command_line.size(
                )
            );
            char* arguments[
                command_line.size(
                ) + 1
            ];
            arguments[
                command_line.size(
                )
            ] = NULL;
            for (
                size_t initializing = 0;
                command_line.size(
                ) > initializing;
                initializing++
            ) {
                argument_buffers[
                    initializing
                ].resize(
                    command_line[
                        initializing
                    ].size(
                    ) + 1
                );
                memcpy(
                    argument_buffers[
                        initializing
                    ].get_data(
                    ),
                    command_line[
                        initializing
                    ].c_str(
                    ),
                    command_line[
                        initializing
                    ].size(
                    ) + 1
                );
                arguments[
                    initializing
                ] = argument_buffers[
                    initializing
                ].get_data(
                );
            }
            execvp(
                arguments[
                    0
                ],
                arguments
            );
            throw component_error(
                "piped_process"
            );
        }
    }
    
    piped_process::~piped_process(
    ) {
        close(
            process_input
        );
        close(
            process_output
        );
        kill(
            child,
            SIGTERM
        );
        waitpid(
            child,
            NULL,
            0
        );
    }
    
    int piped_process::get_input(
        char* buffer,
        size_t size
    ) {
        int const result = read(
            process_output,
            buffer,
            size
        );
        if (
            0 < result
        ) {
            return result;
        }
        if (
            0 == result
        ) {
            return -1;
        }
        if (
            EAGAIN == errno
        ) {
            return 0;
        } else {
            return -1;
        }
    }
    
    int piped_process::put_output(
        char const* buffer,
        size_t size
    ) {
        return write(
            process_input,
            buffer,
            size
        );
    }
}
