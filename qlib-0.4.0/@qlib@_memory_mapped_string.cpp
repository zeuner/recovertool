/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_memory_mapped_string.h"
#include <unistd.h>
#include "@qlib@_throw_exception.h"
#include "@qlib@_component_error.h"
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <cstring>
namespace @qlib@ {
    
    memory_mapped_string::memory_mapped_string(
        std::string const& filename
    ) :
    file(
        open(
            filename.c_str(
            ),
            O_RDONLY
        )
    ) {
        if (
            0 > file
        ) {
            throw_exception(
                component_error(
                    "memory_mapped_string"
                )
            );
        }
        struct stat attributes;
        if (
            0 > fstat(
                file,
                &attributes
            )
        ) {
            close(
                file
            );
            throw_exception(
                component_error(
                    "memory_mapped_string"
                )
            );
        }
        length = attributes.st_size;
        off_t const page_size = sysconf(
            _SC_PAGESIZE
        );
        mapped_length = page_size * (
            (
                length - 1
            ) / page_size + 1
        );
        void* mapped = mmap(
            NULL,
            mapped_length,
            PROT_READ,
            MAP_SHARED,
            file,
            0
        );
        if (
            MAP_FAILED == mapped
        ) {
            close(
                file
            );
            throw_exception(
                component_error(
                    "memory_mapped_string"
                )
            );
        }
        data = static_cast<
            char*
        >(
            mapped
        );
    }
    memory_mapped_string::~memory_mapped_string(
    ) {
        munmap(
            data,
            mapped_length
        );
    }
    memory_mapped_string::size_type memory_mapped_string::size(
    ) const {
        return length;
    }
    char memory_mapped_string::operator[
    ](
        size_type offset
    ) const {
        return data[
            offset
        ];
    }
    memory_mapped_string::iterator::iterator(
    ) :
    position(
        NULL
    ) {
    }
    memory_mapped_string::iterator::iterator(
        char* position
    ) :
    position(
        position
    ) {
    }
    char& memory_mapped_string::iterator::operator*(
    ) {
        return *position;
    }
    bool memory_mapped_string::iterator::operator<(
        memory_mapped_string::iterator const& other
    ) const {
        return position < other.position;
    }
    bool memory_mapped_string::iterator::operator!=(
        memory_mapped_string::iterator const& other
    ) const {
        return position != other.position;
    }
    bool memory_mapped_string::iterator::operator==(
        memory_mapped_string::iterator const& other
    ) const {
        return position == other.position;
    }
    memory_mapped_string::iterator& memory_mapped_string::iterator::operator++(
    ) {
        ++position;
        return *this;
    }
    memory_mapped_string::iterator memory_mapped_string::iterator::operator+(
        difference_type offset
    ) const {
        return iterator(
            position + offset
        );
    }
    memory_mapped_string::iterator::difference_type memory_mapped_string::iterator::operator-(
        iterator other
    ) const {
        return position - other.position;
    }
    memory_mapped_string::iterator memory_mapped_string::begin(
    ) {
        return iterator(
            data
        );
    }
    memory_mapped_string::iterator memory_mapped_string::end(
    ) {
        return iterator(
            data + length
        );
    }
    memory_mapped_string::const_iterator::const_iterator(
    ) :
    position(
        NULL
    ) {
    }
    memory_mapped_string::const_iterator::const_iterator(
        char const* position
    ) :
    position(
        position
    ) {
    }
    char const& memory_mapped_string::const_iterator::operator*(
    ) {
        return *position;
    }
    bool memory_mapped_string::const_iterator::operator<(
        memory_mapped_string::const_iterator const& other
    ) const {
        return position < other.position;
    }
    bool memory_mapped_string::const_iterator::operator!=(
        memory_mapped_string::const_iterator const& other
    ) const {
        return position != other.position;
    }
    bool memory_mapped_string::const_iterator::operator==(
        memory_mapped_string::const_iterator const& other
    ) const {
        return position == other.position;
    }
    memory_mapped_string::const_iterator& memory_mapped_string::const_iterator::operator++(
    ) {
        ++position;
        return *this;
    }
    memory_mapped_string::const_iterator memory_mapped_string::const_iterator::operator+(
        difference_type offset
    ) const {
        return const_iterator(
            position + offset
        );
    }
    memory_mapped_string::const_iterator::difference_type memory_mapped_string::const_iterator::operator-(
        const_iterator other
    ) const {
        return position - other.position;
    }
    memory_mapped_string::const_iterator memory_mapped_string::begin(
    ) const {
        return const_iterator(
            data
        );
    }
    memory_mapped_string::const_iterator memory_mapped_string::end(
    ) const {
        return const_iterator(
            data + length
        );
    }
    template<
    >
    char* copy(
        @qlib@::memory_mapped_string::const_iterator first,
        @qlib@::memory_mapped_string::const_iterator last,
        char* result
    ) {
        @qlib@::memory_mapped_string::const_iterator::difference_type const length(
            last - first
        );
        memcpy(
            result,
            &(
                *first
            ),
            length
        );
        return result + length;
    }
}
