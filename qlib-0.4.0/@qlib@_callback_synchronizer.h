/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef CALLBACK_SYNCHRONIZER_INCLUDED
#define CALLBACK_SYNCHRONIZER_INCLUDED

#include <boost/shared_ptr.hpp>
#include <boost/noncopyable.hpp>
namespace @qlib@ {
    
    
    class callback_synchronizer :
    private boost::noncopyable {
    public:
        callback_synchronizer(
            long callbacks = 1
        );
        void await_callback(
        );
        void call_back(
        );
        void call_back_final(
        );
    private:
        boost::mutex synchronizing;
        boost::shared_ptr<
            boost::unique_lock<
                boost::mutex
            >
        > callback_pending;
        boost::detail::atomic_count callbacks;
    };
}

#endif
