/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_encoded_mac_address.h"
#include "@qlib@_throw_exception.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    std::string encoded_mac_address(
        std::string const& decoded
    ) {
        std::string value;
        static std::string const hex(
            "0123456789ABCDEF"
        );
        for (
            std::string::const_iterator extracting = decoded.begin(
            );
            ;
        ) {
            if (
                decoded.end(
                ) == extracting
            ) {
                throw_exception(
                    component_error(
                        "encoded_mac_address"
                    )
                );
            }
            std::string::size_type upper_position = hex.find(
                toupper(
                    *extracting
                )
            );
            if (
                std::string::npos == upper_position
            ) {
                throw_exception(
                    component_error(
                        "encoded_mac_address"
                    )
                );
            }
            extracting++;
            if (
                decoded.end(
                ) == extracting
            ) {
                throw_exception(
                    component_error(
                        "encoded_mac_address"
                    )
                );
            }
            std::string::size_type lower_position = hex.find(
                toupper(
                    *extracting
                )
            );
            if (
                std::string::npos == lower_position
            ) {
                throw_exception(
                    component_error(
                        "encoded_mac_address"
                    )
                );
            }
            value += char(
                (
                    upper_position << 4
                ) + lower_position
            );
            extracting++;
            if (
                decoded.end(
                ) == extracting
            ) {
                break;
            }
            if (
                ':' != *extracting
            ) {
                throw_exception(
                    component_error(
                        "encoded_mac_address"
                    )
                );
            }
            extracting++;
        }
        return value;
    }
}
