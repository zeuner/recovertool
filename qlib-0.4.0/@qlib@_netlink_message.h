/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef NETLINK_MESSAGE_INCLUDED
#define NETLINK_MESSAGE_INCLUDED

#include "@qlib@_c_buffer.h"
#include <list>
namespace @qlib@ {
    
    
    class netlink_message {
    public:
        netlink_message(
            uint16_t command,
            uint16_t version,
            uint16_t flags,
            uint32_t sequence_number,
            std::list<
                netlink_attribute
            > const& attributes
        );
        ~netlink_message(
        );
        struct nlmsghdr* get_data(
        );
        struct nlmsghdr const* get_data(
        ) const;
    private:
        c_buffer* data;
    };
}

#endif
