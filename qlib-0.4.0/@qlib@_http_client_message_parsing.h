/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef HTTP_CLIENT_MESSAGE_PARSING_INCLUDED
#define HTTP_CLIENT_MESSAGE_PARSING_INCLUDED

#include <unistd.h>
#include <map>
#include <boost/bind.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <vector>
#include <list>
#include <string>
namespace @qlib@ {
    
    
    template<
        typename transmitting = boost::asio::ip::tcp::socket,
        typename allocating = std::allocator<
            char
        >
    >
    class http_client_message_parsing;
    
    template<
        typename transmitting,
        typename allocating
    >
    class http_client_message_parsing {
    public:
        http_client_message_parsing(
        );
        bool operator(
        )(
            typename http_server_acceptor<
                transmitting,
                allocating
            >::request_type* request,
            typename http_server_acceptor<
                transmitting,
                allocating
            >::string& to_parse,
            boost::system::error_code& error
        );
    private:
        typename http_server_acceptor<
            transmitting,
            allocating
        >::string resource;
        typename http_server_acceptor<
            transmitting,
            allocating
        >::string data;
        typename http_server_acceptor<
            transmitting,
            allocating
        >::string header_key;
        typename http_server_acceptor<
            transmitting,
            allocating
        >::string header_value;
        std::map<
            typename http_server_acceptor<
                transmitting,
                allocating
            >::string,
            typename http_server_acceptor<
                transmitting,
                allocating
            >::string
        > headers;
        enum http_server_acceptor<
            transmitting,
            allocating
        >::state_type state;
        enum http_server_acceptor<
            transmitting,
            allocating
        >::accept_continuation_type accept_continuation;
        typename http_server_acceptor<
            transmitting,
            allocating
        >::string::size_type data_required;
        std::vector<
            std::string
        >::const_iterator checked_method;
        std::string::const_iterator matched;
        std::list<
            typename http_server_acceptor<
                transmitting,
                allocating
            >::request_type*
        > unresponded;
    };
    
    template<
        typename transmitting,
        typename allocating
    >
    http_client_message_parsing<
        transmitting,
        allocating
    >::http_client_message_parsing(
    ) :
    state(
        http_server_acceptor<
            transmitting,
            allocating
        >::reading_method
    ),
    accept_continuation(
        http_server_acceptor<
            transmitting,
            allocating
        >::no_operation
    ),
    data_required(
        0
    ),
    checked_method(
        http_server_acceptor<
            transmitting,
            allocating
        >::get_methods(
        ).begin(
        )
    ),
    matched(
        checked_method->begin(
        )
    ) {
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    bool http_client_message_parsing<
        transmitting,
        allocating
    >::operator(
    )(
        typename http_server_acceptor<
            transmitting,
            allocating
        >::request_type* request,
        typename http_server_acceptor<
            transmitting,
            allocating
        >::string& to_parse,
        boost::system::error_code& error
    ) {
        http_stub_peer_handlers handlers;
        bool const result = http_server_acceptor<
            transmitting,
            allocating
        >::parse_http(
            request,
            to_parse,
            resource,
            data,
            header_key,
            header_value,
            headers,
            boost::bind(
                &http_stub_peer_handlers::close_link,
                &handlers
            ),
            boost::bind(
                &http_stub_peer_handlers::invoke_accept_handler,
                &handlers,
                _1
            ),
            boost::bind(
                &http_stub_peer_handlers::invoke_deferred_response_handler,
                &handlers
            ),
            state,
            accept_continuation,
            data_required,
            matched,
            checked_method,
            unresponded,
            0x400
        );
        error = handlers.get_error(
        );
        return result;
    }
}

#endif
