/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef JSON_PARSER_INCLUDED
#define JSON_PARSER_INCLUDED

#include <string>
#include <vector>
#include <map>
namespace @qlib@ {
    
    
    template<
        typename iterator
    >
    class json_parser :
    public boost::spirit::qi::grammar<
        iterator,
        json_object(
        )
    > {
    public:
        json_parser(
        );
    private:
        boost::spirit::qi::rule<
            iterator,
            json_object(
            )
        > object;
        boost::spirit::qi::rule<
            iterator,
            std::map<
                std::string,
                json_object
            >(
            )
        > slotted_object;
        boost::spirit::qi::rule<
            iterator,
            std::vector<
                json_object
            >(
            )
        > object_array;
        boost::spirit::qi::rule<
            iterator,
            double(
            )
        > number;
        boost::spirit::qi::rule<
            iterator,
            std::string(
            )
        > string;
        boost::spirit::qi::rule<
            iterator,
            null_policy(
            )
        > null;
        boost::spirit::qi::rule<
            iterator,
            bool(
            )
        > boolean;
    };
    
    template<
        typename iterator
    >
    json_parser<
        iterator
    >::json_parser(
    ) :
    json_parser<
        iterator
    >::base_type(
        object
    ) {
        object = (
            number | object_array | string | null | boolean | slotted_object
        )[
            boost::spirit::_val = boost::spirit::_1 
        ];
        number %= boost::spirit::qi::double_;
        null = boost::spirit::qi::eps >> "null";
        boolean = (
            boost::spirit::qi::eps >> "true"
        )[
            boost::spirit::_val = true
        ] | (
            boost::spirit::qi::eps >> "false"
        )[
            boost::spirit::_val = false
        ];
        string = '"' >> *(
            (
                boost::spirit::ascii::char_ - '\\' - '\n' - '"'
            )[
                boost::spirit::_val += boost::spirit::_1
            ] | (
                boost::spirit::qi::eps >> "\\\\"
            )[
                boost::spirit::_val += '\\'
            ] | (
                boost::spirit::qi::eps >> "\\n"
            )[
                boost::spirit::_val += '\n'
            ] | (
                boost::spirit::qi::eps >> "\\\""
            )[
                boost::spirit::_val += '"'
            ]
        ) >> '"';
        slotted_object = "{" >> (
            (
                (
                    string >> ":" >> object
                )[
                    boost::phoenix::bind(
                        &std::map<
                            std::string,
                            json_object
                        >::operator[
                        ],
                        boost::spirit::_val,
                        boost::spirit::_1
                    ) = boost::spirit::_2
                ] >> *(
                    "," >> (
                        string >> ":" >> object
                    )[
                        boost::phoenix::bind(
                            &std::map<
                                std::string,
                                json_object
                            >::operator[
                            ],
                            boost::spirit::_val,
                            boost::spirit::_1
                        ) = boost::spirit::_2
                    ]
                )
            ) | boost::spirit::qi::eps
        ) >> "}";
        object_array = "[" >> (
            (
                object[
                    boost::phoenix::bind(
                        &std::vector<
                            json_object
                        >::push_back,
                        boost::spirit::_val,
                        boost::spirit::_1
                    )
                ] >> *(
                    "," >> object[
                        boost::phoenix::bind(
                            &std::vector<
                                json_object
                            >::push_back,
                            boost::spirit::_val,
                            boost::spirit::_1
                        )
                    ]
                )
            ) | boost::spirit::qi::eps
        ) >> "]";
    }
}

#endif
