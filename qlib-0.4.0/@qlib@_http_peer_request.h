/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef HTTP_PEER_REQUEST_INCLUDED
#define HTTP_PEER_REQUEST_INCLUDED

#include <string>
#include "@qlib@_http_peer_message.h"
namespace @qlib@ {
    
    
    template<
        typename transmitting
    >
    class http_peer_request :
    public http_peer_message<
        transmitting
    > {
    public:
        http_peer_request(
            transmitting& transmitter
        );
        void set_method(
            std::string const& method
        );
        std::string const& get_method(
        ) const;
        typedef typename transmitting::response_type response_type;
        response_type* create_response(
        );
    private:
        std::string method;
    };
    
    template<
        typename transmitting
    >
    http_peer_request<
        transmitting
    >::http_peer_request(
        transmitting& transmitter
    ) :
    http_peer_message<
        transmitting
    >(
        transmitter
    ) {
    }
    
    template<
        typename transmitting
    >
    void http_peer_request<
        transmitting
    >::set_method(
        std::string const& method
    ) {
        this->method = method;
    }
    
    template<
        typename transmitting
    >
    std::string const& http_peer_request<
        transmitting
    >::get_method(
    ) const {
        return method;
    }
    
    template<
        typename transmitting
    >
    typename http_peer_request<
        transmitting
    >::response_type* http_peer_request<
        transmitting
    >::create_response(
    ) {
        typename http_peer_request<
        transmitting
    >::response_type* response = new response_type(
            this->get_transmitter(
            )
        );
        response->set_finished(
            false
        );
        return response;
    }
}

#endif
