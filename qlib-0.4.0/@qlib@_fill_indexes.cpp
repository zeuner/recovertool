/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_fill_indexes.h"
namespace @qlib@ {
    
    
    void fill_indexes(
        std::map<
            std::string,
            std::string
        > const& indexes,
        database_connection& database
    ) {
        std::map<
            std::string,
            std::string
        > missing_indexes = indexes;
        do {
            database_result indexes(
                database,
                "SELECT indexname\n"
                "    FROM pg_indexes"
            );
            for (
                database_result::row_type current = indexes.fetch_row(
                );
                current;
                current = indexes.fetch_row(
                )
            ) {
                missing_indexes.erase(
                    current[
                        0
                    ]
                );
            }
        } while (
            false
        );
        for (
            std::map<
                std::string,
                std::string
            >::const_iterator created = missing_indexes.begin(
            );
            missing_indexes.end(
            ) != created;
            created++
        ) {
            database_push(
                database,
                created->second
            );
        }
    }
}
