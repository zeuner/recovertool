/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef HTTP_POST_REQUEST_INCLUDED
#define HTTP_POST_REQUEST_INCLUDED

#include <string>
#include "@qlib@_stringified.h"
#include <unistd.h>
namespace @qlib@ {
    
    
    template<
        typename socket
    >
    class http_post_request :
    public stream_one_shot_request<
        socket
    > {
    public:
        template<
            typename initializing
        >
        http_post_request(
            initializing& initializer
        );
        template<
            typename initializing1,
            typename initializing2
        >
        http_post_request(
            initializing1& initializer1,
            initializing2& initializer2
        );
        template<
            typename handling
        >
        void async_request(
            std::string const& host,
            unsigned short port,
            std::string const& resource,
            std::string const& data,
            std::string const& agent,
            handling handler,
            std::string const& content_type = std::string(
                "application/x-www-form-urlencoded"
            )
        );
    };
    
    template<
        typename socket
    >
    template<
        typename initializing
    >
    http_post_request<
        socket
    >::http_post_request(
        initializing& initializer
    ) :
    stream_one_shot_request<
        socket
    >(
        initializer
    ) {
    }
    
    template<
        typename socket
    >
    template<
        typename initializing1,
        typename initializing2
    >
    http_post_request<
        socket
    >::http_post_request(
        initializing1& initializer1,
        initializing2& initializer2
    ) :
    stream_one_shot_request<
        socket
    >(
        initializer1,
        initializer2
    ) {
    }
    
    template<
        typename socket
    >
    template<
        typename handling
    >
    void http_post_request<
        socket
    >::async_request(
        std::string const& host,
        unsigned short port,
        std::string const& resource,
        std::string const& data,
        std::string const& agent,
        handling handler,
        std::string const& content_type
    ) {
        stream_one_shot_request<
            socket
        >::async_request(
            host,
            port,
            "POST " + resource + " HTTP/1.0\r\n"
            "Host: " + host + "\r\n"
            "User-Agent: " + agent + "\r\n"
            "Content-Length: " + stringified(
                data.size(
                )
            ) + "\r\n"
            "Content-Type: " + content_type + "\r\n"
            "Connection: close\r\n"
            "\r\n"
            "" + data,
            handler
        );
    }
}

#endif
