/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef ITERATE_XML_PATH_INCLUDED
#define ITERATE_XML_PATH_INCLUDED

namespace @qlib@ {
    
    
    template<
        typename iterating_path,
        typename iterating_output
    >
    void iterate_xml_path(
        xmlNodePtr parent,
        iterating_path path_begin,
        iterating_path path_end,
        iterating_output* output
    ) {
        if (
            path_end == path_begin
        ) {
            **output = parent;
            (
                *output
            )++;
        } else {
            iterating_path path_next = path_begin;
            path_next++;
            for (
                xmlNodePtr child = parent->children;
                child;
                child = child->next
            ) {
                if (
                    XML_ELEMENT_NODE != child->type
                ) {
                    continue;
                }
                if (
                    NULL == child->ns
                ) {
                    continue;
                }
                if (
                    path_begin->first != child->ns->href
                ) {
                    continue;
                }
                if (
                    path_begin->second != child->name
                ) {
                    continue;
                }
                iterate_xml_path(
                    child,
                    path_next,
                    path_end,
                    output
                );
            }
        }
    }
}

#endif
