/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef IP_LAYER_LINK_INCLUDED
#define IP_LAYER_LINK_INCLUDED

namespace @qlib@ {
    
    
    class ip_layer_link {
    public:
        ip_layer_link(
            in_addr_t source_ip,
            in_addr_t destination_ip,
            uint8_t transport_protocol
        );
        typedef struct iphdr native_type;
        void write_header(
            native_type& written,
            size_t payload_size
        );
        in_addr_t get_source_ip(
        ) const;
        in_addr_t get_destination_ip(
        ) const;
        static uint16_t get_protocol(
        );
    private:
        static integer_random_source choose_short;
        native_type native_header;
        uint16_t identification;
    };
}

#endif
