/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef PITCH_CONTROLLED_SOURCE_INCLUDED
#define PITCH_CONTROLLED_SOURCE_INCLUDED

#include <boost/noncopyable.hpp>
#include <cstring>
namespace @qlib@ {
    
    
    template<
        typename playable,
        typename pitching
    >
    class pitch_controlled_source :
    private boost::noncopyable {
    public:
        pitch_controlled_source(
            playable& source,
            pitching& pitch_source
        );
        size_t get_samples(
            amplitude_value* first,
            amplitude_value* last
        );
        bool is_master(
        ) const;
        bool get_sample(
            amplitude_value& retrieved
        );
    protected:
        void resize_samples(
            size_t new_size
        );
        void resize_pitches(
            size_t new_size
        );
    private:
        playable& source;
        pitching& pitch_source;
        time_value source_sample_position;
        resizable_buffer<
            malloc_c_allocator,
            amplitude_value
        > samples;
        size_t available_source_samples;
        amplitude_value* samples_end;
        amplitude_value* current_source_sample;
        amplitude_value* next_source_sample;
        resizable_buffer<
            malloc_c_allocator,
            time_value
        > pitches;
        time_value* current_pitch;
        time_value* available_pitches_end;
        time_value* pitches_end;
    };
    
    template<
        typename playable,
        typename pitching
    >
    class bulk_audio_retrieval<
        pitch_controlled_source<
            playable,
            pitching
        >
    > {
    public:
        static size_t get_samples(
            pitch_controlled_source<
                playable,
                pitching
            >& source,
            amplitude_value* first,
            amplitude_value* last
        );
    };
    
    template<
        typename playable,
        typename pitching
    >
    pitch_controlled_source<
        playable,
        pitching
    >::pitch_controlled_source(
        playable& source,
        pitching& pitch_source
    ) :
    source(
        source
    ),
    pitch_source(
        pitch_source
    ),
    source_sample_position(
        0
    ),
    samples(
        2
    ),
    available_source_samples(
        0
    ),
    samples_end(
        samples.get_data(
        ) + samples.get_size(
        )
    ),
    current_source_sample(
        samples.get_data(
        )
    ),
    next_source_sample(
        samples.get_data(
        )
    ),
    pitches(
        1
    ),
    current_pitch(
        pitches.get_data(
        )
    ),
    available_pitches_end(
        pitches.get_data(
        )
    ),
    pitches_end(
        pitches.get_data(
        ) + pitches.get_size(
        )
    ) {
    }
    
    template<
        typename playable,
        typename pitching
    >
    size_t pitch_controlled_source<
        playable,
        pitching
    >::get_samples(
        amplitude_value* first,
        amplitude_value* last
    ) {
        bool pitches_available = true;
        bool samples_available = true;
        size_t supplied = 0;
        do {
            size_t const pitches_required = last - first;
            if (
                pitches_required > pitches.get_size(
                )
            ) {
                resize_pitches(
                    pitches_required
                );
            }
            size_t const available_pitches = available_pitches_end - current_pitch;
            if (
                available_pitches < pitches_required
            ) {
                size_t const with_available = pitches_required - available_pitches;
                time_value* retrieval_end(
                    available_pitches_end + with_available
                );
                if (
                    pitches_end < retrieval_end
                ) {
                    retrieval_end = pitches_end;
                }
                if (
                    retrieval_end > available_pitches_end
                ) {
                    size_t const new_pitches = bulk_audio_retrieval<
                        pitching,
                        time_value
                    >::get_samples(
                        pitch_source,
                        available_pitches_end,
                        retrieval_end
                    );
                    if (
                        0 == new_pitches
                    ) {
                        pitches_available = false;
                    }
                    available_pitches_end += new_pitches;
                }
            }
            if (
                current_pitch >= available_pitches_end
            ) {
                break;
            }
            size_t first_required_sample = floor(
                source_sample_position
            );
            size_t const last_required_sample = floor(
                source_sample_position + (
                    (
                        *current_pitch
                    ) * (
                        last - first - 1
                    )
                )
            ) + 1;
            size_t samples_required(
                last_required_sample - first_required_sample + 1
            );
            if (
                samples_required > samples.get_size(
                )
            ) {
                resize_samples(
                    samples_required
                );
            }
            if (
                0 < first_required_sample
            ) {
                size_t shift = first_required_sample;
                if (
                    available_source_samples < shift
                ) {
                    shift = available_source_samples;
                }
                first_required_sample -= shift;
                available_source_samples -= shift;
                current_source_sample += shift;
                if (
                    samples_end <= current_source_sample
                ) {
                    current_source_sample -= samples.get_size(
                    );
                }
                source_sample_position -= shift;
            }
            if (
                available_source_samples < samples_required
            ) {
                samples_required -= available_source_samples;
                amplitude_value* last_from_source(
                    next_source_sample + samples_required
                );
                if (
                    last_from_source > samples_end
                ) {
                    last_from_source = samples_end;
                }
                size_t const first_run = bulk_audio_retrieval<
                    playable
                >::get_samples(
                    source,
                    next_source_sample,
                    last_from_source
                );
                if (
                    0 == first_run
                ) {
                    samples_available = false;
                }
                available_source_samples += first_run;
                samples_required -= first_run;
                next_source_sample += first_run;
                if (
                    samples_end == next_source_sample
                ) {
                    next_source_sample = samples.get_data(
                    );
                    if (
                        0 < samples_required
                    ) {
                        size_t const second_run = bulk_audio_retrieval<
                            playable
                        >::get_samples(
                            source,
                            next_source_sample,
                            next_source_sample + samples_required
                        );
                        available_source_samples += second_run;
                        samples_required -= second_run;
                        next_source_sample += second_run;
                    }
                }
            }
            do {
                if (
                    available_source_samples <= (
                        first_required_sample + 1
                    )
                ) {
                    break;
                }
                available_source_samples -= first_required_sample;
                current_source_sample += first_required_sample;
                if (
                    samples_end <= current_source_sample
                ) {
                    current_source_sample -= samples.get_size(
                    );
                }
                source_sample_position -= first_required_sample;
                amplitude_value* following_source_sample = current_source_sample;
                following_source_sample++;
                if (
                    samples_end == following_source_sample
                ) {
                    following_source_sample = samples.get_data(
                    );
                }
                *first = (
                    *current_source_sample
                ) + source_sample_position * (
                    (
                        *following_source_sample
                    ) - (
                        *current_source_sample
                    )
                );
                first++;
                supplied++;
                source_sample_position += *current_pitch;
                current_pitch++;
                if (
                    available_pitches_end == current_pitch
                ) {
                    break;
                }
                if (
                    last == first
                ) {
                    break;
                }
                first_required_sample = floor(
                    source_sample_position
                );
            } while (
                true
            );
            if (
                pitches_end == current_pitch
            ) {
                current_pitch = pitches.get_data(
                );
                available_pitches_end = pitches.get_data(
                );
            }
        } while (
            (
                last > first
            ) && (
                0 == supplied
            ) && pitches_available && samples_available
        );
        return supplied;
    }
    
    template<
        typename playable,
        typename pitching
    >
    bool pitch_controlled_source<
        playable,
        pitching
    >::is_master(
    ) const {
        return true;
    }
    
    template<
        typename playable,
        typename pitching
    >
    bool pitch_controlled_source<
        playable,
        pitching
    >::get_sample(
        amplitude_value& retrieved
    ) {
        return 0 < get_samples(
            &retrieved,
            (
                &retrieved
            ) + 1
        );
    }
    
    template<
        typename playable,
        typename pitching
    >
    void pitch_controlled_source<
        playable,
        pitching
    >::resize_pitches(
        size_t new_size
    ) {
        size_t const current_offset = current_pitch - pitches.get_data(
        );
        size_t const end_offset = available_pitches_end - pitches.get_data(
        );
        pitches.resize(
            new_size
        );
        current_pitch = pitches.get_data(
        ) + current_offset;
        available_pitches_end = pitches.get_data(
        ) + end_offset;
        pitches_end = pitches.get_data(
        ) + pitches.get_size(
        );
    }
    
    template<
        typename playable,
        typename pitching
    >
    void pitch_controlled_source<
        playable,
        pitching
    >::resize_samples(
        size_t new_size
    ) {
        size_t const current_offset = current_source_sample - samples.get_data(
        );
        size_t const old_size = samples.get_size(
        );
        samples.resize(
            new_size
        );
        memcpy(
            samples.get_data(
            ) + old_size,
            samples.get_data(
            ),
            (
                samples.get_size(
                ) - old_size
            ) * sizeof(
                *next_source_sample
            )
        );
        current_source_sample = samples.get_data(
        ) + current_offset;
        next_source_sample = samples.get_data(
        ) + (
            (
                current_offset + available_source_samples
            ) % samples.get_size(
            )
        );
        samples_end = samples.get_data(
        ) + samples.get_size(
        );
    }
    
    template<
        typename playable,
        typename pitching
    >
    size_t bulk_audio_retrieval<
        pitch_controlled_source<
            playable,
            pitching
        >
    >::get_samples(
        pitch_controlled_source<
            playable,
            pitching
        >& source,
        amplitude_value* first,
        amplitude_value* last
    ) {
        return source.get_samples(
            first,
            last
        );
    }
}

#endif
