/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_posix_time.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    std::ostream& operator <<(
        std::ostream& destination,
        posix_time const& written
    ) {
        destination << written.tv_sec << "." << least_digits(
            written.tv_usec,
            6
        );
        return destination;
    }
    namespace asio {
        boost::posix_time::time_duration time_traits<
            @qlib@::posix_time
        >::to_posix_duration(
            time_traits<
                @qlib@::posix_time
            >::duration_type const& raw
        ) {
            return boost::posix_time::seconds(
                raw.seconds
            ) + boost::posix_time::microseconds(
                raw.microseconds
            );
        }
        bool time_traits<
            @qlib@::posix_time
        >::less_than(
            time_traits<
                @qlib@::posix_time
            >::time_type const& less,
            time_traits<
                @qlib@::posix_time
            >::time_type const& more
        ) {
            return timercmp(
                &less,
                &more,
                <
            );
        }
        time_traits<
            @qlib@::posix_time
        >::duration_type time_traits<
            @qlib@::posix_time
        >::subtract(
            time_traits<
                @qlib@::posix_time
            >::time_type const& base,
            time_traits<
                @qlib@::posix_time
            >::time_type const& subtracted
        ) {
            duration_type result;
            result.seconds = base.tv_sec;
            result.microseconds = base.tv_usec;
            result.seconds -= subtracted.tv_sec;
            result.microseconds = subtracted.tv_usec;
            return result;
        }
        time_traits<
            @qlib@::posix_time
        >::time_type time_traits<
            @qlib@::posix_time
        >::add(
            time_traits<
                @qlib@::posix_time
            >::time_type const& base,
            time_traits<
                @qlib@::posix_time
            >::duration_type const& added
        ) {
            time_type result = base;
            result.tv_sec += added.seconds;
            result.tv_usec += added.microseconds;
            if (
                0 > result.tv_usec
            ) {
                result.tv_usec += 1000000;
                result.tv_sec--;
            } else if (
                1000000 <= result.tv_usec
            ) {
                result.tv_usec -= 1000000;
                result.tv_sec++;
            }
            return result;
        }
        time_traits<
            @qlib@::posix_time
        >::time_type time_traits<
            @qlib@::posix_time
        >::now(
        ) {
            time_type result;
            if (
                0 != gettimeofday(
                    &result,
                    NULL
                )
            ) {
                throw @qlib@::component_error(
                    "posix_time"
                );
            }
            return result;
        }
    }
}
