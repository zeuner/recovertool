/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef SOAP_SERVICE_INCLUDED
#define SOAP_SERVICE_INCLUDED

#include <boost/shared_ptr.hpp>
#include <list>
#include <string>
#include <boost/noncopyable.hpp>
#include <map>
namespace @qlib@ {
    
    
    class soap_element;
    
    class soap_service :
    private boost::noncopyable {
    public:
        soap_service(
            xml_document const& definition
        );
        class type_descriptor;
        type_descriptor const& get_type(
            std::string const& name,
            std::string const& name_space
        ) const;
        type_descriptor const& get_element_type(
            std::string const& name,
            std::string const& name_space
        ) const;
        boost::shared_ptr<
            soap_element
        > get_request_header(
            std::string const& request
        ) const;
        boost::shared_ptr<
            soap_element
        > get_request_body(
            std::string const& request
        ) const;
        boost::shared_ptr<
            soap_element
        > get_response_header(
            std::string const& request
        ) const;
        boost::shared_ptr<
            soap_element
        > get_response_body(
            std::string const& request
        ) const;
        typedef std::pair<
            std::string,
            std::string
        > element_identification;
        std::string const& get_address(
        ) const;
    private:
        typedef std::pair<
            std::string,
            std::string
        > header_body;
        typedef std::pair<
            std::string,
            std::string
        > type_identification;
        typedef std::pair<
            int,
            int
        > bounds;
        typedef std::pair<
            bounds,
            type_descriptor*
        > value_list;
    public:
        typedef std::pair<
            element_identification,
            value_list
        > element_type;
        class type_descriptor :
        public boost::variant<
            std::list<
                element_type
            >,
            std::string
        > {
        public:
            type_descriptor(
            );
            template<
                typename assignable
            >
            type_descriptor(
                assignable const& copied
            );
            template<
                typename assignable
            >
            type_descriptor& operator=(
                assignable const& assigned
            );
        };
    protected:
        static bool type_dependency(
            type_identification& dependency,
            std::pair<
                type_identification,
                xmlNodePtr
            > const& dependent
        );
        boost::shared_ptr<
            soap_element
        > get_header_element(
            std::string const& request,
            std::map<        
                std::string,
                header_body
            > const& direction
        ) const;
        boost::shared_ptr<
            soap_element
        > get_body_element(
            std::string const& request,
            std::map<        
                std::string,
                header_body
            > const& direction
        ) const;
        std::list<
            element_type
        > complex_content_value(
            std::string const& target_namespace,
            xmlNodePtr complex_content
        );
        std::list<
            element_type
        > sequence_value(
            std::string const& target_namespace,
            xmlNodePtr sequence,
            std::list<
                element_type
            > result = std::list<
                element_type
            >(
            )
        );
        static type_descriptor& basic_type(
            std::string const& name
        );
        type_descriptor& lookup_type(
            xmlNodePtr element,
            xml_string const& attribute = BAD_CAST "type"
        );
        static void lookup_namespaced(
            xmlNodePtr element,
            xml_string const& attribute,
            std::string& name,
            std::string& name_space
        );
        void populate_types(
            xmlNodePtr const root
        );
        void populate_messages(
            xmlNodePtr const root
        );
        void populate_bindings(
            xmlNodePtr const root
        );
        void populate_service(
            xmlNodePtr const root
        );
    private:
        std::map<
            type_identification,
            type_descriptor
        > known_types;
        std::list<
            boost::shared_ptr<
                type_descriptor
            >
        > anonymous_types;
        std::map<
            element_identification,
            type_descriptor*
        > element_types;
        std::map<
            std::string,
            std::map<
                element_identification,
                std::string
            >
        > known_messages;
        std::map<
            std::string,
            header_body
        > known_operations_input;
        std::map<
            std::string,
            header_body
        > known_operations_output;
        std::string address;
        static std::map<
            std::string,
            type_descriptor
        > basic_types;
    };
    
    template<
        typename assignable
    >
    soap_service::type_descriptor::type_descriptor(
        assignable const& copied
    ) :
    boost::variant<
        std::list<
            element_type
        >,
        std::string
    >(
        copied
    ) {
    }
    
    template<
        typename assignable
    >
    soap_service::type_descriptor& soap_service::type_descriptor::operator=(
        assignable const& assigned
    ) {
        boost::variant<   
            std::list<
                element_type
            >,
            std::string
        >::operator=(
            assigned
        );
        return *this;
    }
}

#endif
