/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_packet_capture_source.h"
#include <boost/asio/placeholders.hpp>
#include <boost/bind.hpp>
namespace @qlib@ {
    
    
    packet_capture_source::packet_capture_source(
        boost::asio::io_service& communicating,
        std::string const& interface,
        bool promiscuous,
        size_t snapshot_length,
        size_t buffer_size,
        int timeout_milliseconds
    ) :
    packet_capture_socket(
        communicating,
        interface,
        promiscuous,
        snapshot_length,
        buffer_size,
        timeout_milliseconds
    ) {
    }
    
    void packet_capture_source::async_get_packet(
        packet_handler const& handling
    ) {
        handling_packet.reset(
            new packet_handler(
                handling
            )
        );
        async_receive(
            boost::asio::null_buffers(
            ),
            boost::bind(
                &packet_capture_source::handle_packet,
                this,
                boost::asio::placeholders::error
            )
        );
    }
    
    void packet_capture_source::handle_packet(
        boost::system::error_code const& error
    ) {
        std::auto_ptr<
            packet_handler
        > handling_packet = this->handling_packet;
        if (
            error
        ) {
            (
                *handling_packet
            )(
                error,
                NULL,
                0,
                0,
                timeval(
                )
            );
            return;
        }
        struct pcap_pkthdr* header;
        u_char const* data;
        int const result = pcap_next_ex(
            get_pcap_handle(
            ),
            &header,
            &data
        );
        if (
            0 == result
        ) {
            (
                *handling_packet
            )(
                boost::system::error_code(
                    pcap_error_codes::timeout,
                    pcap_category(
                    )
                ),
                NULL,
                0,
                0,
                timeval(
                )
            );
            return;
        }
        if (
            1 != result
        ) {
            (
                *handling_packet
            )(
                boost::system::error_code(
                    pcap_error_codes::generic_error,
                    pcap_category(
                    )
                ),
                NULL,
                0,
                0,
                timeval(
                )
            );
            return;
        }
        (
            *handling_packet
        )(
            error,
            data,
            header->caplen,
            header->len,
            header->ts
        );
    }
}
