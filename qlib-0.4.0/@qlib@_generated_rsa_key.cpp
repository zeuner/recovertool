/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_generated_rsa_key.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    generated_rsa_key::generated_rsa_key(
    ) :
    data(
        RSA_generate_key(
            0x400,
            0x10001,
            NULL,
            NULL
        )
    ) {
        if (
            NULL == data
        ) {
            throw component_error(
                "generated_rsa_key"
            );
        }
    }
    
    generated_rsa_key::~generated_rsa_key(
    ) {
        RSA_free(
            data
        );
    }
    
    RSA* generated_rsa_key::get_data(
    ) const {
        return data;
    }
    
    void generated_rsa_key::write_key_data(
        memory_sink_bio& destination
    ) {
        if (
            1 != PEM_write_bio_RSAPrivateKey(
                destination.get_data(
                ),
                data,
                NULL,
                NULL,
                0,
                NULL,
                NULL
            )
        ) {
            throw component_error(
                "generated_rsa_key"
            );
        }
    }
    
    void generated_rsa_key::write_public_key_data(
        memory_sink_bio& destination
    ) {
        if (
            1 != PEM_write_bio_RSA_PUBKEY(
                destination.get_data(
                ),
                data
            )
        ) {
            throw component_error(
                "generated_rsa_key"
            );
        }
    }
}
