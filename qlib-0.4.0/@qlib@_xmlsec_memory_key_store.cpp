/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_xmlsec_memory_key_store.h"
namespace @qlib@ {
    
    
    void xmlsec_memory_key_store::add_key(
        xmlSecKeyPtr added
    ) {
        keys[
            xmlSecKeyGetName(
                added
            )
        ].reset(
            new xmlsec_key_wrapper(
                added
            )
        );
    }
    
    bool xmlsec_memory_key_store::has_key(
        xml_string const& name
    ) const {
        return keys.end(
        ) != keys.find(
            name
        );
    }
    
    xmlSecKeyPtr xmlsec_memory_key_store::get_key(
        xml_string const& name,
        xmlSecKeyInfoCtxPtr key_info_context
    ) const {
        return keys.at(
            name
        )->get_copy(
        );
    }
}
