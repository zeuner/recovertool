/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef XMLSEC_MEMORY_IMPORTED_KEY_INCLUDED
#define XMLSEC_MEMORY_IMPORTED_KEY_INCLUDED

#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    template<
        typename name_string,
        typename key_string
    >
    xmlSecKeyPtr xmlsec_memory_imported_key(
        name_string const& name,
        key_string const& key
    ) {
        xmlSecKeyPtr const result = xmlSecCryptoAppKeyLoadMemory(
            key.data(
            ),
            key.size(
            ),
            xmlSecKeyDataFormatPem,
            NULL,
            NULL,
            NULL
        );
        if (
            NULL == result
        ) {
            throw component_error(
                "xmlsec_memory_imported_key"
            );
        }
        if (
            0 > xmlSecKeySetName(
                result,
                name.c_str(
                )
            )
        ) {
            xmlSecKeyDestroy(
                result
            );
            throw component_error(
                "xmlsec_memory_imported_key"
            );
        }
        return result;
    }
}

#endif
