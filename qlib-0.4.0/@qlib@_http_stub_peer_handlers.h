/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef HTTP_STUB_PEER_HANDLERS_INCLUDED
#define HTTP_STUB_PEER_HANDLERS_INCLUDED

#include <unistd.h>
#include <boost/noncopyable.hpp>
namespace @qlib@ {
    
    
    class http_stub_peer_handlers :
    private boost::noncopyable {
    public:
        http_stub_peer_handlers(
        );
        void close_link(
        );
        bool invoke_accept_handler(
            boost::system::error_code const& error
        );
        void invoke_response_handler(
            boost::system::error_code const& error
        );
        bool invoke_deferred_response_handler(
        );
        boost::system::error_code const& get_error(
        ) const;
    private:
        boost::system::error_code error;
    };
}

#endif
