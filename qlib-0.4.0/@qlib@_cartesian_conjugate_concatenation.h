/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef CARTESIAN_CONJUGATE_CONCATENATION_INCLUDED
#define CARTESIAN_CONJUGATE_CONCATENATION_INCLUDED

#include <string>
namespace @qlib@ {
    
    
    template<
        typename prefix_sequenced,
        typename suffix_sequenced
    >
    prefix_sequenced cartesian_conjugate_concatenation(
        prefix_sequenced const& prefix_sequence,
        std::string const& delimiter,
        suffix_sequenced const& suffix_sequence
    );
    
    template<
        typename prefix_sequenced,
        typename suffix_sequenced
    >
    prefix_sequenced cartesian_conjugate_concatenation(
        prefix_sequenced const& prefix_sequence,
        std::string const& delimiter,
        suffix_sequenced const& suffix_sequence
    ) {
        prefix_sequenced result;
        result.clear(
        );
        for (
            typename suffix_sequenced::const_iterator suffix_added(
                suffix_sequence.begin(
                )
            );
            suffix_sequence.end(
            ) != suffix_added;
            suffix_added++
        ) {
            for (
                typename prefix_sequenced::const_iterator prefix_added(
                    prefix_sequence.begin(
                    )
                );
                prefix_sequence.end(
                ) != prefix_added;
                prefix_added++
            ) {
                result.push_back(
                    (
                        *prefix_added
                    ) + delimiter + (
                        *suffix_added
                    )
                );
            }
        }
        return result;
    }
}

#endif
