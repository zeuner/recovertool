/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_soap_response.h"
namespace @qlib@ {
    
    
    soap_response::soap_response(
        soap_service const& service,
        std::string const& request,
        xml_document const& received
    ) :
    header(
        service.get_response_header(
            request
        )
    ),
    body(
        service.get_response_body(
            request
        )
    ) {
        xmlNodePtr root = received.get_root_node(
        );
        check_xml_node(
            root,
            BAD_CAST "Envelope",
            BAD_CAST "http://schemas.xmlsoap.org/soap/envelope/"
        );
        xml_matching_child const header_node(
            root,
            BAD_CAST "Header",
            BAD_CAST "http://schemas.xmlsoap.org/soap/envelope/"
        );
        xml_matching_child const body_node(
            root,
            BAD_CAST "Body",
            BAD_CAST "http://schemas.xmlsoap.org/soap/envelope/"
        );
        header->parse_xml(
            header_node.get_data(
            )
        );
        body->parse_xml(
            body_node.get_data(
            )
        );
    }
    
    soap_element& soap_response::get_header(
    ) {
        return *header;
    }
    
    soap_element& soap_response::get_body(
    ) {
        return *body;
    }
}
