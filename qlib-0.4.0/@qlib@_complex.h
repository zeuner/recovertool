/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef COMPLEX_INCLUDED
#define COMPLEX_INCLUDED

namespace @qlib@ {
    
    
    template<
        typename real
    >
    class complex {
    public:
        complex(
            real const& real_part = 0,
            real const& imaginary_part = 0
        );
        complex& operator*=(
            complex const& other
        );
        complex& operator+=(
            complex const& other
        );
        bool operator==(
            complex const& other
        ) const;
        complex operator+(
            complex const& other
        ) const;
        complex operator-(
        ) const;
        complex operator-(
            complex const& other
        ) const;
        complex operator*(
            complex const& other
        ) const;
        complex& operator*=(
            real const& other
        );
        complex operator/(
            complex const& other
        ) const;
        complex& operator/=(
            real const& other
        );
        real squared_abs(
        ) const;
        complex conjugate(
        ) const;
        real const& get_real_part(
        ) const;
        real const& get_imaginary_part(
        ) const;
    private:
        real real_part;
        real imaginary_part;
    };
    
    template<
        typename real
    >
    real const& get_real_part(
        complex<
            real
        > const& complete
    );
    
    template<
        typename real
    >
    real const& get_imaginary_part(
        complex<
            real
        > const& complete
    );
    
    template<
        typename real
    >
    complex<
        real
    > operator*(
        real one,
        complex<
            real
        > const& other
    );
    
    template<
        typename real
    >
    complex<
        real
    > exp(
        complex<
            real
        > const& exponent
    );
    
    template<
        typename real
    >
    real abs(
        complex<
            real
        > const& signed_value
    );
    
    template<
        typename real
    >
    real arg(
        complex<
            real
        > const& signed_value
    );
    
    template<
        typename character,
        typename traits,
        typename real
    >
    std::basic_ostream<
        character,
        traits
    >& operator<<(
        std::basic_ostream<
            character,
            traits
        >& destination,
        complex<
            real
        > const& printed
    );
    
    template<
        typename real
    >
    complex<
        real
    >::complex(
        real const& real_part,
        real const& imaginary_part
    ) :
    real_part(
        real_part
    ),
    imaginary_part(
        imaginary_part
    ) {
    }
    
    template<
        typename real
    >
    complex<
        real
    >& complex<
        real
    >::operator*=(
        complex<
            real
        > const& other
    ) {
        real const product_real_part(
            real_part * other.real_part - imaginary_part * other.imaginary_part
        );
        real const product_imaginary_part(
            real_part * other.imaginary_part + imaginary_part * other.real_part
        );
        real_part = product_real_part;
        imaginary_part = product_imaginary_part;
        return *this;
    }
    
    template<
        typename real
    >
    complex<
        real
    >& complex<
        real
    >::operator+=(
        complex<
            real
        > const& other
    ) {
        real_part += other.real_part;
        imaginary_part += other.imaginary_part;
        return *this;
    }
    
    template<
        typename real
    >
    bool complex<
        real
    >::operator==(
        complex<
            real
        > const& other
    ) const {
        return (
            real_part == other.real_part
        ) && (
            imaginary_part == other.imaginary_part
        );
    }
    
    template<
        typename real
    >
    complex<
        real
    > complex<
        real
    >::operator+(
        complex<
            real
        > const& other
    ) const {
        return complex<
            real
        >(
            real_part + other.real_part,
            imaginary_part + other.imaginary_part
        );
    }
    
    template<
        typename real
    >
    complex<
        real
    > complex<
        real
    >::operator-(
    ) const {
        return complex<
            real
        >(
            -real_part,
            -imaginary_part
        );
    }
    
    template<
        typename real
    >
    complex<
        real
    > complex<
        real
    >::operator-(
        complex<
            real
        > const& other
    ) const {
        return complex<
            real
        >(
            real_part - other.real_part,
            imaginary_part - other.imaginary_part
        );
    }
    
    template<
        typename real
    >
    complex<
        real
    > complex<
        real
    >::operator*(
        complex<
            real
        > const& other
    ) const {
        return complex<
            real
        >(
            real_part * other.real_part - imaginary_part * other.imaginary_part,
            real_part * other.imaginary_part + imaginary_part * other.real_part
        );
    }
    
    template<
        typename real
    >
    complex<
        real
    >& complex<
        real
    >::operator*=(
        real const& other
    ) {
        real_part *= other;
        imaginary_part *= other;
        return *this;
    }
    
    template<
        typename real
    >
    complex<
        real
    > complex<
        real
    >::operator/(
        complex<
            real
        > const& other
    ) const {
        real scaling = other.squared_abs(
        );
        return complex<
            real
        >(
            real_part / scaling,
            imaginary_part / scaling
        ) * other.conjugate(
        );
    }
    
    template<
        typename real
    >
    complex<
        real
    >& complex<
        real
    >::operator/=(
        real const& other
    ) {
        real_part /= other;
        imaginary_part /= other;
        return *this;
    }
    
    template<
        typename real
    >
    real complex<
        real
    >::squared_abs(
    ) const {
        return (
            real_part * real_part
        ) + (
            imaginary_part * imaginary_part
        );
    }
    
    template<
        typename real
    >
    complex<
        real
    > complex<
        real
    >::conjugate(
    ) const {
        return complex<
            real
        >(
            real_part,
            -imaginary_part
        );
    }
    
    template<
        typename real
    >
    real const& complex<
        real
    >::get_real_part(
    ) const {
        return real_part;
    }
    
    template<
        typename real
    >
    real const& complex<
        real
    >::get_imaginary_part(
    ) const {
        return imaginary_part;
    }
    
    template<
        typename real
    >
    real const& get_real_part(
        complex<
            real
        > const& complete
    ) {
        return complete.get_real_part(
        );
    }
    
    template<
        typename real
    >
    real const& get_imaginary_part(
        complex<
            real
        > const& complete
    ) {
        return complete.get_imaginary_part(
        );
    }
    
    template<
        typename real
    >
    complex<
        real
    > operator*(
        real one,
        complex<
            real
        > const& other
    ) {
        return complex<
            real
        >(
            one * other.get_real_part(
            ),
            one * other.get_imaginary_part(
            )
        );
    }
    
    template<
        typename real
    >
    complex<
        real
    > exp(
        complex<
            real
        > const& exponent
    ) {
        real const scaling = std::exp(
            exponent.get_real_part(
            )
        );
        return complex<
            real
        >(
            scaling * std::cos(
                exponent.get_imaginary_part(
                )
            ),
            scaling * std::sin(
                exponent.get_imaginary_part(
                )
            )
        );
    }
    
    template<
        typename real
    >
    real abs(
        complex<
            real
        > const& signed_value
    ) {
        return sqrt(
            signed_value.squared_abs(
            )
        );
    }
    
    template<
        typename real
    >
    real arg(
        complex<
            real
        > const& signed_value
    ) {
        return std::atan2(
            signed_value.get_imaginary_part(
            ),
            signed_value.get_real_part(
            )
        );
    }
    
    template<
        typename character,
        typename traits,
        typename real
    >
    std::basic_ostream<
        character,
        traits
    >& operator<<(
        std::basic_ostream<
            character,
            traits
        >& destination,
        complex<
            real
        > const& printed
    ) {
        return destination << "(" << printed.get_real_part(
        ) << "," << printed.get_imaginary_part(
        ) << ")";
    }
}

#endif
