/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_base64_encoded.h"
namespace @qlib@ {
    
    
    std::string base64_encoded(
        std::string const& raw
    ) {
        const char characters[
        ] = ""
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz"
        "0123456789"   
        "+/";
        std::string base64_data;
        int column = 0;
        int remaining = 0;
        int bits = 0;
        for (
            std::string::const_iterator added = raw.begin(
            );
            raw.end(
            ) != added;
            added++
        ) {
            bits = bits << 8 | *added;
            remaining += 2;
            base64_data += characters[
                bits >> remaining & (
                    (
                        1 << 6
                    ) - 1
                )
            ];
            if (
                76 <= (
                    ++column
                )
            ) {
                base64_data += '\n';
                column = 0;
            }
            if (
                remaining >= 6
            ) {
                remaining -= 6;
                base64_data += characters[
                    bits >> remaining & (
                        (
                            1 << 6
                        ) - 1
                    )
                ];
                if (
                    76 <= (
                        ++column
                    )
                ) {
                    base64_data += '\n';
                    column = 0;
                }
            }
        }
        if (
            remaining > 0
        ) {
            bits <<= 8;
            remaining += 2;
            base64_data += characters[
                bits >> remaining & (
                    (
                        1 << 6
                    ) - 1
                )
            ];
            if (
                76 <= (
                    ++column
                )
            ) {
                base64_data += '\n';
                column = 0;
            }
            for (
                ;
                remaining < 8;
            ) {
                remaining += 2;
                base64_data += '=';
                if (
                    76 <= (
                        ++column
                    )
                ) {
                    base64_data += '\n';
                    column = 0;
                }
            }
        }
        return base64_data;
    }
}
