/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_scoped_transaction.h"
#include "@qlib@_system_log.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    scoped_transaction::scoped_transaction(
        database_connection& storing
    ) :
    storage(
        storing.get_storage(
        )
    ),
    finished(
        false
    ) {
        storing.flush_query_durations(
        );
        if (
            0 != mysql_autocommit(
                storage,
                0
            )
        ) {
            throw component_error(
                "scoped_transaction"
            );
        }
    }
    
    void scoped_transaction::commit(
    ) {
        if (
            0 != mysql_commit(
                storage
            )
        ) {
            throw component_error(
                "scoped_transaction"
            );
        }
        finished = true;
    }
    
    void scoped_transaction::rollback(
    ) {
        if (
            0 != mysql_rollback(
                storage
            )
        ) {
            throw component_error(
                "scoped_transaction"
            );
        }
        finished = true;
    }
    
    scoped_transaction::~scoped_transaction(
    ) {
        if (
            !finished
        ) {
    #ifdef QLIB_NO_EXCEPTIONS
            do {
    #else
            try {
    #endif
                rollback(
                );
    #ifdef QLIB_NO_EXCEPTIONS
            } while (
                false
            );
    #else
            } catch (
                std::exception& caught
            ) {
                system_log.log_error(
                    "problem issuing rollback"
                );
            }
    #endif
        }
        if (
            0 != mysql_autocommit(
                storage,
                1
            )
        ) {
            system_log.log_error(
                "problem committing to database"
            );
        }
    }
}
