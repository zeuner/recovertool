/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef DISSECT_HEADER_VALUE_INCLUDED
#define DISSECT_HEADER_VALUE_INCLUDED

#include <map>
namespace @qlib@ {
    
    
    template<
        typename string
    >
    void dissect_header_value(
        string const& whole,
        string& value,
        std::map<
            string,
            string
        >& attributes
    );
    
    template<
        typename string
    >
    void dissect_header_value(
        string const& whole,
        string& value,
        std::map<
            string,
            string
        >& attributes
    ) {
        attributes.clear(
        );
        typename string::size_type at_delimiter = whole.find(
            ';'
        );
        if (
            string::npos == at_delimiter
        ) {
            value = whole;
            return;
        }
        value = string(
            whole.begin(
            ),
            whole.begin(
            ) + at_delimiter
        );
        bool last = false;
        do {
            typename string::const_iterator attribute_start = whole.begin(
            ) + at_delimiter + 1;
            while (
                (
                    ' ' == *attribute_start
                ) && (
                    whole.end(
                    ) != attribute_start
                )
            ) {
                attribute_start++;
            }
            typename string::const_iterator attribute_end;
            string key;
            string value;
            dissect_header_attribute(
                attribute_start,
                attribute_end,
                whole.end(
                ),
                last,
                key,
                value
            );
            attributes[
                key
            ] = value;
            at_delimiter = attribute_end - whole.begin(
            );
        } while (
            !last
        );
    }
}

#endif
