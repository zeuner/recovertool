/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef MEMORY_X509_REQUEST_INCLUDED
#define MEMORY_X509_REQUEST_INCLUDED

#include "@qlib@_c_string.h"
#include <string>
namespace @qlib@ {
    
    
    class memory_x509_request {
    public:
        memory_x509_request(
            std::string const& input
        );
        ~memory_x509_request(
        );
        X509_REQ* get_data(
        ) const;
    private:
        c_string password;
        X509_REQ* data;
    };
}

#endif
