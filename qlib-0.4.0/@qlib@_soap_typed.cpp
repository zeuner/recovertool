/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_soap_typed.h"
#include "@qlib@_stringified.h"
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    soap_typed::soap_typed(
        std::list<
            soap_service::element_type
        > const& type
    ) :
    type(
        type
    ),
    current_element(
        this->type.begin(
        )
    ),
    element_count(
        0
    ) {
    }
    
    void soap_typed::dump_to_xml(
        xmlNodePtr augmented
    ) const {
        if (
            value_type
        ) {
            xmlNewNsProp(
                augmented,
                namespace_for_child(
                    "http://www.w3.org/2001/XMLSchema-instance",
                    augmented
                ),
                BAD_CAST "type",
                BAD_CAST (
                    std::string(
                        reinterpret_cast<
                            char const*
                        >(
                            namespace_for_child(
                                value_type->second,
                                augmented
                            )->prefix
                        )
                    ) + ":" + value_type->first
                ).c_str(
                )
            );
        }
        for (
            std::list<
                std::pair<
                    soap_service::element_identification,
                    element_value
                >
            >::const_iterator dumped = elements.begin(
            );
            elements.end(
            ) != dumped;
            dumped++
        ) {
            xmlNodePtr const added = xmlNewChild(
                augmented,
                namespace_for_child(
                    dumped->first.second,
                    augmented
                ),
                BAD_CAST dumped->first.first.c_str(
                ),
                NULL
            );
            boost::apply_visitor(
                dumping_visitor(
                    added
                ),
                dumped->second
            );
        }
    }
    
    void soap_typed::parse_xml(
        xmlNodePtr const parsed
    ) {
        for (
            xmlNodePtr child = parsed->children;
            child;
            child = child->next
        ) {
            if (
                XML_ELEMENT_NODE != child->type
            ) {
                continue;
            }
            if (
                NULL == child->ns
            ) {
                throw component_error(
                    "soap_typed"
                );
            }
            std::string const local(
                reinterpret_cast<
                    char const*
                >(
                    child->name
                )
            );
            std::string const ns(
                reinterpret_cast<
                    char const*
                >(
                    child->ns->href
                )
            );
            boost::apply_visitor(
                parsing_visitor(
                    local,
                    ns,
                    child,
                    *this
                ),
                next_element_type(
                    local,
                    ns
                )
            );
        }
    }
    
    std::vector<
        soap_typed::element_value
    > soap_typed::matching_elements(
        std::string const& element_name,
        std::string const& element_namespace
    ) {
        std::vector<
            element_value
        > result;
        for (
            std::list<
                std::pair<
                    soap_service::element_identification,
                    element_value
                >
            >::const_iterator dumped = elements.begin(
            );
            elements.end(
            ) != dumped;
            dumped++
        ) {
            if (
                soap_service::element_identification(
                    element_name,
                    element_namespace
                ) == dumped->first
            ) {
                result.push_back(
                    dumped->second
                );
            }
        }
        return result;
    }
    
    void soap_typed::set_value_type(
        std::string const& local,
        std::string const& ns
    ) {
        value_type.reset(
            new soap_service::element_identification(
                local,
                ns
            )
        );
    }
    
    soap_service::type_descriptor const& soap_typed::next_element_type(
        std::string const& element_name,
        std::string const& element_namespace
    ) {
        do {
            if (
                type.end(
                ) == current_element
            ) {
                throw component_error(
                    "soap_typed"
                );
            }
            if (
                (
                    element_name == current_element->first.first
                ) && (
                    element_namespace == current_element->first.second
                )
            ) {
                element_count++;
                std::list<
                    soap_service::element_type
                >::const_iterator const matched_element = current_element;
                if (
                    element_count == current_element->second.first.second
                ) {
                    element_count = 0;
                    current_element++;
                }
                return *(
                    matched_element->second.second
                );
            }
            if (
                current_element->second.first.first <= element_count
            ) {
                element_count = 0;
                current_element++;
            } else {
                throw component_error(
                    "soap_typed"
                );
            }
        } while (
            true
        );
    }
    
    void soap_typed::add_with_type(
        std::string const& element_name,
        std::string const& element_namespace,
        soap_service::type_descriptor const& type,
        std::string const& value
    ) {
        if (
            "string" != boost::get<
                std::string
            >(
                type
            )
        ) {
            throw component_error(
                "soap_typed"
            );
        }
        elements.push_back(
            std::make_pair(
                soap_service::element_identification(
                    element_name,
                    element_namespace
                ),
                value
            )
        );
    }
    
    void soap_typed::add_with_type(
        std::string const& element_name,
        std::string const& element_namespace,
        soap_service::type_descriptor const& type,
        long value
    ) {
        if (
            "long" != boost::get<
                std::string
            >(
                type
            )
        ) {
            throw component_error(
                "soap_typed"
            );
        }
        elements.push_back(
            std::make_pair(
                soap_service::element_identification(
                    element_name,
                    element_namespace
                ),
                value
            )
        );
    }
    
    void soap_typed::add_with_type(
        std::string const& element_name,
        std::string const& element_namespace,
        soap_service::type_descriptor const& type,
        float value
    ) {
        if (
            "float" != boost::get<
                std::string
            >(
                type
            )
        ) {
            throw component_error(
                "soap_typed"
            );
        }
        elements.push_back(
            std::make_pair(
                soap_service::element_identification(
                    element_name,
                    element_namespace
                ),
                value
            )
        );
    }
    
    void soap_typed::add_with_type(
        std::string const& element_name,
        std::string const& element_namespace,
        soap_service::type_descriptor const& type,
        double value
    ) {
        if (
            "double" != boost::get<
                std::string
            >(
                type
            )
        ) {
            throw component_error(
                "soap_typed"
            );
        }
        elements.push_back(
            std::make_pair(
                soap_service::element_identification(
                    element_name,
                    element_namespace
                ),
                value
            )
        );
    }
    
    void soap_typed::add_with_type(
        std::string const& element_name,
        std::string const& element_namespace,
        soap_service::type_descriptor const& type,
        boost::shared_ptr<
            soap_typed
        > const& value
    ) {
        if (
            value->type != boost::get<
                std::list<
                    soap_service::element_type
                >
            >(
                type
            )
        ) {
            throw component_error(
                "soap_typed"
            );
        }
        elements.push_back(
            std::make_pair(
                soap_service::element_identification(
                    element_name,
                    element_namespace
                ),
                value
            )
        );
    }
    
    xmlNsPtr soap_typed::namespace_for_child(
        std::string const& href,
        xmlNodePtr parent
    ) {
        xmlNsPtr ns = xmlSearchNsByHref(
            parent->doc,
            parent,
            BAD_CAST href.c_str(
            )
        );
        if (
            NULL == ns
        ) {
            ns = xmlNewNs(
                parent,
                BAD_CAST href.c_str(
                ),
                BAD_CAST "x"
            );
        }
        if (
            NULL == ns
        ) {
            throw component_error(
                "soap_typed"
            );
        }
        return ns;
    }
    
    soap_typed::dumping_visitor::dumping_visitor(
        xmlNodePtr augmented
    ) :
    augmented(
        augmented
    ) {
    }
    
    void soap_typed::dumping_visitor::operator(
    )(
        std::string const& visited
    ) const {
        xmlNodePtr text_node = xmlNewDocText(
            augmented->doc,
            BAD_CAST visited.c_str(
            )
        );
        xmlAddChild(
            augmented,
            text_node
        );
    }
    
    void soap_typed::dumping_visitor::operator(
    )(
        long visited
    ) const {
        xmlNodePtr text_node = xmlNewDocText(
            augmented->doc,
            BAD_CAST stringified(
                visited
            ).c_str(
            )
        );
        xmlAddChild(
            augmented,
            text_node
        );
    }
    
    void soap_typed::dumping_visitor::operator(
    )(
        float visited
    ) const {
        xmlNodePtr text_node = xmlNewDocText(
            augmented->doc,
            BAD_CAST stringified(
                visited
            ).c_str(
            )
        );
        xmlAddChild(
            augmented,
            text_node
        );
    }
    
    void soap_typed::dumping_visitor::operator(
    )(
        double visited
    ) const {
        xmlNodePtr text_node = xmlNewDocText(
            augmented->doc,
            BAD_CAST stringified(
                visited
            ).c_str(
            )
        );
        xmlAddChild(
            augmented,
            text_node
        );
    }
    
    void soap_typed::dumping_visitor::operator(
    )(
        boost::shared_ptr<
            soap_typed
        > const& visited
    ) const {
        visited->dump_to_xml(
            augmented
        );
    }
    
    soap_typed::parsing_visitor::parsing_visitor(
        std::string const& local,
        std::string const& ns,
        xmlNodePtr const parsed,
        soap_typed& augmented
    ) :
    local(
        local
    ),
    ns(
        ns
    ),
    parsed(
        parsed
    ),
    augmented(
        augmented
    ) {
    }
    
    void soap_typed::parsing_visitor::operator(
    )(
        std::list<
            soap_service::element_type
        > const& visited
    ) const {
        boost::shared_ptr<
            soap_typed
        > added(
            new soap_typed(
                visited
            )
        );
        augmented.add_with_type(
            local,
            ns,
            visited,
            added
        );
        added->parse_xml(
            parsed
        );
    }
    
    void soap_typed::parsing_visitor::operator(
    )(
        std::string const& visited
    ) const {
        if (
            NULL == parsed->children
        ) {
            throw component_error(
                "soap_typed"
            );
        }
        char const* data = reinterpret_cast<
            char const*
        >(
            parsed->children->content
        );
        if (
            "string" == visited
        ) {
            augmented.add_with_type(
                local,
                ns,
                visited,
                data
            );
        } else if (
            "long" == visited
        ) {
            augmented.add_with_type(
                local,
                ns,
                visited,
                atol(
                    data
                )
            );
        } else if (
            "float" == visited
        ) {
            augmented.add_with_type(
                local,
                ns,
                visited,
                strtof(
                    data,
                    NULL
                )
            );
        } else if (
            "double" == visited
        ) {
            augmented.add_with_type(
                local,
                ns,
                visited,
                strtod(
                    data,
                    NULL
                )
            );
        } else {
            throw component_error(
                "soap_typed"
            );
        }
    }
}
