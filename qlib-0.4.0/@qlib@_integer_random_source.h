/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef INTEGER_RANDOM_SOURCE_INCLUDED
#define INTEGER_RANDOM_SOURCE_INCLUDED

#include <vector>
namespace @qlib@ {
    
    
    class integer_random_source {
    public:
        typedef uint32_t seed_value_type;
        integer_random_source(
            unsigned range
        );
        integer_random_source(
            std::vector<
                seed_value_type
            > const& seed,
            unsigned range
        );
        unsigned operator(
        )(
        );
    private:
        typedef boost::mt19937 randomizing;
        randomizing generator;
        boost::uniform_int<
            unsigned
        > distribution;
        boost::variate_generator<
            randomizing&,
            boost::uniform_int<
                unsigned
            >
        > chooser;
    };
}

#endif
