/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef HTTP_CLIENT_CONNECTION_INCLUDED
#define HTTP_CLIENT_CONNECTION_INCLUDED

#include "@qlib@_http_client_request.h"
#include <string>
#include "@qlib@_component_error.h"
#include <boost/asio/placeholders.hpp>
#include <boost/asio/write.hpp>
#include "@qlib@_http_category.h"
#include <unistd.h>
#include "@qlib@_throw_exception.h"
#include <boost/bind/bind.hpp>
#include "@qlib@_http_peer_session.h"
#include <map>
#include "@qlib@_http_peer_response.h"
#include <boost/function.hpp>
#include "@qlib@_downcased.h"
#include <boost/asio/ip/tcp.hpp>
#include "@qlib@_http_error_codes.h"
namespace @qlib@ {
    
    using namespace boost::placeholders;
    
    template<
        typename transmitting = boost::asio::ip::tcp::socket,
        typename allocating = std::allocator<
            char
        >
    >
    class http_client_connection;
    
    template<
        typename transmitting,
        typename allocating
    >
    class http_client_connection :
    public http_peer_session<
        transmitting,
        allocating
    > {
    public:
        template<
            typename initializing
        >
        http_client_connection(
            initializing& initializer,
            typename http_client_connection<
                transmitting,
                allocating
            >::string::size_type maximum_data = 0x400
        );
        template<
            typename initializing
        >
        http_client_connection(
            initializing const& initializer,
            typename http_client_connection<
                transmitting,
                allocating
            >::string::size_type maximum_data = 0x400
        );
        typedef http_client_request<
            http_client_connection<
                transmitting,
                allocating
            >
        > request_type;
        typedef http_peer_response<
            http_client_connection<
                transmitting,
                allocating
            >
        > response_type;
        void async_request(
            request_type& requested,
            response_type& response,
            boost::function<
                void(
                    boost::system::error_code const& error
                )
            > const& response_handler
        );
        void async_continue_request(
            request_type& requested,
            boost::function<
                void(
                    boost::system::error_code const& error
                )
            > const& response_handler
        );
        void async_continue(
            boost::function<
                void(
                    boost::system::error_code const& error
                )
            > const& response_handler
        );
        enum state_type {
            reading_http_version_head,
            reading_http_version_tail,
            skipping_result_code_space,
            reading_result_code,
            finish_header,
            reading_header_key,
            skipping_header_value_space,
            reading_header_value,
            finishing_request_headers,
            reading_data_by_length,
            reading_data_until_eof,
        };
        static bool parse_http(
            response_type* current_response,
            typename http_client_connection<
                transmitting,
                allocating
            >::string& to_parse,
            typename http_client_connection<
                transmitting,
                allocating
            >::string& result_code,
            typename http_client_connection<
                transmitting,
                allocating
            >::string& data,
            typename http_client_connection<
                transmitting,
                allocating
            >::string& header_key,
            typename http_client_connection<
                transmitting,
                allocating
            >::string& header_value,
            std::map<
                typename http_client_connection<
                    transmitting,
                    allocating
                >::string,
                typename http_client_connection<
                    transmitting,
                    allocating
                >::string
            >& headers,
            boost::function<
                void(
                )
            > const& closer,
            boost::function<
                void(
                    boost::system::error_code const& error
                )
            > const& response_handler_invoker,
            enum state_type& state,
            typename http_client_connection<
                transmitting,
                allocating
            >::string::size_type& data_required,
            std::string::const_iterator& matched,
            typename http_client_connection<
                transmitting,
                allocating
            >::string::size_type const& maximum_data
        );
        void close_link(
        );
        static std::string const& get_http_version_head(
        );
    protected:
        bool parse_data(
        );
        void invoke_response_handler(
            boost::system::error_code const& error
        );
        void handle_write(
            boost::system::error_code const& error,
            bool request_finished
        );
        void handle_read(
            boost::system::error_code error = boost::system::error_code(
            ),
            size_t bytes = 0
        );
    private:
        response_type* current_response;
        boost::function<
            void(
                boost::system::error_code const& error
            )
        > response_handler;
        enum {
            input_size = 0x400
        };
        char input[
            input_size
        ];
        enum state_type state;
        typename http_client_connection<
            transmitting,
            allocating
        >::string output;
        static std::string const http_version_head;
        static std::string const line_terminator;
        std::string::const_iterator matched;
        typename http_client_connection<
            transmitting,
            allocating
        >::string result_code;
        typename http_client_connection<
            transmitting,
            allocating
        >::string header_key;
        typename http_client_connection<
            transmitting,
            allocating
        >::string header_value;
        std::map<
            typename http_client_connection<
                transmitting,
                allocating
            >::string,
            typename http_client_connection<
                transmitting,
                allocating
            >::string
        > headers;
        typename http_client_connection<
            transmitting,
            allocating
        >::string data;
        typename http_client_connection<
            transmitting,
            allocating
        >::string::size_type data_required;
        typename http_client_connection<
            transmitting,
            allocating
        >::string::size_type const maximum_data;
        typename http_client_connection<
            transmitting,
            allocating
        >::string to_parse;
    };
    
    template<
        typename transmitting,
        typename allocating
    >
    template<
        typename initializing
    >
    http_client_connection<
        transmitting,
        allocating
    >::http_client_connection(
        initializing& initializer,
        typename http_client_connection<
            transmitting,
            allocating
        >::string::size_type maximum_data
    ) :
    http_peer_session<
        transmitting,
        allocating
    >(
        initializer
    ),
    current_response(
        NULL
    ),
    state(
        reading_http_version_head
    ),
    matched(
        http_version_head.begin(
        )
    ),
    maximum_data(
        maximum_data
    ) {
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    template<
        typename initializing
    >
    http_client_connection<
        transmitting,
        allocating
    >::http_client_connection(
        initializing const& initializer,
        typename http_client_connection<
            transmitting,
            allocating
        >::string::size_type maximum_data
    ) :
    http_peer_session<
        transmitting,
        allocating
    >(
        initializer
    ),
    current_response(
        NULL
    ),
    state(
        reading_http_version_head
    ),
    matched(
        http_version_head.begin(
        )
    ),
    maximum_data(
        maximum_data
    ) {
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    void http_client_connection<
        transmitting,
        allocating
    >::async_request(
        request_type& requested,
        response_type& response,
        boost::function<
            void(
                boost::system::error_code const& error
            )
        > const& response_handler
    ) {
        if (
            NULL != current_response
        ) {
            throw_exception(
                component_error(
                    "http_client_connection"
                )
            );
        }
        current_response = &response;
        this->response_handler = response_handler;
        output.clear(
        );
        if (
            !requested.get_headers_sent(
            )
        ) {
            output += requested.get_method(
            ) + " " + requested.get_resource(
            ) + " HTTP/1.0\r\n";
            for (
                typename std::map<
                    typename http_client_connection<
                        transmitting,
                        allocating
                    >::string,
                    typename http_client_connection<
                        transmitting,
                        allocating
                    >::string
                >::const_iterator adding = requested.get_headers(
                ).begin(
                );
                requested.get_headers(
                ).end(
                ) != adding;
                adding++
            ) {
                output += adding->first + ": " + adding->second + "\r\n";
            }
            requested.set_headers_sent(
            );
            output += "\r\n";
        }
        output += requested.get_data(
        );
        boost::asio::async_write(
            *this,
            boost::asio::buffer(
                output.data(
                ),
                output.size(
                )
            ),
            boost::bind(
                &http_client_connection::handle_write,
                this,
                boost::asio::placeholders::error,
                requested.get_finished(
                )
            )
        );
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    void http_client_connection<
        transmitting,
        allocating
    >::async_continue_request(
        request_type& requested,
        boost::function<
            void(
                boost::system::error_code const& error
            )
        > const& response_handler
    ) {
        if (
            NULL == current_response
        ) {
            throw_exception(
                component_error(
                    "http_client_connection"
                )
            );
        }
        this->response_handler = response_handler;
        output.clear(
        );
        output += requested.get_data(
        );
        boost::asio::async_write(
            *this,
            boost::asio::buffer(
                output.data(
                ),
                output.size(
                )
            ),
            boost::bind(
                &http_client_connection::handle_write,
                this,
                boost::asio::placeholders::error,
                requested.get_finished(
                )
            )
        );
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    void http_client_connection<
        transmitting,
        allocating
    >::async_continue(
        boost::function<
            void(
                boost::system::error_code const& error
            )
        > const& response_handler
    ) {
        if (
            NULL == current_response
        ) {
            throw_exception(
                component_error(
                    "http_client_connection"
                )
            );
        }
        this->response_handler = response_handler;
        if (
            !to_parse.empty(
            )
        ) {
            handle_read(
            );
            return;
        }
        this->async_read_some(
            boost::asio::buffer(
                input,
                input_size  
            ),  
            boost::bind(
                &http_client_connection::handle_read,
                this, 
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    void http_client_connection<
        transmitting,
        allocating
    >::handle_write(
        boost::system::error_code const& error,
        bool request_finished
    ) {
        if (
            error
        ) {
            this->invoke_response_handler(
                error
            );
            return;
        }
        if (
            !request_finished
        ) {
            this->invoke_response_handler(
                error
            );
            return;
        }
        this->async_read_some(
            boost::asio::buffer(
                input,
                input_size  
            ),
            boost::bind(
                &http_client_connection::handle_read,
                this, 
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    void http_client_connection<
        transmitting,
        allocating
    >::close_link(
    ) {
        this->lowest_layer(
        ).close(
        );
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    std::string const& http_client_connection<
        transmitting,
        allocating
    >::get_http_version_head(
    ) {
        return http_version_head;
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    void http_client_connection<
        transmitting,
        allocating
    >::invoke_response_handler(
        boost::system::error_code const& error
    ) {
        boost::function<
            void(
                boost::system::error_code const& error
            )
        > response_handler = this->response_handler;
        this->response_handler.clear(
        );
        response_handler(
            error
        );
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    bool http_client_connection<
        transmitting,
        allocating
    >::parse_http(
        response_type* current_response,
        typename http_client_connection<
            transmitting,
            allocating
        >::string& to_parse,
        typename http_client_connection<
            transmitting,
            allocating
        >::string& result_code,
        typename http_client_connection<
            transmitting,
            allocating
        >::string& data,
        typename http_client_connection<
            transmitting,
            allocating
        >::string& header_key,
        typename http_client_connection<
            transmitting,
            allocating
        >::string& header_value,
        std::map<
            typename http_client_connection<
                transmitting,
                allocating
            >::string,
            typename http_client_connection<
                transmitting,
                allocating
            >::string
        >& headers,
        boost::function<
            void(
            )
        > const& closer,
        boost::function<
            void(
                boost::system::error_code const& error
            )
        > const& response_handler_invoker,
        enum state_type& state,
        typename http_client_connection<
            transmitting,
            allocating
        >::string::size_type& data_required,
        std::string::const_iterator& matched,
        typename http_client_connection<
            transmitting,
            allocating
        >::string::size_type const& maximum_data
    ) {
        typename http_client_connection<
            transmitting,
            allocating
        >::string::const_iterator processed = to_parse.begin(
        );
        bool rechecking = false;
        while (
            (
                to_parse.end(
                ) != processed
            ) || rechecking
        ) {
            rechecking = false;
            switch (
                state
            ) {
            case reading_http_version_head:
                if (
                    *processed == *matched
                ) {
                    processed++;
                    matched++;
                    if (
                        http_version_head.end(
                        ) == matched
                    ) {
                        state = reading_http_version_tail;
                    }
                    continue;
                }
                closer(
                );
                response_handler_invoker(
                    boost::system::error_code(
                        http_error_codes::invalid_version,
                        http_category(
                        )
                    )
                );
                return false;
            case reading_http_version_tail:
                if (
                    http_client_connection<
                        transmitting,
                        allocating
                    >::string::npos != typename http_client_connection<
                        transmitting,
                        allocating
                    >::string(
                        "01"
                    ).find(
                        *processed
                    )
                ) { 
                    processed++;
                    state = skipping_result_code_space;
                    continue;
                }
                closer(
                );
                response_handler_invoker(
                    boost::system::error_code(
                        http_error_codes::invalid_version,
                        http_category(
                        )
                    )
                );
                return false;
            case skipping_result_code_space:
                if (
                    ' ' != *processed
                ) {
                    throw_exception(
                        component_error(
                            "http_client_connection"
                        )
                    );
                }
                processed++;
                state = reading_result_code;
                continue;
            case reading_result_code:
                if (
                    true
                ) {
                    typename http_client_connection<
                        transmitting,
                        allocating
                    >::string::const_iterator start = processed;
                    while (
                        (   
                            to_parse.end(
                            ) != processed
                        ) && (
                            line_terminator[
                                0
                            ] != *processed
                        )
                    ) {
                        processed++;
                    }
                    result_code += typename http_client_connection<
                        transmitting,
                        allocating
                    >::string(
                        start,
                        processed
                    );
                    if (
                        line_terminator[
                            0
                        ] == *processed
                    ) {
                        current_response->set_status(
                            result_code
                        );
                        result_code.clear(
                        );
                        matched = line_terminator.begin(
                        );
                        state = finish_header;
                    }
                    continue;
                }
            case finish_header:
                if (
                    *processed == *matched
                ) { 
                    processed++;
                    matched++;
                    if (
                        line_terminator.end(
                        ) == matched
                    ) {
                        state = reading_header_key;
                    }
                    continue;
                }
                closer(
                );
                response_handler_invoker(
                    boost::system::error_code(
                        http_error_codes::broken_line_terminator,
                        http_category(
                        )
                    )
                );
                return false;
            case reading_header_key:
                if (
                    *processed == line_terminator[
                        0
                    ]
                ) {
                    matched = line_terminator.begin(
                    );
                    state = finishing_request_headers;
                    continue;
                }
                if (
                    true
                ) {
                    typename http_client_connection<
                        transmitting,
                        allocating
                    >::string::const_iterator start = processed;
                    while (
                        (
                            to_parse.end(
                            ) != processed
                        ) && (
                            ':' != *processed
                        )
                    ) {
                        processed++;
                    }
                    header_key += typename http_client_connection<
                        transmitting,
                        allocating
                    >::string(
                        start,
                        processed
                    );
                    if (
                        ':' == *processed
                    ) {
                        processed++;
                        state = skipping_header_value_space;
                    }
                    continue;
                }
            case skipping_header_value_space:
                if (
                    ' ' == *processed
                ) {
                    processed++;
                    continue;
                }
                state = reading_header_value;
                continue;
            case reading_header_value:
                if (
                    true
                ) { 
                    typename http_client_connection<
                        transmitting,
                        allocating
                    >::string::const_iterator start = processed;
                    while (
                        (
                            to_parse.end(
                            ) != processed
                        ) && (
                            line_terminator[
                                0
                            ] != *processed
                        )
                    ) {
                        processed++;
                    }
                    header_value += typename http_client_connection<
                        transmitting,
                        allocating
                    >::string(
                        start,
                        processed
                    );
                    if (
                        line_terminator[
                            0
                        ] == *processed
                    ) {
                        headers[
                            downcased(
                                header_key
                            )
                        ] = header_value;
                        header_key.clear(
                        );
                        header_value.clear(
                        );
                        matched = line_terminator.begin(
                        );
                        state = finish_header;
                    }
                    continue;
                }
            case finishing_request_headers:
                if (
                    *processed == *matched
                ) {
                    processed++;
                    matched++;
                    if (
                        line_terminator.end(
                        ) == matched
                    ) {
                        current_response->set_headers(
                            headers
                        );
                        typename std::map<
                            typename http_client_connection<
                                transmitting,
                                allocating
                            >::string,
                            typename http_client_connection<
                                transmitting,
                                allocating
                            >::string
                        >::const_iterator content_length = headers.find(
                            "content-length"
                        );
                        bool const content_length_set = headers.end(
                        ) != content_length;
                        if (
                            content_length_set
                        ) {
                            data_required = atoi(
                                content_length->second.c_str(
                                )
                            );
                        }
                        headers.clear(
                        );
                        rechecking = true;
                        if (
                            content_length_set
                        ) {
                            state = reading_data_by_length;
                        } else {
                            state = reading_data_until_eof;
                        }
                    }
                    continue;
                }
                closer(
                );
                response_handler_invoker(
                    boost::system::error_code(
                        http_error_codes::broken_header_terminator,
                        http_category(
                        )
                    )
                );
                return false;
            case reading_data_by_length:
                while (
                    (
                        to_parse.end(
                        ) != processed
                    ) || (
                        !(
                            (
                                0 != data_required
                            ) && (
                                data.size(
                                ) < maximum_data
                            )
                        )
                    )
                ) {
                    if (
                        (
                            0 != data_required
                        ) && (
                            data.size(
                            ) < maximum_data
                        )
                    ) {
                        typename http_client_connection<
                            transmitting,
                            allocating
                        >::string::size_type available = to_parse.end(
                        ) - processed;
                        if (
                            data_required < available
                        ) {
                            available = data_required;
                        }
                        typename http_client_connection<
                            transmitting,
                            allocating
                        >::string::size_type const allowed(
                            maximum_data - data.size(
                            )
                        );
                        if (
                            allowed < available
                        ) {
                            available = allowed;
                        }
                        data += typename http_client_connection<
                            transmitting,
                            allocating
                        >::string(
                            processed,
                            processed + available
                        );
                        processed += available;
                        data_required -= available;
                        continue;
                    }
                    current_response->set_data(
                        data
                    );
                    data.clear(
                    );
                    if (
                        0 == data_required
                    ) {
                        current_response->set_finished(
                            true
                        );
                        current_response = NULL;
                        state = reading_http_version_head;
                    } else {
                        current_response->set_finished(
                            false
                        );
                    }
                    to_parse = typename http_client_connection<
                        transmitting,
                        allocating
                    >::string(
                        processed,
                        typename http_client_connection<
                            transmitting,
                            allocating
                        >::string::const_iterator(
                            to_parse.end(
                            )
                        )
                    );
                    response_handler_invoker(
                        boost::system::error_code(           
                        )
                    );
                    return false;
                }
                continue;
            case reading_data_until_eof:
                while (
                    (
                        to_parse.end(
                        ) != processed
                    ) || (
                        data.size(
                        ) >= maximum_data
                    )
                ) {
                    if (
                        data.size(
                        ) < maximum_data
                    ) {
                        typename http_client_connection<
                            transmitting,
                            allocating
                        >::string::size_type available = to_parse.end(
                        ) - processed;
                        typename http_client_connection<
                            transmitting,
                            allocating
                        >::string::size_type const allowed(
                            maximum_data - data.size(
                            )
                        );
                        if (
                            allowed < available
                        ) {
                            available = allowed;
                        }
                        data += typename http_client_connection<
                            transmitting,
                            allocating
                        >::string(
                            processed,
                            processed + available
                        );
                        processed += available;
                        continue;
                    }
                    current_response->set_data(
                        data
                    );
                    data.clear(
                    );
                    current_response->set_finished(
                        false
                    );
                    to_parse = typename http_client_connection<
                        transmitting,
                        allocating
                    >::string(
                        processed,
                        typename http_client_connection<
                            transmitting,
                            allocating
                        >::string::const_iterator(
                            to_parse.end(
                            )
                        )
                    );
                    response_handler_invoker(
                        boost::system::error_code(           
                        )
                    );
                    return false;
                }
                continue;
            default:
                throw_exception(
                    component_error(
                        "http_client_connection"
                    )
                );
            }
        }
        to_parse = typename http_client_connection<
            transmitting,
            allocating
        >::string(
            processed,
            typename http_client_connection<
                transmitting,
                allocating
            >::string::const_iterator(
                to_parse.end(
                )
            )
        );
        return true;
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    bool http_client_connection<
        transmitting,
        allocating
    >::parse_data(
    ) {
        return this->parse_http(
            current_response,
            to_parse,
            result_code,
            data,
            header_key,
            header_value,
            headers,
            boost::bind(
                &http_client_connection::close_link,
                this
            ),
            boost::bind(
                &http_client_connection::invoke_response_handler,
                this,
                _1
            ),
            state,
            data_required,
            matched,
            maximum_data
        );
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    void http_client_connection<
        transmitting,
        allocating
    >::handle_read(
        boost::system::error_code error,
        size_t bytes
    ) {
        if (
            error
        ) {
            if (
                reading_data_until_eof == state
            ) {
                current_response->set_finished(
                    true
                );
                current_response->set_data(
                    data
                );
                this->close_link(
                );
                current_response = NULL;
                this->invoke_response_handler(
                    boost::system::error_code(
                    )
                );
                return;
            }
            this->invoke_response_handler(
                error
            );
            return;
        }
        try {
            to_parse += typename http_client_connection<
                transmitting,
                allocating
            >::string(
                input,
                bytes
            );
            if (
                this->parse_data(
                )
            ) {
                 this->async_read_some(
                    boost::asio::buffer(
                        input,
                        input_size  
                    ), 
                    boost::bind( 
                        &http_client_connection::handle_read,
                        this,       
                        boost::asio::placeholders::error,
                        boost::asio::placeholders::bytes_transferred
                    )          
                );
            }
        } catch (
            std::exception& caught
        ) {
            this->close_link(
            );
            this->invoke_response_handler(
                boost::system::error_code(
                    http_error_codes::parser_error,
                    http_category(
                    )
                )
            );
        }
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    std::string const http_client_connection<
        transmitting,
        allocating
    >::http_version_head = "HTTP/1.";
    
    template<
        typename transmitting,
        typename allocating
    >
    std::string const http_client_connection<
        transmitting,
        allocating
    >::line_terminator = "\r\n";
}

#endif
