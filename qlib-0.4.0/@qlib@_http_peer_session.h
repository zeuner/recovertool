/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef HTTP_PEER_SESSION_INCLUDED
#define HTTP_PEER_SESSION_INCLUDED

#include "@qlib@_c_string.h"
#include <boost/asio/ip/tcp.hpp>
#include <new>
#include "@qlib@_allocator_c_wrapper.h"
namespace @qlib@ {
    
    
    template<
        typename transmitting = boost::asio::ip::tcp::socket,
        typename allocating = std::allocator<
            char
        >
    >
    class http_peer_session;
    
    template<
        typename transmitting,
        typename allocating
    >
    class http_peer_session :
    public transmitting {
    public:
        typedef transmitting socket_type;
        typedef allocating allocator_type;
        typedef std::basic_string<
            char,
            std::char_traits<
                char
            >,
            typename allocator_type::template rebind<
                char
            >::other
        > string;
        template<
            typename initializing
        >
        http_peer_session(
            initializing& initializer
        );
        void* operator new(
            size_t size
        );
        void operator delete(
            void* deleted
        );
    };
    
    template<
        typename transmitting,
        typename allocating
    >
    template<
        typename initializing
    >
    http_peer_session<
        transmitting,
        allocating
    >::http_peer_session(
        initializing& initializer
    ) :
    transmitting(
        initializer
    ) {
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    void* http_peer_session<
        transmitting,
        allocating
    >::operator new(
        size_t size
    ) {
        void* result = allocator_c_wrapper<
            allocator_type
        >::malloc(
            size
        );
        if (
            NULL == result
        ) {
            throw std::bad_alloc(
            );
        }
        return result;
    }
    
    template<
        typename transmitting,
        typename allocating
    >
    void http_peer_session<
        transmitting,
        allocating
    >::operator delete(
        void* deleted
    ) {
        allocator_c_wrapper<
            allocator_type
        >::free(
            deleted
        );
    }
}

#endif
