/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef SOCKS5_LISTENER_INCLUDED
#define SOCKS5_LISTENER_INCLUDED

#include <boost/bind.hpp>
#include <boost/asio/ip/address_v4.hpp>
#include <boost/asio/placeholders.hpp>
#include "@qlib@_component_error.h"
#include <boost/function.hpp>
#include "@qlib@_stringified.h"
#include <boost/asio/ip/tcp.hpp>
#include <string>
#include <boost/asio/write.hpp>
#include <map>
namespace @qlib@ {
    
    
    template<
        typename connected
    >
    class socks5_listener {
    public:
        socks5_listener(
        );
        socks5_listener(
            std::map<
                char,
                boost::function<
                    void(
                        socks5_listener<
                            connected
                        >& self,
                        connected& socket
                    )
                >
            > const& authentication_handlers
        );
        void async_negotiate(
            connected& socket,
            boost::function<
                void(
                    boost::asio::ip::tcp::endpoint const& resolved
                )
            > const& resolved_handler,
            boost::function<
                void(
                    std::string const& hostname,
                    unsigned short port
                )
            > const& unresolved_handler,
            boost::function<
                void(
                    boost::system::error_code const& resolved
                )
            > const& error_handler
        );
        template<
            typename outbound,
            typename handling
        >
        void async_write_connect_response(
            outbound& socket,
            handling write_handler
        );
        void handle_error(
            boost::system::error_code const& error
        );
        void async_read_command(
        );
    protected:
        void handle_greeting_read(
            boost::system::error_code const& error,
            size_t bytes
        );
        void handle_methods_read(
            size_t methods,
            boost::system::error_code const& error,
            size_t bytes
        );
        void handle_greeting_write(
            boost::system::error_code const& error,
            size_t bytes
        );
        void handle_command_read(
            boost::system::error_code const& error,
            size_t bytes
        );
        void handle_ip_read(
            boost::system::error_code const& error,
            size_t bytes
        );
        void handle_hostname_size_read(
            boost::system::error_code const& error,
            size_t bytes
        );
        void handle_hostname_read(
            size_t size,
            boost::system::error_code const& error,
            size_t bytes
        );
        void handle_port_read(
            boost::system::error_code const& error,
            size_t bytes
        );
    private:
        boost::function<
            void(
                boost::asio::ip::tcp::endpoint const& resolved
            )
        > resolved_handler;
        boost::function<
            void(
                std::string const& hostname,
                unsigned short port
            )
        > unresolved_handler;
        boost::function<
            void(
                boost::system::error_code const& resolved
            )
        > error_handler;
        connected* socket;
        bool query_is_resolved;
        std::string hostname;
        boost::asio::ip::address_v4 ip;
        enum {
            max_data = 0x100
        };
        char input[
            max_data
        ];
        std::string output;
        std::map<
            char,
            boost::function<
                void(
                    socks5_listener<
                        connected
                    >& self,
                    connected& socket
                )
            >
        > authentication_handlers;
        boost::function<
            void(
                socks5_listener<
                    connected
                >& self,
                connected& socket
            )
        > authentication_processing;
    };
    
    template<
        typename connected
    >
    socks5_listener<
        connected
    >::socks5_listener(
    ) {
        authentication_handlers[
            '\x00'
        ] = boost::bind(
            &socks5_listener::async_read_command,
            _1
        );
    }
    
    template<
        typename connected
    >
    socks5_listener<
        connected
    >::socks5_listener(
        std::map<
            char,
            boost::function<
                void(
                    socks5_listener<
                        connected
                    >& self,
                    connected& socket
                )
            >
        > const& authentication_handlers
    ) :
    authentication_handlers(
        authentication_handlers
    ) {
    }
    
    template<
        typename connected
    >
    void socks5_listener<
        connected
    >::async_negotiate(
        connected& socket,
        boost::function<
            void(
                boost::asio::ip::tcp::endpoint const& resolved
            )
        > const& resolved_handler,
        boost::function<
            void(
                std::string const& hostname,
                unsigned short port
            )
        > const& unresolved_handler,
        boost::function<
            void(
                boost::system::error_code const& resolved
            )
        > const& error_handler
    ) {
        this->resolved_handler = resolved_handler;
        this->unresolved_handler = unresolved_handler;
        this->error_handler = error_handler;
        this->socket = &socket;
        boost::asio::async_read(
            socket,
            boost::asio::buffer(
                input,
                2
            ),
            boost::asio::transfer_at_least(
                2
            ),
            boost::bind(
                &socks5_listener<
                    connected
                >::handle_greeting_read,
                this,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
    
    template<
        typename connected
    >
    template<
        typename outbound,
        typename handling
    >
    void socks5_listener<
        connected
    >::async_write_connect_response(
        outbound& socket,
        handling write_handler
    ) {
        boost::asio::ip::tcp::endpoint const local_endpoint = socket.local_endpoint(
        );
        unsigned const port = local_endpoint.port(
        );
        unsigned char const port_high = port >> 8;
        unsigned char const port_low = port & 0xff;
        output = std::string(
            "\x05\x00\x00\x01",
            4
        ) + static_cast<
            char
        >(
            local_endpoint.address(
            ).to_v4(
            ).to_bytes(
            )[  
                0
            ]
        ) + static_cast<
            char
        >(
            local_endpoint.address(
            ).to_v4(
            ).to_bytes(
            )[
                1
            ]
        ) + static_cast<
            char
        >(
            local_endpoint.address(
            ).to_v4(
            ).to_bytes(
            )[
                2
            ]
        ) + static_cast<
            char
        >(
            local_endpoint.address(
            ).to_v4(
            ).to_bytes(
            )[
                3
            ]
        ) + static_cast<
            char
        >(
            port_high
        ) + static_cast<
            char
        >(
            port_low
        );
        boost::asio::async_write(
            *this->socket,
            boost::asio::buffer(
                output.data(
                ),
                output.size(
                )
            ),
            write_handler
        );
    }
    
    template<
        typename connected
    >
    void socks5_listener<
        connected
    >::handle_error(
        boost::system::error_code const& error
    ) {
        error_handler(
            error
        );
    }
    
    template<
        typename connected
    >
    void socks5_listener<
        connected
    >::handle_greeting_read(
        boost::system::error_code const& error,
        size_t bytes
    ) {
        if (
            error
        ) {
            error_handler(
                error
            );
            return;
        }
        if (
            2 != bytes
        ) {
            throw component_error(
                "socks5_listener"
            );
        }
        if (
            '\x05' != input[
                0
            ]
        ) {
            error_handler(
                boost::system::error_code(
                    socks5_error_codes::invalid_greeting,
                    socks5_category(
                    )
                )
            );
            return;
        }
        int const methods = static_cast<
            int
        >(
            input[
                1
            ]
        );
        if (
            max_data < methods
        ) {
            error_handler(
                boost::system::error_code(
                    socks5_error_codes::invalid_greeting,
                    socks5_category(
                    )
                )
            );
            return;
        }
        boost::asio::async_read(
            *socket,
            boost::asio::buffer(
                input,
                methods
            ),
            boost::asio::transfer_at_least(
                methods
            ),
            boost::bind(
                &socks5_listener<
                    connected
                >::handle_methods_read,
                this,
                methods,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
    
    template<
        typename connected
    >
    void socks5_listener<
        connected
    >::handle_methods_read(
        size_t methods,
        boost::system::error_code const& error,
        size_t bytes
    ) {
        if (
            error
        ) {
            error_handler(
                error
            );
            return;
        }
        if (
            methods != bytes
        ) {
            throw component_error(
                "socks5_listener"
            );
        }
        std::string const method_string(
            input,
            bytes
        );
        bool method_found = false;
        typename std::map<
            char,
            boost::function<
                void(
                    socks5_listener<
                        connected
                    >& self,
                    connected& socket
                )
            >
        >::const_iterator used_method;
        for (
            std::string::const_iterator trying = method_string.begin(
            );
            method_string.end(
            ) != trying;
            trying++
        ) {
            used_method = authentication_handlers.find(
                *trying
            );
            if (
                authentication_handlers.end(
                ) != used_method
            ) {
                method_found = true;
                break;
            }
        }
        if (
            !method_found
        ) {
            error_handler(
                boost::system::error_code(
                    socks5_error_codes::unsupported_authentication,
                    socks5_category(
                    )
                )
            );
            return;
        }
        authentication_processing = used_method->second;
        output = std::string(
            "\x05",
            1
        ) + std::string(
            1,
            used_method->first
        );
        boost::asio::async_write(
            *socket,
            boost::asio::buffer(
                output.data(
                ),
                output.size(
                )
            ),
            boost::bind(
                &socks5_listener<
                    connected
                >::handle_greeting_write,
                this,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
    
    template<
        typename connected
    >
    void socks5_listener<
        connected
    >::handle_greeting_write(
        boost::system::error_code const& error,
        size_t bytes
    ) {
        if (
            error
        ) {
            error_handler(
                error
            );
            return;
        }
        if (
            output.size(
            ) != bytes
        ) {
            throw component_error(
                "socks5_listener"
            );
        }
        authentication_processing(
            *this,
            *socket
        );
    }
    
    template<
        typename connected
    >
    void socks5_listener<
        connected
    >::async_read_command(
    ) {
        boost::asio::async_read(
            *socket,
            boost::asio::buffer(
                input,
                4
            ),
            boost::asio::transfer_at_least(
                4
            ),
            boost::bind(
                &socks5_listener<
                    connected
                >::handle_command_read,
                this,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
    
    template<
        typename connected
    >
    void socks5_listener<
        connected
    >::handle_command_read(
        boost::system::error_code const& error,
        size_t bytes
    ) {
        if (
            error
        ) {
            error_handler(
                error
            );
            return;
        }
        if (
            4 != bytes
        ) {
            throw component_error(
                "socks5_listener"
            );
        }
        if (
            std::string(
                "\x05\x01\x00",
                3
            ) != std::string(
                input,
                3
            )
        ) {
            error_handler(
                boost::system::error_code(
                    socks5_error_codes::invalid_command,
                    socks5_category(
                    )
                )
            );
            return;
        }
        switch (
            input[
                3
            ]
        ) {
        case '\x01':
            boost::asio::async_read(
                *socket,
                boost::asio::buffer(
                    input,
                    4
                ),
                boost::asio::transfer_at_least(
                    4
                ),
                boost::bind(
                    &socks5_listener<
                        connected
                    >::handle_ip_read,
                    this,
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred
                )
            );
            break;
        case '\x03':
            boost::asio::async_read(
                *socket,
                boost::asio::buffer(
                    input,
                    1
                ),
                boost::asio::transfer_at_least(
                    1
                ),
                boost::bind(
                    &socks5_listener<
                        connected
                    >::handle_hostname_size_read,
                    this,
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred
                )
            );
            break;
        default:
            error_handler(
                boost::system::error_code(
                    socks5_error_codes::invalid_command,
                    socks5_category(
                    )
                )
            );
            return;
        }
    }
    
    template<
        typename connected
    >
    void socks5_listener<
        connected
    >::handle_ip_read(
        boost::system::error_code const& error,
        size_t bytes
    ) {
        if (
            error
        ) {
            error_handler(
                error
            );
            return;
        }
        if (
            4 != bytes
        ) {
            throw component_error(
                "socks5_listener"
            );
        }
        ip = boost::asio::ip::address_v4::from_string(
            stringified(
                static_cast<
                    unsigned
                >(
                    static_cast<
                        unsigned char
                    >(
                        input[
                            0
                        ]
                    )
                )
            ) + "." + stringified(
                static_cast<
                    unsigned
                >(
                    static_cast<
                        unsigned char
                    >(
                        input[
                            1
                        ]
                    )   
                )
            ) + "." + stringified(
                static_cast<
                    unsigned
                >(
                    static_cast<
                        unsigned char
                    >(
                        input[
                            2
                        ]
                    )   
                )
            ) + "." + stringified(
                static_cast<
                    unsigned
                >(
                    static_cast<
                        unsigned char
                    >(
                        input[
                            3
                        ]
                    )   
                )
            )
        );
        query_is_resolved = true;
        boost::asio::async_read(
            *socket,
            boost::asio::buffer(
                input,
                2
            ),
            boost::asio::transfer_at_least(
                2
            ),
            boost::bind(
                &socks5_listener<
                    connected
                >::handle_port_read,
                this,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
    
    template<
        typename connected
    >
    void socks5_listener<
        connected
    >::handle_hostname_size_read(
        boost::system::error_code const& error,
        size_t bytes
    ) {
        if (
            error
        ) {
            error_handler(
                error
            );
            return;
        }
        if (
            1 != bytes
        ) {
            throw component_error(
                "socks5_listener"
            );
        }
        int const size = static_cast<
            unsigned char
        >(
            input[
                0
            ]
        );
        if (
            max_data < size
        ) {
            error_handler(
                boost::system::error_code(
                    socks5_error_codes::hostname_too_long,
                    socks5_category(
                    )
                )
            );
            return;
        }
        boost::asio::async_read(
            *socket,
            boost::asio::buffer(
                input,
                size
            ),
            boost::asio::transfer_at_least(
                size
            ),
            boost::bind(
                &socks5_listener<
                    connected
                >::handle_hostname_read,
                this,
                size,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
    
    template<
        typename connected
    >
    void socks5_listener<
        connected
    >::handle_hostname_read(
        size_t size,
        boost::system::error_code const& error,
        size_t bytes
    ) {
        if (
            error
        ) {
            error_handler(
                error
            );
            return;
        }
        if (
            size != bytes
        ) {
            throw component_error(
                "socks5_listener"
            );
        }
        hostname = std::string(
            input,
            bytes
        );
        query_is_resolved = false;
        boost::asio::async_read(
            *socket,
            boost::asio::buffer(
                input,
                2
            ),
            boost::asio::transfer_at_least(
                2
            ),
            boost::bind(
                &socks5_listener<
                    connected
                >::handle_port_read,
                this,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred
            )
        );
    }
    
    template<
        typename connected
    >
    void socks5_listener<
        connected
    >::handle_port_read(
        boost::system::error_code const& error,
        size_t bytes
    ) {
        if (
            error
        ) {
            error_handler(
                error
            );
            return;
        }
        if (
            2 != bytes
        ) {
            throw component_error(
                "socks5_listener"
            );
        }
        unsigned short port = static_cast<
            unsigned char
        >(
            input[
                0
            ]
        );
        port <<= 8;
        port += static_cast<
            unsigned char
        >(
            input[
                1
            ]
        );
        if (
            query_is_resolved
        ) {
            resolved_handler(
                boost::asio::ip::tcp::endpoint(
                    ip,
                    port
                )
            );
        } else {
            unresolved_handler(
                hostname,
                port
            );
        }
    }
}

#endif
