/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef JSON_OBJECT_INCLUDED
#define JSON_OBJECT_INCLUDED

#include "@qlib@_fixed_point_number.h"
#include <string>
#include <map>
#include <vector>
#include <boost/variant.hpp>
#include "@qlib@_null_policy.h"
namespace @qlib@ {
    
    
    class json_object :
    public boost::variant<
        null_policy,
        fixed_point_number,
        std::string,
        bool,
        std::vector<
            json_object
        >,
        std::map<
            std::string,
            json_object
        >
    > {
    public:
        json_object(
        );
        template<
            typename initializing
        >
        json_object(
            initializing const& initializer
        );
    };
    
    template<
        typename initializing
    >
    json_object::json_object(
        initializing const& initializer
    ) :
    boost::variant<
        null_policy,
        fixed_point_number,
        std::string,
        bool,
        std::vector<
            json_object
        >,
        std::map<
            std::string,
            json_object
        >
    >(
        initializer
    ) {
    }
}

#endif
