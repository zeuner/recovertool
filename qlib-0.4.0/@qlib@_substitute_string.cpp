/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_substitute_string.h"
namespace @qlib@ {
    
    
    std::string substitute_string(
        std::string const& raw,
        std::string const& searched,
        std::string const& replaced
    ) {
        std::string result;
        std::string::size_type last = 0;
        std::string::size_type found;
        while (
            std::string::npos != (
                found = raw.find(
                    searched,
                    last
                )
            )
        ) {
            result += std::string(
                raw.begin(
                ) + last,
                raw.begin(
                ) + found
            ) + replaced;
            last = found + searched.size(
            );
        }
        result += std::string(
            raw.begin(
            ) + last,
            raw.end(
            )
        );
        return result;
    }
}
