/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_soap_request.h"
namespace @qlib@ {
    
    
    soap_request::soap_request(
        soap_service const& service,
        std::string const& request
    ) :
    header(
        service.get_request_header(
            request
        )
    ),
    body(
        service.get_request_body(
            request
        )
    ) {
    }
    
    soap_element& soap_request::get_header(
    ) {
        return *header;
    }
    
    soap_element& soap_request::get_body(
    ) {
        return *body;
    }
    
    std::string soap_request::dump_request(
    ) const {
        xml_string ns(
            BAD_CAST "http://schemas.xmlsoap.org/soap/envelope/"
        );
        xml_document dumping(
            BAD_CAST "<Envelope xmlns='" + ns + BAD_CAST "'/>\n"
        );
        for (
            std::map<
                std::string,
                std::string
            >::const_iterator adding = prefixes.begin(
            );
            prefixes.end(
            ) != adding;
            adding++
        ) {
            xmlNewNs(
                dumping.get_root_node(
                ),
                BAD_CAST adding->first.c_str(
                ),
                BAD_CAST adding->second.c_str(
                )
            );
        }
        xmlNodePtr const header_dumping = xmlNewChild(
            dumping.get_root_node(
            ),
            dumping.get_root_node(
            )->ns,
            BAD_CAST "Header",
            NULL
        );
        xmlNodePtr const body_dumping = xmlNewChild(
            dumping.get_root_node(
            ),
            dumping.get_root_node(
            )->ns,
            BAD_CAST "Body",
            NULL
        );
        header->dump_to_xml(
            header_dumping
        );
        body->dump_to_xml(
            body_dumping
        );
        xml_string result;
        dumping.export_text(
            result
        );
        return std::string(
            reinterpret_cast<
                char const*
            >(
                result.data(
                )
            ),
            result.size(
            )
        );
    }
    
    void soap_request::add_namespace(
        std::string const& prefix,
        std::string const& href
    ) {
        prefixes[
            href
        ] = prefix;
    }
}
