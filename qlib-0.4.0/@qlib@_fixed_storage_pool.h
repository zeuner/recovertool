/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef FIXED_STORAGE_POOL_INCLUDED
#define FIXED_STORAGE_POOL_INCLUDED

#include <vector>
namespace @qlib@ {
    
    
    class fixed_storage_pool {
    public:
        fixed_storage_pool(
            size_t element_size,
            size_t count
        );
        ~fixed_storage_pool(
        );
        void* get(
        );
        void put(
            void* freed
        );
    private:
        size_t const element_size;
        char* const allocated;
        char* last_unused;
        std::vector<
            void*
        > free_list;
        std::vector<
            void*
        >::iterator const first_free;
        std::vector<
            void*
        >::iterator last_free;
    };
}

#endif
