/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef TCP_LAYER_LINK_INCLUDED
#define TCP_LAYER_LINK_INCLUDED

#include "@qlib@_c_string.h"
#include <unistd.h>
#include <cstring>
#include <string>
#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    template<
        typename network_layer_link
    >
    class tcp_layer_link {
    public:
        tcp_layer_link(
            unsigned short from_port,
            unsigned short to_port,
            ethernet_layer_link const& ethernet_layer,
            network_layer_link& network_layer,
            uint32_t sequence_number
        );
        unsigned short get_local_port(
        ) const;
        unsigned short get_remote_port(
        ) const;
        void set_window(
            unsigned short window
        );
        bool consume_packet(
            struct tcphdr const* received,
            size_t length
        );
        void finish_close(
        );
        void reset_sequence(
            uint32_t sequence_number
        );
        void send_syn_packet(
            std::basic_string<
                unsigned char
            > const& options,
            memory_mapped_send_socket& transmitting
        );
        void send_fin_packet(
            std::basic_string<
                unsigned char
            > const& options,
            memory_mapped_send_socket& transmitting
        );
        void send_rst_packet(
            std::basic_string<
                unsigned char
            > const& options,
            memory_mapped_send_socket& transmitting
        );
        void send_packet(
            std::basic_string<
                unsigned char
            > const& options,
            std::string const& payload,
            bool syn,
            bool ack,
            bool psh,
            bool rst,
            bool fin,
            memory_mapped_send_socket& transmitting
        );
        void resend_packet(
            std::basic_string<
                unsigned char
            > const& options,
            std::string const& payload,
            memory_mapped_send_socket& transmitting
        );
        enum tcp_state {
            closed,
            syn_sent,
            established,
            fin_wait_1,
            fin_wait_2,
            close_wait,
            time_wait,
            last_ack,
        };
        void set_state(
            enum tcp_state new_state
        );
        enum tcp_state get_state(
        ) const;
        uint32_t get_local_sequence_number(
        ) const;
        uint32_t get_remote_sequence_number(
        ) const;
        network_layer_link const& get_network_layer(
        ) const;
        static uint8_t get_protocol(
        );
    private:
        network_layer_link& network_layer;
        struct ether_header ethernet_header;
        typename network_layer_link::native_type network_header;
        struct tcphdr tcp_header;
        uint32_t local_sequence_number;
        uint32_t remote_sequence_number;
        enum tcp_state connection_state;
    };
    
    template<
        typename network_layer_link
    >
    tcp_layer_link<
        network_layer_link
    >::tcp_layer_link(
        unsigned short from_port,
        unsigned short to_port,
        ethernet_layer_link const& ethernet_layer,
        network_layer_link& network_layer,
        uint32_t sequence_number
    ) :
    network_layer(
        network_layer
    ),
    local_sequence_number(
        sequence_number
    ),
    remote_sequence_number(
        0
    ),
    connection_state(
        closed
    ) {
        memset(
            &tcp_header,
            0,
            sizeof(
                tcp_header
            )
        );
        ethernet_layer.write_header(
            ethernet_header
        );
        tcp_header.source = htons(
            from_port
        );
        tcp_header.dest = htons(
            to_port
        );
        tcp_header.window = htons(
            14600
        );
    }
    
    template<
        typename network_layer_link
    >
    unsigned short tcp_layer_link<
        network_layer_link
    >::get_local_port(
    ) const {
        return ntohs(
            tcp_header.source
        );
    }
    
    template<
        typename network_layer_link
    >
    unsigned short tcp_layer_link<
        network_layer_link
    >::get_remote_port(
    ) const {
        return ntohs(
            tcp_header.dest
        );
    }
    
    template<
        typename network_layer_link
    >
    void tcp_layer_link<
        network_layer_link
    >::set_window(
        unsigned short window
    ) {
        tcp_header.window = htons(
            window
        );
    }
    
    template<
        typename network_layer_link
    >
    bool tcp_layer_link<
        network_layer_link
    >::consume_packet(
        struct tcphdr const* received,
        size_t length
    ) {
        uint32_t const received_local_seq = ntohl(
            received->ack_seq
        );
        uint32_t const received_remote_seq = ntohl(
            received->seq
        );
        if (
            local_sequence_number != received_local_seq
        ) {
            return false;
        }
        switch (
            connection_state
        ) {
        case syn_sent:
            if (
                received->ack && received->syn
            ) {
                connection_state = established;
                remote_sequence_number = received_remote_seq + 1;
            }
            break;
        case established:
            if (
                received->psh
            ) {
                remote_sequence_number = received_remote_seq;
                remote_sequence_number += length - 4 * received->doff;
            }
            if (
                received->fin
            ) {
                remote_sequence_number = received_remote_seq + 1;
                connection_state = close_wait;
            }
            break;
        case fin_wait_1:
            if (
                received->ack
            ) {
                remote_sequence_number = received_remote_seq;
                connection_state = fin_wait_2;
            }
            break;
        case fin_wait_2:
            if (
                received->fin
            ) {
                remote_sequence_number = received_remote_seq + 1;
                connection_state = time_wait;
            }
            break;
        case last_ack:
            if (
                received->ack
            ) {
                remote_sequence_number = received_remote_seq;
                connection_state = closed;
            }
            break;
        default:
            throw component_error(
                "tcp_layer_link"
            );
        }
        return true;
    }
    
    template<
        typename network_layer_link
    >
    void tcp_layer_link<
        network_layer_link
    >::send_fin_packet(
        std::basic_string<
            unsigned char
        > const& options,
        memory_mapped_send_socket& transmitting
    ) {
        send_packet(
            options,
            std::string(
            ),
            false,
            true,
            false,
            false,
            true,
            transmitting
        );
        switch (
            connection_state
        ) {
        case established:
            connection_state = fin_wait_1;
            break;
        case close_wait:
            connection_state = last_ack;
            break;
        default:
            throw component_error(
                "tcp_layer_link"
            );
        }
        local_sequence_number++;
    }
    
    template<
        typename network_layer_link
    >
    void tcp_layer_link<
        network_layer_link
    >::send_rst_packet(
        std::basic_string<
            unsigned char
        > const& options,
        memory_mapped_send_socket& transmitting
    ) {
        if (
            closed == connection_state
        ) {
            throw component_error(
                "tcp_layer_link"
            );
        }
        send_packet(
            options,
            std::string(
            ),
            false,
            false,
            false,
            true,
            false,
            transmitting
        );
        connection_state = closed;
    }
    
    template<
        typename network_layer_link
    >
    void tcp_layer_link<
        network_layer_link
    >::finish_close(
    ) {
        if (
            time_wait != connection_state
        ) {
            throw component_error(
                "tcp_layer_link"
            );
        }
        connection_state = closed;
    }
    
    template<
        typename network_layer_link
    >
    void tcp_layer_link<
        network_layer_link
    >::reset_sequence(
        uint32_t sequence_number
    ) {
        if (
            closed != connection_state
        ) {
            throw component_error(
                "tcp_layer_link"
            );
        }
        local_sequence_number = sequence_number;
        remote_sequence_number = 0;
    }
    
    template<
        typename network_layer_link
    >
    void tcp_layer_link<
        network_layer_link
    >::send_syn_packet(
        std::basic_string<
            unsigned char
        > const& options,
        memory_mapped_send_socket& transmitting
    ) {
        if (
            closed != connection_state
        ) {
            throw component_error(
                "tcp_layer_link"
            );
        }
        send_packet(
            options,
            std::string(
            ),
            true,
            false,
            false,
            false,
            false,
            transmitting
        );
        connection_state = syn_sent;
        local_sequence_number++;
    }
    
    template<
        typename network_layer_link
    >
    void tcp_layer_link<
        network_layer_link
    >::send_packet(
        std::basic_string<
            unsigned char
        > const& options,
        std::string const& payload,
        bool syn,
        bool ack,
        bool psh,
        bool rst,
        bool fin,
        memory_mapped_send_socket& transmitting
    ) {
        int tcp_padding = 0;
        int const tcp_offset = sizeof(
            tcp_header
        ) % 4;
        if (
            0 != options.size(
            )
        ) {
            if (
                0 != tcp_offset
            ) {
                tcp_padding = 4 - tcp_offset;
            }
        }
        unsigned const required_size = sizeof(
            ethernet_header
        ) + sizeof(
            network_header
        ) + sizeof(
            tcp_header
        ) + tcp_padding + options.size(
        ) + payload.size(
        );
        if (
            transmitting.get_frame_size(
            ) < required_size
        ) {
            throw component_error(
                "tcp_layer_link"
            );
        }
        unsigned long* status;
        unsigned* size;
        unsigned char* buffer;
        bool found = transmitting.get_packet_buffer(
            status,
            size,
            buffer
        );
        if (
            !found
        ) {
            throw component_error(
                "tcp_layer_link"
            );
        }
        struct ether_header* written_ethernet_header = reinterpret_cast<
            struct ether_header*
        >(
            buffer
        );
        typename network_layer_link::native_type* written_network_header(
            reinterpret_cast<
                typename network_layer_link::native_type*
            >(
                written_ethernet_header + 1
            )
        );
        struct tcphdr* written_tcp_header = reinterpret_cast<
            struct tcphdr*
        >(
            written_network_header + 1
        );
        memcpy(
            written_ethernet_header,
            &ethernet_header,
            sizeof(
                ethernet_header
            )
        );
        network_layer.write_header(
            *written_network_header,
            sizeof(
                tcp_header
            ) + options.size(
            ) + payload.size(
            )
        );
        tcp_header.seq = htonl(
            local_sequence_number
        );
        tcp_header.ack_seq = htonl(
            remote_sequence_number
        );
        tcp_header.doff = (
            sizeof(
                tcp_header
            ) + options.size(
            )
        ) / 4;
        tcp_header.syn = syn;
        tcp_header.ack = ack;
        tcp_header.psh = psh;
        tcp_header.rst = rst;
        tcp_header.fin = fin;
        memcpy(
            written_tcp_header,
            &tcp_header,
            sizeof(
                tcp_header
            )
        );
        written_tcp_header->check = 0;
        unsigned char* const after_tcp_header = reinterpret_cast<
            unsigned char*
        >(
            written_tcp_header + 1
        );
        memset(
            after_tcp_header,
            0,
            tcp_padding
        );
        unsigned char* const after_padding = after_tcp_header + tcp_padding;
        memcpy(
            after_padding,
            options.data(
            ),
            options.size(
            )
        );
        unsigned char* const after_options = after_padding + options.size(
        );
        memcpy(
            after_options,
            payload.data(
            ),
            payload.size(
            )
        );
        unsigned char* const after_payload = after_options + payload.size(
        );
        tcp_header.check = written_tcp_header->check = tcp_header_checksum(
            written_network_header->saddr,
            written_network_header->daddr,
            reinterpret_cast<
                unsigned short const*
            >(
                written_tcp_header
            ),
            after_payload - reinterpret_cast<
                unsigned char*
            >(
                written_tcp_header
            )
        );
        *size = after_payload - reinterpret_cast<
            unsigned char*
        >(
            written_ethernet_header
        );
        *status = TP_STATUS_SEND_REQUEST;
        local_sequence_number += payload.size(
        );
    }
    
    template<
        typename network_layer_link
    >
    void tcp_layer_link<
        network_layer_link
    >::resend_packet(
        std::basic_string<
            unsigned char
        > const& options,
        std::string const& payload,
        memory_mapped_send_socket& transmitting
    ) {
        int tcp_padding = 0;
        int const tcp_offset = sizeof(
            tcp_header
        ) % 4;
        if (
            0 != options.size(
            )
        ) {
            if (
                0 != tcp_offset
            ) {
                tcp_padding = 4 - tcp_offset;
            }
        }
        unsigned long* status;
        unsigned* size;
        unsigned char* buffer;
        bool found = transmitting.get_packet_buffer(
            status,
            size,
            buffer
        );
        if (
            !found
        ) {
            throw component_error(
                "tcp_layer_link"
            );
        }
        struct ether_header* written_ethernet_header = reinterpret_cast<
            struct ether_header*
        >(
            buffer
        );
        typename network_layer_link::native_type* written_network_header(
            reinterpret_cast<
                typename network_layer_link::native_type*
            >(
                written_ethernet_header + 1
            )
        );
        struct tcphdr* written_tcp_header = reinterpret_cast<
            struct tcphdr*
        >(
            written_network_header + 1
        );
        memcpy(
            written_ethernet_header,
            &ethernet_header,
            sizeof(
                ethernet_header
            )
        );
        network_layer.write_header(
            *written_network_header,
            sizeof(
                tcp_header
            ) + options.size(
            ) + payload.size(
            )
        );
        memcpy(
            written_tcp_header,
            &tcp_header,
            sizeof(
                tcp_header
            )
        );
        unsigned char* const after_tcp_header = reinterpret_cast<
            unsigned char*
        >(
            written_tcp_header + 1
        );
        memset(
            after_tcp_header,
            0,
            tcp_padding
        );
        unsigned char* const after_padding = after_tcp_header + tcp_padding;
        memcpy(
            after_padding,
            options.data(
            ),
            options.size(
            )
        );
        unsigned char* const after_options = after_padding + options.size(
        );
        memcpy(
            after_options,
            payload.data(
            ),
            payload.size(
            )
        );
        unsigned char* const after_payload = after_options + payload.size(
        );
        *size = after_payload - reinterpret_cast<
            unsigned char*
        >(
            written_ethernet_header
        );
        *status = TP_STATUS_SEND_REQUEST;
        local_sequence_number += payload.size(
        );
    }
    
    template<
        typename network_layer_link
    >
    void tcp_layer_link<
        network_layer_link
    >::set_state(
        enum tcp_state new_state
    ) {
        connection_state = new_state;
    }
    
    template<
        typename network_layer_link
    >
    enum tcp_layer_link<
        network_layer_link
    >::tcp_state tcp_layer_link<
        network_layer_link
    >::get_state(
    ) const {
        return connection_state;
    }
    
    template<
        typename network_layer_link
    >
    uint32_t tcp_layer_link<
        network_layer_link
    >::get_local_sequence_number(
    ) const {
        return local_sequence_number;
    }
    
    template<
        typename network_layer_link
    >
    uint32_t tcp_layer_link<
        network_layer_link
    >::get_remote_sequence_number(
    ) const {
        return remote_sequence_number;
    }
    
    template<
        typename network_layer_link
    >
    network_layer_link const& tcp_layer_link<
        network_layer_link
    >::get_network_layer(
    ) const {
        return network_layer;
    }
    
    template<
        typename network_layer_link
    >
    uint8_t tcp_layer_link<
        network_layer_link
    >::get_protocol(
    ) {
        return IPPROTO_TCP;
    }
}

#endif
