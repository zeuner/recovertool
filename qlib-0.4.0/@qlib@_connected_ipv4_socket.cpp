/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_connected_ipv4_socket.h"
#include <boost/asio/placeholders.hpp>
#include <boost/bind.hpp>
namespace @qlib@ {
    
    
    connected_ipv4_socket::connected_ipv4_socket(
        boost::asio::io_service& io_service,
        boost::asio::ip::address_v4 const& destination,
        short port,
        boost::function<
            void(
                boost::system::error_code const& error,
                connected_ipv4_socket* connected
            )
        > const& connect_handler
    ) :
    boost::asio::ip::tcp::socket(
        io_service
    ),
    connect_handler(
        connect_handler
    ) {
        async_connect(
            destination,
            port
        );
    }
    
    connected_ipv4_socket::connected_ipv4_socket(
        boost::asio::io_service& io_service,
        boost::asio::ip::address_v4 const& destination,
        short port,
        boost::function<
            void(
                boost::system::error_code const& error,
                connected_ipv4_socket* connected
            )
        > const& connect_handler,
        boost::asio::ip::tcp::endpoint const& local_endpoint
    ) :
    boost::asio::ip::tcp::socket(
        io_service,
        local_endpoint
    ),
    connect_handler(
        connect_handler
    ) {
        async_connect(
            destination,
            port
        );
    }
    
    
    void connected_ipv4_socket::async_connect(
        boost::asio::ip::address_v4 const& destination,
        short port
    ) {
        boost::asio::ip::tcp::endpoint endpoint(
            destination,
            port
        );
        boost::asio::ip::tcp::socket::async_connect(
            endpoint,
            boost::bind(
                &connected_ipv4_socket::handle_connect,
                this,
                boost::asio::placeholders::error
            )
        );
    }
    
    void connected_ipv4_socket::handle_connect(
        boost::system::error_code const& error
    ) {
        connect_handler(
            error,
            this
        );
    }
}
