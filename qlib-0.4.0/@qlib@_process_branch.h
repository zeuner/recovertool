/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef PROCESS_BRANCH_INCLUDED
#define PROCESS_BRANCH_INCLUDED

namespace @qlib@ {
    
    
    class process_branch {
    public:
        process_branch(
        );
        ~process_branch(
        );
        bool is_child(
        ) const;
        bool is_parent_done(
        ) const;
    protected:
        static void handle_done_signal(
            int signal_number
        );
    private:
        pid_t const child;
        bool parent_done;
        static process_branch* parent_connection;
    };
}

#endif
