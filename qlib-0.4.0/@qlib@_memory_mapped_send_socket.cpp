/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_memory_mapped_send_socket.h"
#include "@qlib@_component_error.h"
#include <cstring>
#include <unistd.h>
namespace @qlib@ {
    
    
    memory_mapped_send_socket::memory_mapped_send_socket(
        boost::asio::io_service& communicating,
        std::string const& interface,
        unsigned block_size,
        unsigned block_count,
        unsigned frame_size,
        unsigned frame_count
    ) {
        buffer_request.tp_block_size = block_size;
        buffer_request.tp_block_nr = block_count;
        buffer_request.tp_frame_size = frame_size;
        buffer_request.tp_frame_nr = frame_count;
        memset(
            &next_hop,
            0,
            sizeof(
                next_hop
            )
        );
        next_hop.sll_family = AF_PACKET;
        next_hop.sll_protocol = htons(
            ETH_P_IP
        );
        std::string const mac_address = interface_next_hop_mac(
            interface
        );
        if (
            6 < mac_address.size(
            )
        ) {
            throw component_error(
                "memory_mapped_send_socket"
            );
        }
        memcpy(
            next_hop.sll_addr,
            mac_address.data(
            ),
            mac_address.size(
            )
        );
        next_hop.sll_halen = mac_address.size(
        );
        next_hop.sll_ifindex = if_nametoindex(
            interface.c_str(
            )
        );
        int native = socket(
            PF_PACKET,
            SOCK_RAW,
            IPPROTO_RAW
        );
        if (
            0 > native
        ) {
            throw component_error(
                "memory_mapped_send_socket"
            );
        }
        struct sockaddr_ll native_address;
        memset(
            &native_address,
            0,
            sizeof(
                native_address
            )
        );
        native_address.sll_ifindex = if_nametoindex(
            interface.c_str(
            )
        );  
        native_address.sll_family = AF_PACKET;
        native_address.sll_protocol = htons(
            ETH_P_IP
        );
        if (
            0 != bind(
                native,
                reinterpret_cast<
                    struct sockaddr*
                >(
                    &native_address
                ),
                sizeof(
                    native_address
                )
            )
        ) {
            close(
                native
            );
            throw component_error(
                "memory_mapped_send_socket"
            );
        }
        if (
            0 != setsockopt(
                native,
                SOL_PACKET,
                PACKET_TX_RING,
                &buffer_request,
                sizeof(
                    buffer_request
                )
            )
        ) {
            close(
                native
            );
            throw component_error(
                "memory_mapped_send_socket"
            );
        }
        void* mapped;
        if (
            MAP_FAILED == (
                mapped = mmap(
                    NULL,
                    buffer_request.tp_block_size * buffer_request.tp_block_nr,
                    PROT_READ | PROT_WRITE,
                    MAP_SHARED,
                    native,
                    0
                )
            )
        ) {
            close(
                native
            );
            throw component_error(
                "memory_mapped_send_socket"
            );
        }
        mapped_bytes = static_cast<
            unsigned char*
        >(
            mapped
        );
        last_frame = mapped_bytes;
        frames_end = last_frame + frame_size * frame_count;
        handle = new boost::asio::basic_raw_socket<
            packet_raw_protocol
        >(
            communicating,
            packet_raw_protocol(
            ),
            native
        );
    }
    
    memory_mapped_send_socket::~memory_mapped_send_socket(
    ) {
        munmap(
            mapped_bytes,
            buffer_request.tp_block_size * buffer_request.tp_block_nr
        );
        delete handle;
    }
    
    bool memory_mapped_send_socket::get_packet_buffer(
        unsigned long*& status,
        unsigned*& size,
        unsigned char*& buffer
    ) {
        unsigned char* current_frame = last_frame;
        do {
            struct tpacket_hdr* header = reinterpret_cast<
                struct tpacket_hdr*
            >(
                current_frame
            );
            current_frame += buffer_request.tp_frame_size;
            if (
                frames_end == current_frame
            ) {
                current_frame = mapped_bytes;
            }
            if (
                TP_STATUS_AVAILABLE == header->tp_status
            ) {
                buffer = reinterpret_cast<
                    unsigned char*
                >(
                    header
                ) + TPACKET_ALIGN(
                    sizeof(
                        *header
                    )
                );
                size = &(
                    header->tp_len
                );
                status = &(
                    header->tp_status
                );
                last_frame = current_frame;
                return true;
            }
            if (
                last_frame == current_frame
            ) {
                return false;
            }
        } while (
            true
        );
    }
    
    unsigned memory_mapped_send_socket::get_frame_size(
    ) const {
        return buffer_request.tp_frame_size;
    }
    
    void memory_mapped_send_socket::send_pending_packets(
    ) {
        sendto(
            handle->native(
            ),
            NULL,
            0,
            MSG_DONTWAIT,
            reinterpret_cast<
                struct sockaddr*
            >(
                &next_hop
            ),
            sizeof(
                next_hop
            )
        );
    }
    
    boost::asio::io_service& memory_mapped_send_socket::get_io_service(
    ) {
        return handle->get_io_service(
        );
    }
}
