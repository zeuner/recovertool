/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef GENERIC_SOCKET_INCLUDED
#define GENERIC_SOCKET_INCLUDED

#include <unistd.h>
#include <boost/function.hpp>
#include <string>
#include <boost/asio/ip/tcp.hpp>
namespace @qlib@ {
    
    
    class generic_socket {
    public:
        virtual ~generic_socket(
        );
        typedef generic_socket lowest_layer_type;
        lowest_layer_type& lowest_layer(
        );
        virtual void async_connect(
            boost::asio::ip::tcp::endpoint const& endpoint,
            boost::function<
                void(
                    boost::system::error_code const& error
                )
            > connect_handler
        ) = 0;
        virtual void async_connect(
            std::string const& hostname,
            unsigned short port,
            boost::function<
                void(
                    boost::system::error_code const& error
                )
            > const& connect_handler
        ) = 0;
        virtual void close(
        ) = 0;
        virtual void cancel(
        ) = 0;
        virtual boost::asio::io_service& get_io_service(
        ) = 0;
        virtual void async_read_some(
            boost::asio::mutable_buffer const& destination,
            boost::function<
                void(
                    boost::system::error_code const& error,
                    size_t received
                )
            > const& read_handler
        ) = 0;
        virtual void async_write_some(
            boost::asio::mutable_buffer const& source,
            boost::function<
                void(
                    boost::system::error_code const& error,
                    size_t sent
                )
            > const& write_handler
        ) = 0;
        virtual void async_write_some(
            boost::asio::const_buffer const& source,
            boost::function<
                void(
                    boost::system::error_code const& error,
                    size_t sent
                )
            > const& write_handler
        ) = 0;
        virtual boost::asio::ip::tcp::endpoint local_endpoint(
        ) const = 0;
    };
}

#endif
