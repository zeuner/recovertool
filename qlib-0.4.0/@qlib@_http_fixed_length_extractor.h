/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef HTTP_FIXED_LENGTH_EXTRACTOR_INCLUDED
#define HTTP_FIXED_LENGTH_EXTRACTOR_INCLUDED

#include <algorithm>
namespace @qlib@ {
    
    template<
        typename string
    >
    class http_fixed_length_extractor {
    public:
        http_fixed_length_extractor(
            typename string::size_type required
        );
        bool done(
        ) const;
        string extract(
            typename string::const_iterator& from,
            typename string::const_iterator const& to
        );
    private:
        typename string::size_type required;
    };
    
    template<
        typename string
    >
    http_fixed_length_extractor<
        string
    >::http_fixed_length_extractor(
        typename string::size_type required
    ) :
    required(
        required
    ) {
    }
    
    template<
        typename string
    >
    bool http_fixed_length_extractor<
        string
    >::done(
    ) const {
        return 0 == required;
    }
    
    template<
        typename string
    >
    string http_fixed_length_extractor<
        string
    >::extract(
        typename string::const_iterator& from,
        typename string::const_iterator const& to
    ) {
        typename string::const_iterator const old_from = from;
        typename string::size_type const available = to - from;
        typename string::size_type const usable = std::min(
            required,
            available
        );
        from += usable;
        required -= usable;
        return string(
            old_from,
            from
        );
    }
}

#endif
