/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef NETLINK_RECEIVED_MESSAGE_INCLUDED
#define NETLINK_RECEIVED_MESSAGE_INCLUDED

namespace @qlib@ {
    
    
    class netlink_received_message {
    public:
        netlink_received_message(
            int descriptor,
            size_t buffer_size
        );
        ~netlink_received_message(
        );
        size_t get_size(
        ) const;
        class iterator;
        iterator begin(
        ) const;
        iterator end(
        ) const;
        class iterator {
        public:
            iterator(
                netlink_received_message const& message,
                struct nlmsghdr const* position
            );
            size_t payload_length(
            ) const;
            void const* data(
            ) const;
            struct nlmsghdr const* operator*(
            ) const;
            struct nlmsghdr const* operator->(
            ) const;
            bool operator==(
                iterator const& other
            ) const;
            bool operator!=(
                iterator const& other
            ) const;
            iterator operator++(
            );
            iterator operator++(
                int
            );
        private:
            netlink_received_message const& message;
            struct nlmsghdr const* position;
            size_t available;
        };
    private:
        char* buffer;
        struct iovec io_vector;
        struct msghdr data;
        size_t size;
    };
}

#endif
