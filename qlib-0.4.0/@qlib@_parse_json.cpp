/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_parse_json.h"
#include <vector>
#include <unistd.h>
#include <map>
#include "@qlib@_throw_exception.h"
#include "@qlib@_component_error.h"
#include <list>
namespace @qlib@ {
    
    
    void parse_json(
        std::string const& raw,
        json_object& parsed,
        bool* error
    ) {
        typedef std::string::const_iterator iterator;
        iterator from = raw.begin(
        );
        iterator const to = raw.end(
        );
        enum {
            normal,
            number,
            string,
        } lexer_mode = normal;
        enum parser_mode {
            immediate,
            array,
            slot_name,
            slot_value,
        };
        parser_mode current_parser_mode = immediate;
        std::list<
            int
        > recursive_parser_modes;
        std::list<
            json_object
        > recursive_objects;
        enum {
            none,
            token_string,
            token_number,
            token_object_open,
            token_object_close,
            token_array_open,
            token_array_close,
            token_colon,
            token_comma,
            token_null,
            token_true,
            token_false,
        } current_token;
        std::string current_number;
        std::string current_string;
        std::string current_slot_name;
        bool done = false;
        do {
            current_token = none;
            switch (
                lexer_mode
            ) {
            case normal:
                switch (
                    *from
                ) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '.':
                case '-':
                    current_number.clear(
                    );
                    current_number += *from;
                    lexer_mode = number;
                    break;
                case '"':
                    current_string.clear(
                    );
                    lexer_mode = string;
                    break;
                case '[':
                    current_token = token_array_open;
                    break;
                case ']':
                    current_token = token_array_close;
                    break;
                case '{':
                    current_token = token_object_open;
                    break;
                case '}':
                    current_token = token_object_close;
                    break;
                case ',':
                    current_token = token_comma;
                    break;
                case ':':
                    current_token = token_colon;
                    break;
                case 'n':
                    do {
                        std::string const token_string(
                            "null"
                        );
                        if (
                            !matches_prefix(
                                token_string,
                                std::string(
                                    from,
                                    to
                                )
                            )
                        ) {
                            if (
                                NULL != error
                            ) {
                                *error = true;
                                return;
                            }
                            throw_exception(
                                component_error(
                                    "parse_json"
                                )
                            );
                        }
                        current_token = token_null;
                        from += token_string.size(
                        ) - 1;
                    } while (
                        false
                    );
                    break;
                case 't':
                    do {
                        std::string const token_string(
                            "true"
                        );
                        if (
                            !matches_prefix(
                                token_string,
                                std::string(
                                    from,
                                    to
                                )
                            )
                        ) {
                            if (
                                NULL != error
                            ) {
                                *error = true;
                                return;
                            }
                            throw_exception(
                                component_error(
                                    "parse_json"
                                )
                            );
                        }
                        current_token = token_true;
                        from += token_string.size(
                        ) - 1;
                    } while (
                        false
                    );
                    break;
                case 'f':
                    do {
                        std::string const token_string(
                            "false"
                        );
                        if (
                            !matches_prefix(
                                token_string,
                                std::string(
                                    from,
                                    to
                                )
                            )
                        ) {
                            if (
                                NULL != error
                            ) {
                                *error = true;
                                return;
                            }
                            throw_exception(
                                component_error(
                                    "parse_json"
                                )
                            );
                        }
                        current_token = token_false;
                        from += token_string.size(
                        ) - 1;
                    } while (
                        false
                    );
                    break;
                default:
                    if (
                        NULL != error
                    ) {
                        *error = true;
                        return;
                    }
                    throw_exception(
                        component_error(
                            "parse_json"
                        )
                    );
                }
                break;
            case number:
                switch (
                    *from
                ) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '.':
                case '-':
                    current_number += *from;
                    break;
                default:
                    current_token = token_number;
                    lexer_mode = normal;
                    from--;
                }
                break;
            case string:
                switch (
                    *from
                ) {
                case '"':
                    current_token = token_string;
                    lexer_mode = normal;
                    break;
                case '\\':
                    from++;
                default:
                    current_string += *from;
                }
            }
            from++;
            if (
                to == from
            ) {
                switch (
                    lexer_mode
                ) {
                case number:
                    current_token = token_number;
                    break;
                default:
                    break;
                }
                done = true;
            }
            switch (
                current_token
            ) {
            case token_colon:
                if (
                    slot_name != current_parser_mode
                ) {
                    if (
                        NULL != error
                    ) {
                        *error = true; 
                        return;
                    }
                    throw_exception(
                        component_error(
                            "parse_json"
                        )
                    );
                }
                current_parser_mode = slot_value;
                break;
            case token_object_open:
                recursive_parser_modes.push_back(
                    current_parser_mode
                );
                current_parser_mode = slot_name;
                recursive_objects.push_back(
                    std::map<
                        std::string,
                        json_object
                    >(
                    )
                );
                break;
            case token_array_open:
                recursive_parser_modes.push_back(
                    current_parser_mode
                );
                current_parser_mode = array;
                recursive_objects.push_back(
                    std::vector<
                        json_object
                    >(
                    )
                );
                break;
            case token_comma:
                switch (
                    current_parser_mode
                ) {
                case slot_value:
                    current_parser_mode = slot_name;
                case array:
                    break;
                default:
                    if (
                        NULL != error
                    ) {
                        *error = true; 
                        return;
                    }   
                    throw_exception(   
                        component_error(
                            "parse_json"
                        )
                    );
                }
                break;
            case token_object_close:
                do {
                    if (
                        slot_value != current_parser_mode
                    ) {
                        if (
                            NULL != error
                        ) {
                            *error = true; 
                            return;
                        }   
                        throw_exception(   
                            component_error(
                                "parse_json"
                            )
                        );
                    }
                    json_object const value = recursive_objects.back(
                    );
                    recursive_objects.pop_back(
                    );
                    current_parser_mode = static_cast<
                        parser_mode
                    >(
                        recursive_parser_modes.back(
                        )
                    );
                    recursive_parser_modes.pop_back(
                    );
                    switch (
                        current_parser_mode
                    ) {
                    case immediate:
                        parsed = value;
                        break;
                    case array:
                        boost::get<
                            std::vector<
                                json_object
                            >
                        >(
                            recursive_objects.back(
                            )
                        ).push_back(
                            value
                        );
                        break;
                    case slot_value:
                        boost::get<
                            std::map<
                                std::string,
                                json_object
                            >
                        >(
                            recursive_objects.back(
                            )
                        )[
                            current_slot_name
                        ] = value;
                        break;
                    default:
                        if (
                            NULL != error
                        ) {
                            *error = true; 
                            return;
                        }
                        throw_exception(
                            component_error(
                                "parse_json"
                            )
                        );
                    }
                } while (
                    false
                );
                break;
            case token_array_close:
                do {
                    if (
                        array != current_parser_mode
                    ) {
                        if (
                            NULL != error
                        ) {
                            *error = true; 
                            return;
                        }   
                        throw_exception(   
                            component_error(
                                "parse_json"
                            )
                        );
                    }
                    json_object const value = recursive_objects.back(
                    );
                    recursive_objects.pop_back(
                    );
                    current_parser_mode = static_cast<
                        parser_mode
                    >(
                        recursive_parser_modes.back(
                        )
                    );
                    recursive_parser_modes.pop_back(
                    );
                    switch (
                        current_parser_mode
                    ) {
                    case immediate:
                        parsed = value;
                        break;
                    case array:
                        boost::get<
                            std::vector<
                                json_object
                            >
                        >(
                            recursive_objects.back(
                            )
                        ).push_back(
                            value
                        );
                        break;
                    case slot_value:
                        boost::get<
                            std::map<
                                std::string,
                                json_object
                            >
                        >(
                            recursive_objects.back(
                            )
                        )[
                            current_slot_name
                        ] = value;
                        break;
                    default:
                        if (
                            NULL != error
                        ) {
                            *error = true; 
                            return;
                        }
                        throw_exception(
                            component_error(
                                "parse_json"
                            )
                        );
                    }
                } while (
                    false
                );
                break;
            case token_null:
                do {
                    null_policy const value;
                    switch (
                        current_parser_mode
                    ) {
                    case immediate:
                        parsed = value;
                        break;
                    case array:
                        boost::get<
                            std::vector<
                                json_object
                            >
                        >(
                            recursive_objects.back(
                            )
                        ).push_back(
                            value
                        );
                        break;
                    case slot_value:
                        boost::get<
                            std::map<
                                std::string,
                                json_object
                            >
                        >(
                            recursive_objects.back(
                            )
                        )[
                            current_slot_name
                        ] = value;
                        break;
                    default:
                        if (
                            NULL != error
                        ) {
                            *error = true; 
                            return;
                        }
                        throw_exception(
                            component_error(
                                "parse_json"
                            )
                        );
                    }
                } while (
                    false
                );
                break;
            case token_false:
                do {
                    bool const value = false;
                    switch (
                        current_parser_mode
                    ) {
                    case immediate:
                        parsed = value;
                        break;
                    case array:
                        boost::get<
                            std::vector<
                                json_object
                            >
                        >(
                            recursive_objects.back(
                            )
                        ).push_back(
                            value
                        );
                        break;
                    case slot_value:
                        boost::get<
                            std::map<
                                std::string,
                                json_object
                            >
                        >(
                            recursive_objects.back(
                            )
                        )[
                            current_slot_name
                        ] = value;
                        break;
                    default:
                        if (
                            NULL != error
                        ) {
                            *error = true; 
                            return;
                        }
                        throw_exception(
                            component_error(
                                "parse_json"
                            )
                        );
                    }
                } while (
                    false
                );
                break;
            case token_true:
                do {
                    bool const value = true;
                    switch (
                        current_parser_mode
                    ) {
                    case immediate:
                        parsed = value;
                        break;
                    case array:
                        boost::get<
                            std::vector<
                                json_object
                            >
                        >(
                            recursive_objects.back(
                            )
                        ).push_back(
                            value
                        );
                        break;
                    case slot_value:
                        boost::get<
                            std::map<
                                std::string,
                                json_object
                            >
                        >(
                            recursive_objects.back(
                            )
                        )[
                            current_slot_name
                        ] = value;
                        break;
                    default:
                        if (
                            NULL != error
                        ) {
                            *error = true; 
                            return;
                        }
                        throw_exception(
                            component_error(
                                "parse_json"
                            )
                        );
                    }
                } while (
                    false
                );
                break;
            case token_string:
                do {
                    std::string const& value = current_string;
                    switch (
                        current_parser_mode
                    ) {
                    case immediate:
                        parsed = value;
                        break;
                    case array:
                        boost::get<
                            std::vector<
                                json_object
                            >
                        >(
                            recursive_objects.back(
                            )
                        ).push_back(
                            value
                        );
                        break;
                    case slot_name:
                        current_slot_name = value;
                        break;
                    case slot_value:
                        boost::get<
                            std::map<
                                std::string,
                                json_object
                            >
                        >(
                            recursive_objects.back(
                            )
                        )[
                            current_slot_name
                        ] = value;
                        break;
                    default:
                        if (
                            NULL != error
                        ) {
                            *error = true; 
                            return;
                        }
                        throw_exception(
                            component_error(
                                "parse_json"
                            )
                        );
                    }
                } while (
                    false
                );
                break;
            case token_number:
                do {
                    fixed_point_number const value = parse_fixed(
                        current_number
                    );
                    switch (
                        current_parser_mode
                    ) {
                    case immediate:
                        parsed = value;
                        break;
                    case array:
                        boost::get<
                            std::vector<
                                json_object
                            >
                        >(
                            recursive_objects.back(
                            )
                        ).push_back(
                            value
                        );
                        break;
                    case slot_value:
                        boost::get<
                            std::map<
                                std::string,
                                json_object
                            >
                        >(
                            recursive_objects.back(
                            )
                        )[
                            current_slot_name
                        ] = value;
                        break;
                    default:
                        if (
                            NULL != error
                        ) {
                            *error = true; 
                            return;
                        }
                        throw_exception(
                            component_error(
                                "parse_json"
                            )
                        );
                    }
                } while (
                    false
                );
            case none:
                break;
            default:
                if (
                    NULL != error
                ) {
                    *error = true; 
                    return;
                }
                throw_exception(
                    component_error(
                        "parse_json"
                    )
                );
            }
        } while (
            !done
        );
    }
}
