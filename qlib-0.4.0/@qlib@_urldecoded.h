/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */

#ifndef URLDECODED_INCLUDED
#define URLDECODED_INCLUDED

#include "@qlib@_component_error.h"
namespace @qlib@ {
    
    
    template<
        typename string
    >
    string urldecoded(
        string const& urlencoded
    );
    
    template<
        typename string
    >
    string urldecoded(
        string const& urlencoded
    ) {
        char unhex[
            0x100
        ];
        for (
            int hex = '0',
            value = 0;
            '9' >= hex;
            hex++,
            value++
        ) {
            unhex[
                hex
            ] = value;
        }
        for (
            int hex = 'A',
            value = 0xa;
            'F' >= hex;
            hex++,
            value++
        ) {
            unhex[
                hex
            ] = value;
        }
        for (
            int hex = 'a',
            value = 0xa;
            'f' >= hex;
            hex++,
            value++
        ) {
            unhex[
                hex
            ] = value;
        }
        string result;
        for (
            typename string::const_iterator traversed = urlencoded.begin(
            );
            urlencoded.end(
            ) != traversed;
            traversed++
        ) {
            if (
                '+' == *traversed
            ) {
                result += ' ';
                continue;
            }
            if (
                '%' != *traversed
            ) {
                result += *traversed;
                continue;
            }
            traversed++;
            if (
                urlencoded.end(
                ) == traversed
            ) {
                throw component_error(
                    "urldecoded"
                );
            }
            char big = unhex[
                static_cast<
                    int
                >(
                    *traversed
                )
            ];
            traversed++;
            if (
                urlencoded.end(
                ) == traversed
            ) {
                throw component_error(
                    "urldecoded"
                );
            }
            char little = unhex[
                static_cast<
                    int
                >(
                    *traversed
                )
            ];
            result += (
                big << 4
            ) + little;
        }
        return result;
    }
}

#endif
