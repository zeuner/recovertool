/*
 *
 * This file is part of qlib -- A multi-purpose C++ utility library
 *
 * qlib is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1
 * of the License, or (at your option) any later version.
 *
 * qlib is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with qlib.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010-2023 Isidor Zeuner <qlib@quidecco.de>
 *
 */


#include "@qlib@_memory_mapped_receive_socket.h"
#include <cstring>
#include <unistd.h>
namespace @qlib@ {
    
    
    memory_mapped_receive_socket::memory_mapped_receive_socket(
        boost::asio::io_service& communicating,
        std::string const& interface,
        unsigned block_size,
        unsigned block_count,
        unsigned frame_size,
        unsigned frame_count,
        uint16_t received_protocol,
        bool promiscuous
    ) :
    usable_frames(
        frame_count
    ),
    promiscuous(
        promiscuous
    ),
    interface(
        interface
    ) {
        buffer_request.tp_block_size = block_size;
        buffer_request.tp_block_nr = block_count;
        buffer_request.tp_frame_size = frame_size;
        buffer_request.tp_frame_nr = frame_count;
        int const native = socket(
            PF_PACKET,
            SOCK_RAW,
            IPPROTO_RAW
        );
        if (
            0 > native
        ) {
            throw component_error(
                "memory_mapped_receive_socket"
            );
        }
        if (
            promiscuous
        ) {
            struct ifreq interface_request;
            memset(
                &interface_request,
                0,
                sizeof(
                    interface_request
                )
            );
            if (
                interface.size(
                ) >= IFNAMSIZ
            ) {
                close(
                    native
                );
                throw component_error(
                    "memory_mapped_receive_socket"
                );
            }
            strcpy(
                interface_request.ifr_name,
                interface.data(
                )
            );
            if (
                0 > ioctl(
                    native,
                    SIOCGIFFLAGS,
                    &interface_request
                )
            ) { 
                close(
                    native
                );
                throw component_error(
                    "memory_mapped_receive_socket"
                );
            }       
            interface_request.ifr_flags |= IFF_PROMISC;
            if (
                0 > ioctl(
                    native,
                    SIOCSIFFLAGS,
                    &interface_request
                )   
            ) { 
                close(
                    native
                );
                throw component_error(
                    "memory_mapped_receive_socket"
                );  
            }
        }
        if (
            0 != setsockopt(
                native,
                SOL_PACKET,
                PACKET_RX_RING,
                &buffer_request,
                sizeof(
                    buffer_request
                )
            )
        ) {
            close(
                native
            );
            throw component_error(
                "memory_mapped_receive_socket"
            );
        }
        struct sockaddr_ll native_address;
        memset(
            &native_address,
            0,
            sizeof(
                native_address
            )
        );
        native_address.sll_ifindex = if_nametoindex(
            interface.c_str(
            )
        );  
        native_address.sll_family = AF_PACKET;
        native_address.sll_protocol = htons(
            received_protocol
        );
        if (
            0 != bind(
                native,
                reinterpret_cast<
                    struct sockaddr*
                >(
                    &native_address
                ),
                sizeof(
                    native_address
                )
            )
        ) {
            close(
                native
            );
            throw component_error(
                "memory_mapped_receive_socket"
            );
        }
        void* mapped;
        if (
            MAP_FAILED == (
                mapped = mmap(
                    NULL,
                    buffer_request.tp_block_size * buffer_request.tp_block_nr,
                    PROT_READ | PROT_WRITE,
                    MAP_SHARED,
                    native,
                    0
                )
            )
        ) {
            close(
                native
            );
            throw component_error(
                "memory_mapped_receive_socket"
            );
        }
        mapped_bytes = static_cast<
            unsigned char*
        >(
            mapped
        );
        handle = new boost::asio::basic_raw_socket<
            packet_raw_protocol
        >(
            communicating,
            packet_raw_protocol(
            ),
            native
        );
        unsigned char* const frames_end = mapped_bytes + (
            buffer_request.tp_frame_size * buffer_request.tp_frame_nr
        );
        for (
            unsigned char* current_frame = mapped_bytes;
            frames_end > current_frame; 
            current_frame += buffer_request.tp_frame_size
        ) {
            usable_frames.insert(
                current_frame
            );
        }
    }
    
    memory_mapped_receive_socket::~memory_mapped_receive_socket(
    ) {
        if (
            promiscuous
        ) {
            do { 
                int const native = socket(
                    AF_INET,
                    SOCK_STREAM,
                    0
                ); 
                if (
                    0 > native
                ) { 
                    break;
                }
                struct ifreq interface_request;
                memset(
                    &interface_request,
                    0,
                    sizeof(
                        interface_request
                    )
                );
                if (
                    interface.size(
                    ) >= IFNAMSIZ
                ) {
                    close(  
                        native
                    );
                    break;
                }
                strcpy(
                    interface_request.ifr_name,
                    interface.data(
                    )
                );
                if (
                    0 > ioctl(
                        native,
                        SIOCGIFFLAGS,
                        &interface_request
                    )
                ) {
                    close(  
                        native
                    );
                    break;
                }
                interface_request.ifr_flags &= ~IFF_PROMISC;
                if (
                    0 > ioctl(
                        native,
                        SIOCSIFFLAGS,
                        &interface_request
                    )   
                ) {
                    close(  
                        native
                    );
                    break;
                }
                close(  
                    native
                );
            } while (
                false
            );
        }
        munmap(
            mapped_bytes,
            buffer_request.tp_block_size * buffer_request.tp_block_nr
        );
        delete handle;
    }
    
    bool memory_mapped_receive_socket::flush(
    ) {
        unsigned char* const frames_end = mapped_bytes + (
            buffer_request.tp_frame_size * buffer_request.tp_frame_nr
        );
        bool found = false;
        for (
            unsigned char* current_frame = mapped_bytes;
            frames_end > current_frame;
            current_frame += buffer_request.tp_frame_size
        ) {
            struct tpacket_hdr* header = reinterpret_cast<
                struct tpacket_hdr*
            >(
                current_frame
            );
            if (
                TP_STATUS_USER & header->tp_status
            ) {
                found = true;
                header->tp_status = TP_STATUS_KERNEL;
            }
        }
        return found;
    }
    
    void memory_mapped_receive_socket::reuse_frame(
        unsigned char* frame
    ) {
        usable_frames.insert(
            frame
        );
        struct tpacket_hdr* header = reinterpret_cast<
            struct tpacket_hdr*
        >(
            frame
        );
        header->tp_status = TP_STATUS_KERNEL;
    }
    
    bool memory_mapped_receive_socket::can_poll_asynchronously(
    ) const {
        return !usable_frames.empty(
        );
    }
    
    boost::asio::io_service& memory_mapped_receive_socket::get_io_service(
    ) {
        return handle->get_io_service(
        );
    }
    
    boost::asio::basic_raw_socket<
        packet_raw_protocol
    >& memory_mapped_receive_socket::get_handle(
    ) {
        return *handle;
    }
    
    size_t memory_mapped_receive_socket::get_snapshot_length(
    ) const {
        return buffer_request.tp_frame_size;
    }
}
