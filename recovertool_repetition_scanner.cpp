/*
 *
 * This file is part of recovertool -- A toolbox for recovering files
 *  from damaged file systems
 *
 * recovertool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3.0 of the
 * License, or (at your option) any later version.
 *
 * recovertool is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with recovertool.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2023 Isidor Zeuner <devel@quidecco.de>
 *
 */


#include "recovertool_repetition_scanner.h"
#include <cstring>
namespace recovertool {
    
    repetition_scanner::repetition_scanner(
        size_t buffer_size,
        size_t pattern_size,
        size_t pattern_distance
    ) :
    data(
        buffer_size
    ),
    pattern_size(
        pattern_size
    ),
    pattern_distance(
        pattern_distance
    ),
    buffer_start(
        data.get_data(
        )
    ),
    buffer_end(
        data.get_data(
        ) + data.get_size(
        )
    ),
    buffer_position(
        data.get_data(
        )
    ),
    repetition_position(
        data.get_data(
        ) - pattern_distance
    ),
    filled(
        false
    ),
    filled_size(
        0
    ),
    matched(
        0
    ) {
    }
    
    bool repetition_scanner::scan_data(
        char const*& from,
        char const* const& to,
        std::string& found
    ) {
        size_t const can_copy = std::min(
            to - from,
            buffer_end - buffer_position
        );
        memcpy(
            buffer_position,
            from,
            can_copy
        );
        char const* const copied_end = buffer_position + can_copy;
        bool success = false;
        for (
            ;
            buffer_position < copied_end;
            buffer_position++, from++, repetition_position++
        ) {
            if (
                !filled
            ) {
                filled_size++;
                if (
                    filled_size >= pattern_distance
                ) {
                    filled = true;
                } else {
                    continue;
                }
            }
            if (
                buffer_end == repetition_position
            ) {
                repetition_position = buffer_start;
            }
            if (
                *repetition_position == *buffer_position
            ) {
                matched++;
                if (
                    pattern_size <= matched
                ) {
                    buffer_position++;
                    from++;
                    repetition_position++;
                    auto match_start = buffer_position - pattern_size;
                    if (
                        buffer_start > match_start
                    ) {
                        found = std::string(
                            buffer_end - (
                                buffer_start - match_start
                            ),
                            buffer_start - match_start
                        ) + std::string(
                            buffer_start,
                            buffer_position - buffer_start
                        );
                    } else {
                        found = std::string(
                            match_start,
                            buffer_position - match_start
                        );
                    }
                    success = true;
                    break;
                }
            } else {
                matched = 0;
            }
        }
        if (
            buffer_end == buffer_position
        ) {
            buffer_position -= data.get_size(
            );
        }
        if (
            success
        ) {
            return success;
        }
        if (
            to > from
        ) {
            return scan_data(
                from,
                to,
                found
            );
        }
        return false;
    }
}
