/*
 *
 * This file is part of recovertool -- A toolbox for recovering files
 *  from damaged file systems
 *
 * recovertool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3.0 of the
 * License, or (at your option) any later version.
 *
 * recovertool is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with recovertool.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2023 Isidor Zeuner <devel@quidecco.de>
 *
 */

#include <cstdio>
#include "recovertool_component_error.h"
#include "recovertool_deserialize.h"
#include "recovertool_substitute_string.h"
#include <iostream>
#include <string>
#include "recovertool_throw_exception.h"

int get_csv_string(
    int argc,
    char* argv[
    ]
) {
    size_t index = 0;
    std::string const column_name = argv[
        ++index
    ];
    std::cout << "\"" << recovertool::substitute_string(
        column_name,
        "\"",
        "\"\""
    ) << "\"\n";
    enum {
        buffer_size = 0x400,
    };
    char buffer[
        buffer_size
    ];
    std::string unprocessed;
    bool no_data = false;
    while (
        true
    ) {
        size_t const read_bytes = fread(
            buffer,
            1,
            buffer_size,
            stdin
        );
        if (
            buffer_size > read_bytes
        ) {
            if (
                !feof(
                    stdin
                )
            ) {
                recovertool::throw_exception(
                    recovertool::component_error(
                        "get_csv_string"
                    )
                );
            }
            no_data = true;
        }
        unprocessed += std::string(
            buffer,
            read_bytes
        );
        std::string::const_iterator from = unprocessed.begin(
        );
        std::string::const_iterator const to = unprocessed.end(
        );
        do {
            std::string::const_iterator from_new = from;
            std::string deserialized;
            bool short_data = false;
            recovertool::deserialize(
                from_new,
                to,
                deserialized,
                short_data
            );
            if (
                short_data
            ) {
                if (
                    !no_data
                ) {
                    unprocessed = std::string(
                        from,
                        to
                    );
                    break;
                }
                if (
                    to != from
                ) {
                    recovertool::throw_exception(
                        recovertool::component_error(
                            "get_csv_string"
                        )
                    );
                }
                return 0;
            }
            std::cout << "\"" << recovertool::substitute_string(
                deserialized,
                "\"",
                "\"\""
            ) << "\"\n";
            from = from_new;
        } while (
            true
        );
    };
    return 0;
}
