all:\
		recovertool_scanpng\
		recovertool_scanendpng\
		recovertool_scanpdf\
		recovertool_scanpk\
		recovertool_scan_filename\
		recovertool_scan_repeated\
		recovertool_get_surrounding\
		recovertool_offset_diff\
		recovertool_byte_repetition_randomness\
		recovertool_xor_word_randomness\
		recovertool_get_csv_record\
		recovertool_generate_block_scanner\
		recovertool_get_csv_string\
		recovertool_get_subsequence\
		recovertool_surrounding_subscan\
		recovertool_basic_zip_start_end\
		recovertool_get_pre_text\
		recovertool_get_length_string

prepare:
	qlib-0.4.0/qlib-prepare recovertool deserialize
	qlib-0.4.0/qlib-prepare recovertool deserializing_short_data
	qlib-0.4.0/qlib-prepare recovertool substitute_string
	qlib-0.4.0/qlib-prepare recovertool c_string
	qlib-0.4.0/qlib-prepare recovertool stringified
	qlib-0.4.0/qlib-prepare recovertool stringify
	qlib-0.4.0/qlib-prepare recovertool supported_type
	qlib-0.4.0/qlib-prepare recovertool c_buffer
	qlib-0.4.0/qlib-prepare recovertool component_error
	qlib-0.4.0/qlib-prepare recovertool from_hex
	qlib-0.4.0/qlib-prepare recovertool hex_string
	qlib-0.4.0/qlib-prepare recovertool malloc_c_allocator
	qlib-0.4.0/qlib-prepare recovertool posix_file
	qlib-0.4.0/qlib-prepare recovertool resizable_buffer
	qlib-0.4.0/qlib-prepare recovertool serialized
	qlib-0.4.0/qlib-prepare recovertool throw_exception

recovertool_scanpng:\
	recovertool_scanpng.o
	g++ $(LDFLAGS)\
		-o $@ $^

recovertool_scanendpng:\
	recovertool_scanendpng.o
	g++ $(LDFLAGS)\
		-o $@ $^

recovertool_scanpdf:\
	recovertool_scanpdf.o
	g++ $(LDFLAGS)\
		-o $@ $^

recovertool_scanpk:\
	recovertool_scanpk.o
	g++ $(LDFLAGS)\
		-o $@ $^

recovertool_scan_filename:\
	recovertool_scan_filename.o
	g++ $(LDFLAGS)\
		-o $@ $^

recovertool_scan_repeated:\
	recovertool_scan_repeated.o\
	recovertool_repetition_scanner.o\
	librecovertool_common.so
	g++ $(LDFLAGS)\
		-o $@ $^\
		-Wl,-R$(CURDIR)

recovertool_get_surrounding:\
	recovertool_get_surrounding.o\
	librecovertool_common.so
	g++ $(LDFLAGS)\
		-o $@ $^\
		-Wl,-R$(CURDIR)

recovertool_offset_diff:\
	recovertool_offset_diff.o\
	librecovertool_common.so
	g++ $(LDFLAGS)\
		-o $@ $^\
		-Wl,-R$(CURDIR)

recovertool_byte_repetition_randomness:\
	recovertool_byte_repetition_randomness.o\
	librecovertool_common.so
	g++ $(LDFLAGS)\
		-o $@ $^\
		-Wl,-R$(CURDIR)

recovertool_xor_word_randomness:\
	recovertool_xor_word_randomness.o\
	librecovertool_common.so
	g++ $(LDFLAGS)\
		-o $@ $^\
		-Wl,-R$(CURDIR)

recovertool_get_csv_record:\
	recovertool_get_csv_record.o\
	recovertool_format_uint.o\
	recovertool_format_uuid.o\
	librecovertool_common.so
	g++ $(LDFLAGS)\
		-o $@ $^\
		-Wl,-R$(CURDIR)

recovertool_generate_block_scanner:\
	recovertool_generate_block_scanner.o\
	librecovertool_common.so
	g++ $(LDFLAGS)\
		-o $@ $^\
		-Wl,-R$(CURDIR)

recovertool_get_csv_string:\
	recovertool_get_csv_string.o\
	recovertool_substitute_string.o\
	librecovertool_common.so
	g++ $(LDFLAGS)\
		-o $@ $^\
		-Wl,-R$(CURDIR)\
		-lboost_system \
		-pthread 

recovertool_get_subsequence:\
	recovertool_get_subsequence.o\
	librecovertool_common.so
	g++ $(LDFLAGS)\
		-o $@ $^\
		-Wl,-R$(CURDIR)

recovertool_surrounding_subscan:\
	recovertool_surrounding_subscan.o\
	librecovertool_common.so
	g++ $(LDFLAGS)\
		-o $@ $^\
		-Wl,-R$(CURDIR)

recovertool_basic_zip_start_end:\
	recovertool_basic_zip_start_end.o\
	librecovertool_common.so
	g++ $(LDFLAGS)\
		-o $@ $^\
		-Wl,-R$(CURDIR)

recovertool_get_pre_text:\
	recovertool_get_pre_text.o\
	recovertool_file_back_scanner.o\
	recovertool_c_string.o\
	librecovertool_common.so
	g++ $(LDFLAGS)\
		-o $@ $^\
		-Wl,-R$(CURDIR)\
		-lboost_system \
		-pthread 

recovertool_get_length_string:\
	recovertool_get_length_string.o\
	librecovertool_common.so
	g++ $(LDFLAGS)\
		-o $@ $^\
		-Wl,-R$(CURDIR)\
		-lboost_system \
		-pthread 

librecovertool_common.so:\
	recovertool_component_error.o\
	recovertool_from_hex.o\
	recovertool_malloc_c_allocator.o\
	recovertool_posix_file.o
	g++ $(LDFLAGS) -shared\
		-o $@ $^

recovertool_scanpng.cpp: recovertool_scanpng.flex
	flex -o$@ $^

recovertool_scanpng.o:\
		recovertool_scanpng.cpp
	g++ $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Dscanpng=main -c recovertool_scanpng.cpp -o $@

recovertool_scanendpng.cpp: recovertool_scanendpng.flex
	flex -o$@ $^

recovertool_scanendpng.o:\
		recovertool_scanendpng.cpp
	g++ $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Dscanendpng=main -c recovertool_scanendpng.cpp -o $@

recovertool_scanpdf.cpp: recovertool_scanpdf.flex
	flex -o$@ $^

recovertool_scanpdf.o:\
		recovertool_scanpdf.cpp
	g++ $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Dscanpdf=main -c recovertool_scanpdf.cpp -o $@

recovertool_scanpk.cpp: recovertool_scanpk.flex
	flex -o$@ $^

recovertool_scanpk.o:\
		recovertool_scanpk.cpp
	g++ $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Dscanpk=main -c recovertool_scanpk.cpp -o $@

recovertool_scan_filename.cpp: recovertool_scan_filename.flex
	flex -o$@ $^

recovertool_scan_filename.o:\
		recovertool_scan_filename.cpp
	g++ $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Dscan_filename=main -c recovertool_scan_filename.cpp -o $@

recovertool_scan_repeated.o: recovertool_scan_repeated.h\
		recovertool_scan_repeated.cpp\
		recovertool_c_buffer.h\
		recovertool_component_error.h\
		recovertool_malloc_c_allocator.h\
		recovertool_repetition_scanner.h\
		recovertool_resizable_buffer.h\
		recovertool_throw_exception.h
	g++ $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Dscan_repeated=main -c recovertool_scan_repeated.cpp -o $@

recovertool_repetition_scanner.o: recovertool_repetition_scanner.h\
		recovertool_repetition_scanner.cpp\
		recovertool_c_buffer.h\
		recovertool_component_error.h\
		recovertool_malloc_c_allocator.h\
		recovertool_resizable_buffer.h\
		recovertool_throw_exception.h
	g++ $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Dscan_repeated=main -c recovertool_repetition_scanner.cpp -o $@

recovertool_get_surrounding.o: recovertool_get_surrounding.h\
		recovertool_get_surrounding.cpp\
		recovertool_c_buffer.h\
		recovertool_component_error.h\
		recovertool_malloc_c_allocator.h\
		recovertool_posix_file.h\
		recovertool_resizable_buffer.h\
		recovertool_throw_exception.h
	g++ $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Dget_surrounding=main -c recovertool_get_surrounding.cpp -o $@

recovertool_offset_diff.o: recovertool_offset_diff.h\
		recovertool_offset_diff.cpp\
		recovertool_component_error.h\
		recovertool_throw_exception.h
	g++ $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Doffset_diff=main -c recovertool_offset_diff.cpp -o $@

recovertool_byte_repetition_randomness.o: recovertool_byte_repetition_randomness.h\
		recovertool_byte_repetition_randomness.cpp\
		recovertool_component_error.h\
		recovertool_malloc_c_allocator.h\
		recovertool_resizable_buffer.h\
		recovertool_throw_exception.h
	g++ $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Dbyte_repetition_randomness=main -c recovertool_byte_repetition_randomness.cpp -o $@

recovertool_xor_word_randomness.o: recovertool_xor_word_randomness.h\
		recovertool_xor_word_randomness.cpp\
		recovertool_component_error.h\
		recovertool_malloc_c_allocator.h\
		recovertool_resizable_buffer.h\
		recovertool_throw_exception.h
	g++ $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Dxor_word_randomness=main -c recovertool_xor_word_randomness.cpp -o $@

recovertool_get_csv_record.o: recovertool_get_csv_record.h\
		recovertool_get_csv_record.cpp\
		recovertool_c_buffer.h\
		recovertool_component_error.h\
		recovertool_format_uint.h\
		recovertool_format_uuid.h\
		recovertool_malloc_c_allocator.h\
		recovertool_resizable_buffer.h\
		recovertool_throw_exception.h
	g++ $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Dget_csv_record=main -c recovertool_get_csv_record.cpp -o $@

recovertool_format_uint.o: recovertool_format_uint.h\
		recovertool_format_uint.cpp\
		recovertool_c_string.h\
		recovertool_hex_string.h
	g++ $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Dget_csv_record=main -c recovertool_format_uint.cpp -o $@

recovertool_format_uuid.o: recovertool_format_uuid.h\
		recovertool_format_uuid.cpp\
		recovertool_c_string.h\
		recovertool_hex_string.h
	g++ $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Dget_csv_record=main -c recovertool_format_uuid.cpp -o $@

recovertool_generate_block_scanner.o: recovertool_generate_block_scanner.h\
		recovertool_generate_block_scanner.cpp\
		recovertool_c_buffer.h\
		recovertool_component_error.h\
		recovertool_c_string.h\
		recovertool_hex_string.h\
		recovertool_malloc_c_allocator.h\
		recovertool_resizable_buffer.h\
		recovertool_throw_exception.h
	g++ $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Dgenerate_block_scanner=main -c recovertool_generate_block_scanner.cpp -o $@

recovertool_get_csv_string.o: recovertool_get_csv_string.h\
		recovertool_get_csv_string.cpp\
		recovertool_component_error.h\
		recovertool_deserialize.h\
		recovertool_deserializing_short_data.h\
		recovertool_substitute_string.h\
		recovertool_supported_type.h\
		recovertool_throw_exception.h
	g++ $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Dget_csv_string=main -c recovertool_get_csv_string.cpp -o $@

recovertool_substitute_string.o: recovertool_substitute_string.h\
		recovertool_substitute_string.cpp
	g++ $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Dget_csv_string=main -c recovertool_substitute_string.cpp -o $@

recovertool_get_subsequence.o: recovertool_get_subsequence.h\
		recovertool_get_subsequence.cpp\
		recovertool_c_buffer.h\
		recovertool_component_error.h\
		recovertool_malloc_c_allocator.h\
		recovertool_resizable_buffer.h\
		recovertool_throw_exception.h
	g++ $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Dget_subsequence=main -c recovertool_get_subsequence.cpp -o $@

recovertool_surrounding_subscan.o: recovertool_surrounding_subscan.h\
		recovertool_surrounding_subscan.cpp\
		recovertool_c_buffer.h\
		recovertool_component_error.h\
		recovertool_from_hex.h\
		recovertool_malloc_c_allocator.h\
		recovertool_posix_file.h\
		recovertool_resizable_buffer.h\
		recovertool_throw_exception.h
	g++ $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Dsurrounding_subscan=main -c recovertool_surrounding_subscan.cpp -o $@

recovertool_basic_zip_start_end.o: recovertool_basic_zip_start_end.h\
		recovertool_basic_zip_start_end.cpp\
		recovertool_c_buffer.h\
		recovertool_component_error.h\
		recovertool_from_hex.h\
		recovertool_malloc_c_allocator.h\
		recovertool_posix_file.h\
		recovertool_resizable_buffer.h\
		recovertool_throw_exception.h
	g++ $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Dbasic_zip_start_end=main -c recovertool_basic_zip_start_end.cpp -o $@

recovertool_get_pre_text.o: recovertool_get_pre_text.h\
		recovertool_get_pre_text.cpp\
		recovertool_c_buffer.h\
		recovertool_component_error.h\
		recovertool_c_string.h\
		recovertool_file_back_scanner.h\
		recovertool_malloc_c_allocator.h\
		recovertool_posix_file.h\
		recovertool_resizable_buffer.h\
		recovertool_serialized.h\
		recovertool_stringified.h\
		recovertool_stringify.h\
		recovertool_supported_type.h\
		recovertool_throw_exception.h
	g++ $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Dget_pre_text=main -c recovertool_get_pre_text.cpp -o $@

recovertool_file_back_scanner.o: recovertool_file_back_scanner.h\
		recovertool_file_back_scanner.cpp\
		recovertool_c_buffer.h\
		recovertool_component_error.h\
		recovertool_malloc_c_allocator.h\
		recovertool_posix_file.h\
		recovertool_resizable_buffer.h\
		recovertool_throw_exception.h
	g++ $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Dget_pre_text=main -c recovertool_file_back_scanner.cpp -o $@

recovertool_c_string.o: recovertool_c_string.h\
		recovertool_c_string.cpp\
		recovertool_component_error.h
	g++ $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Dget_pre_text=main -c recovertool_c_string.cpp -o $@

recovertool_get_length_string.o: recovertool_get_length_string.h\
		recovertool_get_length_string.cpp\
		recovertool_c_buffer.h\
		recovertool_component_error.h\
		recovertool_c_string.h\
		recovertool_malloc_c_allocator.h\
		recovertool_posix_file.h\
		recovertool_resizable_buffer.h\
		recovertool_serialized.h\
		recovertool_stringified.h\
		recovertool_stringify.h\
		recovertool_supported_type.h\
		recovertool_throw_exception.h
	g++ $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Dget_length_string=main -c recovertool_get_length_string.cpp -o $@

recovertool_component_error.o: recovertool_component_error.h\
		recovertool_component_error.cpp
	g++ -fPIC $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Dcommon=main -c recovertool_component_error.cpp -o $@

recovertool_from_hex.o: recovertool_from_hex.h\
		recovertool_from_hex.cpp\
		recovertool_component_error.h\
		recovertool_throw_exception.h
	g++ -fPIC $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Dcommon=main -c recovertool_from_hex.cpp -o $@

recovertool_malloc_c_allocator.o: recovertool_malloc_c_allocator.h\
		recovertool_malloc_c_allocator.cpp
	g++ -fPIC $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Dcommon=main -c recovertool_malloc_c_allocator.cpp -o $@

recovertool_posix_file.o: recovertool_posix_file.h\
		recovertool_posix_file.cpp\
		recovertool_component_error.h\
		recovertool_throw_exception.h
	g++ -fPIC $(CXXFLAGS) $(DEFS)\
		-DPACKAGE_DIR='"$(shell pwd)"'\
		-DPACKAGE_NAME='"recovertool"'\
		-Dcommon=main -c recovertool_posix_file.cpp -o $@

