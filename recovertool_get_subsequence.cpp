/*
 *
 * This file is part of recovertool -- A toolbox for recovering files
 *  from damaged file systems
 *
 * recovertool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3.0 of the
 * License, or (at your option) any later version.
 *
 * recovertool is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with recovertool.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2023 Isidor Zeuner <devel@quidecco.de>
 *
 */

#include <cstdio>
#include "recovertool_throw_exception.h"
#include "recovertool_component_error.h"
#include <cstdlib>
#include "recovertool_c_buffer.h"

int get_subsequence(
    int argc,
    char* argv[
    ]
) {
    size_t index = 0;
    int const full_record = atoi(
        argv[
            ++index
        ]
    );
    int const offset_start = atoi(
        argv[
            ++index
        ]
    );
    int const offset_end = atoi(
        argv[
            ++index
        ]
    );
    recovertool::c_buffer retrieved(
        full_record
    );
    while (
        true
    ) {
        size_t read_records = fread(
            retrieved.get_data(
            ),
            full_record,
            1,
            stdin
        );
        if (
            0 == read_records
        ) {
            if (
                feof(
                    stdin
                )
            ) {
                break;
            }
            recovertool::throw_exception(
                recovertool::component_error(
                    "get_subsequence"
                )
            );
        }
        if (
            1 != fwrite(
                retrieved.get_data(
                ) + offset_start,
                offset_end - offset_start,
                1,
                stdout
            )
        ) {
            recovertool::throw_exception(
                recovertool::component_error(
                    "get_subsequence"
                )
            );
        }
    };
    return 0;
}
