/*
 *
 * This file is part of recovertool -- A toolbox for recovering files
 *  from damaged file systems
 *
 * recovertool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3.0 of the
 * License, or (at your option) any later version.
 *
 * recovertool is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with recovertool.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2023 Isidor Zeuner <devel@quidecco.de>
 *
 */

#include <set>
#include "recovertool_from_hex.h"
#include <string>
#include "recovertool_posix_file.h"
#include "recovertool_c_buffer.h"
#include "recovertool_component_error.h"
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "recovertool_throw_exception.h"
#include <cstdio>

int surrounding_subscan(
    int argc,
    char* argv[
    ]
) {
    size_t index = 0;
    std::string const offset_filename = argv[
        ++index
    ];
    std::string const surrounding_filename = argv[
        ++index
    ];
    std::set<
        std::string
    > required;
    while (
        ++index < argc
    ) {
        required.insert(
            recovertool::from_hex(
                argv[
                    index
                ]
            )
        );
    }
    if (
        required.empty(
        )
    ) {
        recovertool::throw_exception(
            recovertool::component_error(
                "surrounding_subscan"
            )
        );
    }
    size_t const chunk_size = required.begin(
    )->size(
    );
    recovertool::posix_file offset_file(
        offset_filename,
        O_RDONLY
    );
    recovertool::posix_file surrounding_file(
        surrounding_filename,
        O_RDONLY
    );
    recovertool::c_buffer surrounding(
        chunk_size
    );
    while (
        true
    ) {
        unsigned long offset;
        ssize_t read_bytes = read(
            offset_file.get_data(
            ),
            &offset,
            sizeof(
                offset
            )
        );
        if (
            0 > read_bytes
        ) {
            recovertool::throw_exception(
                recovertool::component_error(
                    "surrounding_subscan"
                )
            );
        }
        if (
            0 == read_bytes
        ) {
            break;
        }
        if (
            sizeof(
                offset
            ) > read_bytes
        ) {
            recovertool::throw_exception(
                recovertool::component_error(
                    "surrounding_subscan"
                )
            );
        }
        read_bytes = read(
            surrounding_file.get_data(
            ),
            surrounding.get_data(
            ),
            chunk_size
        );
        if (
            chunk_size != read_bytes
        ) {
            recovertool::throw_exception(
                recovertool::component_error(
                    "surrounding_subscan"
                )
            );
        }
        if (
            required.end(
            ) != required.find(
                std::string(
                    surrounding.get_data(
                    ),
                    chunk_size
                )
            )
        ) {
            if (
                1 != fwrite(
                    &offset,
                    sizeof(
                        offset
                    ),
                    1,
                    stdout
                )
            ) {
                recovertool::throw_exception(
                    recovertool::component_error(
                        "surrounding_subscan"
                    )
                );
            }
        }
    };
    return 0;
}
