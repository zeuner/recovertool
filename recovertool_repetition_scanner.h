/*
 *
 * This file is part of recovertool -- A toolbox for recovering files
 *  from damaged file systems
 *
 * recovertool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3.0 of the
 * License, or (at your option) any later version.
 *
 * recovertool is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with recovertool.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2023 Isidor Zeuner <devel@quidecco.de>
 *
 */

#ifndef REPETITION_SCANNER_INCLUDED
#define REPETITION_SCANNER_INCLUDED

#include <string>
#include "recovertool_c_buffer.h"
namespace recovertool {
    
    class repetition_scanner {
    public:
        repetition_scanner(
            size_t buffer_size,
            size_t pattern_size,
            size_t pattern_distance
        );
        bool scan_data(
            char const*& from,
            char const* const& to,
            std::string& found
        );
    private:
        c_buffer data;
        size_t const pattern_size;
        size_t const pattern_distance;
        char const* const buffer_start;
        char const* const buffer_end;
        char* buffer_position;
        char const* repetition_position;
        bool filled;
        size_t filled_size;
        size_t matched;
    };
}

#endif
