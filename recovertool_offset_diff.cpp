/*
 *
 * This file is part of recovertool -- A toolbox for recovering files
 *  from damaged file systems
 *
 * recovertool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3.0 of the
 * License, or (at your option) any later version.
 *
 * recovertool is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with recovertool.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2023 Isidor Zeuner <devel@quidecco.de>
 *
 */

#include "recovertool_throw_exception.h"
#include <cstdio>
#include "recovertool_component_error.h"

int offset_diff(
) {
    unsigned long last = 0;
    while (
        true
    ) {
        unsigned long offset;
        size_t read_records = fread(
            &offset,
            sizeof(
                offset
	    ),
            1,
            stdin
        );
        if (
            1 != read_records
        ) {
            if (
                feof(
                    stdin
                )
            ) {
                break;
            }
            recovertool::throw_exception(
                recovertool::component_error(
                    "offset_diff"
                )
            );
        }
        unsigned long const diff = offset - last;
        if (
            1 != fwrite(
                &diff,
                sizeof(
                    diff
	        ),
                1,
                stdout
            )
        ) {
            recovertool::throw_exception(
                recovertool::component_error(
                    "offset_diff"
                )
            );
        }
        last = offset;
    };
    return 0;
}
