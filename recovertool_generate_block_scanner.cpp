/*
 *
 * This file is part of recovertool -- A toolbox for recovering files
 *  from damaged file systems
 *
 * recovertool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3.0 of the
 * License, or (at your option) any later version.
 *
 * recovertool is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with recovertool.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2023 Isidor Zeuner <devel@quidecco.de>
 *
 */

#include "recovertool_throw_exception.h"
#include <cstdlib>
#include <iostream>
#include "recovertool_hex_string.h"
#include "recovertool_c_buffer.h"
#include "recovertool_component_error.h"
#include <cstdio>
#include <set>

int generate_block_scanner(
    int argc,
    char* argv[
    ]
) {
    size_t index = 0;
    std::string::size_type const block_size = atoi(
        argv[
            ++index
        ]
    );
    std::string::size_type const signature_size = atoi(
        argv[
            ++index
        ]
    );
    recovertool::c_buffer retrieved(
        block_size
    );
    std::set<
        std::string
    > known;
    std::cout << "%option noyywrap\n"
    "	/*\n"
    "	 *\n"
    "	 * This file is generated, DO NOT EDIT!\n"
    "	 * See " << PACKAGE_NAME << "_generate_block_scanner.cpp instead.\n"
    "	 *\n"
    "	 */\n"
    "\n"
    "	#include <cstdio>\n"
    "	long num_chars = 0;\n"
    "%%\n";
    size_t block = 0;
    while (
        true
    ) {
        size_t const read_records = fread(
            retrieved.get_data(
            ),
            block_size,
            1,
            stdin
        );
        if (
            0 == read_records
        ) {
            if (
                feof(
                    stdin
                )
            ) {
                break;
            }
            recovertool::throw_exception(
                recovertool::component_error(
                    "generate_block_scanner"
                )
            );
        }
        std::string const signature(
            retrieved.get_data(
            ),
            signature_size
        );
        if (
            known.end(
            ) != known.find(
                signature
            )
        ) {
            block++;
            continue;
        }
        known.insert(
            signature
        );
        std::string const signature_hex = recovertool::hex_string(
            signature
        );
        std::cout << "\"";
        for (
            std::string::size_type byte = 0;
            signature_hex.size(
            ) > byte;
            byte += 2
        ) {
            std::cout << "\\x" << std::string(
                signature_hex.begin(
                ) + byte,
                signature_hex.begin(
                ) + byte + 2
            );
        }
        std::cout << "\"	{\n"
        "	    long const block = " << block << ";\n"
        "	    fwrite(\n"
        "	        &block,\n"
        "	        sizeof(\n"
        "	            block\n"
        "	        ),\n"
        "	        1,\n"
        "	        stdout\n"
        "	    );\n"
        "	    fwrite(\n"
        "	        &num_chars,\n"
        "	        sizeof(\n"
        "	            num_chars\n"
        "	        ),\n"
        "	        1,\n"
        "	        stdout\n"
        "	    );\n"
        "	    num_chars += " << signature_size << ";\n"
        "	}\n";
        block++;
    };
    std::cout << "\\n	++num_chars;\n"
    ".	++num_chars;\n"
    "%%\n"
    "int main(\n"
    ") {\n"
    "    yylex(\n"
    "    );\n"
    "    return 0;\n"
    "}\n";
    return 0;
}
