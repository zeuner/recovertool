/*
 *
 * This file is part of recovertool -- A toolbox for recovering files
 *  from damaged file systems
 *
 * recovertool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3.0 of the
 * License, or (at your option) any later version.
 *
 * recovertool is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with recovertool.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright 2023 Isidor Zeuner <devel@quidecco.de>
 *
 */

#include <iostream>
#include "recovertool_serialized.h"
#include "recovertool_posix_file.h"
#include "recovertool_throw_exception.h"
#include <string>
#include "recovertool_file_back_scanner.h"
#include <sys/stat.h>
#include <unistd.h>
#include "recovertool_component_error.h"
#include <sys/types.h>
#include <fcntl.h>

int get_pre_text(
    int argc,
    char* argv[
    ]
) {
    size_t index = 0;
    std::string const input_filename = argv[
        ++index
    ];
    recovertool::posix_file input_file(
        input_filename,
        O_RDONLY
    );
    while (
        true
    ) {
        unsigned long offset;
        ssize_t read_bytes = read(
            0,
            &offset,
            sizeof(
                offset
            )
        );
        if (
            0 > read_bytes
        ) {
            recovertool::throw_exception(
                recovertool::component_error(
                    "get_pre_text"
                )
            );
        }
        if (
            0 == read_bytes
        ) {
            break;
        }
        if (
            sizeof(
                offset
            ) > read_bytes
        ) {
            recovertool::throw_exception(
                recovertool::component_error(
                    "get_pre_text"
                )
            );
        }
        recovertool::file_back_scanner scanning(
            input_file,
            offset
        );
        std::string pre_text;
        char added;
        while (
            scanning.get_char(
                added
            )
        ) {
            if (
                '\x00' == added
            ) {
                break;
            }
            pre_text = added + pre_text;
        }
        std::cout << recovertool::serialized(
            pre_text
        );
    };
    return 0;
}
